<!DOCTYPE html>
<html lang="en">
<head>

<title>Sistem Informasi Pertanian</title>

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery-2.1.4.js"></script>
<script src="js/bootstrap.js"></script>

</head>
<body >

<div class="container-fluid" id="navbar-brand">
	 <ul class="nav navbar-nav navbar-left">
         <li>BRAND</li>
     </ul>
</div>
<!-- end navbar -->
<!-- sidebar -->
<nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.php">Beranda<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li>
				<li ><a href="about_us.php">Tentang Kami<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>
				<li ><a href="#">Kontak<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span></a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi Pertanian <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
					<ul class="dropdown-menu forAnimate" role="menu">
						<li><a href="#">Tanaman</a></li>
						<li class="divider"></li>
						<li><a href="#">Lahan</a></li>
						
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistem Pertanian <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
					<ul class="dropdown-menu forAnimate" role="menu">
						<li><a href="#">Sistem 1</a></li>
						<li class="divider"></li>
						<li><a href="#">Sistem 2</a></li>
						<li class="divider"></li>
						<li><a href="#">Sistem 3</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>
<div class="main">
<!-- Content Here -->

<!-- ini step untuk signup -->
	<div class="row">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified thumbnail">
                <li ><a href="#">
                    <h4 class="list-group-item-heading">Step 1</h4>
                    <p class="list-group-item-text">Daftar Sebagai</p>
                </a></li>
                <li ><a href="#">
                    <h4 class="list-group-item-heading">Step 2</h4>
                    <p class="list-group-item-text">Informasi Akun</p>
                </a></li>
                <li class="active"><a href="#">
                    <h4 class="list-group-item-heading">Step 3</h4>
                    <p class="list-group-item-text">Informasi Pribadi</p>
                </a></li>
            </ul>
        </div>
	</div>
	<!-- end step signup -->


	  <!--pertanyaan signup  -->
	  <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-title">
            <h4>Informasi Pribadi</h4>
          </div>
        </div>
        <div class="panel-body">
          
          <form class="form form-vertical">
            <div class="control-group">
              <label>Nama</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="nama" name="nama">
                
              </div>
            </div> 
            <!-- ddi database ditambahi foto -->
            <div class="control-group">
              <label>Foto</label>
              <div class="controls">
                <input type="file" class="form-control" placeholder="Foto" name="Foto">
                
              </div>
            </div> 
            
             <div class="control-group">
              <label>Email</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Email" name="Email">
                
              </div>
            </div> 

            <div class="control-group">
              <label>Alamat</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Alamat	" name="alamat">
                
              </div>
            </div> 
           
            <div class="control-group">
              <label>Desa Kelurahan</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Desa Kelurahan" name="DesaKelurahan">
                
              </div>
            </div> 
            <div class="control-group">
              <label>Kecamatan</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Kecamatan" name="Kecamatan">
                
              </div>
            </div>
            <div class="control-group">
              <label>Kabupaten</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Kabupaten" name="Kabupaten">
                
              </div>
            </div> 
             
            <div class="control-group">
              <label>Provinsi</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Provinsi" name="Provinsi">
                
              </div>
            </div> 
            
            
            <div class="control-group">
              <label>Nomor Telpon</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Nomor Telpon" name="NomorTelpon">
                
              </div>
            </div> 
             
              <div class="control-group">
              <label>Pendidikan Terakhir</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Pendidikan Terakhir" name="PendidikanTerakhir">
                
              </div>
            </div> 
             
              <div class="control-group">
              <label>Jurusan</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Jurusan" name="Jurusan">
                
              </div>
            </div> 
             
              <div class="control-group">
              <label>Kompetensi Keahlian</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Kompetensi Keahlian" name="KompetensiKeahlian">
                
              </div>
            </div> 
             
              <div class="control-group">
              <label>Riwayat Pendidikan</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Riwayat Pendidikan" name="RiwayatPendidikan">
                
              </div>
            </div> 
             
              <div class="control-group">
              <label>Pengalaman Kerja</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Pengalaman Kerja" name="PengalamanKerja">
                
              </div>
            </div> 
             


            <div class="control-group">
              <label></label>
              <div class="controls">
                <button type="submit" class="btn btn-primary">
                  Kirim
                </button>
              </div>
            </div>   
            
          </form>
          
          
        </div><!--/panel content-->
      </div><!--/panel-->
	    
	<!-- end content -->
	</div>
</div>
<!-- end sidebar -->
</body>
</html>            