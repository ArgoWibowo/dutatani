<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_berita extends CI_Controller {
	
    public function __construct(){
		parent::__construct();

		if (!$this->session->has_userdata('Org_Unit')){
			redirect();
		}
		$this->load->model('berita');
    }


	private function view($main){
		$css=$this->load->view('admin/css',array(),true);

		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("berita");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index($hal=1){
		$this->load->library('not_null');
		$halaman=$this->berita->lihatBerita("count(id) hal");
		$jumlah=$halaman->result_array()[0]['hal'];
		if ($jumlah!=0) {
			$link=base_url()."admin_berita/index";
			$banyak=5;
			$page= $this->not_null->halaman($jumlah,$banyak,$link,$hal);


			$load=$banyak*($hal-1);

			$data['data']=$this->berita->lihatBerita("*","1=1",$banyak,$load);
			$data['link']=base_url()."admin_berita/baca_berita";
			$data['cari']=base_url()."admin_berita/cari";
			$data['halaman']=$page;
			
			$main=$this->load->view('list_berita',$data,true);
		}else{
			$data['data']=array("error"=>"Tidak ada berita untuk ditampilkan");
			$data['link']="";
			$data['cari']=base_url()."admin_berita/cari";
			$data['halaman']="Tidak ada berita untuk ditampilkan";
			$main=$this->load->view('list_berita',$data,true);
		}
		$this->view($main);
	}

	public function baca_berita($id){
		$rand['main']=$this->berita->lihatBerita("*","1=1",5,0,"rand()");
		$rand['link']=base_url()."admin_berita/baca_berita";



		$where['md5(id)']=$id;
		$rand['data']=$this->berita->lihatBerita("*",$where);
		if ($rand['data']->num_rows()==1) {
			$main=$this->load->view("baca_berita",$rand,true);
			$this->view($main);
		}
	}

	public function tambah(){
		$data['data']="";
		$data['link']=base_url()."/admin_berita/insert";
		$data['isi']="";
		$data['judul']="";
		$main=$this->load->view('admin/tambah_berita',$data,true);
		$this->view($main);
	}

	public function insert(){
		if (isset($_POST['judul']) && !empty($_POST['judul'])) {
			$foto=$this->session->flashdata("img");
			if (is_null($foto)) {
				$foto="";
			}
			$post=array(
				"judul"		=> $_POST['judul'],
				"isi"		=> $_POST['isi'],
				"NIK"		=> $this->session->userdata('NIK'),
				"foto"		=>$foto
			);
			if($this->berita->bikinBerita($post)) {
				redirect('admin_berita');
			}
		}else{
			redirect('admin_berita');
		}

	}
	public function hapus($id){
		$where['md5(id)']=$id;
		if ($this->berita->hapusBerita($where)) {
			redirect('admin_berita');
		}
	}

	public function edit($id){
		$data['data']=$this->berita->lihatBerita("*","md5(id)='".$id."'");
		$data['link']=base_url().'admin_berita/ganti/'.$id;
		if ($data['data']->num_rows()==1) {
			foreach ($data['data']->result_array() as $key) {
                $data['isi']=$key['isi'];
                $data['judul']=$key['judul'];
            }

			$main=$this->load->view("admin/tambah_berita",$data,true);
			$this->view($main);
		}

	}
	public function ganti($id){
		$set['isi']=$_POST['isi'];
		$set['judul']=$_POST['judul'];
		$where="md5(id)='".$id."'";
		if ($this->berita->update($set,$where)) {
			redirect("admin_berita");
		}

	}

	public function cari(){
		if (isset($_POST['cari'])|| !empty($_POST['cari'])) {
			$cari=preg_replace('/\s\s+/', ' ', $_POST['cari']);
			redirect('admin_berita/search/'.$cari);
		}
	}

	public function search($cari="",$hal=1){
		$isi= substr($cari, 0,3);
		if ($isi=="%20") {
			$cari=substr($cari,3, strlen($cari));
		}

		$where=$this->where($cari,"judul");
		$where=$where." or ".$this->where($cari,"isi");


		$this->load->library('not_null');
		$halaman=$this->berita->lihatBerita("count(id) hal");
		$jumlah=$halaman->result_array()[0]['hal'];
		$link=base_url()."admin_berita/index";
		$banyak=5;
		$page= $this->not_null->halaman($jumlah,$banyak,$isi,$hal);


		$load=$banyak*($hal-1);

		$data['data']=$this->berita->lihatBerita("*",$where,$banyak,$load);
		$data['link']=base_url()."admin_berita/baca_berita";
		$data['cari']=base_url()."admin_berita/cari";
		$data['halaman']=$page;
		$main=$this->load->view('list_berita',$data,true);
		$this->view($main);
		
	}

	private function where($string,$kolom){
		$string = str_replace("%20", "%' or ".$kolom." LIKE '%", $string);
		$string= $kolom." LIKE '%".$string."%'";
		return $string;
	}
}
?>

