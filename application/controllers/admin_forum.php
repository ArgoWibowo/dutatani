<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_forum extends CI_Controller {
	private function view($main){
		$css=$this->load->view('admin/css',array(),true);

		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("forum");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$this->db->select('Year(trans_topik_diskusi.tanggal) as tahun, Month(trans_topik_diskusi.tanggal) as bulan,count(ID_topik) as id');
		$this->db->group_by(array("tahun", "bulan"));
		$diskusi=$this->db->get('trans_topik_diskusi');


		$this->db->select('Year(trans_komentar_diskusi.tanggal) as tahun, Month(trans_komentar_diskusi.tanggal) as bulan,count(nomor_komentar) as id');
		$this->db->group_by(array("tahun", "bulan"));
		$komen=$this->db->get('trans_komentar_diskusi');




		$main=$this->load->view('admin/forum',array('diskusi'=>$diskusi,'komen'=>$komen),true);
		$this->view($main);

	}
}

?>

