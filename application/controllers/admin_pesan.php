<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_pesan extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Tahu');
        $this->Tahu->navigasi();
    }


	private function view($main){
		$css=$this->load->view('admin/css',array(),true);

		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("pesan");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$this->load->model('pesan');
		$data['pesan']=$this->pesan->lihat();



		$main=$this->load->view('admin/pesan',$data,true);
		$this->view($main);

	}

	public function lihat($id){
		$this->db->where("md5(id_pesan)",$id);
		$this->db->set('status',0);
		if ($this->db->update('pesan')) {
			$this->db->where("md5(id_pesan)",$id);
			$data=$this->db->get('pesan');
			$isi=$this->load->view('admin/isi_pesan',array("data"=>$data),true);
			$this->view($isi);	
		}


		

	}

	public function hapus_pesan($id=""){

		if ($id!="") {
			$this->load->model("pesan");
			$this->pesan->hapus($id);
		}
		redirect('admin_pesan');
	}
}
?>

