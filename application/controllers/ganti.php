<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ganti extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
    	if (!$this->session->userdata('ID_User')) {
    		redirect('');
        	$this->load->helper(array('form', 'url'));
    	}
    	$this->load->model('Tahu');
        $this->Tahu->navigasi();

    }
    private function getWilayah(){
		$data=array(
			"prov"=>$this->db->get('provinsi')
		);
		return $this->load->view('user/lokasi',$data,true);
		
	}


	private function view($main){
		$css=$this->load->view('user/css',array(),true);

		$this->load->model('navigasi');
		$nav=$this->navigasi->index("");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$provinsi=$this->getWilayah();

		$kat="";

		$id=$this->session->userdata('ID_User');


		$this->db->select('ID_User, master_kategori.ID_Kategori, Nama_Kategori , count(ID_User) id');
		$this->db->join('master_user_kat','(master_kategori.ID_Kategori=master_user_kat.ID_Kategori) and (ID_User="'.$id.'" or ID_User is null)','left');
		$this->db->group_by('master_kategori.ID_Kategori, Nama_Kategori,ID_User');
		$this->db->order_by('id','DESC');
		$temp['db']=$this->db->get('master_kategori');



		$this->db->select('master_jenis_sup.ID_Jenis_Sup,Nama_Jenis_Sup,ID_User');
		$this->db->join('master_supplier','master_supplier.ID_Jenis_Sup=master_jenis_sup.ID_Jenis_Sup and ID_User="'.$this->session->userdata('ID_User').'"','left');
		
      	$data=$this->db->get('master_jenis_sup');
      	$temp['jenis']="";
      	foreach ($data->result_array() as $key) {
        	$temp['jenis']=$temp['jenis'] . "<option value='".$key['ID_Jenis_Sup']."'";
        	if (!empty($key['ID_User'])) {
        		$temp['jenis']=$temp['jenis'] . 'selected=""';
        	}
          	$temp['jenis']=$temp['jenis'] . ">" . $key['Nama_Jenis_Sup'];
          	$temp['jenis']=$temp['jenis'] . "</option>";
      	}


      	$this->db->where("ID_User",$this->session->userdata('ID_User'));
      	$temp['petani']=$this->db->get('master_petani');


      	$this->db->where("ID_User",$this->session->userdata('ID_User'));
      	$temp['fas']=$this->db->get('master_fasilitator');




		$kat=$this->load->view('user/tambah_kategori',$temp,true);



		$main=$this->load->view("user/ganti_profile",array(
			"kategori"=>$kat,
			"provinsi"=>$provinsi
		),true);
		$this->view($main);
	}
	public function setprofile()
	{
		$set=array(
			"nama"=>$_POST['nama'],
			"tanggal_lahir"=>$_POST['tgl'],
			"alamat"=>$_POST['alamat'],
			"jenis_kelamin"=>$_POST['jekel'],
			"nomor_telpon"=>$_POST['NomorTelpon'],
			"Email"=>$_POST['Email'],
			"provinsi"=>$_POST['Provinsi'],
			"kabupaten"=>$_POST['Kabupaten'],
			"kecamatan"=>$_POST['Kecamatan'],
			"keluran_desa	"=>$_POST['DesaKelurahan'],
		);
		if (isset($_POST['password']) && !empty($_POST['password'])) {
			$serarr['Password']=sha1($_POST['password']);
			if ($this->update("master_user",$serarr,$where)) {
				redirect($this->session->userdata('url'));
			}
		}
		$where="ID_User='".$this->session->userdata('ID_User')."'";
		if ($this->update("master_detail_user",$set,$where)) {
			$this->session->set_userdata($set);
		}

		$this->tambahKategori($_POST);

		redirect($this->session->userdata('url'));

	}

	private function tambahKategori($post){
		$kat=$this->db->get('master_kategori');
		
		foreach ($kat->result_array() as $key) {
			$kunci=$key['ID_Kategori'];
			if (isset($post[$kunci]) && !empty($post[$kunci])) {
				if ($this->addkat($post[$kunci])) {
					echo "string";
				}
			}else{
				if ($this->delkat($kunci)) {
					echo "string";
				}
			}
		}


		if (isset($post['PET']) && !empty($post["PET"])) {
			if ($this->addpetani($post)) {
				echo "berhasil";
			}
		}
		if (isset($post['SUP']) && !empty($post["SUP"])) {
			if ($this->addsup($post)) {
				echo "berhasil";
			}
		}
		if (isset($post['FAS']) && !empty($post["FAS"])) {
			if ($this->addfas($post)) {
				echo "berhasil";
			}
		}

	}

	private function addfas($post){
		$where['ID_User']=$this->session->userdata('ID_User');
		$this->db->where($where);
		$user=$this->db->get('master_fasilitator');
		if ($user->num_rows()==0) {
			$fas=array(
				"ID_User"			=> 	$this->session->userdata('ID_User'),
				"Nama_Fasilitator"	=>	"",
				"Alamat_Fasilitator"=>	"",
				"Kabupaten"			=>	"",
				"Kecamatan"			=>	"",
				"Provinsi"			=>	"",
				"tanggal_lahir"		=>	"",
				"Desa"				=>	"",
				"Email"				=>	"",
				"Foto"				=>	"",
				"Telpon"			=>	"",
				"Pendidikan_Terakhir"	=>	"",
				"Jurusan"			=>	"",
				"Kompetensi_Keahlian"	=>	"",
				"Riwayat_Pendidikan"=>	$post['Terakhir'],
				"Pengalaman_Kerja"=>	$post['Pengalaman'],
				"jns_kelamin"		=> "",

			);
				$this->db->set($fas);
				if ($this->db->insert('master_fasilitator')) {
					echo "tambah master_fasilitator";
				}
		}else{

			$fas=array(
				"Riwayat_Pendidikan"	=>	$post['Terakhir'],
				"Pengalaman_Kerja"=>	$post['Pengalaman']
			);
			$this->db->where("ID_User",$this->session->userdata('ID_User'));
			if ($this->db->update('master_fasilitator',$fas)) {
				echo "ganti master_fasilitator";
			}

		}
	}
	
	private function addsup($post){
		$where['ID_User']=$this->session->userdata('ID_User');
		$this->db->where($where);
		$user=$this->db->get('master_supplier');
		if ($user->num_rows()==0) {
			$suplayer=array(
				"ID_User"			=>$this->session->userdata('ID_User'),
				"Nama_Supplier"		=>'',
				"Alamat_Supplier"	=>'',
				"Kabupaten"			=>'',
				"Kecamatan"			=>'',
				"Provinsi"			=>'',
				"Desa"				=>'',
				"Email"				=>'',
				"tanggal_lahir"		=>'',
				"jns_kelamin"		=>'',
				"Foto"				=>'',
				"No_Handphone"		=>'',
				"Kontak_Person"		=>'',
				"ID_Jenis_Sup"		=>$post['Suplayer'],
				"No_Telpon"			=>'',
			);

			$this->db->set($suplayer);
			if ($this->db->insert('master_supplier')) {
				echo "tambah master_supplier";
			}
		}else{


			$suplayer=array(
				"ID_Jenis_Sup"		=>$post['Suplayer']
			);
			$this->db->where("ID_User",$this->session->userdata('ID_User'));
			if ($this->db->update('master_supplier',$suplayer)) {
				echo "ganti master_supplier";
			}

		}
	}

	private function addpetani($post){
		$where['ID_User']=$this->session->userdata('ID_User');
		$this->db->where($where);
		$user=$this->db->get('master_petani');
		if ($user->num_rows()==0) {
			$master_petani=array(
				"ID_User"			=> 	$where['ID_User'],
				"Nama_Petani"		=>	'',
				"Alamat_Petani"		=>	'',
				"Kabupaten"			=>	'',
				"Kecamatan"			=>	'',
				"Provinsi"			=>	'',
				"Desa_Kelurahan"	=>	'',
				"Foto"				=>	'',
				"Nomor_Telpon"		=>	'',
				"Jumlah_Tanggungan"	=>	$post['tanggungan'],
				"Email"				=>	'',
				"Agama"				=>	'',
				"Tanggal_Lahir"		=>	'',
				"Deskripsi_Keahlian"=>	'',
				"Status"			=>  1,
				"jns_kelamin"		=>  '',
			);
			$this->db->set($master_petani);
			if ($this->db->insert('master_petani')) {
				echo "tambah petani";
			}
		}else{
			$master_petani=array(
				"Jumlah_Tanggungan"	=>	$post['tanggungan'],
			);
			$this->db->where("ID_User",$this->session->userdata('ID_User'));
			if ($this->db->update('master_petani',$master_petani)) {
				echo "ganti petani";
			}
		}
	}



	private function addkat($kat){
		$where['ID_User']=$this->session->userdata('ID_User');
		$where['ID_Kategori']=$kat;
		$this->db->where($where);
		$get=$this->db->get('master_user_kat');
		if ($get->num_rows()==0) {
			$this->db->set($where);
			return $this->db->insert('master_user_kat');
		}
	}

	private function delkat($kat){
		$where['ID_User']=$this->session->userdata('ID_User');
		$where['ID_Kategori']=$kat;
		if ($kat=="PET") {
			$this->db->where('ID_User',$this->session->userdata('ID_User'));
			if ($this->db->delete('master_petani')) {
				echo "hapus petani";
			}
		}elseif ($kat=="SUP") {
			$this->db->where('ID_User',$this->session->userdata('ID_User'));
			if ($this->db->delete('master_supplier')) {
				echo "hapus sup";
			}
		}elseif ($kat=="FAS") {
			$this->db->where('ID_User',$this->session->userdata('ID_User'));
			if ($this->db->delete('master_fasilitator')) {
				echo "hapus fas";
			}
		}
		$this->db->where($where);
		return $this->db->delete('master_user_kat');
	}
	private function update($table,$set,$where){
		$this->db->where($where);
		return $this->db->update($table, $set);
	}

	public function upload(){
		if($_FILES['imageUpload']['type']=="image/png")
		{
			$tipe=".PNG";
		}else{
			$tipe=".jpg";
		}
		$name=md5( date("Y/M/d-D h:i:s"));
		$config['file_name']     		= $name.$tipe;
		$config['upload_path']          = 'Foto/';
        $config['allowed_types']        = 'jpg|png';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('imageUpload'))
        {	
        		
        }
        else
        {
           $foto['Foto']=$config['file_name'];
           $this->session->set_userdata($foto);
           $where['ID_User']=$this->session->userdata('ID_User');
           if ($this->update("master_detail_user",$foto,$where)) {
           		redirect($this->session->userdata('url'));
           }
        }
	}
}
?>

