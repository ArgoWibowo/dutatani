<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class about_admin extends CI_Controller {
	private function view($main){
		$css=$this->load->view('admin/css',array(),true);

		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("tentang");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$data=$this->db->get('about');
		$main=$this->load->view('tentang_kami',array('data' =>$data),true);
		$this->view($main);
	}

	public function __construct()
        {
            parent::__construct();
            $this->load->helper(array('form', 'url'));
            if (!$this->session->has_userdata('Org_Unit')){
				redirect();
            }
		}

    public function do_upload()
    {
		if($_FILES['image']['type']=="image/png")
		{
			$tipe=".PNG";
		}else{
			$tipe=".jpg";
		}
		$name=md5( date("Y/M/d-D h:i:s"));
		$config['file_name']     		= $name.$tipe;
		$config['upload_path']          = 'tentang/';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 100000;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('image'))
        {
                echo  $this->upload->display_errors();
        }
        else{
            $this->session->set_flashdata("img","tentang/".$config['file_name']);
            echo base_url()."tentang/".$config['file_name'];
        }
    }

    public function update(){
    	$post=array(
    		'tentang'=>$_POST['about']
    	);
    	$this->db->set($post);
    	if ($this->db->update('about')) {
    		redirect('about_admin');
    	}

    }
}

?>

