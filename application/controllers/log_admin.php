<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class log_admin extends CI_Controller {
	public function __construct()
    {
        parent::__construct();

    }
	public function index(){
        if ($this->session->has_userdata('NIK') || $this->session->has_userdata('Password')) {
        	redirect('admin');
        }
		$this->load->view('admin/login');
	}

	public function masuk(){
		if ($this->session->has_userdata('NIK') || $this->session->has_userdata('Password')) {
        	redirect('adm');
        }
        

		$this->load->view('admin/login');
		$this->load->library('not_null');
		if ($this->not_null->POST($_POST)) {
			$this->load->model('admin_login');
			$data=$this->admin_login->login($_POST);
			if ($data->num_rows()==1) {
				foreach ($data->result_array() as $key => $value) {
					foreach ($value as $ky => $val) {
						$this->session->set_userdata(array(
								$ky => $val
						));
					}
				}

				redirect('admin');
			}else{
				$this->load->library('session');
				$this->session->set_flashdata("error",$this->load->view("admin/error",array(
					"error"	=>	'Username dan Password tidak Di temukan'
				),true));
			redirect('log_admin');
			}
		}else{
			$this->load->library('session');
			$this->session->set_flashdata("error",$this->load->view("admin/error",array(
							"error"	=>	'Semua field harus diisi!!!'
						),true));
			redirect('log_admin');
		}

	}

	public function keluar(){
		$this->load->library('session');
		$user=$this->session->userdata();
		foreach ($user as $key => $value) {
			$this->session->unset_userdata($key);
		}
		$user=$this->session->userdata();
		$this->session->sess_destroy();
		redirect();
	}
	}
?>
