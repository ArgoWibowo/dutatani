<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class list_admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->has_userdata('Org_Unit')){
			redirect();
		}
		if (!$this->session->has_userdata('Pendaftaran_Anggota')||!$this->session->userdata('Pendaftaran_Anggota')){
			redirect('admin');
		}
		$this->load->model('admin_acc');
    }

	private function view($main){
		$css=$this->load->view('admin/css',array(),true);

		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("list");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$result=$this->admin_acc->getAdmin();
		$org=$this->admin_acc->getOrg();
		
		$arr = array(
			"admin"	=> $result,
			"org"	=>$org
		);


		$main=$this->load->view('admin/list_admin',$arr,true);
		$this->view($main);
	}

	public function tambah_admin(){
		$where['Org_Unit !=']=1;
		$this->db->where($where);
		$unit=$this->db->get('master_org_unit');
		$arr=array(
			'NIK'=>'',
			'Nama_Karyawan'=>'',
			'Tanggal_Lahir'=>'',
			'Jenis_Kelamin'=>'L',
			'No_Telpon'=>'',
			'Alamat_Rumah'=>'',
			'Org_Unit'=>'',
			'kelola_user'=>0,
			'Kelola_forum'=>0,
			'kelola_website'=>0,
			'Email'=>'',
			'url'=> base_url().'/list_admin/post_admin',
			'unit'=>$unit,
			'disable'=>false,
			'pass'=>false

		);
		$main=$this->load->view('admin/tambah_admin',$arr,true);
		$this->view($main);
	}

	public function tambah_unit(){
		$this->db->select('*');
		$this->db->from('master_org_unit');
		$unit=$this->db->get();
		$array=array(
					"Org_Unit"			=> "",
					"Nama_Organisasi"	=> "",
					"Tingkatan"			=> "",
					"Org_Unit_Atasan"	=> "",
					"Pelayanan"			=> 0,
					"Pelatihan"			=> 0,
					"Pendaftaran_Anggota"=> 0,
					"Konsultasi"		=> 0,
					"Penawaran"			=> 0,
					"Permintaan"		=> 0,
					"Memberi_Informasi"	=> 0,
					"Meminta_Informasi"	=> 0,
					"Berbagi_Informasi"	=> 0,
					"Data_Spesifik_Anggota"=> 0,
					"unit"=>$unit,
					"url"=> base_url().'/list_admin/post_unit'
				);
		$main=$this->load->view('admin/tambah_unit',$array,true);
		$this->view($main);
	}
	public function post_admin(){
		$this->load->library('not_null');
		$pst['NIK']=$_POST['NIK'];
		$this->db->where('NIK',"711301505");
		$jum=$this->db->get("master_user_org");
		
		if ($this->not_null->POST($_POST) && $jum->num_rows()==0) {
			$config['NIK']=$_POST['NIK'];
			$config["Password"]=sha1($_POST['NIK']);
			$config['Nama_Karyawan']=$_POST['nama'];
			$config['Tanggal_Lahir']=$_POST['tgl'];
			$config['Alamat_Rumah']=$_POST['alamat'];
			$config['No_Telpon']=$_POST['NomorTelpon'];
			$config['Org_Unit']=$_POST['atasan'];
			$config['Jenis_Kelamin']=$_POST['jekel'];
			$config['Email']=$_POST['email'];
			if (isset($_POST['1'])) {
				$config['kelola_website']=1;
			}else{
				$config['kelola_website']=0;
			}

			if (isset($_POST['2'])) {
				$config['kelola_user']=1;
			}else{
				$config['kelola_user']=0;
			}

			if (isset($_POST['3'])) {
				$config['Kelola_forum']=1;
			}else{
				$config['Kelola_forum']=0;
			}

			$this->db->set($config);

			if ($this->db->insert('master_user_org')) {
				$error=$this->load->view('sukses',array("error"=>"Data berhasil disimpan"),true);
				$this->session->set_flashdata('error1',$error);
				redirect('list_admin');
			}
		}elseif ($jum->num_rows()!=0) {
			$error=$this->load->view('error',array("error"=>"NIK Sudah ada di database"),true);
			$this->session->set_flashdata("error",$error);
			redirect('list_admin/tambah_admin');
		}else{
			$error=$this->load->view('error',array("error"=>"Semua form harus diisi"),true);
			$this->session->set_flashdata("error",$error);
			redirect('list_admin/tambah_admin');
		}
	}

	public function post_unit(){
		$this->load->library('not_null');
		$post = array(
			'nama' 		=>	$_POST['nama'],
			'tingkatan'	=>	$_POST['tingkatan']
		);
		if ($this->not_null->POST($post)) {

			$org=sha1($_POST['nama']);
			$config['Nama_Organisasi']=$_POST['nama'];
			$config['Tingkatan']=$_POST['tingkatan'];
			$config['Org_Unit_Atasan']=$_POST['atasan'];

			for($i=1;$i<=10;$i++){
				if (isset($_POST[$i])) {
					$config[$i]=1;
				}else{
					$config[$i]=0;
				}
			}
			$unit=$this->transUnit($config);
			$this->db->set($unit);
			if ($this->db->insert('master_org_unit')) {
				$error=$this->load->view('sukses',array("error"=>"Data berhasil disimpan"),true);

				$this->session->set_flashdata("error",$error);
				redirect('list_admin');
			}else{
				echo "string";
			}
			
		}else{
			$error=$this->load->view('error',array("error"=>"Nama Unit, Tingkatan, dan Unit Atasan harus diisi"),true);

			$this->session->set_flashdata("error",$error);
			redirect('list_admin/tambah_unit');
		}
	}

	private function transUnit($unit){
		$array=array(
			"Nama_Organisasi"	=> $unit['Nama_Organisasi'],
			"Tingkatan"			=> $unit['Tingkatan'],
			"Org_Unit_Atasan"	=> $unit['Org_Unit_Atasan'],
			"Pelayanan"			=> $unit['1'],
			"Pelatihan"			=> $unit['2'],
			"Pendaftaran_Anggota"=> $unit['3'],
			"Konsultasi"		=> $unit['4'],
			"Penawaran"			=> $unit['5'],
			"Permintaan"		=> $unit['6'],
			"Memberi_Informasi"	=> $unit['7'],
			"Meminta_Informasi"	=> $unit['8'],
			"Berbagi_Informasi"	=> $unit['9'],
			"Data_Spesifik_Anggota"=> $unit['10']
		);
		if (!isset($unit['Org_Unit'])) {
			$array['Org_Unit']=sha1($unit['Nama_Organisasi']);
		}
		return $array;

	}
	public function hapusAdmin($nik){
		
		$where=array(
			"NIK"		=>$nik,
			"NIK !="	=> $this->session->userdata('NIK')
		);

		$this->db->where($where);
		if ($this->db->delete('master_user_org')) {
			$error=$this->load->view('sukses',array("error"=>"Data berhasil di hapus"),true);
			$this->session->set_flashdata('error1',$error);
			redirect('list_admin');
		}else{
			$error=$this->load->view('error',array("error"=>"Data gagal di hapus"),true);
			$this->session->set_flashdata('error1',$error);
			redirect('list_admin');
		}
	}

	public function hapusUnit($org){
		$this->admin_acc->hpsOrg($org);
	}

	public function editAdmin($nik){
		$where=array(
			"NIK"=>$nik
		);
		$adm=$this->admin_acc->getAdmin($where);
		if ($adm->num_rows()==1) {
			foreach ($adm->result_array() as $key) {
				$arr=$key;
			}
			$unit=$this->db->get('master_org_unit');
			$arr['unit']=$unit;
			$arr['url']= base_url().'/list_admin/edit_admin/'.$nik;
			$arr['disable']=true;
			$arr['pass']=false;
			$main=$this->load->view('admin/tambah_admin',$arr,true);
			$this->view($main);
		}else{
			redirect('list_admin');
		}

	}
	public function edit_admin($nik){
		$where=array(
			"NIK"=>$nik
		);
		$adm=$this->admin_acc->getAdmin($where);
		if ($adm->num_rows()==1) {
			$this->load->library('not_null');
			if ($this->not_null->POST($_POST)) {
				$config['NIK']=$_POST['NIK'];
				$config['Nama_Karyawan']=$_POST['nama'];
				$config['Tanggal_Lahir']=$_POST['tgl'];
				$config['Alamat_Rumah']=$_POST['alamat'];
				$config['No_Telpon']=$_POST['NomorTelpon'];
				$config['Org_Unit']=$_POST['atasan'];
				$config['Jenis_Kelamin']=$_POST['jekel'];
				$this->db->where($where);
				if ($this->db->update('master_user_org', $config)) {
					$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di ganti"),true);
					$this->session->set_flashdata('error1',$error);
					redirect('list_admin');
				}else{
					$error=$this->load->view('error',array("error"=>"Data gagal di ganti"),true);
					$this->session->set_flashdata('error1',$error);
					redirect('list_admin');
				}
			}
			$error=$this->load->view('error',array("error"=>"Semua form harus diisi"),true);
			$this->session->set_flashdata('error1',$error);
			redirect('list_admin/editAdmin/'.$nik);
		}
		redirect('list_admin');
	}

	public function editUnit($org){
		$this->db->select('*');
		$where['Org_Unit']=$org;
		$this->db->where($where);
		$this->db->from('master_org_unit');
		$unit=$this->db->get();
		if ($unit->num_rows()==1) {
			foreach ($unit->result_array() as $key) {
				$arr=$key;
			}
			$this->db->select('*');
			$this->db->from('master_org_unit');
			$unit=$this->db->get();
			$arr['unit']=$unit;
			$arr['url']=base_url().'list_admin/edit_Unit/'.$org;
			$main=$this->load->view('admin/tambah_unit',$arr,true);
			$this->view($main);
		}else{
			redirect('list_admin');
		}
	}
	public function edit_Unit($org){
		$this->db->select('*');
		$where['Org_Unit']=$org;
		$this->db->where($where);
		$this->db->from('master_org_unit');
		$unit=$this->db->get();
		if ($unit->num_rows()==1) {
			$this->load->library('not_null');
			if ($this->not_null->POST($_POST)) {
				$config['Org_Unit']=$org;
				$config['Nama_Organisasi']=$_POST['nama'];
				$config['Tingkatan']=$_POST['tingkatan'];
				$config['Org_Unit_Atasan']=$_POST['atasan'];
				for($i=1;$i<=10;$i++){
					if (isset($_POST[$i])) {
						$config[$i]=1;
					}else{
						$config[$i]=0;
					}
				}
				$data=$this->transUnit($config);
				$where['Org_Unit']=$org;
				$this->db->where($where);
				if ($this->db->update('master_org_unit',$data)) {
					$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di ganti"),true);
					$this->session->set_flashdata('error',$error);
					redirect('list_admin');
				}else{
					$error=$this->load->view('error',array("error"=>"Data gagal di ganti"),true);
					$this->session->set_flashdata('error',$error);
					redirect('list_admin');
				}
			}else{
				$error=$this->load->view('error',array("error"=>"Semua form harus diisi"),true);
				$this->session->set_flashdata('error',$error);
				redirect('list_admin/editAdmin/'.$nik);
			}

		}
	}
}

?>
