<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {
	public function __construct()
    {
            parent::__construct();
            $this->load->library('session');
            $this->session->set_userdata(array('url'=>uri_string()));
            $this->load->model('Tahu');
            $this->Tahu->navigasi();
    }

	public function index($offset=1)
	{
		$this->load->model('diskusi');
		$jmlh=$this->diskusi->getcount();
		$query=$this->diskusi->getperpage($offset,'forum/index',$jmlh->num_rows());
		$this->load->library('pagination');
		$data=array(
				"css"		=>	$this->load->view('user/css','',true),
				"db"		=>	$query,
				"nav"		=>  $this->Navigasi->index('forum'),
				"tambah"	=>	$this->diskusi->tambah(),
				"halaman"	=>	$this->pagination->create_links(),
				"offset"	=>	$offset,
				"url"		=> 	"all"
			);
		
		$this->load->view('user/discustion',$data);
	}

	public function diskusiku($offset=1)
	{
		if (!$this->session->has_userdata('ID_User')) {
			redirect('forum');
		}
		$this->load->model('diskusi');
		$on="trans_topik_diskusi.ID_user='" . $this->session->userdata('ID_User')."'";
		$jmlh=$this->diskusi->getcount($on);
		$query=$this->diskusi->getperpage($offset,'forum/diskusiku/',$jmlh->num_rows(),$on);
		$this->load->library('pagination');
		$data=array(
				"css"		=>	$this->load->view('user/css','',true),
				"db"		=>	$query,
				"nav"		=>  $this->Navigasi->index('forum'),
				"tambah"	=>	$this->diskusi->tambah(),
				"halaman"	=>	$this->pagination->create_links(),
				"offset"	=>	$offset,
				"url"		=> 	"my"
			);
		
		$this->load->view('user/discustion',$data);
	}

	public function cari(){
		if (isset($_POST['cari'])|| !empty($_POST['cari'])) {
			$cari=preg_replace('/\s\s+/', ' ', $_POST['cari']);
			redirect('forum/search/'.$cari);
		}
	}

	public function search($cari="",$hal=1){
		$isi= substr($cari, 0,3);
		if ($isi=="%20") {
			$cari=substr($cari,3, strlen($cari));
		}
		

		$where=$this->where($cari,"trans_topik_diskusi.Judul_Topik");
		$this->load->model('diskusi');



		$this->load->library('not_null');
		$halaman=$this->diskusi->lihatDiskusi("count(master_kategori_topik.ID_kategori_topik) hal",$where);
		$temp=$halaman->result_array();


		$link=base_url()."forum/search/".$cari;
		$banyak=5;
		$page= $this->not_null->halaman($temp[0]['hal'],$banyak,$link,$hal);




		$this->load->model('diskusi');
		$on="trans_topik_diskusi.ID_user=" . $this->session->userdata('ID_User');
		$query=$this->diskusi->lihatDiskusi("*",$where,$banyak,$where);
		$this->load->library('pagination');
		$data=array(
				"css"		=>	$this->load->view('user/css','',true),
				"db"		=>	$query,
				"nav"		=>  $this->Navigasi->index('forum'),
				"tambah"	=>	$this->diskusi->tambah(),
				"halaman"	=>	$page,
				"url"		=> 	"my"
			);
		
		$this->load->view('user/discustion',$data);
		
	}

	
	private function where($string,$kolom){
		$string = str_replace("%20", "%' or ".$kolom." LIKE '%", $string);
		$string= $kolom." LIKE '%".$string."%'";
		return $string;
	}


	public function hapusDiskusi($id){ //Fungsi Hapus Diskusi
		$this->load->model('diskusi');
		$where['ID_topik']=$id;
		if (!$this->session->has_userdata('Kelola_forum') || $this->session->userdata('Kelola_forum')==0) {
			$where['ID_user']=$this->session->userdata('ID_User');
		}
		$data=$this->diskusi->cekDiskusi($where);
		if ($data->num_rows()==1) {
			$data=$this->diskusi->hapus_komentar(array('ID_topik'=>$id));
			$data=$this->diskusi->hapus_diskusi($where);
			if ($data==1) {
				$error=$this->load->view('sukses',array("error"=>"Data berhasil dihapus"),true);
				$this->session->set_flashdata("error",$error);
				redirect('forum/diskusiku');
			}
		}else{
			$error=$this->load->view('sukses',array("error"=>"Data berhasil gagal dihapus"),true);
			$this->session->set_flashdata("error",$error);
			redirect('forum/diskusiku');
		}


		// $where=array();
		// $this->diskusi->hapus_komentar($where)
	}

	public function editDiskusi(){ //Fungsi Edit Diskusi
		if (!isset($_POST['judul']) || empty($_POST['judul']) || !isset($_POST['deskripsi']) || empty($_POST['deskripsi']) || !isset($_POST['id_kategori_topik']) || empty($_POST['id_kategori_topik']) || !isset($_POST['id_topik']) || empty($_POST['id_topik'])) {
			//print_r($_POST);
			redirect('forum?error=1');
		}else{
			$this->load->model('user');
			$this->db->set(array(
				'Judul_kategori_topik'=>$_POST['judul']
			));
			$this->db->where( 'ID_kategori_topik', $_POST['id_kategori_topik'] );
			$this->db->update('master_kategori_topik');

			$this->db->set(array(
    			'ID_user'=>$this->session->userdata('ID_User'),
    			'Judul_Topik'=>$_POST['judul'],
    			'isi_diskusi'=>$_POST['deskripsi']
    		));
			$this->db->set(array('tanggal' => 'now()'), '', FALSE);
			$this->db->set(array('waktu' => 'now()'), '', FALSE);
			$this->db->where( 'ID_topik', $_POST['id_topik']);
			$this->db->update('trans_topik_diskusi');

			redirect('forum');
		}
	}

	public function postDiskusi(){
		if (!isset($_POST['judul']) || empty($_POST['judul']) || !isset($_POST['deskripsi']) || empty($_POST['deskripsi'])) {
			redirect('forum?error=1');
		}else{
			$this->load->model('user');
			$kategori=md5($_POST['judul'].$_POST['deskripsi']);
			$kategori=substr($kategori, 0,10);
			$kategori=$this->user->sama('master_kategori_topik',array('ID_kategori_topik'=>$kategori),'ID_kategori_topik',10);
			
			$this->db->set(array(
				'ID_kategori_topik'=>$kategori,
				'Judul_kategori_topik'=>$_POST['judul']

			));
			$this->db->insert('master_kategori_topik');

			$this->load->library('session');

			$kategori1=md5($this->session->userdata('ID_User').$kategori);
			$kategori1=substr($kategori1, 0,10);
			$kategori1=$this->user->sama('trans_topik_diskusi',array('ID_kategori_topik'=>$kategori),'ID_kategori_topik',10);
			$this->db->set(array(
    			'ID_topik' => $kategori1,
    			'ID_kategori_topik' => $kategori,
    			'ID_user'=>$this->session->userdata('ID_User'),
    			'Judul_Topik'=>$_POST['judul'],
    			'isi_diskusi'=>$_POST['deskripsi']

    		));
			$this->db->set(array('tanggal' => 'now()'), '', FALSE);
			$this->db->set(array('waktu' => 'now()'), '', FALSE);
			$this->db->insert('trans_topik_diskusi');

			redirect('forum');
		}
	}

	public function komentar($id="")
	{
		
		$this->load->model('diskusi');
		
		if ($id=="" || empty($id)) {
			redirect('forum');
		}else{
			$topik=$this->diskusi->getTopik($id);
		}
	}

	public function postKomen($id){
		if (isset($_POST['komen'])) {
			// $this->db->where(array('ID_topik'=>$id));
			$this->load->model('diskusi');
			if($this->diskusi->setKomen($id,$_POST['komen'])){
				redirect('forum/komentar/'.$id);
			}
			
		}
	}

	public function hapusKomentar($komentar,$diskusi){
		if (!$this->session->has_userdata('NIK')) {
			$where=array(
				'ID_user'	=>	$this->session->userdata('ID_User'),
				'md5(nomor_komentar)'=>$komentar,
				'ID_topik'	=>	$diskusi

			);
		}else{
			$where=array(
				'md5(nomor_komentar)'=>$komentar,
				'ID_topik'	=>	$diskusi

			);
			echo "string";
		}
		$this->db->delete('trans_komentar_diskusi',$where);
		
		redirect('forum/komentar/'.$diskusi);
	}

	public function ajaxKomen(){
		$this->load->library('session');
		$this->load->model('diskusi');
		
		$page = $_POST['page'];
		$topic_id = $_POST['topic_id'];
		$offset = $page * 10;

		$komentar = $this->diskusi->getKomen($topic_id, $offset, 10);
		$total_komen = $this->diskusi->count_komen($topic_id);
		$next_offset = ($page + 1) * 10;

		$html = "";
		foreach ($komentar->result_array() as $komen) {
			$newDate = date("d-M-Y", strtotime($komen['Tanggal']));
			$encrypt_no_komen = md5($komen['nomor_komentar']);
			$hapusURL = base_url().'form/hapusKomentar'.$encrypt_no_komen.'/'.$komen['ID_topik'];

			$html .= '<div class="panel panel-info">';
	        $html .= '<div class="panel-heading">';
	        $html .= '<h3 class="panel-title">';
	        $html .=    '<div class="row"> Pengirim:';
	        $html .=        $komen['nama'];
	        $html .=        '<div style="float: right;">';
			$html .=	            $newDate;
	        $html .=    	'</div>';
	        $html .=    '</div>';
	                if ( $komen['ID_user']==$this->session->userdata('ID_User') ||$this->session->has_userdata('NIK')) {
	        $html .=            "<button type='button' style='float: right;' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#$encrypt_no_komen'>Hapus";
	        $html .=            '</button>';
	        $html .=     		"<div class='modal fade' id='$encrypt_no_komen' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>";
	        $html .=                '<div class="modal-dialog" role="document">';
	        $html .=                    '<div class="modal-content">';
	        $html .=                        '<div class="modal-header">';
	        $html .=                            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	        $html .=                        '</div>';
	        $html .=                        '<div class="modal-body" style="color:black"> Apa Anda Yakin ingin menghapus ini?'; 
	        $html .=                     '</div>';
	        $html .=                        '<div class="modal-footer">';
	        $html .=                          "<button type='button' class='btn btn-default' data-dismiss='modal'>Kembali</button>";
	        $html .=                               '<a class="btn btn-danger" href="$hapusURL">Hapus</a>';
	        $html .=                        '</div>';
	        $html .=                    '</div>';
	        $html .=                '</div>';
	        $html .=            '</div>';
	                         }
	        $html .=   		'<br>';
	        $html .=        '</h3>';
	        $html .=    '</div>';
	        $html .=    '<div class="panel-body">';
	        $html .=       $komen['Komentar'];
	        $html .=    '</div>';
	        $html .=	'</div>';
		}

		$json_result['html'] = $html;
		$json_result['expandable'] = ( $next_offset < $total_komen ) ? true : false;

		echo json_encode($json_result);
	}
}
?>
