<?php
//localhost/kp/welcome

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	//constructor me load fungsi cuma di file welcome
	public function __construct()
    {
            parent::__construct();
            $this->load->library('session');
            $this->session->set_userdata(array('url'=>uri_string()));
            $this->load->model('Tahu'); //ngeload fungsi database di folder model->tahu
            $this->Tahu->navigasi(); //ngeload fungsi navigasi di model tahu(pemberitahuan)
            $this->load->model('berita');
    }

	public function index($hal=1) //ocalhost/kp/welcome/index/... ->halaman berita
	{
		$this->load->library('not_null');

		$halaman=$this->berita->lihatBerita("count(id) hal");
		$jumlah=$halaman->result_array()[0]['hal'];
		$link=base_url().'Welcome/index';
		$banyak=5;
		$page= $this->not_null->halaman($jumlah,$banyak,$link,$hal);

		$load=$banyak*($hal-1);

		$data['link']=base_url()."Welcome/baca_berita";
		$data['cari']=base_url()."Welcome/cari";
		$data['halaman']=$page;
		if ($hal==0) {
			$main="";
		}else{
			$data['data']=$this->berita->lihatBerita("*","1=1",$banyak,$load);
			$main=$this->load->view('list_berita',$data,true);
		}
		$data=array(
				"css"	=>	$this->load->view('user/css','',true),
				"nav"	=>  $this->Navigasi->index('home'),
				"berita"=>	$main,
				);
		$this->load->view('user/index',$data);
		$this->load->model('user');
	}



	private function view($main){
		$css=$this->load->view('user/css',array(),true);

		$this->load->model('navigasi');
		$nav=$this->navigasi->index("beranda");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}



	public function baca_berita($id){
		$rand['main']=$this->berita->lihatBerita("*","1=1",5,0,"rand()");
		$rand['link']=base_url()."Welcome/baca_berita";



		$where['md5(id)']=$id;
		$rand['data']=$this->berita->lihatBerita("*",$where);
		if ($rand['data']->num_rows()==1) {
			$main=$this->load->view("baca_berita",$rand,true);
			$this->view($main);
		}
	}

	public function cari(){
		if (isset($_POST['cari'])|| !empty($_POST['cari'])) {
			$cari=preg_replace('/\s\s+/', ' ', $_POST['cari']);
			redirect('Welcome/search/'.$cari);
		}
	}

	public function search($cari="",$hal=1){
		$isi= substr($cari, 0,3);
		if ($isi=="%20") {
			$cari=substr($cari,3, strlen($cari));
		}

		$where=$this->where($cari,"judul");
		$where=$where." or ".$this->where($cari,"isi");


		$this->load->library('not_null');
		$halaman=$this->berita->lihatBerita("count(id) hal");
		$jumlah=$halaman->result_array()[0]['hal'];
		$link=base_url()."Welcome/index";
		$banyak=5;
		$page= $this->not_null->halaman($jumlah,$banyak,$isi,$hal);


		$load=$banyak*($hal-1);

		$data['data']=$this->berita->lihatBerita("*",$where,$banyak,$load);
		$data['link']=base_url()."Welcome/baca_berita";
		$data['cari']=base_url()."Welcome/cari";
		$data['halaman']=$page;
		$main=$this->load->view('list_berita',$data,true);
		$this->view($main);
		
	}

	
	private function where($string,$kolom){
		$string = str_replace("%20", "%' or ".$kolom." LIKE '%", $string);
		$string= $kolom." LIKE '%".$string."%'";
		return $string;
	}
}
