<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pemberitahuan extends CI_Controller {


	private function view($main){
		$css=$this->load->view('user/css',array(),true);

		$this->load->model('navigasi');
		$nav=$this->navigasi->index("tentang");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$where["ID_User"]=$this->session->userdata('ID_User');
		$this->db->where($where);
		$data=$this->db->get('pemberitahuan');
		$main=$this->load->view('user/tahu',array('tahu' =>$data),true);
		$this->view($main);
	}
	public function link($id){
		$where['ID_User']=$this->session->userdata('ID_User');
		$where['sha(id)']=$id;
		$this->db->select('link');
		$this->db->where($where);
		$data=$this->db->get('pemberitahuan');
		$arr=$data->result_array();

		$set['status']= 1;
		$this->db->set($set);
		$this->db->where($where);
		$this->db->update('pemberitahuan');

		redirect($arr[0]['link']);


	}
}
?>

