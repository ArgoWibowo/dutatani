<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Controller {
  
  public function __construct(){
    parent::__construct();
    
    $this->load->model('MenuModel'); // Load MenuModel ke controller ini
  }
  
  public function index(){
    $data['menu'] = $this->MenuModel->view();
    //print_r($data);
    $this->load->view('menu/menu', $data);

  }
  
  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->MenuModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->MenuModel->save(); // Panggil fungsi save() yang ada di MenuModel.php
        redirect('Crud');
      }
    }
    
    $this->load->view('menu/form_tambah');
  }
  
  public function ubah($id){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->MenuModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->MenuModel->edit($id); // Panggil fungsi edit() yang ada di MenuModel.php
        redirect('Crud');
      }
    }
    
    $data['menu'] = $this->MenuModel->view_by($id);
    
    $this->load->view('menu/form_ubah', $data);
  }

  public function ubahicon($id){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      
        $this->MenuModel->editicon($id); // Panggil fungsi edit() yang ada di MenuModel.php
        redirect('Crud');
      
    }
    
    $data['menu'] = $this->MenuModel->view_by($id);
    $this->load->view('menu/form_ubahicon', $data);
  }

  public function ubahsemua($id){
    $this->MenuModel->edit($id); // Panggil fungsi edit() yang ada di MenuModel.php
  }
  
  public function hapus($id){
    $this->MenuModel->delete($id); // Panggil fungsi delete() yang ada di MenuModel.php
    redirect('Crud');
  }

  public function view(){
    $data['menu'] = $this->MenuModel->view();
    //print_r($data);
    $this->load->view('user/navigasi', $data);

  }
}