<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class about extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Tahu');
        $this->Tahu->navigasi();
    }


	private function view($main){
		$css=$this->load->view('user/css',array(),true);

		$this->load->model('navigasi');
		$nav=$this->navigasi->index("tentang");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$data=$this->db->get('about');
		$main=$this->load->view('user/about',array('tentang' =>$data),true);
		$this->view($main);

	}
	public function kirim_pesan(){
		$this->load->model('pesan');
		$this->pesan->kirim();
	}
}
?>

