<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_ganti extends CI_Controller {
	public function __construct()
    {
    	parent::__construct();
    	if (!$this->session->userdata('NIK')) {
    		echo "<pre>";
    		print_r($this->session->userdata());
    		echo "</pre>";
    	}

    }


	private function view($main){
		$css=$this->load->view('user/css',array(),true);

		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}

	public function index(){
		$where=array(
			"NIK"=>$this->session->userdata('NIK')
		);
		$this->load->model('admin_acc');
		$adm=$this->admin_acc->getAdmin($where);
		if ($adm->num_rows()==1) {
			foreach ($adm->result_array() as $key) {
				$arr=$key;
			}
			$unit=$this->db->get('master_org_unit');
			$arr['unit']=$unit;
			$arr['url']= base_url()."admin_ganti/ganti_profile";
			$arr['disable']=true;
			$arr['pass']=true;
			$main=$this->load->view('admin/tambah_admin',$arr,true);
			$this->view($main);
		}else{
			redirect('list_admin');
		}
	}
	public function ganti_profile(){
		$where=array(
			"NIK"=>$this->session->userdata('NIK')
		);
		$this->load->model('admin_acc');
		$nik=$this->session->userdata('NIK');
		$adm=$this->admin_acc->getAdmin($where);
		if ($adm->num_rows()==1) {
			if (!empty($_POST['password'])) {
				$config['Password']=sha1($_POST['password']);
			}
			if (!empty($_POST['NIK']) && $this->session->userdata('Org_Unit')==1) {
				$config['NIK']=$_POST['NIK'];
				$nik=$config['NIK'];
			}
			$config['Nama_Karyawan']=$_POST['nama'];
			$config['Tanggal_Lahir']=$_POST['tgl'];
			$config['Alamat_Rumah']=$_POST['alamat'];
			$config['No_Telpon']=$_POST['NomorTelpon'];
			$config['Jenis_Kelamin']=$_POST['jekel'];
				$this->db->where($where);
				if ($this->db->update('master_user_org', $config)) {
					$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di ganti"),true);
					$this->session->set_flashdata('error1',$error);
					$this->session->set_userdata($config);
					redirect('admin');
				}else{
					$error=$this->load->view('error',array("error"=>"Data gagal di ganti"),true);
					$this->session->set_flashdata('error1',$error);
					redirect('admin?error=1');
				}
			$error=$this->load->view('error',array("error"=>"Semua form harus diisi"),true);
			$this->session->set_flashdata('error1',$error);
			redirect('list_admin/editAdmin/'.$nik);
		}
		// redirect(base_url());
	}
	

	private function update($table,$set,$where){
		$this->db->where($where);
		return $this->db->update($table, $set);
	}
}
?>

