<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('user');
        $this->load->model('Tahu');
        $this->Tahu->navigasi();
        $this->load->library( 'nativesession' );
        
    }
	public function index()
	{
		if ($this->session->has_userdata('ID_User') && uri_string()!="login/keluar") {
			redirect($this->session->userdata('url'));
		}
		
		$tampilan=array(
				"css"	=>	$this->load->view('user/css','',true),
				"nav"	=>	$this->Navigasi->index(''),
				"main"	=>	$this->load->view('user/masuk','',true)
		);
		$this->load->view('user/main',$tampilan);
		
		
	}

	public function daftar($tahap=1){
		if ($this->session->has_userdata('ID_User') && uri_string()!="login/keluar") {
			redirect($this->session->userdata('url'));
		}
		echo "$tahap";

		if ($tahap<3) {
			$form=$tahap;
		}elseif($tahap==3){
			if ($this->session->userdata('tahap')!=2) {
					redirect('login/daftar/2');
				}
			$kategori=$this->session->userdata('kategori');
			if ($kategori=="PET"){
				$form="petani";
			}elseif ($kategori=="SUP"){
				$form="supplier";
			}elseif ($kategori=="FAS"){
				$form="fasilitator";
			}else{
				$form="Detail";
			}
		}
		$provinsi=$this->getWilayah();
		$get=array(
			"provinsi"=>$provinsi,
		);
		$tampilan=array(
				"css"	=>	$this->load->view('user/css','',true),
				"nav"	=>	$this->Navigasi->index(''),
				"main"	=>	$this->load->view('user/tahap',array(
				"tahap"	=> $tahap,
			    "form"	=> $this->load->view('user/form/'.$form,$get,true)
					),true)
		);
		$this->load->view('user/main',$tampilan);
	}

	public function input($tahap){
		if ($this->session->has_userdata('ID_User') && uri_string()!="login/keluar") {
			redirect($this->session->userdata('url'));
		}
		
		if ($tahap==1) {
			$user=array(
					"tahap"		=> $tahap,
					"kategori"	=> $_POST['kategori'],
				);
			$this->session->set_userdata($user);
			$tahap=2;
			redirect('login/daftar/'.$tahap);
		}elseif($tahap==2){
				$error=0;
				

				if ($_POST['confrim']!=$_POST['password']) {
					$error=1;
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
							"error"	=>	'"Confirm Password" dan "Password" harus sama'
						),true));
				}

				if (strlen($_POST['confrim'])<6 || strlen($_POST['confrim'])>14) {
					$error=1;
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
							"error"	=>	'"Password" harus lebih dari 6 dan kurang dari 15'
						),true));
				}

				if (strlen($_POST['PIN'])!=6) {
					$error=1;
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
							"error"	=>	'"PIN" harus 6 angka'
						),true));
				}

				if (!is_numeric($_POST['PIN'])) {
					$error=1;
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
						"error"	=>	'Pin harus angka'
					),true));
				}

				if (strlen($_POST['username'])<6 || strlen($_POST['username'])>10) {
					$error=1;
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
							"error"	=>	'"Username" harus 6 karakter'
						),true));
				}

				if (strpos($_POST['username'],' ')>0) {
					$error=1;
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
							"error"	=>	'"username" tidak boleh ada spasi'
						),true));
				}

				if ((!isset($_POST['PIN'])||empty($_POST['PIN'])) || (!isset($_POST['password'])||empty($_POST['password'])) || (!isset($_POST['username'])||empty($_POST['username'])) || (!isset($_POST['confrim'])||empty($_POST['confrim']))) {
					$error=1;
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
						"error"	=>	'Semua form harus diisi'
					),true));
				}


				if ($error==1) {
					redirect("login/daftar/2");
				}

				if ($this->user->cek_user($_POST['username'])===0) {
					$user=array(
						"tahap"		=>$tahap,
						"id"		=>$_POST['username'],
						"password"	=>$_POST['password'],
						"PIN"		=>$_POST['PIN'],
					);
					$this->session->set_userdata($user);
					$tahap=3;
					redirect('login/daftar/'.$tahap);
				}else{
					$this->session->set_flashdata("error",$this->load->view("user/error",array(
							"error"	=>	"Username yang anda gunakan sudah di gunakan seseorang"
						),true));
					redirect("login/daftar/2");
				}
		}elseif($tahap==3){
			$this->load->model('user');
			$kategori=$this->session->userdata('kategori');
			$this->user->insert($_POST,$_FILES);
		}else{
			redirect("login/daftar");
		}
	}

	public function masuk(){
		if ($this->session->has_userdata('ID_User') && uri_string()!="login/keluar") {
			redirect($this->session->userdata('url'));
		}

		if (!isset($_POST['username']) || empty($_POST['username'] || !isset($_POST['password']) || empty($_POST['password']))) {
			echo "gagal";
		}else{
			$where=array(
				"ID_User"	=>	$_POST['username'],
				"Password"	=>	$_POST['password']
				);
			$this->load->model('user');
			$this->user->login($where);
		}
	}
	private function getWilayah(){
		$data=array(
			"prov"=>$this->db->get('provinsi')
		);
		return $this->load->view('user/lokasi',$data,true);
		
	}
	public function getKabupaten(){
		$where=array(
			'Nama_Provinsi'=>$_POST['Provinsi']
		);
		$this->db->where($where);
		$data=$this->db->get('kabupaten');
			echo "<option>=== Kabupaten/Kota ===</option>";
			foreach ($data->result_array() as $key) {
				echo "<option value='".$key['Nama_Kabupaten']."'";
				if ($this->session->userdata('kabupaten')==$key['Nama_Kabupaten']) {
					echo "selected=''";
				}
				echo ">".$key["Nama_Kabupaten"];
				echo "</option>";
			}
	}
	public function getKecamatan(){
		$where=array(
			'Nama_Kabupaten'=>$_POST['kabupaten']
		);
		$this->db->where($where);
		$data=$this->db->get('kecamatan');
		echo "<option>=== Kecamatan ===</option>";
		foreach ($data->result_array() as $key) {
			echo "<option value='".$key['Nama_Kecamatan']."'";
			if ($this->session->userdata('kecamatan')==$key['Nama_Kecamatan']) {
				echo "selected=''";
			}
			echo ">".$key["Nama_Kecamatan"];
			echo "</option>";
		}
	}

	public function getDesa(){
		$where=array(
			'Nama_Kecamatan'=>$_POST['Kecamatan']
		);
		$this->db->where($where);
		$data=$this->db->get('kelurahan_desa');
		echo "<option>=== Desa/Kelurahan ===</option>";
		foreach ($data->result_array() as $key) {
			echo "<option value='".$key['Nama_Desa']."'";
			if ($this->session->userdata('keluran_desa')==$key['Nama_Desa']) {
				echo "selected=''";
			}
			echo ">".$key["Nama_Desa"];
			echo "</option>";
		}
	}

	public function keluar(){
		$this->user->logout();
		redirect('login');
	}
}
?>