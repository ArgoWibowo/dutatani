<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Autocomplete extends CI_Controller {
		
		public function __construct()
	    {
            parent::__construct();
            $this->load->library('session');
            $this->load->model('Diskusi');
            $this->load->model('berita');
	    }

	    public function autocomplete_berita_forum() //Fungsi Autocomplete untuk berita dan diskusi yang dicari
	    {
	    	if ( isset( $_GET['term'] ) ) {
	            $berita_data = $this->berita->search( $_GET['term'] );
	            $forum_data = $this->Diskusi->search( $_GET['term'] );

	            $arr_data = array();
	            foreach($berita_data->result() as $berita) {
	            	$arr_data[] = array(
	            		"category"	=> "Berita",
	            		"link"		=> base_url() . "/Welcome/baca_berita/" . md5($berita->id),
	            		"title"		=> $berita->judul
	            	);
	            }

	            foreach($forum_data->result() as $forum) {
	            	$arr_data[] = array(
	            		"category"	=> "Diskusi",
	            		"link"		=> base_url() . "/forum/komentar/" . $forum->ID_kategori_topik,
	            		"title"		=> $forum->Judul_kategori_topik
	            	);
	            }

	            echo json_encode($arr_data);  //Return json yang dalam arti sudah terformat
	    	}
	    }
		
	}

?>
