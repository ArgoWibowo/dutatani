<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->has_userdata('Org_Unit') ){
			redirect();
		}
    }
    private function getUser($where){
    	$this->db->where($where);
    	return $this->db->get('master_user_kat');
    }
	public function index(){
		$css=$this->load->view('admin/css',array(),true);

		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("user");

		$user=$this->getUser(array('ID_Kategori'=>'FAS'));
		$fas=$user->num_rows();
		$user=$this->getUser(array('ID_Kategori'=>'PET'));
		$pet=$user->num_rows();
		$user=$this->getUser(array('ID_Kategori'=>'SUP'));
		$sup=$user->num_rows();
		$where="ID_Kategori <> 'FAS' and ID_Kategori <> 'PET' and ID_Kategori<>'SUP'";
		$user=$this->getUser($where);
		$lain=$user->num_rows();

		$this->db->select("master_kategori.ID_Kategori, Nama_Kategori, Deskripsi,COUNT(master_user_kat.ID_User) jumlah,aktif");
		$this->db->from('master_kategori');
		$this->db->join('master_user_kat','master_user_kat.ID_Kategori=master_kategori.ID_Kategori','left');
		$this->db->group_by('master_kategori.ID_Kategori, Nama_Kategori, Deskripsi');
		$this->db->order_by("aktif","desc");
		$kategori=$this->db->get();


		



		$arr = array(
			'fas' 	=> $fas,
			'pet'	=> $pet,
			'sup'	=> $sup,
			'lain'	=>$lain,
			'kategori'=>$kategori
		);

		

		$main=$this->load->view('admin/user',$arr,true);

		$this->load->view('admin/main',array(
			'css'	=>$css,
			'nav'	=>$nav,
			'main'	=>$main,
		));
	}
	private function view($main){
		$css=$this->load->view('admin/css',array(),true);
		$this->load->model('admin_nav');
		$nav=$this->admin_nav->getNav("user");
		$this->load->view('admin/main',array(
			'css'	=>$css,
			'main'	=>$main,
			'nav'	=>$nav
		));
	}
	public function user($kategori){
		
		if (strtoupper($kategori)==strtoupper('Lain')) {
			$where="ID_Kategori <> 'FAS' and ID_Kategori <> 'PET' and ID_Kategori<>'SUP'";
		}else{
			$where['ID_Kategori']=$kategori;
		}
		$str=strtoupper($kategori);

		if ($str==strtoupper("FAS")) {
			$kat="Fasilitator";
		}elseif ($str==strtoupper("Lain")) {
			$kat="Lain-Lain";
		}elseif ($str==strtoupper("SUP")) {
			$kat="Suplayer";
		}elseif ($str==strtoupper("PET")) {
			$kat="Petani";
		}




		$this->db->join('master_detail_user','master_user_kat.ID_User=master_detail_user.ID_User');
		$this->db->join('master_user','master_user.ID_User=master_detail_user.ID_User');
		$this->db->where($where);
		// $this->db->where('Tingkat_Priv=1');
		$data=$this->db->get('master_user_kat');
		$main=$this->load->view('admin/tabel',array(
			"Kat"=>$kat,
			"data"=>$data
		),true);
		$this->view($main);

	}

	public function hapus_user($id){
		if ($this->session->userdata('kelola_user')==1) {

			$this->db->set('Tingkat_Priv', '0');
			$this->db->where('ID_User', $id);
			if ($this->db->update('master_user')) {
				$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di <b>non-aktifkan</b>"),true);
				$this->session->set_flashdata('error1',$error);	
				redirect('admin');
				
			}else{
				$error=$this->load->view('error',array("error"=>"Data Gagal Di ganti"),true);
				$this->session->set_flashdata('error1',$error);	
				echo "gagal";
			}

		}
	}

	public function aktif_user($id){
		if ($this->session->userdata('kelola_user')==1) {

			$this->db->set('Tingkat_Priv', '1');
			$this->db->where('ID_User', $id);
			if ($this->db->update('master_user')) {
				$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di <b>aktifkan</b>"),true);
				$this->session->set_flashdata('error1',$error);	
				redirect('admin');
				
			}else{
				$error=$this->load->view('error',array("error"=>"Data Gagal Di ganti"),true);
				$this->session->set_flashdata('error1',$error);	
				echo "gagal";
			}

		}
	}

	public function tambah_kategori()
	{
		$this->load->model('user');
		$temp=substr($_POST['nama'], 0,3);
		$where['ID_Kategori']=$temp;

		$nama= $this->user->sama('master_kategori',$where,'ID_Kategori',3);

		$arr=array(
			"ID_Kategori"	=> $nama,
			"Nama_Kategori"	=> $_POST['nama'],
			"Deskripsi"		=> $_POST['Deskripsi']
		);
		$this->db->set($arr);
		if ($this->db->insert("master_kategori")) {
			$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di tambahkan"),true);
			$this->session->set_flashdata('error1',$error);	
			redirect('admin');
		}else{
			$error=$this->load->view('error',array("error"=>"Data Berhasil Di ganti"),true);
			$this->session->set_flashdata('error1',$error);	
			echo "gagal";
		}

	}

	public function aktif($id){
		$this->db->where(array("ID_Kategori" =>$id));
		$this->db->set("aktif","1");
		if ($this->db->update("master_kategori")) {
			$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di aktifkan kembali"),true);
			$this->session->set_flashdata('error1',$error);	
			redirect('admin');
		}else{
			$error=$this->load->view('error',array("error"=>"Data Berhasil Di ganti"),true);
			$this->session->set_flashdata('error1',$error);	
			echo "gagal";
		}
	}

	public function non_aktif($id){
		$this->db->where(array("ID_Kategori" =>$id));
		$this->db->set("aktif","0");
		if ($this->db->update("master_kategori")) {
			$error=$this->load->view('sukses',array("error"=>"Data Berhasil Di non aktifan"),true);
			$this->session->set_flashdata('error1',$error);	
			redirect('admin');
		}else{
			$error=$this->load->view('error',array("error"=>"Data Gagal Di ubah"),true);
			$this->session->set_flashdata('error1',$error);	
			echo "gagal";
		}
	}
}

?>
