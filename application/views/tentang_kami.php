 <h1>Tentang Kami</h1>
<hr>
<script type="text/javascript">
        $(function() {
        $('#summernote').summernote({
        toolbar:    [
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'fullscreen', 'hr']]
                    ],
        height: ($(window).height() - 300),
        callbacks: {
        onImageUpload: function(image) {
            uploadImage(image[0]);
        }
    }
});

function uploadImage(image) {
    alert('Tunggu Sampai Muncul Gambar atau pemberitahuan');
    var data = new FormData();
    data.append("image", image);
    $.ajax({
        url: '<?php echo base_url() ?>about_admin/do_upload',
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: "post",
        success: function(url) {
            var image = url;
            if (image==1) {
                alert('Gagal Membaca File');
            }else{
                $('#summernote').summernote("editor.insertImage", image);
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
}


});

    </script>
    <style type="text/css">
    nav.navbar.navbar-inverse.sidebar{
        z-index: 1;
    }
    </style>
    <form action="<?php echo base_url() ?>/about_admin/update" method="post">
        <textarea id="summernote" class="col-md-12" name="about">
            <?php 
                $isi="";
                foreach ($data->result_array() as $key) {
                    $isi=$key['tentang'];
                }
                echo $isi;
             ?>
        </textarea>
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-send"></i>
                Kirim
        </button>
    </form>