<?php 
if ($this->session->has_userdata('NIK')) {
	echo '<a style="float:right;" href="'.base_url().'admin_berita/tambah" class="btn btn-info">Tambah berita</a>';
}
?>

<h1>Berita</h1>
<?php ?>
<div class="container-fluid">
<?php
if (!is_array($data)) {
	# code...
	foreach ($data->result_array() as $key) {
	?>
		
		<div class="row" style="border:solid; border-color: transparent; border-top-color: #eeeeee; border-bottom-color: #eeeeee; padding-top: 2%; padding-bottom: 2%;">
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
				<img height="100px" width="100px" src="<?php echo base_url() . $key['foto']; ?>" >
			</div>
			<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10" style="">
				<a href="<?php echo $link."/".md5($key['id']); ?>"><?php echo $key['judul']; ?></a><br>
				<?php if ($this->session->has_userdata('NIK')): ?>
					<a style="float: right; margin-left: 5px; " class="btn btn-danger" href="<?php echo base_url()."admin_berita/hapus/".md5($key['id']); ?>">
					<i class="glyphicon glyphicon-remove"></i>
					Hapus</a>

					<a style="float: right;" class="btn btn-success" href="<?php echo base_url()."admin_berita/edit/".md5($key['id']); ?>">
					<i class="glyphicon glyphicon-edit"></i>
					Edit</a>
				<?php endif ?>
				<span class="glyphicon glyphicon-calendar" style="float: right; padding-right: 15px;">
				<?php 
					$tgl= $key['tanggal']; 
					$tgl=date_create($tgl);
					echo date_format($tgl,"d-M-Y");
				?>
				</span>
				<hr>
				<b>Penulis : <?php echo $key['Nama_Karyawan'] ?> </b>
			</div>
		</div>
		<hr>
	<?php 
		} 
		echo $halaman;
	}else{
		echo "<h1>".$data['error']."</h1>";
	}
	?>

</div>
