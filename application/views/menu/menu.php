<html>
  <head>
    <title>CRUD Codeigniter</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="<?php echo base_url(); ?>js/jquery-2.1.4.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <script>
    function deleted(x) {
      var r = confirm("Apakah anda yakin akan menghapus?");
      if (r == true) {
        window.location.href = "<?php echo base_url('crud/hapus/')?>" + "/" + x;
      }
    }


    function myFunction(x,a) {
      var ma = document.getElementById("a"+a);
      var mb = document.getElementById("b"+a);
      var mc = document.getElementById("c"+a);
      var md = document.getElementById("d"+a);
      var me = document.getElementById("e"+a);
      var mf = document.getElementById("f"+a);
      
      $.ajax({  
          type: "POST",
          url: "<?php echo base_url('crud/ubahsemua/')?>" + "/" + ma.innerText,
          // data: '{"input_id":"' + ma.innerText + '","input_urutan":"' + mb.innerText + '","input_induk":"' + mc.innerText + '","input_judul":"' + md.innerText + '","input_link":"' + me.innerText + '","input_lokasi":"' + mf.options[mf.selectedIndex].value + '"}',
          data: {
            input_id : ma.innerText,
            input_urutan : mb.innerText,
            input_induk : mc.innerText,
            input_judul : md.innerText,
            input_link : me.innerText,
            input_lokasi : mf.options[mf.selectedIndex].value,
          },
          error : function(result){
            alert("Data Gagal Diupdate");
          },
          success : function(result){
            alert("Data Berhasil Diupdate");
          }
      });
    }
    </script>
    <style type="text/css">
        @import "compass/css3";

        .table-editable {
          position: relative;
          text-align: center;
          
        }
        .glyphicon {
            font-size: 16px;
          }

        .table-edit,.table-remove {
          color: #000;          
        }

        .table-add {
          color: #000;
        }

        .btn{
          color: #000;
          background: #fff;
          text-align: center;
        }

        .icon-flipped {
          transform: scaleX(-1);
          -moz-transform: scaleX(-1);
          -webkit-transform: scaleX(-1);
          -ms-transform: scaleX(-1);
        }
        .a{
          float: right;
        }
    </style>
  </head>
  <body>
    <div class="container">
    <h1>Data Menu</h1>
    <hr>
    
    <a href='<?php echo base_url("CRUD/tambah"); ?>'>
      <button id="tambah-btn" class="btn btn-primary">Tambah Data Menu <span class="table-add glyphicon glyphicon-plus"></span>
      </button>
    </a>
    <a class="a" href='<?php echo base_url();?>'>
      <button id="tambah-btn" class="btn btn-primary">Kembali <span class="icon-flipped glyphicon glyphicon-share-alt"></span>
      </button>
    </a>
    <br><br>
    <a>Untuk Pengubahan isi menu dapat langsung klik pada tabel</a>
    <div id="table" class="table-editable">
    <table border="0" cellpadding="7" class="table">
        <tr>
          <th>ID</th>
          <th>Urutan Menu</th>
          <th>ID Induk</th>
          <th>Judul Menu</th>
          <th>Link</th>
          <th>Icon</th>
          <th>Posisi Menu</th>
          <th></th>
          <th>Aksi</th>
          <th></th>
        </tr>

        <?php
        $z =0;
        $data = $this->db->query("SELECT * FROM ta order by position");
        if( ! empty($data)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
          foreach($data->result() as $menu){
            $z++;
            ?>
            <tr>
            <td id="a<?php echo $z ?>"><?php echo"".$menu->id."</td>"?>
            <td id="b<?php echo $z ?>" contenteditable="true"><?php echo"".$menu->position."</td>"?>
            <td id="c<?php echo $z ?>" contenteditable="true"><?php echo"".$menu->parent_id."</td>"?>
            <td id="d<?php echo $z ?>" contenteditable="true"><?php echo"".$menu->title."</td>"?>
            <td id="e<?php echo $z ?>" contenteditable="true"><?php echo"".$menu->link."</td>"?>
            <td>
            
            <div>
            
            <?php echo "
            <span class='".$menu->icon."'></span>
            "?>
            
            </div>
            </td>

            <td  contenteditable="true">
              <select class="form-control" id="f<?php echo $z ?>">
                 <?php
                  if($menu->is_top == 0){
                  ?>
                    <option value="0">Kiri</option>
                    <option value="1">Atas</option>
                    <?php  
                  }else{
                    ?>
                    <option value="1">Atas</option>
                    <option value="0">Kiri</option>
                    <?php } ?>
                   
                  
              </select>
           
            </td>
            <td onclick="myFunction(this,'<?php echo $z ?>')" 
              <?php echo "><button id='tambah-btn' class='btn btn-primary'><span class='table-edit glyphicon glyphicon-pencil'></span> Update</button></td>" ?>

             <td><a href="<?php echo"".base_url("crud/ubahicon/$menu->id") ?>"><button id='tambah-btn' class='btn btn-primary' ><span class='table-edit glyphicon glyphicon-pencil'></span> Ubah Icon</button></a></td>

            <td onclick="deleted(<?php echo $menu->id?>)"><a><button id='tambah-btn' class='btn btn-primary'><span class='table-edit glyphicon glyphicon-remove'></span> Hapus</button></a></td>

            <td></td>

          </tr>
          <?php }  
        }else{?> 
        // Jika data siswa kosong
          <tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>
        <?php } ?>
      </table>
      </div>
      <hr>


  </div>
  </body>
</html>