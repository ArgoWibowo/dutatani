<html>
  <head>
    <title>Form Ubah - CRUD Codeigniter</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <style type="text/css">
        @import "compass/css3";

        .table-editable {
          position: relative;
          
        }
        .glyphicon {
            font-size: 20px;
          }
        .pilih {
            color: blue;
          }
        .btn{
          color: #000;
          background: #fff;
          text-align: center;
        }

    </style>
  </head>
  <body>
    <h1>Form Ubah Icon Menu</h1>
    <hr>
    <!-- Menampilkan Error jika validasi tidak valid -->
    <div style="color: red;"><?php $this->load->library('form_validation'); echo validation_errors(); ?></div>
    <?php 
     echo form_open("crud/ubahicon/".$menu->id);
    ?>
      <table style="width: 46%" cellpadding="8" class="table">
        <tr>
          <td>ID</td>
          <td><input type="text" name="input_id" value="<?php echo set_value('input_id',$menu->id); ?>" readonly></td>
        </tr>
        <td>Icon Menu</td>
        <td>
          <?php
                  $menu = $this->db->query("select * FROM icon");;
                  $count = 0;
                  foreach ($menu->result() as $icon) {
                      
                      
                      ?>
                      
                      <input class="pilih" type="radio" name="input_icon" value="<?php echo $icon->text;?>" <?php  echo set_radio('icon', $icon->text);$count ++;?>>
                      
                      <?php
                      echo "<span class='".$icon->text."'></span>";
                      //print_r($count);
                  }
                  ?>
        </td>
      </table>
        
      <hr>
      <input class="btn btn-primary" type="submit" name="submit" value="Ubah">
      <a href="<?php echo base_url('crud'); ?>"><input class="btn btn-primary" type="button" value="Batal"></a>
    
  </body>
</html>