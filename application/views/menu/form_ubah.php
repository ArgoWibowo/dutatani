<html>
  <head>
    <title>Form Ubah - CRUD Codeigniter</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <style type="text/css">
        @import "compass/css3";

        .glyphicon {
            font-size: 20px;
          }
        .pilih {
            color: blue;
          }

    </style>
  </head>
  <body>
    <h1>Form Ubah Menu</h1>
    <hr>
    <!-- Menampilkan Error jika validasi tidak valid -->
    <div style="color: red;"><?php $this->load->library('form_validation'); echo validation_errors(); ?></div>
    <?php 
    //echo "$menu";
    //$data = $this->db->query("SELECT * FROM ta");;
    
    echo form_open("crud/ubah/".$menu->id);
     ?>
      <table style="width: 48%" cellpadding="8" class="table">
        <tr>
          <td>ID</td>
          <td><input type="text" name="input_id" value="<?php echo set_value('input_id',$menu->id); ?>" readonly></td>
        </tr>
        <tr>
          <td>
              <label>Induk</label>
            </td><td>
              <select class="form-control" name="input_induk" id="induk" style="width: 36%">
                  <option value="">Tidak ada Induk</option>
                  <?php
                  $data2 = $this->db->query("select * FROM ta order by position");;
                  foreach ($data2->result() as $menu2) {
                      ?>
                      <option value="<?php echo set_value('input_induk', $menu2->id); ?>"><?php echo $menu2->title ?></option>
                      <?php
                  }
                  ?>
              </select>
          <td>
        </tr>
        <tr>
          <td>Judul</td>
          <td><input type="text" name="input_judul" value="<?php echo set_value('input_judul', $menu->title); ?>"></td>
        </tr>
        <tr>
          <td>Link</td>
          <td><input type="text" name="input_link" value="<?php echo set_value('input_link', $menu->link); ?>"></td>
        </tr>
        <tr>
          <td>Urutan Menu</td>
          <td><input type="text" name="input_urutan" value="<?php echo set_value('input_urutan', $menu->position); ?>"></td>
        </tr>
        <td>Posisi Menu</td>
        <td>
          <input type="radio" name="input_lokasi" value="<?php echo set_value('input_lokasi', $menu->is_top); ?>"> Kiri
          <input type="radio" name="input_lokasi" value="<?php echo set_value('input_lokasi', $menu->is_top); ?>"> Atas
        </td>
        <tr>
        <td>Icon Menu</td>
        <td>
          <?php
                  $menu = $this->db->query("select * FROM icon");;
                  $count = 0;
                  foreach ($menu->result() as $icon) {
                      
                      
                      ?>
                      
                      <input class="pilih" type="radio" name="input_icon" value="<?php echo $icon->text;?>" <?php  echo set_radio('icon', $icon->text);$count ++;?>>
                      
                      <?php
                      echo "<span class='".$icon->text."'></span>";
                      //print_r($count);
                  }
                  ?>
        </td>
        </tr>
      </table>
        
      <hr>
      <input type="submit" name="submit" value="Ubah">
      <a href="<?php echo base_url('crud'); ?>"><input type="button" value="Batal"></a>
    <?php echo form_close(); ?>
  </body>
</html>