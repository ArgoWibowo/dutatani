<!DOCTYPE html>
<html>
  <head>
    <title>Form Tambah - CRUD Codeigniter</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <style type="text/css">
        @import "compass/css3";

        .glyphicon {
            font-size: 20px;
          }

        .btn{
          color: #000;
          background: #fff;
          text-align: center;
        }

    </style>
  </head>
  <body>
    <h1>Form Tambah Menu</h1>
    <hr>
    <!-- Menampilkan Error jika validasi tidak valid -->
    <div style="color: red;"><?php $this->load->library('form_validation'); echo validation_errors(); ?></div>
    <?php echo form_open("crud/tambah"); ?>
      <table style="width: 47%" cellpadding="8" class="table">
        <tr>
          <td>
              <label>Induk</label>
            </td>
            <td>
              <select class="form-control" name="input_induk" id="induk" style="width: 36%">
                  <option value="">Tidak ada Induk</option>
                  <?php
                  $data = $this->db->query("select * FROM ta order by position");;
                  foreach ($data->result() as $menu) {
                      ?>
                      <option value="<?php echo set_value('input_induk', $menu->id); ?>"><?php echo $menu->title ?></option>
                      <?php
                  }
                  ?>
              </select>
          </td>
        </tr>
        <tr>
          <td>Judul</td>
          <td><input type="text" name="input_judul" value="<?php echo set_value('input_judul'); ?>"></td>
        </tr>
        <tr>
          <td>Link</td>
          <td><input type="text" name="input_link" value="<?php echo set_value('input_link'); ?>"></td>
        </tr>
        <tr>
          <td>Urutan Menu</td>
          <td><input type="text" name="input_urutan" value="<?php echo set_value('input_urutan'); ?>"></td>
        </tr>
        <tr>
        <td>Posisi Menu</td>
        <td>
          <input type="radio" name="input_lokasi" value="0" <?php echo set_radio('is_top', '0'); ?>> Kiri
          <input type="radio" name="input_lokasi" value="1" <?php echo set_radio('is_top', '1'); ?>> Atas
        </td>
        </tr>
        <td>Icon Menu</td>
        <td>
          <?php
                  $menu = $this->db->query("select * FROM icon");;
                  $count = 0;
                  foreach ($menu->result() as $icon) {
                      
                      
                      ?>
                      
                      <input type="radio" name="input_icon" value="<?php echo $icon->text;?>" <?php  echo set_radio('icon', $icon->text);$count ++;?>>
                      
                      <?php
                      echo "<span class='".$icon->text."'></span>";
                      //print_r($count);
                  }
                  ?>
        </td>
      </table>
        
      <hr>
      <input class="btn btn-primary" type="submit" name="submit" value="Simpan">
      <a href="<?php echo base_url('crud'); ?>"><input class="btn btn-primary" type="button" value="Batal"></a>
    <?php echo form_close(); ?>
  </body>
</html>