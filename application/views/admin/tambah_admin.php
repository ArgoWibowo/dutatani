<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format:'YYYY-MM-DD'
		});
	});


</script>
<?php 
	echo $this->session->flashdata('error');
 ?>
<form action="<?php echo $url ?>" class="form form-vertical" enctype="multipart/form-data" method="post">
  <?php echo $this->session->flashdata('error1'); ?>
	<?php if ($disable==false || $this->session->userdata('Org_Unit')==1): ?>
    <div class="control-group col-md-4">
        <label>NIK</label>
        <div class="controls">
          <input type="text" class="form-control" placeholder="Nomor Induk Karyawan" name="NIK" value="<?php echo "$NIK"; ?>">
        </div>
    </div>
  <?php endif ?>
  <?php if ($pass==true): ?>
    <div class="control-group col-md-4">
        <label>Password</label>
        <div class="controls">
          <input type="text" class="form-control" placeholder="Password" name="password" value="">
        </div>
    </div>
  <?php endif ?>
	<div class="control-group col-md-4">
        <label>Nama</label>
      	<div class="controls">
	        <input type="text" class="form-control" placeholder="Nama" name="nama" value="<?php echo $Nama_Karyawan; ?>">
        </div>
    </div>
    <div class="control-group col-md-4">
		<label>Tanggal Lahir</label>
        <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control input-group-addon" name="tgl" value="<?php echo $Tanggal_Lahir; ?>" />
            <span class="input-group-addon">
    	        <span class="glyphicon glyphicon-calendar"></span>
            </span>
         </div>  
    </div>
  <div class="col-md-12">
    <div class="control-group form-group">
        <label class="col-lg-2 control-label">Jenis Kelamin</label>
      <div class="col-lg-10">
        <div class="radio">
              <label>
                <input type="radio" name="jekel" id="optionsRadios1" value="L" <?php if($Jenis_Kelamin=="L")echo 'checked=""'; ?>>
                Laki-laki
              </label>
        </div>
        <div class="radio">
              <label>
                <input type="radio" name="jekel" id="optionsRadios2" value="P" <?php if($Jenis_Kelamin=="P")echo 'checked=""'; ?>>
                Perempuan
              </label>
        </div>
      </div>
    </div>
  </div>
	<div class="control-group col-md-12">
		<label>Alamat</label>
		<div class="controls">
            <textarea class="form-control" placeholder="Alamat" name="alamat"><?php echo $Alamat_Rumah; ?></textarea>
		</div>
	</div>
	<div class="control-group col-md-6">
	  	<label>Nomor Telpon</label>
	  	<div class="controls">
	    <input type="text" class="form-control" placeholder="Nomor Telpon" name="NomorTelpon" value="<?php echo $No_Telpon; ?>">
	    
	  </div>
	</div>
	<div class="control-group col-md-6">
		<label>Email</label>
      	<div class="controls">
    		<input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo $Email ?>">
                
        </div>
	</div>
	<?php if ($disable==false || $this->session->userdata('Org_Unit')==1){ ?>
	<div class="control-group col-md-12">
		<label>Unit</label>
		<select class="form-control" name="atasan" value="<?php echo $Org_Unit; ?>">
		<?php 
      		foreach ($unit->result_array() as $key ) {
      			echo "<option value='".$key['Org_Unit']."'";
            if ($Org_Unit==$key['Org_Unit']) {
              echo "selected";
            }
            echo ">";
      			echo $key['Nama_Organisasi'];
      			echo "</option>";
      		}
      	 ?>
	  	</select>
	</div>
  <?php } ?>
    <div class="control-group col-md-12">
    <label>Pengelolaan</label>        
            <ul style="list-style-type:none">
              <li><input type="checkbox" name="1" value="1" <?php if($kelola_website){echo "checked=''";} ?>> Kelola Website</li>
              <li><input type="checkbox" name="2" value="1" <?php if($kelola_user){echo "checked=''";} ?>> Kelola User</li>
              <li><input type="checkbox" name="3" value="1" <?php if($Kelola_forum){echo "checked=''";} ?>> Kelola Forum</li>
            </ul>
    </div>
	<div class="control-group col-md-12">
	  	<label></label>
	  	<div class="controls">
	    	<button type="submit" class="btn btn-primary">
          <i class="glyphicon glyphicon-send">
            Kirim
          </i>
	    	</button>
	  	</div>
	</div>
</form>