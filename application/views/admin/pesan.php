
<h1>Pesan</h1>
	<?php echo $this->session->flashdata("error1"); ?>
<table class="table ">
	<thead>
		<tr>
			<td>No</td>
			<td>Nama</td>
			<td>E-Mail</td>
			<td>Tanggal</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
	<?php 
		$i=1;
		foreach ($pesan->result_array() as $key): 
	?>
			<?php if ($key['status']==1): ?>
				<tr class="danger">
			<?php endif ?>
			<?php if (!$key['status']==1): ?>
				<tr>
			<?php endif ?>
			<td><?php echo $i; ?></td>
			<td><?php echo $key['nama'] ?></td>
			<td><?php echo $key['email'] ?></td>
			<td><?php echo $key['tanggal_jam'] ?></td>
			<td>
				
				
				<a href="<?php echo base_url()."admin_pesan/lihat/".md5($key['id_pesan']) ?>" class="btn btn-info">
					<i class="glyphicon glyphicon-eye-open"></i>
						Lihat
				</a>

				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#hapus<?php echo $key['id_pesan'] ?>">
					<i class="glyphicon glyphicon-remove"></i>
						Hapus
				</button>
				<div class="modal fade" id="hapus<?php echo $key['id_pesan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  					<div class="modal-dialog" role="document">
    					<div class="modal-content">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        	<h4 class="modal-title" id="myModalLabel">Peringatan
					      	</div>
					      	<div class="modal-body">
					        	Apakah anda yakin menghapus pesan ini?
					        	<br>
					        	<br>
					        	Dari: <b><?php echo $key['nama'] ?></b><br>
					        	Isi : <b><?php echo $key['pesan'] ?></b>
					      	</div>
					      	<div class="modal-footer">
					      		<a href="<?php echo base_url() ?>admin_pesan/hapus_pesan/<?php echo sha1($key["id_pesan"]) ?>" class="btn btn-danger">Yakin</a>
					      	</div>
    					</div>
  					</div>
				</div>
			</td>
		</tr>
	<?php 
	$i++;
		endforeach 
	?>
	</tbody>
</table>