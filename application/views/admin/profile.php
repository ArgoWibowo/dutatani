<ul class="nav navbar-nav navbar-right">
	        <li>
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	            	<?php 
		            	$this->load->library('session');
		  				if ($this->session->has_userdata('NIK')) {
		  					echo '<span class="glyphicon glyphicon-user"></span>'.$this->session->userdata('Nama_Karyawan').'<b class="caret"></b>';
		  				}
	            	 ?>
	            </a>
	            <ul class="dropdown-menu multi-column columns-3">
		          	
		          	<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12" style="text-align:center;">
		          	
		          		<table class="table table-striped">
						  	
						  	<tbody style="text-align:left">
						    	<tr>
							      	<td>Username</td>
							      	<td><?php echo $this->session->userdata('NIK'); ?></td>
							      	<td></td>
						    	</tr>
						    	<tr>
							      	<td>Nama</td>
							      	<td><?php echo $this->session->userdata('Nama_Karyawan'); ?></td>
							      	<td></td>
						    	</tr>
						    	<tr>
							      	<td>No. Telpone</td>
							      	<td><?php echo $this->session->userdata('No_Telpon'); ?></td>
							      	<td></td>
						    	</tr>
						    	<tr>
							      	<td>E-mail</td>
							      	<td><?php echo $this->session->userdata('Email'); ?></td>
							      	<td></td>
						    	</tr>
						    	<tr>
						    		<td></td>
						    		<td colspan="2"><a href="<?php echo base_url(); ?>admin_ganti" class="btn btn-warning" style="float:right">Ganti Profil</a></td>
						    	</tr>
						  	</tbody>
						</table> 
		          	</div>
	            </ul>
	        </li>
	        <li>
	        	<a href="<?php echo base_url(); ?>log_admin/keluar">
	        		<span class="glyphicon glyphicon-log-out"></span>
	        		Keluar
	        	</a>
	        </li>
        </ul>