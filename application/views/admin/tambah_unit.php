<form action="<?php echo $url ?>" class="form form-vertical" enctype="multipart/form-data" method="post">
<?php 
  echo $this->session->flashdata('error');
 ?>
	<div class="control-group col-md-4">
        <label>Nama Unit</label>
      	<div class="controls">
	        <input type="text" class="form-control" placeholder="Nama Unit" name="nama" value="<?php echo $Nama_Organisasi ?>">
        </div>
    </div>
	<div class="control-group col-md-4">
        <label>Tingkatan</label>
      	<div class="controls">
	        <input type="text" class="form-control" placeholder="Tingkatan" name="tingkatan" value="<?php echo $Tingkatan ?>">
        </div>
    </div>
    <div class="control-group col-md-4">
		<label>Unit Atasan</label>
		<select class="form-control" name="atasan">
		<?php 
      		foreach ($unit->result_array() as $key ) {
      			echo "<option value='".$key['Org_Unit']."'";
            if ($Org_Unit_Atasan==$key['Org_Unit']) {
              echo "selected";
            }
      			echo ">".$key['Nama_Organisasi'];
      			echo "</option>";
      		}
      	 ?>
	  	</select>
	</div> 
    <div class="control-group col-md-12">
		<label>Tindakan Unit</label>        
            <ul style="list-style-type:none">
            	<li><input type="checkbox" name="1" value="1" <?php if($Pelayanan){echo "checked=''";} ?>> Pelayanan</li>
            	<li><input type="checkbox" name="2" value="1" <?php if($Pelatihan){echo "checked=''";} ?>> Pelatihan</li>
            	<li><input type="checkbox" name="3" value="1" <?php if($Pendaftaran_Anggota){echo "checked=''";} ?>> Pendaftaran Anggota</li>
            	<li><input type="checkbox" name="4" value="1" <?php if($Konsultasi){echo "checked=''";} ?>> Konsultasi</li>
            	<li><input type="checkbox" name="5" value="1" <?php if($Penawaran){echo "checked=''";} ?>> Penawaran</li>
            	<li><input type="checkbox" name="6" value="1" <?php if($Permintaan){echo "checked=''";} ?>> Permintaan</li>
            	<li><input type="checkbox" name="7" value="1" <?php if($Memberi_Informasi){echo "checked=''";} ?>> Memberi Informasi</li>
            	<li><input type="checkbox" name="8" value="1" <?php if($Meminta_Informasi){echo "checked=''";} ?>> Meminta Informasi</li>
            	<li><input type="checkbox" name="9" value="1" <?php if($Berbagi_Informasi){echo "checked=''";} ?>> Berbagi Informasi</li>
            	<li><input type="checkbox" name="10" value="1" <?php if($Data_Spesifik_Anggota){echo "checked=''";} ?>> Data Spesifik Anggota</li>
            </ul>
    </div>
	<div class="control-group col-md-12" >
	  	<label></label>
	  	<div class="controls" style="float:right">
	    	<button type="reset" class="btn btn-danger">
          <i class="glyphicon glyphicon-repeat">
	      		Reset
          </i>
	    	</button>
	    	<button type="submit" class="btn btn-primary">
          <i class="glyphicon glyphicon-send">
            Kirim
          </i>
	    	</button>
	  	</div>
	</div>
</form>