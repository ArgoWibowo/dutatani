<h1>Daftar Admin</h1>
<hr>
<?php 
echo $this->session->flashdata('error1');
 ?>
<a class="btn btn-success btn_add" style="float:right; margin-bottom: 20px;" href="<?php echo base_url() ?>list_admin/tambah_admin"><b>Tambah Admin</b></a>
<table class="table ">
	<thead>
		<tr>
			<td>NIK</td>
			<td>Nama</td>
			<td>Alamat</td>
			<td>Nomor Telpon</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i=1;
			foreach ($admin->result_array() as $key ) {
		 ?>
		<tr <?php if ($i %2 === 0) {
			echo 'style="background-color:#a3d4e2"';
		} ?>>
			<td><?php echo $key['NIK']; ?></td>
			<td><?php echo $key['Nama_Karyawan']; ?></td>
			<td><?php echo $key['Alamat_Rumah']; ?></td>
			<td><?php echo $key['No_Telpon']; ?></td>
			<td>
				<a href="<?php echo base_url(); ?>list_admin/editAdmin/<?php echo $key['NIK']; ?>" class="btn btn-info">Edit</a>
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $key['NIK']; ?>">
  					Hapus
				</button>
				<div class="modal fade" id="<?php echo $key['NIK']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  	<div class="modal-dialog" role="document">
				    	<div class="modal-content">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        	<h4 class="modal-title" id="myModalLabel">Modal title</h4>
					      	</div>
					      	<div class="modal-body">
					        	Apa anda yakin menghapus <b><?php echo $key['Nama_Karyawan']; ?></b> dari admin?
						    </div>
						    <div class="modal-footer">
					        	<a href="<?php echo base_url(); ?>list_admin/hapusAdmin/<?php echo $key['NIK']; ?>" class="btn btn-danger">Hapus</a>
					      	</div>
				    	</div>
				  	</div>
				</div>

				
			</td>
		</tr>
		 <?php
		 $i++; 
			}
		  ?>
	</tbody>
</table>
