<h1>
	<?php echo $Kat ?>
</h1>
<hr>
<table class="table ">
	<thead>
		<tr>
			<td>Nama</td>
			<td>Username</td>
			<td>Alamat</td>
			<td>Nomor Telpon</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i=1;
			foreach ($data->result_array() as $key ) {
			echo "<tr";
			if ($i %2 === 0) 
			{
				echo 'style="background-color:#a3d4e2"';
			}
			echo ">";
			$i++;
			?>
			<td><?php echo $key['nama']; ?></td>
			<td><?php echo $key['ID_User']; ?></td>
			<td>
				<?php echo "Jalan ".$key['alamat'].'<br> Provinsi '.$key['provinsi']; ?>
				<?php echo "<br>Kecamatan ".$key['kecamatan']; ?>
				<?php echo "<br>Kelurahan/Desa ".$key['keluran_desa']; ?>
			</td>
			<td><?php echo $key['nomor_telpon']; ?></td>
			<td>
			<?php if ($key['Tingkat_Priv']==1): ?>
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $key['ID_User']; ?>">
					<i class="glyphicon glyphicon-ban-circle"></i>
						Non-aktif
				</button>
				<div class="modal fade" id="<?php echo $key['ID_User']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  	<div class="modal-dialog" role="document">
				    	<div class="modal-content">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        	<h4 class="modal-title" id="myModalLabel">Modal title</h4>
					      	</div>
					      	<div class="modal-body">
					        	Apa anda yakin untuk menonaktifkan akun <b><?php echo $key['nama']; ?></b> dari <?php echo $Kat; ?>?
						    </div>
						    <div class="modal-footer">
						    	<a href="<?php echo base_url(); ?>admin/hapus_user/<?php echo $key['ID_User']; ?>" class="btn btn-danger">
						    		Non-aktif
						    	</a>
					      	</div>
				    	</div>
				  	</div>
				</div>
			<?php endif ?>
			<?php if ($key['Tingkat_Priv']==0): ?>
				<a href="<?php echo base_url(); ?>admin/aktif_user/<?php echo $key['ID_User']; ?>/" class="btn btn-info">
					<i class="glyphicon glyphicon-check"></i>
					Aktif
				</a>
			<?php endif ?>
			</td>

			<?php
			echo "</tr>";
			}
		  ?>
	</tbody>
</table>
<hr>
