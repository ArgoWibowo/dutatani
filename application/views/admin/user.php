<div class="container-fluid" >	
	<?php echo $this->session->flashdata('error1');  ?>

	<a class="col-md-3 " href="<?php echo base_url(); ?>admin/user/Fas" >
		<div class="thumbnail" style="background-color: #5cb85c; color: white;">
			<h1> Fasilitator <br> </h1>
		</div>
	</a>
	<a  class="col-md-3 " href="<?php echo base_url(); ?>admin/user/Pet">
		<div class="thumbnail" style="background-color: #5cb85c; color: white;">
			<h1> Petani <br> </h1>
		</div>
	</a>
	<a  class="col-md-3 " href="<?php echo base_url(); ?>admin/user/sup">
		<div class="thumbnail" style="background-color: #5cb85c; color: white;">
			<h1> Supplier <br> </h1>
		</div>
	</a>
	<a class="col-md-3 " href="<?php echo base_url(); ?>admin/user/Lain">
		<div class="thumbnail" style="background-color: #5cb85c; color: white;">
			<h1> User login <br> </h1>
		</div>
	</a>
</div>
<hr>
<form class="form-horizontal" method="post" action="<?php echo base_url() ?>admin/tambah_kategori">
	<fieldset>
    	<legend>Tambah Kategori User</legend>
    	<div class="form-group">
      		<label for="inputEmail" class="col-lg-2 control-label">Nama Kategori</label>
      		<div class="col-lg-10">
        		<input type="text" class="form-control" id="inputEmail" placeholder="Nama" name="nama">
      		</div>
    	</div>
    	<div class="form-group">
      		<label for="inputEmail" class="col-lg-2 control-label">Deskripsi</label>
      		<div class="col-lg-10">
        		<textarea class="form-control" id="inputEmail" placeholder="Deskripsi" name="Deskripsi"></textarea>
      		</div>
    	</div>
    	<div class="form-group">
    		<div class="col-md-push-11 col-md-1">
    			<button type="submit" class="btn btn-success" >
    				<i class="glyphicon glyphicon-send"></i>
	      			Kirim
	      		</button>
    		</div>
    	</div>
    </fieldset>
</form>

<hr>
<h1>
	Daftar User Kategori
</h1>
<table class="table">
	<thead>
		<tr>
			<td>Nama Kategori</td>
			<td>Jumlah</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
	<?php 
		foreach ($kategori->result_array() as $key ) {
			echo "<tr>";
				echo "<td>";
					echo $key['Nama_Kategori'];
				echo "</td>";
				echo "<td>";
					echo $key['jumlah'];
				echo "</td>";
				echo "<td>";
		
	 ?>
	 			<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#des<?php echo $key['ID_Kategori']; ?>">
              		Deskripsi
                </button>
                <div class="modal fade" id="des<?php echo $key['ID_Kategori']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel" style="color:black"><?php echo $key['Nama_Kategori']; ?></h4>
                            </div>
                            <div class="modal-body" style="color:black">
                                <?php echo $key['Deskripsi']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($key['aktif']==1 && ($key['ID_Kategori']!="SUP" && $key['ID_Kategori']!="PET" && $key['ID_Kategori']!="FAS")): ?>
	                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del<?php echo $key['ID_Kategori']; ?>">
	                	<i class="glyphicon glyphicon-ban-circle"></i>
	              		Non Aktif
	                </button>
	                <div class="modal fade" id="del<?php echo $key['ID_Kategori']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	                    <div class="modal-dialog" role="document">
	                        <div class="modal-content">
	                            <div class="modal-header">
	                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                                <h4 class="modal-title" id="myModalLabel" style="color:black"><?php echo $key['Nama_Kategori']; ?></h4>
	                            </div>
	                            <div class="modal-body" style="color:black">
	                                Apa anda yakin Menonaktifkan ini kategori user <b><?php echo $key['Nama_Kategori']; ?></b>?
	                            </div>
	                            <div class="modal-footer">
	                                <a href="<?php echo base_url()."admin/non_aktif/".$key['ID_Kategori']; ?>" class="btn btn-danger" style="float: right;">
		                            	Yakin
		                            </a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                	
                <?php endif ?>
                <?php if ($key['aktif']==0): ?>
	                
	                <a href="<?php echo base_url()."admin/aktif/".$key['ID_Kategori']; ?>" class="btn btn-primary btn-sm">
	                	<i class="glyphicon glyphicon-check"></i>
	                	Aktif
	                </a>
                <?php endif ?>

	 <?php 
					
						
					echo "</td>";
				echo "</tr>";
		}
	  ?>
		
	</tbody>
</table>