<!DOCTYPE html>
<html lang="en">
<head>

<title>Sistem Informasi Pertanian</title>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome.min.css">
<script src="<?php echo base_url(); ?>js/jquery-2.1.4.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.js"></script>




</head>
<body>
	<div class="container-fluid">
		<form class="form-horizontal col-md-push-4 col-md-4" method="post" action="<?php echo base_url(); ?>/log_admin/masuk" >
			<?php echo $this->session->flashdata('error'); ?>
			<div class="panel panel-success">
				<div class="panel-heading">
			    	<h3 class="panel-title">Login</h3>
			  	</div>
  				<div class="panel-body">
					<div class="form-group">
		      			<label for="inputEmail" class="col-lg-2 control-label">Username</label>
		      			<div class="col-lg-10">
		        			<input type="text" name="username" class="form-control" id="inputEmail" placeholder="Username">
		      			</div>
		    		</div>
		    		<div class="form-group">
		      			<label for="inputEmail" class="col-lg-2 control-label">Password</label>
		      			<div class="col-lg-10">
		        			<input type="password" name="password" class="form-control" id="inputEmail" placeholder="Password">
		      			</div>
		    		</div>
  				</div>
  				<div class="panel-footer">
		    		<input type="reset" class="btn btn-danger">
		    		<input type="submit" class="btn btn-info">
  				</div>
			</div>

		</form>
	</div>
</body>
</html>            