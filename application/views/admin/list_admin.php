<h1>Daftar Admin</h1>
<hr>
<?php 
echo $this->session->flashdata('error1');
 ?>
<a class="btn btn-success btn_add" style="float:right; margin-bottom: 20px;" href="<?php echo base_url() ?>list_admin/tambah_admin">
		<b>
			Tambah Admin
		</b>
</a>
<table class="table ">
	<thead>
		<tr>
			<td>NIK</td>
			<td>Nama</td>
			<td>Alamat</td>
			<td>Nomor Telpon</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i=1;
			foreach ($admin->result_array() as $key ) {
		 ?>
		<tr <?php if ($i %2 === 0) {
			echo 'style="background-color:#a3d4e2"';
		} ?>>
			<td><?php echo $key['NIK']; ?></td>
			<td><?php echo $key['Nama_Karyawan']; ?></td>
			<td><?php echo $key['Alamat_Rumah']; ?></td>
			<td><?php echo $key['No_Telpon']; ?></td>
			<td>
				<a href="<?php echo base_url(); ?>list_admin/editAdmin/<?php echo $key['NIK']; ?>" class="btn btn-info">
				<i class="glyphicon glyphicon-edit"></i>
				Edit
				</a>
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $key['NIK']; ?>">
					<i class="glyphicon glyphicon-remove"></i>
  					Hapus
				</button>
				<div class="modal fade" id="<?php echo $key['NIK']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  	<div class="modal-dialog" role="document">
				    	<div class="modal-content">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        	<h4 class="modal-title" id="myModalLabel">Modal title</h4>
					      	</div>
					      	<div class="modal-body">
					        	Apa anda yakin menghapus <b><?php echo $key['Nama_Karyawan']; ?></b> dari admin?
						    </div>
						    <div class="modal-footer">
					        	<a href="<?php echo base_url(); ?>list_admin/hapusAdmin/<?php echo $key['NIK']; ?>" class="btn btn-danger">
					        	<i class="glyphicon glyphicon-remove"></i>
					        	Hapus
					        	</a>
					      	</div>
				    	</div>
				  	</div>
				</div>

				
			</td>
		</tr>
		 <?php
		 $i++; 
			}
		  ?>
	</tbody>
</table>

<h1>Daftar Unit</h1>
<hr>
<a class="btn btn-success btn_add" style="float:right; margin-bottom: 20px;" href="<?php echo base_url() ?>list_admin/tambah_unit">
	<b>
		Tambah Unit
	</b>
</a>
<?php echo "<br><br><br>".$this->session->flashdata('error'); ?>
<table class="table">
	<thead>
		<tr>
			<td>Nama Unit</td>
			<td>Unit Atasan</td>
			<td>Tindakan Unit</td>
			<td>Aksi</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$i=1;
			foreach ($org->result_array() as $key ) {
		 ?>
		<tr <?php if ($i % 2==0) {
			echo 'style="background:#a3d4e2;"';
		} ?>>
			<td>
				<?php echo $key['bawahan']; ?>
			</td>
			<td><?php echo $key['atasan']; ?></td>
			<td>
				<ol>
				<?php 
					if ($key['Pelayanan']) {
						echo "<li>Pelayanan</li>";
					}
					if ($key['Pendaftaran_Anggota']) {
						echo "<li>Pendaftaran Anggota</li>";
					}
					if ($key['Pelatihan']) {
						echo "<li>Pelatihan</li>";
					}
					if ($key['Konsultasi']) {
						echo "<li>Konsultasi</li>";
					}
					if ($key['Penawaran']) {
						echo "<li>Penawaran</li>";
					}
					if ($key['Permintaan']) {
						echo "<li>Permintaan</li>";
					}
					if ($key['Memberi_Informasi']) {
						echo "<li>Memberi Informasi</li>";
					}
					if ($key['Meminta_Informasi']) {
						echo "<li>Meminta Informasi</li>";
					}
					if ($key['Berbagi_Informasi']) {
						echo "<li>Berbagi Informasi</li>";
					}
					if ($key['Data_Spesifik_Anggota']) {
						echo "<li>Data Spesifik Anggota</li>";
					}

				 ?>

				</ol>
			</td>
			<td>
				<a href="<?php echo base_url() ?>list_admin/editUnit/<?php echo $key['Org_Unit'] ?>" class="btn btn-info">
					<i class="glyphicon glyphicon-edit"></i>
					Ubah
				</a>
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#<?php echo $key['Org_Unit']; ?>">
					<i class="glyphicon glyphicon-remove"></i>
  					Hapus
				</button>
				<div class="modal fade" id="<?php echo $key['Org_Unit']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  	<div class="modal-dialog" role="document">
				    	<div class="modal-content">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        	<h4 class="modal-title" id="myModalLabel">Modal title</h4>
					      	</div>
					      	<div class="modal-body">
					        	Apa anda yakin menghapus <b><?php echo $key['bawahan']; ?></b>?
						    </div>
						    <div class="modal-footer">
					        	<a href="<?php echo base_url(); ?>list_admin/hapusUnit/<?php echo $key['Org_Unit']; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>Hapus</a>
					      	</div>
				    	</div>
				  	</div>
				</div>
			</td>
		</tr>
		 <?php
		 $i++; 
			}
		  ?>
	</tbody>
</table>

