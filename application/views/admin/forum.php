
<a href="<?php echo base_url() ?>/forum" class="btn btn-info" style="float: right;">Lihat Diskusi</a>
<hr>
<div class="row" >
	<div class="col-md-6">
		<h1>Diskusi</h1>
		<table class="table ">
			<thead>
				<tr>
					<td>Tahun/Bulan</td>
					<td>Jumlah</td>
				</tr>
			</thead>
			<tbody>
			<?php $tahun = 0; foreach ($diskusi->result_array() as $key): ?>
				
				<?php if ($tahun!=$key['tahun']): ?>
					<tr>
						<td><b><?php echo $key['tahun']; $tahun=$key['tahun']; ?> : </b></td>
						<td></td>
					</tr>
				<?php endif ?>


				<?php if ($tahun==$key['tahun']): ?>
					<tr>
						<td>
							<?php
								switch ($key['bulan']) {
									case '1':
										$bulan='Januari';
										break;
									case '2':
										$bulan='Febuari';
										break;
									case '3':
										$bulan='Maret';
										break;
									case '4':
										$bulan='April';
										break;
									case '5':
										$bulan='Mey';
										break;
									case '6':
										$bulan='Juni';
										break;
									case '7':
										$bulan='Juli';
										break;
									case '8':
										$bulan='Agustus';
										break;
									case '9':
										$bulan='September';
										break;
									case '10':
										$bulan='Oktober';
										break;
									case '11':
										$bulan='November';
										break;
									case '12':
										$bulan='Desember';
										break;
									
								}
								echo "$bulan";
							 ?>	
						</td>
						<td><?php echo $key['id']; ?> Diskusi</td>
					</tr>
				<?php endif ?>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>



	<div class="col-md-6">
		<h1>Komentar</h1>
		<table class="table ">
			<thead>
				<tr>
					<td>Tahun/Bulan</td>
					<td>Jumlah</td>
				</tr>
			</thead>
			<tbody>
			<?php $tahun = 0; foreach ($komen->result_array() as $key): ?>
				
				<?php if ($tahun!=$key['tahun']): ?>
					<tr>
						<td><b><?php echo $key['tahun']; $tahun=$key['tahun']; ?>:</b></td>
						<td></td>
					</tr>
				<?php endif ?>


				<?php if ($tahun==$key['tahun']): ?>
					<tr>
						<td>
							<?php
								switch ($key['bulan']) {
									case '1':
										$bulan='Januari';
										break;
									case '2':
										$bulan='Febuari';
										break;
									case '3':
										$bulan='Maret';
										break;
									case '4':
										$bulan='April';
										break;
									case '5':
										$bulan='Mey';
										break;
									case '6':
										$bulan='Juni';
										break;
									case '7':
										$bulan='Juli';
										break;
									case '8':
										$bulan='Agustus';
										break;
									case '9':
										$bulan='September';
										break;
									case '10':
										$bulan='Oktober';
										break;
									case '11':
										$bulan='November';
										break;
									case '12':
										$bulan='Desember';
										break;
									
								}
								echo "$bulan";
							 ?>	
						</td>
						<td><?php echo $key['id']; ?> Diskusi</td>
					</tr>
				<?php endif ?>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>