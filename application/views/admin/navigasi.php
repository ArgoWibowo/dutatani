<div class="container-fluid " id="navbar-brand">
	 <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">DutaTani</a>
    </div>
  	<div class="navbar-collapse" >
        <?php 
        	if ($this->session->has_userdata('NIK')) {
				echo $this->load->view("admin/profile",'',true);
			}
         ?>
    </div>
</div>

<nav class="navbar navbar-inverse sidebar" role="navigation" id="sidenavbar">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li <?php if ($nav=="user") {
					echo 'class="active"';
				} ?>><a href="<?php echo base_url() ?>admin">Data User<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li>
				<li <?php if ($nav=="pesan") {
					echo 'class="active"';
				} ?>><a href="<?php echo base_url() ?>admin_pesan">Pesan<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span></a></li>
				<li <?php if ($nav=="berita") {
					echo 'class="active"';
				} ?>><a href="<?php echo base_url() ?>admin_berita">Berita<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-header"></span></a></li>
				<?php 
					if ($this->session->userdata('Pendaftaran_Anggota')) {
						# code...
					
				 ?>
				 <li <?php if ($nav=="list") {
					echo 'class="active"';
				} ?>><a href="<?php echo base_url(); ?>list_admin">List Admin<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>

				<?php } ?>
				<li <?php if ($nav=="tentang") {
					echo 'class="active"';
				} ?>><a href="<?php echo base_url(); ?>about_admin">Tentang Kami<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-info-sign"></span></a></li>
				<li <?php if ($nav=="forum") {
					echo 'class="active"';
				} ?>><a href="<?php echo base_url(); ?>admin_forum">Forum<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-question-sign"></span></a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Info Pertanian <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-tree-conifer"></span></a>
					<ul class="dropdown-menu forAnimate" role="menu">
						<li><a href="#">Tanaman</a></li>
						<li class="divider"></li>
						<li><a href="#">Lahan</a></li>
						
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistem Pertanian <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-tree-conifer"></span></a>
					<ul class="dropdown-menu forAnimate" role="menu">
						<li><a href="#">Sistem 1</a></li>
						<li class="divider"></li>
						<li><a href="#">Sistem 2</a></li>
						<li class="divider"></li>
						<li><a href="#">Sistem 3</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>