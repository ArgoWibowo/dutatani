<div class="container-fluid">
	<div class="col-md-8">
		<div class="row" style="border:solid; border-color: rgba(193, 193, 193, 0.2); border-radius: 5px;">
			<?php 
				foreach ($data->result_array() as $key) {
					$isi=$key['isi'];
					$judul=$key['judul'];
					$tanggal=date_create($key['tanggal']);

					$tanggal=date_format($tanggal,"d-M-Y");
				}
			 ?>

			 <div style="text-align: center">
			 	<h3><?php echo $judul ?></h3>
			 	<?php echo $tanggal; ?>
			 </div>
			 <hr>
			 <?php echo $isi ?>
		</div>
	</div>
	<div class="col-md-4" >
		<div class="row" style="border:solid; border-color: rgba(193, 193, 193, 0.2); border-radius: 5px;">
			<h4>Berita</h4>
			<hr>
			<?php foreach ($main->result_array() as $key): ?>
			<div class="row" style="border:solid; border-color: transparent; border-top-color: #eeeeee; border-bottom-color: #eeeeee;">
				<a href="<?php echo $link."/".md5($key['id']); ?>"><?php echo $key['judul']; ?></a><br>
				<span class="glyphicon glyphicon-calendar"></span>
				<?php 
					$tanggal= $key['tanggal'];
					$tanggal= date_create($tanggal);
					echo date_format($tanggal,"d-M-Y");
				?>
			</div>
			<?php endforeach ?>
		</div>
	</div>
</div>