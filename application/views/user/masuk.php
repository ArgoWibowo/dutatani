<div class="col-md-push-3 col-md-6">
	<form method="post" action="<?php echo base_url() ?>login/masuk" id="loginform" class="form-horizontal" role="form">
    	<div class="panel panel-info">
      		<div class=" panel-heading">
        		
        		<h4 class="modal-title" id="myModalLabel">Masuk</h4>
      		</div>
      				<?php echo $this->session->flashdata('error1') ?>
        			<div style="padding-top:30px" class="panel-body" >

		                
		                    <div style="margin-bottom: 25px" class="input-group">
		                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		                        <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="ID Pengguna">                                        
		                    </div>
		                            
		                    <div style="margin-bottom: 25px" class="input-group">
		                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Kata Kunci">
		                    </div>

		                    <div class="form-group">
		                        <div class="col-md-12 control">
		                        	<div style="font-size: 90%; position: relative; top:-10px ">
			                        		Belum punya akun? <a href="<?php echo base_url(); ?>login/daftar">Daftar disini</a>
		                        	</div>
		                        </div>
		                    </div>    
		                    
		            </div>  
      		<div class="modal-footer">
        		<button type="Submit" class="btn btn-primary">Masuk</button>
      		</div>
    	</div>
	</form>
</div>
<div class="col-md-6">
</div>