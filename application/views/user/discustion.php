<!DOCTYPE html>
<html lang="en">
<head>

<title>Sistem Informasi Pertanian</title>
<?php echo $css; ?>

</head>
<body >
<!-- navbar -->

<!-- end navbar -->
<!-- sidebar -->
<?php echo $nav; ?>
<div class="main">
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<div class="container">
    <div class="col-md-12">
        <?php 
            if ($this->session->has_userdata('ID_User')) {            
         ?>
        <ul class="nav nav-pills">
            <li <?php if ($url=='all'): ?>
                <?php echo 'class="active"' ?>
            <?php endif ?>><a href="<?php echo base_url(); ?>forum/">Semua Diskusi</a></li>
            <li <?php if ($url=='my'): ?>
                <?php echo 'class="active"' ?>
            <?php endif ?>><a href="<?php echo base_url(); ?>forum/diskusiku/">Diskusiku</a></li>
            
        </ul>
        <?php } ?>
        <div class="panel panel-white post panel-shadow">
            <div class="post-footer">
                    <?php
                    echo $tambah;
                    $this->load->model('diskusi');
                    foreach ($db->result_array() as $d) {
                        //print_r($d);
                        $originalDate = $d['tanggal']; 
                        $newDate = date("d-M-Y", strtotime($originalDate));
                     ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <?php 
                                        if ($d['ID_user']==$this->session->userdata('ID_User') || 
                                            $this->session->userdata('Kelola_forum')=='1') {
                                    ?>
                                    <span class="pull-left"><?php echo $newDate ?></span>
                                    <button type="button" class="btn btn-danger btn-sm pull-right" data-toggle="modal" data-target="#<?php echo $d['ID_topik']; ?>">
                                        <i class="glyphicon glyphicon-remove"></i> 
                                        <span>Hapus</span> <!--Tombol Hapus Diskusi-->
                                    </button>
                                    <button type="button" class="btn btn-warning btn-sm pull-right" data-toggle="modal" data-target="#<?php echo $d['ID_topik']; ?>-edit" style="margin-right: 5px">
                                        <i class="glyphicon glyphicon-edit"></i>
                                        <span>Ubah</span> <!--Tombol Ubah Diskusi-->
                                    </button>
                                    <div class="clearfix"></div>
                                    <div class="modal fade" id="<?php echo $d['ID_topik']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    
                                                </div>
                                                <div class="modal-body" style="color:black">
                                                    Diskusi dan semua isi komentar diskusi akan di hapus.<br>
                                                    Apa Anda Yakin ingin menghapus ini?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                                                    <a class="btn btn-danger" href="<?php echo base_url().'forum/hapusDiskusi/'.$d['ID_topik']; ?>">Hapus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="<?php echo $d['ID_topik']; ?>-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <form method="post" action="<?php echo base_url(); ?>forum/editDiskusi" class="form-horizontal edit-form">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <div class="modal-body" style="color:black">
                                                            <label>Judul</label>
                                                            <input type="hidden" name="id_kategori_topik" value="<?php echo $d['ID_kategori_topik']; ?>">
                                                            <input type="hidden" name="id_topik" value="<?php echo $d['ID_topik']; ?>">
                                                            <input name="judul" type="text" class="form-control" placeholder="Judul" id="judul" value="<?php echo $d['Judul_Topik']; ?>">
                                                            <br>
                                                            <label>Isi Diskusi</label>
                                                            <textarea name="deskripsi" class="form-control summernote" rows="3" id="textArea"><?php echo $d['isi_diskusi']; ?></textarea>
                                                        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                                                        <button type="submit" class="btn btn-warning" id="editDiskusiButton">Ubah</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <?php }
                                        else {
                                    ?>
                                    <span><?php echo $newDate ?></span>
                                    <?php
                                        }
                                        $enc=$d['ID_topik'];
                                    ?>

                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-3" style="text-align:center">
                                    <img class="avatar img-responsive" width="128px" height="128px" src="<?php echo base_url()."Foto/".$d['Foto']; ?>">
                                    <h4 class="user"><?php  echo $d['nama']; ?></h4>
                                </div>
                                <div class="col-md-10 col-lg-10 col-sm-9 col-xs-9">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <b>
                                                <a href="<?php echo base_url()."forum/komentar/".$enc; ?>"><?php echo $d['Judul_Topik']; ?></a>
                                            </b>
                                        </div>
                                        <div class="panel-body">
                                            <?php echo $d['isi_diskusi']; ?>
                                        </div>
                                    </div>
                                    
                                    <p>
                                        <a href="<?php echo base_url()."forum/komentar/".$enc; ?>">
                                            <span class="btn btn-info img-circle">
                                                <b>
                                                    <?php 
                                                        echo $this->diskusi->count_komen($d['ID_topik']);
                                                     ?>
                                                     Komentar
                                                </b>
                                            </span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php 
                        }
                        echo $halaman;
                     ?>
                
            </div>
        </div>
    </div>
</div>


<!-- end content -->
</div>
<!-- end sidebar -->


</body>
</html>

<script type="text/javascript">
    $(document).ready(function(){

        $('.summernote').summernote(
            {
                toolbar:    [
                            ['font', ['bold', 'italic', 'underline', 'clear']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', '', 'hr']]
                            ],
                disableDragAndDrop: true,
            });
    });
</script>            