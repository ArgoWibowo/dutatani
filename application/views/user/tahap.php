<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-pills nav-justified thumbnail">
            <?php $step=""; ?>
            <li <?php if($tahap==1) {echo 'class="active"'; $step="Step 1: Daftar"; }elseif($tahap>1){}else{echo 'class="disabled"';}?> >
                <a href="<?php echo base_url()."login/daftar/1" ?>">
                    <h4 class="list-group-item-heading">Step 1</h4>
                    <p class="list-group-item-text">Daftar Sebagai</p>
                </a>
            </li>
            <li <?php if($tahap==2) {echo 'class="active"'; $step="Step 2: Informasi Akun";}elseif($tahap>2){}else{echo 'class="disabled"';}?> >
                <a href="<?php echo base_url()."login/daftar/2" ?>">
                    <h4 class="list-group-item-heading">Step 2</h4>
                    <p class="list-group-item-text">Informasi Akun</p>
                </a>
            </li>
            <li <?php if($tahap==3) {echo 'class="active"'; $step="Step 3: Informasi Pribadi";}else{echo 'class="disabled"';}?> >
                <a href="<?php echo base_url()."login/daftar/3" ?>">
                    <h4 class="list-group-item-heading">Step 3</h4>
                    <p class="list-group-item-text">Informasi Pribadi</p>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
      	<div class="panel-title">
        	<h4 ><?php echo $step ?></h4>
      	</div>
    </div>
    <div class="panel-body">
      <?php echo $form; ?>
    </div>
</div>