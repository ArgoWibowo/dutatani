<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>dist/summernote.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
<script src="<?php echo base_url(); ?>js/jquery-2.1.4.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/summernote.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap-datetimepicker.min.js"></script>
<style>
	.ui-autocomplete-category {
		font-weight: bold;
		padding: .2em .4em;
		margin: .8em 0 .2em;
		line-height: 1.5;
	}

	.custom-a {
		background-color: white;
		color: black;
		text-decoration: none;
		border-color: white;
	}

	.custom-a:hover {
		background-color: white;
		color: black;
		text-decoration: none;
		border-color: white;
	}

	.dropdown-wrapper {
		z-index: 9999;
		border-radius: 4px;
	}

	.margin-li {
		padding-left: 15px !important;
		padding-bottom: 15px;
	}

	#load-more{
		cursor : pointer
	}

</style>
<script type="text/javascript">
	
	    function htmlbodyHeightUpdate(){
		var height3 = $( window ).height()
		var height1 = $('.nav').height()+50
		height2 = $('.main').height()
		if(height2 > height3){
			$('html').height(Math.max(height1,height3,height2)+10);
			$('body').height(Math.max(height1,height3,height2)+10);
		}
		else
		{
			$('html').height(Math.max(height1,height3,height2));
			$('body').height(Math.max(height1,height3,height2));
		}
		
	}
	$(document).ready(function () {
		$('.kirim').hide();
		htmlbodyHeightUpdate()
		$( window ).resize(function() {
			htmlbodyHeightUpdate()
		});
		$( window ).scroll(function() {
			height2 = $('.main').height()
  			htmlbodyHeightUpdate()
		});

		$.widget( "custom.catcomplete", $.ui.autocomplete, {
	      	_create: function() {
		      	this._super();
		        this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
	      	},
	      	_renderMenu: function( ul, items ) {
	      		ul.addClass("dropdown-wrapper");
		        var that = this,
		        currentCategory = "";
		        $.each( items, function( index, item ) {
			        var li;
			        if ( item.category != currentCategory ) {
			           	ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			        	currentCategory = item.category;
			        }
		          	li = that._renderItemData( ul, item );
			        if ( item.category ) {
			        	li.attr( "aria-label", item.category + " : " + item.title );
			        }
		        });
	    	},
	    	_renderItem: function( ul, item ) {
			    return $( "<li>" )
			    	.addClass("margin-li")
			        .append( $( "<a>" ).text( item.title ).attr("href", item.link).addClass("custom-a") )
			        .appendTo( ul );
			},

	    });

		$( "#search-berita-diskusi" ).catcomplete({
          	source: "<?php echo site_url('autocomplete/autocomplete_berita_forum');?>"
        });

        $( "#load-more").click( function() {
        	var curr_page = $("#current-page").val();
        	var topic_id = $("#topic-id").val();

        	$.ajax({
        		url 	: "<?php echo base_url(); ?>forum/ajaxKomen",
        		type 	: "POST",
        		data 	: {
        			page 		: curr_page,
        			topic_id	: topic_id
        		},
        		success : function(data) {
        			result = $.parseJSON(data);
        			if ( result.html ) {
        				$(".comment-section").append(result.html);
        			}

        			if ( result.expandable == 'true' ) {
        				curr_page += 1;
        				$("#curr_page").val( curr_page );
        			} else {
        				$("#load-more-section").hide();
        			}
        		}
        	});
        });
	});

	var stop = false;
$(document).ready(function() {
	$('#imageUpload').change(function(){			
			readImgUrlAndPreview(this);
			function readImgUrlAndPreview(input){
				 if (input.files && input.files[0]) {
			            var reader = new FileReader();
			            reader.onload = function (e) {			            	
			                	$('#imagePreview').attr('src', e.target.result);
			                	$('.kirim').show();
			                	$('#LihatProfile').addClass('open');

							}
			          };
			          reader.readAsDataURL(input.files[0]);
			     }	
		});
     var tabCarousel = setInterval(function() {
        var tabs = $('#tabs-home .nav.tabs > li'),
            active = tabs.filter('.active'),
            next = active.next('li'),
            toClick = next.length ? next.find('a') : tabs.eq(0).find('a');

        toClick.trigger('click');
    }, 5000);

    
});
</script>

