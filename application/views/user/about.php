<h1>
	Tentang kami
</h1>
<div class="container-fluid">
	<div class="row">
		<?php 
		$isi="";
		foreach ($tentang->result_array() as $key): 

			$isi=$key['tentang'];
			?>


			
		<?php endforeach ?>
		<?php echo $isi; ?>
		
	</div>
</div>
<hr>
 <div class="jumbotron" id="jumbo1">
	<div class="row">
		<?php echo $this->session->flashdata('error1'); ?>

		<form class="col-md-4" method="post" action="<?php echo base_url(); ?>/about/kirim_pesan">
			<h4 style="color: white;">Kritik/Saran</h4>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" class="form-control" name="Name"
							autocomplete="off" id="nama" placeholder="Nama">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="email" class="form-control" name="email"
							autocomplete="off" id="email" placeholder="E-mail">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control textarea" rows="3" name="pesan"
							id="Message" placeholder="Pesan" cols=""></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn main-btn pull-right">Kirim</button>
				</div>
			</div>
		</form>
		<div class="col-md-4"></div>
		<div class="col-md-4" style="color: white">
			<br>
			UNIVERSITAS KRISTEN DUTA WACANA
			<br>
			<br>
			Jl. Dr. Wahidin Sudiro Husodo No. 5 – 25<br>
			Yogyakarta 55224<br>
			Telp. 0274 – 563929 Fax. 0274 – 513235<br>
			Email: humas@staff.ukdw.ac.id<br>
		</div>
	</div>
</div>