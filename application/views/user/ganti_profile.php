
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format:'YYYY-MM-DD'
		});
	});


</script>
<div class="row">

<form action="<?php echo base_url().'/ganti/setprofile/' ?>" class="form form-vertical" enctype="multipart/form-data" method="post">


    <div class="control-group col-md-4">
        <label>Nama</label>
        <div class="controls">
          <input type="text" class="form-control" placeholder="nama" name="nama" value="<?php echo $this->session->userdata('nama'); ?>">
        </div>
    </div>
    <div class="control-group col-md-4">
    <label>Tanggal Lahir</label>
        <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control input-group-addon" name="tgl" value="<?php echo $this->session->userdata('tanggal_lahir'); ?>" />
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>  
    </div>
    <div class="control-group col-md-4">
        <label>Password</label><small style="color:red">*bila tidak diisi, password tidak akan berubah</small>
        <div class="controls">
          <input type="Password" class="form-control" placeholder="Password" name="password" value="">
        </div>
    </div>
	<div class="control-group col-md-12">
		<label>Alamat</label>
		<div class="controls">
            <input type="text" class="form-control" placeholder="Alamat	" name="alamat" 
            value="<?php echo $this->session->userdata('alamat'); ?>">
		</div>
	</div>
	<div class="control-group form-group">
              <label class="col-lg-2 control-label">Jenis Kelamin</label>
            <div class="col-lg-10">
              <div class="radio">
                    <label>
                      <input type="radio" name="jekel" id="optionsRadios1" value="1"  
                      <?php if ($this->session->userdata('jenis_kelamin')==1) {
                      	echo "checked=''";
                      } ?>
                      >
                      Laki-laki
                    </label>
              </div>
              <div class="radio">
                    <label>
                      <input type="radio" name="jekel" id="optionsRadios2" value="2" 
                      <?php if ($this->session->userdata('jenis_kelamin')==2) {
                      	echo "checked=''";
                      } ?>
                      >
                      Perempuan
                    </label>
              </div>
            </div>
          </div>
	<div class="control-group col-md-6">
	  	<label>Nomor Telpon</label>
	  	<div class="controls">
	    <input type="text" class="form-control" placeholder="Nomor Telpon" name="NomorTelpon"
	    value="<?php echo $this->session->userdata('nomor_telpon'); ?>">
	    
	  </div>
    
	</div>
	<div class="control-group col-md-6">
		<label>Email</label>
      	<div class="controls">
    		<input type="text" class="form-control" placeholder="Email" name="Email" value="<?php echo $this->session->userdata('Email'); ?>">
                
        </div>
	</div>


	<?php echo $provinsi ?>
  <hr>
  <?php echo $kategori; ?>
    </div>
		<div class="control-group col-md-12">
	  	<label></label>
	  	<div class="controls">
	    	<button type="submit" class="btn btn-primary">
	      		Kirim
	    	</button>
	  	</div>
	</div> 
  </form>
</div>
<!-- Button trigger modal -->
