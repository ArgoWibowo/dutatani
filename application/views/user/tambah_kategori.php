<div class="control-group form-group">
              <label class="col-lg-2 control-label">Kategori</label>
            <div class="col-lg-10">
              <!-- <div class="radio">
                    <label>
                      <input type="checkbox" name="pet" id="katsup" value="1">
                      <script type="text/javascript">
                        $("#katsup").click( function(){
                           if( $(this).is(':checked') ) {
                            $('#sup').show();
                           }else{
                            $('#sup').hide();
                           }
                        });
                      </script>
                      Suplayer
                    </label>
              </div> -->
              
              <?php foreach ($db->result_array() as $key): ?>
              	<div class="radio">

                    <label>
                    <?php 
                    	if ($key['id']==1) {
                    		$cek='checked=""';
                    	}else{
                    		$cek='';
                    	}
                     		switch ($key['ID_Kategori']) {
                     			case 'FAS':
                     				$input='<input type="checkbox" '.$cek.' name="'.$key['ID_Kategori'].'" id="katfas" value="'.$key['ID_Kategori'].'">
                    	
                      	<script type="text/javascript">
                        $("#katfas").click( function(){
                           if( $(this).is(":checked") ) {
                            $("#fas").show();
                           }else{
                            $("#fas").hide();
                           }
                        });
                      </script>';
                      				$fasilit=$fas;
                     				$fas='hidden=""';
                      				if ($cek!='') {
                      					$fas='';
                      				}
                     				break;

                     			case 'PET':
                     				$input='<input type="checkbox" '.$cek.' name="'.$key['ID_Kategori'].'" id="katpet" value="'.$key['ID_Kategori'].'">
                      	<script type="text/javascript">
                        $("#katpet").click( function(){
                           if( $(this).is(":checked") ) {
                            $("#pet").show();
                           }else{
                            $("#pet").hide();
                           }
                        });
                      </script>';	
                      				$pet='hidden=""';
                      				if ($cek!='') {
                      					$pet='';
                      				}
                     				break;

                     			case 'SUP':
                     				$input='<input type="checkbox" '.$cek.' name="'.$key['ID_Kategori'].'" id="ketsup" value="'.$key['ID_Kategori'].'">
                      	<script type="text/javascript">
                        $("#ketsup").click( function(){
                           if( $(this).is(":checked") ) {
                            $("#sup").show();
                           }else{
                            $("#sup").hide();
                           }
                        });
                      </script>';
                     				$sup='hidden=""';
                      				if ($cek!='') {
                      					$sup='';
                      				}
                     				break;
                     			
                     			default:
                     				$input='<input type="checkbox" '.$cek.' name="'.$key['ID_Kategori'].'" value="'.$key['ID_Kategori'].'">';
                     				break;
                     		}
                     	?>

                      <?php echo $input." ".$key['Nama_Kategori']; ?>
                    </label>
                    
              </div>
              <?php endforeach ?>



            </div>
          </div>
    <div class="control-group">
      <div class="row" <?php echo $pet ?>  class="kategori" id="pet">
      	<?php 
      		$pet="";
      		foreach ($petani->result_array() as $key) {
      			$pet=$key['Jumlah_Tanggungan'];
      		}
      	 ?>
        <div class="control-group col-md-6">
          <div class="controls">
            <label><b>Jumlah Tanggungan</b></label>
            <input type="text" class="form-control" placeholder="Jumlah Tanggungan" value="<?php echo $pet ?>" name="tanggungan">
          </div>
        </div>
      </div>
      <div class="row" <?php echo $sup ?> class="kategori" id="sup">
        <div class="control-group col-md-6">
          <div class="controls">
            <label><b>Jenis Supplier</b></label>
            <select class="form-control" name="Suplayer">
                <?php 
                  	echo $jenis;
                ?>
                </select>
              </div>
          </div>
        </div>
      <div class="row" <?php echo $fas ?> class="kategori" id="fas">
      	<?php 
      		$fasilit1="";
      		$fasilit2="";
	      		foreach ($fasilit->result_array() as $key) {
	      			$fasilit1=$key['Pendidikan_Terakhir'];
	      			$fasilit2=$key['Pengalaman_Kerja'];
	      		}
      	 ?>
        <div class="control-group col-md-6">
          <div class="controls">
            <label><b>Pendidikan Terakhir</b></label>
            <textarea type="text" class="form-control" placeholder="Pendidikan Terakhir" name="Terakhir"><?php echo $fasilit1 ?></textarea>
          </div>
        </div>
        <div class="controls col-md-6">
            <label><b>Pengalaman Kerja</b></label>
            <textarea type="text" class="form-control" placeholder="Jumlah Tanggungan" name="Pengalaman"><?php echo $fasilit2 ?></textarea>
          </div>
        </div>

      </div>