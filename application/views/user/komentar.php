<?php
if ($judul) {
    # code...

 foreach ($judul->result_array() as $key) {
        	
?>

	<div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title" style="height: 30px;">
                <?php 
                    $newDate = date("d-M-Y", strtotime($key['tanggal']));
                    echo $newDate;
                ?>
                <?php if ((isset($user) && $user!="" )) {?>

                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg" style="float: right;">Tambah Komentar</button>
                <?php } ?>
            </h3>
        </div>
        
        <div class="panel-body">
            <div class="col-md-2 col-lg-2 col-sm-3 col-xs-3" style="text-align:center">
                <img class="avatar img-responsive" src="<?php echo base_url().'foto/'.$key['Foto']; ?>" >
                <h4 class="user"><?php echo $key['nama']; ?></h4>
            </div>
            <div class="col-md-10 col-lg-10 col-sm-9 col-xs-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<b>
                    	<?php echo $key['Judul_Topik']; ?>
	                    </b>
					</div>
                    <div class="panel-body">
                    <?php echo $key['isi_diskusi']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }
    }else echo "<h1>Maaf Kontent sudah dihapus</h1>"; ?>
    <hr>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Tambah Komentar</h3>
                </div>
                <div class="panel-body">
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $('.summernote').summernote(
                                {
                                    toolbar:    [
                                                ['font', ['bold', 'italic', 'underline', 'clear']],
                                                ['color', ['color']],
                                                ['para', ['ul', 'ol', 'paragraph']],
                                                ['table', ['table']],
                                                ['insert', ['link', '', 'hr']]
                                                ],
                                    disableDragAndDrop: true,
                                    height:100,
                                });
                        });
                    </script>
                    <form action="<?php echo base_url().'forum/postKomen/'.$id ?>" method="post">
                        <textarea class="summernote" name="komen">
                        </textarea>
                        <button type="submit" class="btn btn-success" style="float: right;">
                            <li class="glyphicon glyphicon-send"></li>
                            Kirim
                        </button>
                    </form>
                </div>
            </div>
    </div>
</div>

    <hr>

<div class="comment-section">
    <?php 
        foreach ($komen->result_array() as $key) {

 ?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <div class="row">
                                Pengirim: <?php echo $key['nama'];?>
                            <div style="float: right;">
                                <?php 
                                    $newDate = date("d-M-Y", strtotime($key['Tanggal']));
                                    echo $newDate;
                                ?>
                            </div>
                        </div>
                            <?php

                            if ($key['ID_user']==$this->session->userdata('ID_User') ||$this->session->has_userdata('NIK')) {

                            
                        ?>
                                <p style="padding-top: 5px">
                                <button type="button" style="float: right;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#<?php echo md5($key['nomor_komentar']); ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    <span>Hapus</span>
                                </button>
                                </p>
                                <div class="modal fade" id="<?php echo md5($key['nomor_komentar']); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body" style="color:black">
                                                Apa Anda Yakin ingin menghapus ini?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                                                <a class="btn btn-danger" href="<?php echo base_url().'forum/hapusKomentar/'.md5($key['nomor_komentar']).'/'.$key['ID_topik']; ?>">Hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                             }
                         ?>
                         <br>
                    </h3>
                </div>
                <div class="panel-body">
                    <?php echo $key['Komentar']; ?>
                    
                </div>
            </div>
    <?php } ?>    
</div>


<?php if ( $expandable ) : ?> <!-- Fungsi expandable-->
    <div class="panel panel-success text-center" id="load-more-section">
        <input type="hidden" id="current-page" value=1>
        <input type="hidden" id="topic-id" value="<?php echo $id; ?>">
        <div class="panel-heading" id="load-more">Lihat Lebih Banyak</div>
    </div>
<?php endif; ?>
