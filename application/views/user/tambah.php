<script type="text/javascript">
    function checkJudul(){
        var judul=document.getElementById("judul").value;
        if (judul=="") {
            alert("mohon isi Judul Diskusi");
            return false;
        }
        else {
            return true;
        }
    }

</script>
<div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Buat diskusi baru</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-2 col-lg-2 col-sm-3 col-xs-3" style="text-align:center">
                            <img  height="128px" width="128px" class="avatar img-responsive img-rounded" src="<?php echo base_url().'Foto/'.$this->session->userdata('Foto'); ?>" >
                            <h4 class="user">
                                <?php 
                                    $this->load->library('session'); 
                                    echo $this->session->userdata('nama');
                                ?>
                            </h4>
                        </div>
                        <div class="col-md-10 col-lg-10 col-sm-9 col-xs-9">
                            <form method="post" onsubmit="return checkJudul()" action="<?php echo base_url(); ?>forum/postDiskusi" class="form-horizontal balas-komentar">
                                <input name="judul" type="text" class="form-control" placeholder="Judul Topik Diskusi" id="judul">
                                <br>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('.summernote').summernote(
                                            {
                                                toolbar:    [
                                                            ['font', ['bold', 'italic', 'underline', 'clear']],
                                                            ['color', ['color']],
                                                            ['para', ['ul', 'ol', 'paragraph']],
                                                            ['table', ['table']],
                                                            ['insert', ['link', '', 'hr']]
                                                            ],
                                                disableDragAndDrop: true,
                                            });
                                    });
                                </script>
                                <textarea name="deskripsi" class="form-control summernote" rows="3" id="textArea"></textarea>
                                <p class="container-fluid pull-right">
                                    <button type="submit"  class="btn btn-primary" >
                                        <span class="glyphicon glyphicon-send"></span> <b>Kirim</b>
                                    </button>
                                </p>
                            </form>

                        </div>
                    </div>
                </div>