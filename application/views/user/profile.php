<li>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    	<span class="glyphicon glyphicon-user"></span>
    	<?php echo $this->session->userdata('nama').'<b class="caret"></b>'; ?>
    </a>
    <ul class="dropdown-menu multi-column columns-3">
      	<div id="profile" class="col-md-3 col-sm-3 col-lg-3 col-xs-3" style="text-align:center;">
			<div class="controls">
				<form action="<?php echo base_url(); ?>ganti/upload" method="post" enctype="multipart/form-data">
			    	<input type="file" name="imageUpload" id="imageUpload" class="hide"/> 
						<img for="imageUpload" class="avatar img-circle img-rounded" height="128px" width="128px" src="<?php echo base_url().'Foto/'.$this->session->userdata('Foto'); ?>" id="imagePreview" alt="" class="img-responsive" />
					<label for="imageUpload" class="btn btn-large btn-success" >Ganti</label>
					<button type="submit" class="kirim btn btn-large btn-success" >Kirim</button>

				</form>
		  	</div>
      	</div>
      	<div class="col-md-9 col-sm-9 col-lg-9 col-xs-9" style="text-align:center;">
      		<table class="table table-striped"> 	
			  	<tbody style="text-align:left">
			    	<tr>
				      	<td>Nama</td>
				      	<td><?php echo $this->session->userdata('nama'); ?></td>
				      	<td></td>
			    	</tr>
			    	<tr>
				      	<td>No. Telephone</td>
				      	<td><?php echo $this->session->userdata('nomor_telpon'); ?></td>
				      	<td></td>
			    	</tr>
			    	<tr>
				      	<td>Username</td>
				      	<td><?php echo $this->session->userdata('ID_User'); ?></td>
				      	<td></td>
			    	</tr>
			    	<tr>
				      	<td>E-mail</td>
				      	<td><?php echo $this->session->userdata('Email'); ?></td>
				      	<td></td>
			    	</tr>
			    	<tr>
			    		<td><a href="<?php echo base_url("crud"); ?>" class="btn button" style="float: right;background-color: skyblue">Edit Menu</a></td>
			    		<td></td>
			    		<td colspan="2">
			    			<a href="<?php echo base_url(); ?>ganti" class="btn btn-warning" style="float:right">Ganti Profil</a>
			    		</td>
			    	</tr>
			  	</tbody>
			</table> 
      	</div>
    </ul>
</li>
<li>
	<?php $additional_class = $this->session->userdata('time'); ?>
	<a href="<?php echo base_url() ?>pemberitahuan" <?php echo $additional_class; ?> >
    	<span class="glyphicon glyphicon-bell"></span>
    	Pemberitahuan
    </a>
</li>
<li>
	<a href="<?php echo base_url(); ?>login/keluar"">
    	<span class="glyphicon glyphicon-log-out"></span>
    	Keluar
    </a>

</li>