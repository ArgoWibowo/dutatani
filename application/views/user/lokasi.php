<script type="text/javascript">
	$(function(){
		$("#prov").change(function(){
			var provinsi = $("#prov").val();
			$.ajax({
				url:'<?php echo base_url()."login/getKabupaten"; ?>',
				data:"Provinsi="+provinsi,
				cache: false,
				type: "post",
				success: function(url) {
		            var image = url;
		            document.getElementById("Kabupaten").innerHTML=image;
		            document.getElementById("Kecamatan").innerHTML="<option>=== Kecamatan ===</option>";
		            document.getElementById("desa").innerHTML="<option>=== Desa/Kelurahan ===</option>";
		        }
			});
		});
	});
</script>
<div class="control-group col-md-12">
  	<label>Provinsi</label>
  	<div class="controls">
    	<select id="prov" class="form-control" placeholder="Provinsi" name="Provinsi">
    		<option>=== Provinsi ===</option>
    		<?php

    			foreach ($prov->result_array() as $key) {
    				echo "<option value='".$key['Nama_Provinsi']."'";
    				if ($key['Nama_Provinsi']==$this->session->userdata('provinsi')) {
    					echo "selected=''";
    				}
    				echo ">".$key["Nama_Provinsi"];
    				echo "</option>";
    			}
    		 ?>
    	</select>
     </div>
</div>

<script type="text/javascript">
	$(function(){
		$("#Kabupaten").change(function(){
			var kabupaten = $("#Kabupaten").val();
			$.ajax({
				url:'<?php echo base_url()."login/getKecamatan"; ?>',
				data:"kabupaten="+kabupaten,
				cache: false,
				type: "post",
				success: function(url) {
		            var image = url;
		            document.getElementById("Kecamatan").innerHTML=image;
		            document.getElementById("desa").innerHTML="<option>=== Desa/Kelurahan ===</option>";
		        }
			});
		});
	});
</script>

<div id="kab" class="control-group col-md-4">
  	<label>Kabupaten/Kota</label>
  	<div class="controls">
  		<select id="Kabupaten" class="form-control" placeholder="Kabupaten" name="Kabupaten">
  			<option>=== Kabupaten/Kota ===</option>
  			<?php 
  				if ($this->session->has_userdata('kabupaten')) {
  					echo "<option selected=''>";
  					echo $this->session->userdata('kabupaten');
  					echo "</option>";
  				}
  			 ?>
		</select>
    </div>
</div>

<script type="text/javascript">
	$(function(){
		$("#Kecamatan").change(function(){
			var Kecamatan = $("#Kecamatan").val();
			$.ajax({
				url:'<?php echo base_url()."login/getDesa"; ?>',
				data:"Kecamatan="+Kecamatan,
				cache: false,
				type: "post",
				success: function(url) {
		            var image = url;
		            document.getElementById("desa").innerHTML=image;
		        }
			});
		});
	});
</script>

<div id="Kec" class="control-group col-md-4">
  	<label>Kecamatan</label>
  	<div class="controls">
  		<select id="Kecamatan" class="form-control" placeholder="Kecamatan" name="Kecamatan">
  			<option>=== Kecamatan ===</option>
			  			<?php 
				if ($this->session->has_userdata('kecamatan')) {
					echo "<option selected=''>";
  					echo $this->session->userdata('kecamatan');
  					echo "</option>";
				}
			 ?>
		</select>
    </div>
</div>
<div id="des" class="control-groupc col-md-4">
  	<label>Desa Kelurahan</label>
  	<div class="controls">
	  	<select  id="desa" class="form-control" placeholder="Desa Kelurahan" name="DesaKelurahan">
	  		<option>=== Desa/Kelurahan ===</option>
	  		<?php 
  				if ($this->session->has_userdata('keluran_desa')) {
  					echo "<option selected=''>";
  					echo $this->session->userdata('keluran_desa');
  					echo "</option>";
  				}
  			 ?>
		</select>
  	</div>
</div> 