
<?php 
  if ($this->session->userdata('tahap')!=1 && $this->session->userdata('tahap')!=2) {
          redirect('login/daftar');
          
        }
        $pin="";
          $id="";
          if ( $this->session->has_userdata('PIN')) {
              $pin=$this->session->userdata('PIN');
          }
          if ( $this->session->has_userdata('id')) {
              $id=$this->session->userdata('id');
          }
 ?>

<form class="form form-vertical" action="<?php echo base_url().'/login/input/2' ?>" method="post">
            <?php echo $this->session->flashdata('error'); ?>
            <div class="control-group">
              <label>PIN</label> <small style="color:red">*6 karakter dan harus angka</small>
              <div class="controls">
                <input name="PIN" type="text" class="form-control" placeholder="PIN" value="<?php echo $pin; ?>">
              </div>
            </div>      
            <div class="control-group">
              <label>Username</label> <small style="color:red">*6-10 karakter dan Tidak ada spasi </small>
              <div class="controls">
                <input name="username" type="text" class="form-control" placeholder="Username" value="<?php echo $id; ?>">
              </div>
            </div>
            <div class="control-group">
              <label>Password</label> <small style="color:red">*6-14 karakter dan Tidak ada spasi</small>
              <div class="controls">
                <input name="password" type="password" class="form-control" placeholder="Password">
                
              </div>
            </div>   
            <div class="control-group">
              <label>Confirm Password</label> <small style="color:red">*6-14 karakter, Tidak ada spasi, harus sama dengan "Password"</small>
              <div class="controls">
                <input type="password" name="confrim"  class="form-control" placeholder="Confirm Password">
                
              </div>
            </div>   
            
            
            <div class="control-group">
              <label></label>
              <div class="controls">
                <button type="submit" class="btn btn-primary">
                  Kirim
                </button>
              </div>
            </div>   
            
          </form>
