<script type="text/javascript">
  $(function () {
    $('#datetimepicker1').datetimepicker({
      format:'YYYY-MM-DD'
    });
  });


</script>

<form action="<?php echo base_url().'/login/input/3' ?>" class="form form-vertical" enctype="multipart/form-data" method="post">
            <div class="control-group col-md-6">
              <label>Nama</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Nama" name="nama">
                
              </div>
            </div> 
            <!-- ddi database ditambahi foto -->
              <div class="control-group col-md-6" style="text-align:center">
      <label>Foto</label>
      <div class="controls">
        <input type="file" name="imageUpload" id="imageUpload" class="hide"/> 
      <img src="" id="imagePreview" alt="" class="img-responsive" />
      <label for="imageUpload" class="btn btn-large btn-success" >Pilih Foto</label>
      </div>
  </div>
  <div class="control-group col-md-12">
    <label>Tanggal Lahir</label>
        <div class='input-group date' id='datetimepicker1'>
            <input type='text' class="form-control input-group-addon" name="tgl" />
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>  
    </div>
            <div class="control-group col-md-12">
              <label>Jenis Supplier</label>
              <div class="controls">
                <select class="form-control" name="Suplayer">
                <?php 
                  $this->load->model('user');
                  $data=$this->user->getJenisSup();
                  foreach ($data->result_array() as $key) {
                      echo "<option value='".$key['ID_Jenis_Sup']."'>";
                      echo $key['Nama_Jenis_Sup'];
                      echo "</option>";
                  }
                 ?>
                </select>
              </div>
            </div> 
             <div class="control-group col-md-12">
              <label>Email</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Email" name="Email">
                
              </div>
            </div> 
            <div class="control-group col-md-12">
              <label>Alamat</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Alamat	" name="alamat">
                
              </div>
            </div> 
            <div class="control-group form-group">
              <label class="col-lg-2 control-label">Jenis Kelamin</label>
            <div class="col-lg-10">
              <div class="radio">
                    <label>
                      <input type="radio" name="jekel" id="optionsRadios1" value="1" checked="">
                      Laki-laki
                    </label>
              </div>
              <div class="radio">
                    <label>
                      <input type="radio" name="jekel" id="optionsRadios2" value="2" >
                      Perempuan
                    </label>
              </div>
            </div>
          </div>
           
            <?php echo $provinsi ?>

            </div> 
            <!--<div class="control-group col-md-12">
              <label>Kontak Person</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Kontak Person" name="Kontak">
                
              </div>
            </div> 
            -->
            <div class="control-group col-md-12"> <!-- Kontak personnya 1 aja -->
              <label>Nomor Telpon</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Nomor Telpon" name="NomorTelpon">
                
              </div>
            </div> 
             <!--<div class="control-group col-md-12">
              <label>Nomor Handphone</label>
              <div class="controls">
                <input type="text" class="form-control" placeholder="Nomor Handphone" name="NomorHandphone">
                
              </div>
            </div>
            --> 
            <div class="control-group col-md-12">
              <label></label>
              <div class="controls">
                <button type="submit" class="btn btn-primary">
                  Kirim
                </button>
              </div>
            </div>   
            
          </form>