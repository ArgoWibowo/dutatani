<?php 

Class Menu {

    protected $CI;

    // use a constructor, because can't directly call a function
    // from a property definition.
    public function __construct()
    {
            // Assign the CodeIgniter super-object
            $this->CI =& get_instance();
            $this->CI->load->dbforge();
            
    }
    
    function view(){
        $this->CI->db->get('ta')->result_array();
    }

    function view_by($id){
        $this->CI->db->where('id', $id);
        return $this->CI->db->get('ta')->row();
    }

    function save(){
        $data = array(
          "title" => $this->CI->input->post('input_judul'),
          "parent_id" => $this->CI->input->post('input_induk'),
          "link" => $this->CI->input->post('input_link'),
          "position" => $this->CI->input->post('input_urutan'),
          "icon" => $this->CI->input->post('input_icon'),
          "is_top" => $this->CI->input->post('input_lokasi')
    );
        $this->CI->db->insert('ta', $data);
    }

    function edit($id){
        $data = array(
          "id" => $this->CI->input->post('input_id'),
          "title" => $this->CI->input->post('input_judul'),
          "parent_id" => $this->CI->input->post('input_induk'),
          "link" => $this->CI->input->post('input_link'),
          "position" => $this->CI->input->post('input_urutan'),
          "is_top" => $this->CI->input->post('input_lokasi')
    );
        $this->CI->db->where('id', $id);
        $this->CI->db->update('ta', $data);
    }

    function editicon($id){
        $data = array(
          'icon' => $this->CI->input->post('input_icon')
    );
        $this->CI->db->where('id', $id);
        $this->CI->db->update('ta', $data);
    }

    function delete($id){
        $this->CI->db->where('id', $id);
        $this->CI->db->delete('ta');
    }

    function load($parentid = 0)
        {
            
            $a = $this->CI->db->query("select title, link FROM ta WHERE parent_id =" .$parentid." order by position");
            foreach ($a->result_array() as $row)
            {
                echo $row['title']."\t\n ";
                echo $row['link'];
                echo "<br/>";
            }
        }
    function load2($parentid = 0)
        {
            //mencari parent
            $a = $this->CI->db->query("select * FROM ta WHERE parent_id =" .$parentid." order by position");
            foreach ($a->result_array() as $row)
            {
                echo $row['title']."\t\n ";
                echo $row['link'];
                echo "<br/>";
                //mencari child dari parent
                $b = $this->CI->db->query("select * FROM ta WHERE parent_id =" .$row['id']." order by position");
                foreach ($b->result_array()as $su) {
                    echo "INI ANAK COK-> \t\t\t\n".$su['title']."\t\n ";
                    echo $su['link']; 
                    echo "<br/>";
                    //mencari child dari child
                    $c = $this->CI->db->query("select * FROM ta WHERE parent_id =" .$su['id']." order by position");
                    foreach ($c->result_array()as $ah) {
                    echo "INI ANAKNYA ANAK-> \t\t\t\t\n".$ah['title']."\t\n ";
                    echo $ah['link'];
                    echo "<br/>";
                    };

                };
                
            }
        }
}