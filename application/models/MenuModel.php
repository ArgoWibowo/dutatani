<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MenuModel extends CI_Model {
  // Fungsi untuk menampilkan semua data siswa
  public function view(){
    $this->load->library('menu');
    $this->menu->view();
  }
  
  // Fungsi untuk menampilkan data siswa berdasarkan NIS nya
  public function view_by($id){
    $this->load->library('menu');
    return $this->menu->view_by($id);
  }
  
  // Fungsi untuk validasi form tambah dan ubah
  public function validation($mode){
    $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya
    
    // Tambahkan if apakah $mode save atau update
    // Karena ketika update, NIS tidak harus divalidasi
    // Jadi NIS di validasi hanya ketika menambah data siswa saja
    if($mode == "save")
    $this->form_validation->set_rules('input_judul', 'title', 'required');
    $this->form_validation->set_rules('input_link', 'link', 'required');
    $this->form_validation->set_rules('input_lokasi', 'is_top', 'required');
      
    if($this->form_validation->run()) // Jika validasi benar
      return TRUE; // Maka kembalikan hasilnya dengan TRUE
    else // Jika ada data yang tidak sesuai validasi
      return FALSE; // Maka kembalikan hasilnya dengan FALSE
  }
  
  // Fungsi untuk melakukan simpan data ke tabel siswa
  public function save(){
       
    $this->load->library('menu');
    $this->menu->save(); // Untuk mengeksekusi perintah insert data
  }
  
  // Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
  public function edit($id){
    $this->load->library('menu');
    $this->menu->edit($id); // Untuk mengeksekusi perintah update data
  }

  public function editicon($id){
    $this->load->library('menu');
    $this->menu->editicon($id); // Untuk mengeksekusi perintah update data
  }
  
  // Fungsi untuk melakukan menghapus data siswa berdasarkan NIS siswa
  public function delete($id){
    $this->load->library('menu');
    $this->menu->delete($id); // Untuk mengeksekusi perintah delete data
  }

  public function viewmenu(){
    return $this->load->library('menu');
    $this->menu->load();
  }
}