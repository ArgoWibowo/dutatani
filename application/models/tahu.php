<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahu extends CI_Model {
	public function navigasi()
	{
		if ($this->session->has_userdata('ID_User')) {
			$temp=$this->getTimeline(array("status"=>0));
			if ($temp->num_rows()>0) {
				$this->session->set_userdata(array("time"=>"style='background: #d21d1d;'"));
			}else{
				$this->session->set_userdata(array("time"=>""));
			}
		}else{
			$this->session->set_userdata(array("time"=>""));
		}
	}
	public function setTimeline($id_user,$isi,$link){
		$array=array(
			"ID_User"	=> $id_user,
			"link"		=> $link,
			"isi"		=> $isi,
			"status"	=> 0,
			"tanggal"	=> date('Y-m-d'),
		);
		$this->db->set($array);
		return $this->db->insert('pemberitahuan');

	}
	public function getTimeline($where=array()){
		if ($this->session->userdata('ID_User')) {

			$where['ID_User']=$this->session->userdata('ID_User');

			$this->db->where($where);
			return $this->db->get('pemberitahuan');
		}
	}

}
?>
