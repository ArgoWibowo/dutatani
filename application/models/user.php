<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

	public function login($where)
	{
		$w=array(
				"master_user.ID_User"	=> 	$where['ID_User'],
				"Password"				=>	sha1($where['Password']),
			);

		$this->db->select('*');
		$this->db->from('master_user');
		$this->db->join('master_detail_user','master_user.ID_User = master_detail_user.ID_User');
		$this->db->where($w);
		$data=$this->db->get();
		$this->load->library('session');


		if ($data->num_rows()==1) {
			$this->db->select('*');
			$this->db->from('master_detail_user');
			$this->db->where('ID_User',$where['ID_User']);
			$dat=$this->db->get();
			

			foreach ($dat->result_array() as $key => $value) {

				foreach ($value as $ky => $val) {
					$this->session->set_userdata(array(
							$ky => $val
					));
				}
			}
			$this->session->set_userdata(array(
					"Tingkat_Priv" => 1
			));

			$this->db->from("master_user_kat");
			$this->db->where('ID_User',$where['ID_User']);
			$dat=$this->db->get();
			$i=0;
			foreach ($dat->result_array() as $key => $value) {
				$this->session->set_userdata(array(
							"ID_Kategori".$i => $value["ID_Kategori"]
					));
				$i++;
			}


			redirect($this->session->userdata('url'));
		}
		else{
			$error=$this->load->view('error',array("error"=>"Maaf Username dan Password tidak ditemukan"),true);
			$this->session->set_flashdata('error1',$error);	
			redirect('login');
		}
	}

	private function upload($image){
		if($image['imageUpload']['type']=="image/png")
		{
			$tipe=".PNG";
		}else{
			$tipe=".jpg";
		}
		$name=md5( date("Y/M/d-D h:i:s"));
		$config['file_name']     		= $name.$tipe;
		$config['upload_path']          = 'Foto/';
        $config['allowed_types']        = 'jpg|png';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('imageUpload'))
        {	
        		return "maxresdefault.jpg";
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
               return $config['file_name'];
        }
	}

	public function cek_user($username){
		
		$this->db->select("*");
		$this->db->where("ID_User",$username);
		$user=$this->db->get("master_user");



		return $user->num_rows();
	}
	public function logout(){
		$this->load->library('session');
		$user=$this->session->userdata();
		foreach ($user as $key => $value) {
			$this->session->unset_userdata($key);
		}
		$user=$this->session->userdata();
		
		$this->session->sess_destroy();
		
	}

	public function insert($post,$file){
		if ($this->cek_user($this->session->userdata('id'))===0) {
			$master_user=array(
				"ID_User"		=> 	$this->session->userdata('id'),
				"Password"		=>	sha1($this->session->userdata('password')),
				"PIN"			=>	$this->session->userdata('PIN'),
				"Tingkat_Priv"	=>	1
			);

			$this->db->insert("master_user",$master_user);

			$master_user_kat=array(
				"ID_User"		=> 	$this->session->userdata('id'),
				"ID_Kategori"	=>	$this->session->userdata('kategori'),
			);
			$this->db->insert('master_user_kat',$master_user_kat);
			if ($this->session->userdata('kategori')=='PET') {
				$this->petani($post,$file);
			}elseif ($this->session->userdata('kategori')=='FAS') {
				$this->fasilitator($post,$file);
			}elseif ($this->session->userdata('kategori')=="SUP") {
				$this->suplayer($post,$file);
			}else{
				$this->detail($post,$file);
			}
			$this->session->set_userdata("id","");
			$this->session->set_userdata("PIN","");

		}else{
			$this->session->set_flashdata("error",$this->load->view("user/error",array(
					"error"	=>	"Username Sudah Di gunakan oleh pengguna lain"
				),true));
			redirect("login/daftar/2");
		}

	}
	private function petani($post,$file){
		$master_petani=array(
				"ID_User"			=> 	$this->session->userdata('id'),
				"Nama_Petani"		=>	$post['nama'],
				"Alamat_Petani"		=>	$post['alamat'],
				"Kabupaten"			=>	$post['Kabupaten'],
				"Kecamatan"			=>	$post['Kecamatan'],
				"Provinsi"			=>	$post['Provinsi'],
				"Desa_Kelurahan"	=>	$post['DesaKelurahan'],
				"Foto"				=>	$this->upload($file),
				"Nomor_Telpon"		=>	$post['NomorTelpon'],
				"Jumlah_Tanggungan"	=>	$post['JumlahTanggungan'],
				"Email"				=>	$post['Email'],
				"Agama"				=>	$post['Agama'],
				"Tanggal_Lahir"		=>	$post['tgl'],
				"Deskripsi_Keahlian"=>	$post['DeskripsiKeahlian'],
				"Status"			=>  1,
				"jns_kelamin"		=>  $post['jekel'],
			);
			//$this->petDetail($master_petani);
			//$this->db->insert('master_petani',$master_petani);
			$this->session->set_flashdata("error",$this->load->view("user/signupSuccsess",array(
					"error"	=>	"Selamat Datang. Silahkan Login untuk masuk ke sistem.<br> Klik Login Di menu Paling bawah dan masukan username dan password anda"
				),true));
			redirect();
	}


	public function fasilitator($post,$file){
		$master_petani=array(
				"ID_User"			=> 	$this->session->userdata('id'),
				"Nama_Fasilitator"	=>	$post['nama'],
				"Alamat_Fasilitator"=>	$post['alamat'],
				"Kabupaten"			=>	$post['Kabupaten'],
				"Kecamatan"			=>	$post['Kecamatan'],
				"Provinsi"			=>	$post['Provinsi'],
				"tanggal_lahir"		=>$post['tgl'],
				"Desa"				=>	$post['DesaKelurahan'],
				"Email"				=>	$post['Email'],
				"Foto"				=>	$this->upload($file),
				"Telpon"			=>	$post['NomorTelpon'],
				"Pendidikan_Terakhir"	=>	$post['PendidikanTerakhir'],
				"Jurusan"			=>	$post['Jurusan'],
				"Kompetensi_Keahlian"	=>	$post['KompetensiKeahlian'],
				"Riwayat_Pendidikan"=>	$post['RiwayatPendidikan'],
				"Pengalaman_Kerja"=>	$post['PengalamanKerja'],
				"jns_kelamin"		=> $post['jekel']
			);
		$this->fasDetail($master_petani);
			$this->db->insert('master_fasilitator',$master_petani);
			$this->session->set_flashdata("error",$this->load->view("user/signupSuccsess",array(
					"error"	=>	"Selamat Datang. Silahkan Login untuk masuk ke sistem.<br> Klik Login Di menu Paling bawah dan masukan username dan password anda"
				),true));
			redirect();
	}
	public function suplayer($post,$file){
		$suplayer=array(
			"ID_User"			=>$this->session->userdata('id'),
			"Nama_Supplier"		=>$post['nama'],
			"Alamat_Supplier"	=>$post['alamat'],
			"Kabupaten"			=>$post['Kabupaten'],
			"Kecamatan"			=>$post['Kecamatan'],
			"Provinsi"			=>$post['Provinsi'],
			"Desa"				=>$post['DesaKelurahan'],
			"Email"				=>$post['Email'],
			"tanggal_lahir"		=>$post['tgl'],
			"jns_kelamin"		=>$post['jekel'],
			"Foto"				=>$this->upload($file),
			"No_Handphone"		=>$post['NomorTelpon'],
			"Kontak_Person"		=>$post['Kontak'],
			"ID_Jenis_Sup"		=>$post['Suplayer'],
			"No_Telpon"			=>$post['NomorHandphone'],
		);
		$this->supDetail($suplayer);
		$this->db->insert('master_supplier',$suplayer);
		$this->session->set_flashdata("error",$this->load->view("user/signupSuccsess",array(
				"error"	=>	"Selamat Datang. Silahkan Login untuk masuk ke sistem.<br> Klik Login Di menu Paling bawah dan masukan username dan password anda"
			),true));
		redirect();
		
	}

	public function getJenisSup(){
		return $this->db->get('master_jenis_sup');
	}

	public function supDetail($sup){
		$detail=array(
			"ID_User"		=> $sup['ID_User'],
			"nama"			=> $sup['Nama_Supplier'],
			"tanggal_lahir"	=> $sup['tanggal_lahir'],
			"alamat"		=> $sup['Alamat_Supplier'],
			"provinsi"		=> $sup['Provinsi'],
			"kabupaten"		=> $sup['Kabupaten'],
			"kecamatan"		=> $sup['Kecamatan'],
			"keluran_desa"  => $sup['Desa'],
			"nomor_telpon"	=> $sup['No_Telpon'],
			"Email"			=> $sup['Email'],
			"Foto"			=> $sup['Foto'],
			"jenis_kelamin" => $sup['jns_kelamin'],

		);
		$this->db->set($detail);
		$this->db->insert("master_detail_user");
	}
	public function fasDetail($sup){
		$detail=array(
			"ID_User"		=> $sup['ID_User'],
			"nama"			=> $sup['Nama_Fasilitator'],
			"tanggal_lahir"	=> $sup['tanggal_lahir'],
			"alamat"		=> $sup['Alamat_Fasilitator'],
			"provinsi"		=> $sup['Provinsi'],
			"kabupaten"		=> $sup['Kabupaten'],
			"kecamatan"		=> $sup['Kecamatan'],
			"keluran_desa"  => $sup['Desa'],
			"nomor_telpon"	=> $sup['Telpon'],
			"Email"			=> $sup['Email'],
			"Foto"			=> $sup['Foto'],
			"jenis_kelamin" => $sup['jns_kelamin'],
		);
		$this->db->set($detail);
		$this->db->insert("master_detail_user");
	}

	public function petDetail($sup){
		$detail=array(
			"ID_User"		=> $sup['ID_User'],
			"nama"			=> $sup['Nama_Petani'],
			"tanggal_lahir"	=> $sup['Tanggal_Lahir'],
			"alamat"		=> $sup['Alamat_Petani'],
			"provinsi"		=> $sup['Provinsi'],
			"kabupaten"		=> $sup['Kabupaten'],
			"kecamatan"		=> $sup['Kecamatan'],
			"keluran_desa"  => $sup['Desa_Kelurahan'],
			"nomor_telpon"	=> $sup['Nomor_Telpon'],
			"Email"			=> $sup['Email'],
			"Foto"			=> $sup['Foto'],
			"jenis_kelamin" => $sup['jns_kelamin'],
		);
		$this->db->set($detail);
		$this->db->insert("master_detail_user");
	}
	public function getKategori(){
		if (!$this->session->has_userdata('NIK')) {
			$this->db->where("aktif","1");
		}
		return $this->db->get("master_kategori");
	}
	private function Detail($post,$file){
		echo "<pre>";
		print_r($post);
		echo "</pre>";
		$Detail = array(
			'ID_User' 		=>$this->session->userdata('id'),
			'nama'			=>$post['nama'],
			'jenis_kelamin'	=>$post['jekel'],
			'alamat'		=>$post['alamat'],
			'provinsi'		=>$post['Provinsi'],
			'kecamatan'		=>$post['Kecamatan'],
			'keluran_desa'	=>$post['DesaKelurahan'],
			'nomor_telpon'	=>$post['NomorTelpon'],
			'Email'			=>$post['Email'],
			'Foto'			=>$this->upload($file),
		 );
		$this->db->set($Detail);
		$this->db->insert('master_detail_user');
		$this->session->set_flashdata("error",$this->load->view("user/signupSuccsess",array(
				"error"	=>	"Selamat Datang. Silahkan Login untuk masuk ke sistem.<br> Klik Login Di menu Paling bawah dan masukan username dan password anda"
			),true));
		redirect();
	}

	public function sama($table,$where,$kolom,$karakter)
	{
		$this->db->where($where);
		$data=$this->db->get($table);
		if ($data->num_rows()>0) {
			$where[$kolom]=substr(md5($where[$kolom]), 0,$karakter);
			$where[$kolom]=$this->sama($table,$where,$kolom,$karakter);
		}
		return $where[$kolom];
	}
	

}
