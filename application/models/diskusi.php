<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diskusi extends CI_Model {

	public function index($num,$offset,$on="1=1")
	{
		$this->db->select('*');
		$this->db->join('trans_topik_diskusi', 'trans_topik_diskusi.ID_kategori_topik = master_kategori_topik.ID_kategori_topik');
		$this->db->join('master_detail_user', 'master_detail_user.ID_user = trans_topik_diskusi.ID_user ');
		$this->db->order_by('waktu', 'DESC');
		$this->db->where($on);
		return $this->db->get('master_kategori_topik',$num,$offset);
	}
	public function getcount($on="1=1"){
		$this->db->select('*');
		$this->db->join('trans_topik_diskusi', 'trans_topik_diskusi.ID_kategori_topik = master_kategori_topik.ID_kategori_topik');
		$this->db->join('master_detail_user', 'master_detail_user.ID_user = trans_topik_diskusi.ID_user');
		$this->db->where($on);
		return $this->db->get('master_kategori_topik');
	}

	public function getperpage($offset,$url,$jml,$on="1=1"){
		$this->load->library('pagination');
		$config['base_url'] = base_url().$url;
		$config['total_rows'] = $jml;
		$config['per_page'] = 5; /*Jumlah data yang dipanggil perhalaman*/
		$config['uri_segment'] = 3;
		$config['use_page_numbers']=TRUE;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		return $this->index($config['per_page'] , ($offset*$config['per_page'])-$config['per_page'],$on);
	}
	

	public function tambah(){
		$this->load->library('session');
		if ($this->session->has_userdata('ID_User')) {
			return $this->load->view("user/tambah",'',true);
		}else {
			return "";
		}
	}

	public function count_komen($id){
		$this->db->select('*');
		$this->db->from('trans_komentar_diskusi');
		$this->db->where('ID_topik',$id);
		$data=$this->db->get();
		return $data->num_rows();
	}

	public function getTopik($id){
		$this->load->library('session');
		$this->db->select('*');
		$this->db->from('trans_topik_diskusi');
		$this->db->join('master_detail_user','master_detail_user.ID_User=trans_topik_diskusi.ID_user');
		$where=array(
			'trans_topik_diskusi.ID_topik'=>$id,
		);
		$this->db->where($where);
		$judul= $this->db->get();

		$komen=$this->getKomen($id, 0, 10);
		$total_komen = $this->count_komen($id);
		$next_offset = 10;

		if ($this->session->has_userdata('ID_User')) {
			$user=$this->session->userdata('ID_User');
		}else{
			$user="";
		}
		if (!$judul->num_rows()) {
			$judul="0";
		}

		$isi=array(
				"judul"			=>	$judul,
				"komen"			=>	$komen,
				"user"			=>	$user,
				"id"			=>	$id,
				"expandable"	=> ( $next_offset < $total_komen ) ? true : false,
			);

		$main=$this->load->view('user/komentar',$isi,true);
		$this->load->view('user/main',array(
			"main"			=> $main,
			"css"			=> $this->load->view('user/css','',true),
			"nav"			=> $this->Navigasi->index("forum"),
		));
	}

	public function getKomen($id, $offset, $limit){
		$this->db->select('*');
		$this->db->from('trans_komentar_diskusi');
		$this->db->order_by('waktu', 'DESC');
		$this->db->join('master_detail_user','master_detail_user.ID_User=trans_komentar_diskusi.ID_user');
		$this->db->where(array('trans_komentar_diskusi.ID_topik'=>$id));
		$this->db->limit($limit, $offset);
		$komen=$this->db->get();
		return $komen;
	}

	public function setKomen($id,$post){
		$this->load->model('tahu');
		$this->db->where(array('ID_topik'=>$id));
		$count=$this->db->get('trans_topik_diskusi');
		if ($count->num_rows()==1) {
			$this->load->library('session');

			$temp=$count->result_array();


			if ($this->session->userdata('Tingkat_Priv')==1) {
				$value=array(
						"ID_topik"		=> $id,
						"ID_user"		=> $this->session->userdata('ID_User'),
						"Komentar"		=> $post
					);
				$this->db->set(array('tanggal' => 'now()'), '', FALSE);
				$this->db->set(array('waktu' => 'now()'), '', FALSE);
				$this->db->insert('trans_komentar_diskusi',$value);
				if ($temp[0]['ID_user']!=$this->session->userdata('ID_User')) {
					$isi= $this->session->userdata('nama'); "mengomentari diskusi anda";
					$link="forum/komentar/".$id;
					return $this->tahu->setTimeline($temp[0]['ID_user'],$isi,$link);
				}

				return 1;
			}else{
				redirect('');
			}
		}
	}

	public function getPengguna($id){
		$where=array(
			"ID_User"	=>$id
		);
		$this->db->from('master_detail_user');
		$this->db->where($where);
		$data=$this->db->get();
		return $data;
	}

	public function hapus_komentar($where){
		$this->db->where($where);
		return $this->db->delete('trans_komentar_diskusi');
	}
	public function hapus_diskusi($where){
		$this->db->select('ID_kategori_topik');
		$this->db->where($where);
		$data=$this->db->get('trans_topik_diskusi');
		if ($data->num_rows()==1) {
			$key=$data->result_array();
			$kategori= $key[0]['ID_kategori_topik'];
			$this->hapus_topik($where);
			$this->db->where(array('ID_kategori_topik'=>$kategori));
			return $this->db->delete('master_kategori_topik');
		}

		// $this->db->where()
	}
	private function hapus_topik($where){
		$this->db->where($where);
		$this->db->delete('trans_topik_diskusi');
	}

	public function cekDiskusi($where){
		$this->db->where($where);
		$data=$this->db->get('trans_topik_diskusi');
		return $data;
	}



	public function lihatDiskusi($select="*",$where="1=1",$banyak="off",$dari="off")
	{
		$this->db->select($select);
		$this->db->join('trans_topik_diskusi', 'trans_topik_diskusi.ID_kategori_topik = master_kategori_topik.ID_kategori_topik');
		$this->db->join('master_detail_user', 'master_detail_user.ID_user = trans_topik_diskusi.ID_user ');
		$this->db->order_by('waktu', 'DESC');
		$this->db->where($where);

		if ($banyak=="off") {
			return $this->db->get('master_kategori_topik');
		}else{
			return $this->db->get('master_kategori_topik',$banyak,$dari);
		}
		
	}

	public function search( $keyword ) {
		$this->db->like("Judul_kategori_topik", $keyword);
		$this->db->order_by("Judul_kategori_topik");
		$this->db->limit(5);
		return $this->db->get('master_kategori_topik');
	}

}
