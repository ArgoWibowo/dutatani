<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_acc extends CI_Model {
	public function getAcces($acc){
		if ($this->session->userdata('Org_Unit')==$this->session->userdata('Org_Unit_Atasan')) {
			return true;
		}elseif ($this->session->userdata('Org_Unit')) {
			return true;
		}else{
			if ($this->session->userdata($acc)) {
				return true;
			}else{
				return false;
			}
		}
	}

	public function getAdmin($where=""){
		$this->db->select('*');
		$org=$this->session->userdata('Org_Unit');
		if ($org==1) {
			$where['1']=1;
		}else{
			$where['master_org_unit.Org_Unit !=']=1;
		}
		
		$this->db->where($where);
		$this->db->from('master_user_org');
		$this->db->join('master_org_unit','master_user_org.Org_Unit = master_org_unit.Org_Unit');
		return $this->db->get();
	}
	public function getOrg($where=""){
		$this->db->select('a.Nama_Organisasi atasan');
		$this->db->select('b.Nama_Organisasi bawahan');
		$this->db->select('b.Pelayanan');
		$this->db->select('b.Org_Unit');
		$this->db->select('b.Pendaftaran_Anggota');
		$this->db->select('b.Pelatihan');
		$this->db->select('b.Konsultasi');
		$this->db->select('b.Penawaran');
		$this->db->select('b.Permintaan');
		$this->db->select('b.Memberi_Informasi');
		$this->db->select('b.Meminta_Informasi');
		$this->db->select('b.Berbagi_Informasi');
		$this->db->select('b.Data_Spesifik_Anggota');
		$this->db->order_by('b.Tingkatan','DESC');
		
		$where['b.Org_Unit !=']=1;
		$this->db->where($where);

		$this->db->from('master_org_unit a');
		$this->db->join('master_org_unit b',"a.Org_Unit=b.Org_Unit_Atasan",'right');


		return $this->db->get();
	}
	public function getOrgUser($nik){
		$where=array(
			"NIK"=>$nik
		);

		$this->db->where($where);
		return $this->db->get('master_user_org');
	}
	public function hpsOrg($org){

		$this->db->select("*");
		$this->db->from("master_user_org");
		$this->db->where(array("Org_Unit"=>$org));
		$data=$this->db->get();
		if ($data->num_rows()==0) {
			$org1=$this->getOrg(array('a.Org_Unit'=>$org));
			if ($org1->num_rows()==0) {
				$this->hpsUnit($org);
			}
			else{
				$error=$this->load->view('error',array("error"=>"Unit ini terhubung dengan unit yang lain.<br>Hapus atau pindahkan unit yang terhubung ke unit yang lain"),true);
				$this->session->set_flashdata('error',$error);
				redirect('list_admin');	
			}
		}else{
			$error=$this->load->view('error',array("error"=>"Unit ini tidak bisa dihapus karena masih ada yang menjabat di <b>Admin</b>.<br>Klik edit pada Admin yang bersangkutan dan pindahkan user tersebut ke unit lain lalu anda dapat menghapus unit tersebut"),true);
			$this->session->set_flashdata('error',$error);
			redirect('list_admin');
		}
	}

	public function hpsUnit($org){
		$this->db->where(array("Org_Unit"=>$org));
		if ($this->db->delete('master_org_unit')) {
			$error=$this->load->view('sukses',array("error"=>"Data berhasil di hapus"),true);
			$this->session->set_flashdata('error',$error);
			redirect('list_admin');
		}else{
			$error=$this->load->view('error',array("error"=>"Data gagal di hapus"),true);
			$this->session->set_flashdata('error',$error);
			redirect('list_admin');
		}
	}

}
