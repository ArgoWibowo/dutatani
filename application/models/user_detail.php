<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_detail extends CI_Model {
	public function cekUser($user){
		$this->db->select('*');
		$this->db->from('master_user');
		$this->db->where(array('ID_User'=>$user));
		$data=$this->db->get();
		if ($data->num_rows()==0) {
			return 1;
		}else{
			return 0;
		}
	}
}