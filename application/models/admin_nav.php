<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_nav extends CI_Model {
	
	public function getNav($nav)
	{	
		return $this->load->view('admin/navigasi',array(
			"nav"	=> $nav,
		),true);
	}

	
}
