<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pesan extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        $this->load->library('not_null');
    }

	public function kirim(){
		
		$kirim=$this->not_null->POST($_POST);
		if ($kirim) {
			echo "<pre>";
			print_r($_POST);
			echo "</pre>";


			$this->db->set("nama",$_POST['Name']);
			$this->db->set("email",$_POST['email']);
			$this->db->set("pesan",$_POST['pesan']);
			$this->db->set("status",1);
			$this->db->set("tanggal_jam","now()",false);

			if ($this->db->insert("pesan")) {
				$this->sukses("Pesan telah terkirim");
			}else{
				$this->error("Pesan gagal dikirim");
			}
		}else{
			$this->error("Semua form harus diisi");
		}

		redirect('about');
	}

	public function lihat(){
		$this->db->order_by("tanggal_jam","DESC");
		return $this->db->get('pesan');
	}

	public function hapus($id){
		$this->db->where("sha1(id_pesan)",$id);
		if ($this->db->delete("pesan")) {
			$this->sukses("Pesan Berhasil Dihapus");
		}else{
			$this->error("Pesan Gagal Fihapus");
		}

	}

	public function sukses($kata){
		$error=$this->load->view('sukses',array("error"=>$kata),true);
		$this->session->set_flashdata('error1',$error);	
	}

	public function error($kata){
		$error=$this->load->view('error',array("error"=>$kata),true);
		$this->session->set_flashdata('error1',$error);	
	}

}
