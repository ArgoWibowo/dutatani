<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class log extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }

	public function login($post){
		$user= str_replace(" ", "",$post['username']);
		$pass= str_replace(" ", "",$post['password']);
		$pass= sha1($pass);
		$where = array('NIK' => $user,'Password' => $pass );

		$this->db->select("*");
		$this->db->from("master_user_org");
		$this->db->join('master_org_unit','master_org_unit.Org_Unit=master_user_org.Org_Unit');
		$this->db->where($where);
		$data=$this->db->get();
		return $data;
	}

}
