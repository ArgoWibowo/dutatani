<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class berita extends CI_Model {
	public function lihatBerita($select="*",$where="1=1",$banyak="off",$dari="off",$rand="tanggal")
	{
		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('master_user_org','master_berita_informasi.NIK=master_user_org.NIK');
		$this->db->order_by($rand, 'DESC');
		if ($banyak=="off") { //kalau ga ada pagination
			return $this->db->get('master_berita_informasi');
		}else{ //kalau ada pagination
			return $this->db->get('master_berita_informasi',$banyak,$dari);
		}
	}


	public function bikinBerita($post){
		$this->db->set($post);
		$this->db->set('tanggal', 'NOW()', FALSE);
		return $this->db->insert('master_berita_informasi');
	}

	public function hapusBerita($where){
		$this->db->where($where);
		return $this->db->delete("master_berita_informasi");
	}
	public function update($set,$where){
		$this->db->where($where);
		$this->db->set($set);
		return $this->db->update('master_berita_informasi');
	}

	public function search( $keyword ) {
		$this->db->select("id,judul");
		$this->db->like("judul", $keyword);
		$this->db->order_by("tanggal", "DESC");
		$this->db->limit(5);
		return $this->db->get('master_berita_informasi');
	}
	
}
?>
