<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navigasi extends CI_Model {

	public function index($nav)
	{	

		$this->load->library('session');

		$arr=array(
				"nav"	=>	$nav,
			);


		return $this->load->view('user/navigasi',$arr,true);
	}

	
}
