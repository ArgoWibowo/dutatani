<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADK")
  {
    header("Location:login.php?pesan=khusus untuk admin & petani");
  }
  //select DATEDIFF(NOW(), Tanggal_Lahir) / 365.25 as umur FROM master_petani
  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
        <style type="text/css">
${demo.css}
        </style>
        <script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Berdasarkan Kabupaten di Provinsi <?php echo $_GET['provinsi']; ?>'
        },
        subtitle: {
            text: 'Per Tanggal <?php echo date('d/m/Y'); ?>'
        },
        xAxis: {
            categories: [
            <?php
                $koneksi = include("koneksi.php"); 
                $hasil = mysqli_query($koneksi,"SELECT kabupaten as nama, count(ID_User) AS jumlah FROM `master_kel_tani` WHERE provinsi = '$_GET[provinsi]' GROUP BY kabupaten ORDER BY kabupaten");
                while($dataprovinsi = mysqli_fetch_array($hasil)){
            ?>
                '<?php echo $dataprovinsi['nama']; ?>',
            <?php } ?>
            ],
            crosshair: true
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Jumlah Kelompok Tani (Kelompok)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">: </td>' +
                '<td style="padding:0;font-size:12px">{point.y:.f} Kelompok</td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {

                borderWidth: 0
                , events: {
                    legendItemClick: function () {
                        return false; 
                    }
                }
            }, 
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            //window.open(this.options.url);
                            document.location = this.options.url;
                        }
                    }
                }
            },
        },
        legend: { symbolHeight: '0px' },
        series: [{
            name: 'Nama Kabupaten',
            data: [
            <?php
                $koneksi = include("koneksi.php"); 
                $hasil = mysqli_query($koneksi,"SELECT kabupaten as nama, count(ID_User) AS jumlah FROM `master_kel_tani` WHERE provinsi = '$_GET[provinsi]' GROUP BY kabupaten ORDER BY kabupaten");
                while($dataprovinsi = mysqli_fetch_array($hasil)){
            ?>
                {y:<?php echo $dataprovinsi['jumlah']; ?>,url:'GrafikKelTaniKecamatan_User.php?kabupaten=<?php echo $dataprovinsi['nama']; ?>&provinsi=<?php echo $_GET['provinsi']; ?>'}, 
            <?php } ?>
            ]

        }
        ]
    });
});
        </script>
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
    <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
    ?>
        <li style="background-color: #024A0B"><a href="HomeiKelTani.php">Selamat Datang <?php echo $id_user ?></a></li>
        <li style="background-color: #024A0B"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
     </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="DataKelompokTaniUser.php"> Daftar Kelompok Tani</a></li>
                        <li><a href="StrukturOrganisasiUser.php">Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetaniUser.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                         <li><a href="GrafikKelTaniWilayah_User.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_TaniUser.php"> Detail Anggota Kelompok Tani</a></li>   
                        <li><a href="StatistikKelompokTani_User.php"> Statistik Kelompok Tani</a></li>   
                    </ul>
                </li>
        </div><!-- /#sidebar-wrapper -->

    <script src="design/side_menu.js"></script>

<body>
    
            <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Informasi Kelompok Tani</h1>
                        > <a href="GrafikKelTaniWilayah_User.php">Provinsi<a> > <a href="GrafikKelTaniKabupaten_User.php?provinsi=<?php echo $_GET['provinsi']; ?>">Kabupaten<a>
                    </div>
                    <div style="margin-bottom:15px;" align="center">

                    </div>
                </div>
            </div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">      
                        <script src="design/highcharts.js"></script>
                        <script src="highcharts/modules/exporting.js"></script>

                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        <div class="row">
                            <div class="col-md-8">  
                            </div>
                            <div class="col-md-3 text-left">
                                <a class="btn btn-lg btn-info" href="SumarryKelTaniKabupaten_User.php?provinsi=<?php echo $_GET['provinsi']; ?>"> Sumarry</a>
                            </div>
                        </div>
                </div>

                
            </div>
</script>
</body>
</html>