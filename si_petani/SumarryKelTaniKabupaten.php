<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADP")
  {
    header("Location:login.php?pesan=khusus untuk admin & petani");
  }
  //select DATEDIFF(NOW(), Tanggal_Lahir) / 365.25 as umur FROM master_petani
  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
    <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
    ?>
        <li style="background-color: #024A0B"><a href="HomeiKelTani.php">Selamat Datang <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li style="background-color: #024A0B"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
     </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Wilayah.php"> Pendataan Wilayah</a></li> 
                        <li><a href="Petani.php">Pendataan Petani</a></li>
                        <li><a href="KelompokTani.php">Pendataan Kelompok Tani</a></li>
                        <li><a href="StrukturOrganisasi.php">Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetani.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Petani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikWilayah.php"> Berdasarkan Wilayah</a></li>
                        <li><a href="#"> Berdasarkan Agama</a>
                          <li>
                              <a href="SumarryTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Usia</a>
                          <li>
                              <a href="SumarryTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Tk. Pendidikan</a>
                          <li>
                              <a href="SumarryTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Status Petani</a>
                          <li>
                                <a href="SumarryTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikKelTaniWilayah.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_Tani.php"> Detail Anggota Kelompok Tani</a></li>
                        <li><a href="StatistikKelompokTani.php"> Statistik Kelompok Tani</a></li>      
                    </ul>
                </li>
        </div><!-- /#sidebar-wrapper -->

    <script src="design/side_menu.js"></script>

<body>
    
            <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Informasi Kelompok Tani</h1>
                         <h4>Berdasarkan Wilayah</h4>
                        <br>
                    </div>
                    <div style="margin-bottom:15px;" align="center">

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">  
            </div>
            <div class="col-md-5">
                <button class="btn btn-lg btn-primary" onclick="window.location.href='DetailKelTani_Kab.php?provinsi=<?php echo $_GET['provinsi']; ?>'"> Cetak Daftar</button>
            </div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">      
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                           <table class="table" style="width: 500px;">
                             <thead>
                            <div style="margin-bottom:15px;" align="center">
                              
                            </div>
                              <tr>
                                   <th>No</th>
                                   <th>Wilayah</th>
                                   <th>Jumlah</th>
                              </tr>
                             </thead>
                                            
                                            <tbody>
                                            <?php
                                                $koneksi = include("koneksi.php");                      
                                                $hasil = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$_GET[provinsi]'  order by Nama_Kabupaten");
                                                $no = 1;
                                                $total=0;
                                                while($kabupaten = mysqli_fetch_array($hasil)){
                                            ?>  
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kecamatan WHERE Nama_Kabupaten = '$kabupaten[Nama_Kabupaten]' order by Nama_Kecamatan");
                                                        while($data3 = mysqli_fetch_array($hasil3)){
                                                            echo $data3['Nama_Kecamatan'].'<br/>';
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;".$kabupaten['Nama_Kabupaten']?>
                                                    </td>
                                                    <td>

                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kabupaten = '$kabupaten[Nama_Kabupaten]'");
                                                   //echo "SELECT count(ID_User) AS jumlah FROM `master_petani` WHERE kabupaten = '$kabupaten[Nama_Kabupaten]'";
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    $total=$total+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kecamatan WHERE Nama_Kabupaten = '$kabupaten[Nama_Kabupaten]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$kabupaten[Nama_Kecamatan]'");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;". $data2['jumlah'];?>
                                                    </td>
                                                </tr>
                                                <?php
                              
                                            }

                                            ?>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>Total
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo $total;
                                                    ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                            </div>
                </div>
                     <div class="col-md-12 text-left">
                         <button class="btn btn-lg btn-danger" onclick="window.history.go(-1);">Back</button>
                    </div>
                
            </div>
</script>
</body>
</html>