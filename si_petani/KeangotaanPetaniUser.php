<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADK")
  {
    header("Location:login.php?pesan=khusus untuk Kelompok Tani");
  }

  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
      <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
      ?>
        <li style="background-color: #34A853"><a href="HomeiKelTani.php">Selamat Datang <?php echo $id_user ?></a></li>
        <li style="background-color: #34A853"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
               
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li class='active'>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        
                        <li><a href="StrukturOrganisasiUser.php"> Pendataan Struktur Organisasi</a></li>
                        <li class="active"><a href="KeangotaanPetaniUser.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
               <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                         <li><a href="GrafikKelTaniWilayah_User.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_TaniUser.php"> Detail Anggota Kelompok Tani</a></li>
                        <li><a href="StatistikKelompokTani_User.php"> Statistik Kelompok Tani</a></li> 
                    </ul>
                       </div><!-- /#sidebar-wrapper -->
        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="design/side_menu.js"></script>

<body>

         <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-pills">                     
                        <h1>Pendataan Keanggotaan Petani</h1>
                        <a class="fa fa-2x fa-list" href="DataKeanggotaanPetaniUser.php">  Daftar Keanggotaan Petani</a>
                    </div>
                </div>
            </div>
        </div>
         
      <div class="container">
        <div class="row">
          <div class="col-md-6">
           <form role="form" action="SimpanKeanggotaanPetaniUser.php" method="post">
              <div class="form-group">
                <label class="control-label">Id Kelompok Tani *</label>
                <input class="form-control autoIDKelTani" name="ID_Kelompok_Tani" placeholder="Id Kelompok Tani" type="text"
                maxlength="10" required>
              </div>
            <div class="form-group">
               <label class="control-label" >Nama Petani *</label>
                 <select class="form-control" name="id_user" required>
                    <option> Nama Petani </option>
                       <?php
                          //include ("koneksi.php");
                          mysql_connect("localhost","root","") or die(mysql_error());
                          mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                          $query= mysql_query('SELECT * FROM master_petani;');
                          if (mysql_num_rows($query) != 0){ 
                          while($brs = mysql_fetch_assoc($query)){ 
                          echo '<option value="'.$brs['ID_User'].'">'.$brs['Nama_Petani'].'</option>';
                          }
                         }
                      ?> 
                  </select>
              </div>
              
              <div class="form-group">
                <label class="control-label">Tanggal Gabung *</label>
                <input class="form-control" name="Tgl_Gabung" type="date" required>
              </div>
              <div class="form-group">
                <label class="control-label">Tanggal Kadaluarsa *</label>
                <input class="form-control" name="Tgl_Expired" type="date" value="2099-12-31" required >
              </div>
              <div class="form-group">
                <label class="control-label">Keterangan</label>
                <textarea class="form-control" name="Keterangan" maxlength="200"></textarea>
              </div>
              <br>
              <input class="btn btn-primary btn-lg" type="submit" value="Simpan">
              <br/>
            </form>
          </div>
        </div>
      </div>

</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
var sourceIDKelTani = [];

<?php
$koneksi = include("koneksi.php");
$hasil = mysqli_query($koneksi, "SELECT ID_Kelompok_Tani FROM master_kel_tani ORDER BY ID_Kelompok_Tani ASC");
while ($row = mysqli_fetch_array($hasil))

{
    ?>
    sourceIDKelTani.push("<?php echo $row["ID_Kelompok_Tani"]; ?>");
    <?php
}
?>

$(function() {
    $(".autoIDKelTani").autocomplete({
        source: function (request, response) {
            response($.ui.autocomplete.filter(sourceIDKelTani, request.term));
        }
    });
});
</script>