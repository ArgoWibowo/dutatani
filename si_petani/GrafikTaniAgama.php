<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADP")
  {
    header("Location:login.php?pesan=khusus untuk admin & petani");
  }
  //select DATEDIFF(NOW(), Tanggal_Lahir) / 365.25 as umur FROM master_petani
  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
        <style type="text/css">
${demo.css}
        </style>
        <script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Informasi Petani Berdasarkan Agama'
        },
        subtitle: {
            text: '<?php 
            if (@$_POST['pilih'] == "provinsi") {
                   echo "Di $_POST[provinsi]";
                } else if (@$_POST['pilih'] == "kabupaten") {
                    echo "Di Kabupaten$_POST[kabupaten]";          
                } else if (@$_POST['pilih'] == "kecamatan") {
                    echo "Di Kecamatan $_POST[kecamatan]";
                } else if (@$_POST['pilih'] == "desa") {
                    echo "Di $_POST[desa]";
                } else {
                    echo "Di Seluruh Wilayah";
                } 
            ?>'
        },
        xAxis: {
            categories: [
                'Islam',
                'Kristen',
                'Katolik',
                'Hindu',
                'Budha',
                'Konghucu'
            ],
            crosshair: true
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Jumlah Petani (Orang)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">: </td>' +
                '<td style="padding:0">{point.y} org</td></tr>',
        },
        plotOptions: {
            column: {

                borderWidth: 0
                , events: {
                    legendItemClick: function () {
                        return false; 
                    }
                }
            } 
        },
        legend: { symbolHeight: '0px' },
        series: [{
            name: ' Agama ',
            data: [{y:
            <?php
                $koneksi = include("koneksi.php");          
                
                if (@$_POST['pilih'] == "provinsi") {
                   $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Islam' AND provinsi = '$_POST[provinsi]'");
                } else if (@$_POST['pilih'] == "kabupaten") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Islam' AND kabupaten = '$_POST[kabupaten]'");
                    
                } else if (@$_POST['pilih'] == "kecamatan") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Islam' AND kecamatan = '$_POST[kecamatan]'");
                } else if (@$_POST['pilih'] == "desa") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Islam' AND desa_kelurahan = '$_POST[desa]'");
                } else {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Islam'");
                } 
                
                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#434348'} , 
            {y:<?php
                $koneksi = include("koneksi.php");          
                if (@$_POST['pilih'] == "provinsi") {
                   $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Kristen' AND provinsi = '$_POST[provinsi]'");
                } else if (@$_POST['pilih'] == "kabupaten") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Kristen' AND kabupaten = '$_POST[kabupaten]'");
                    
                } else if (@$_POST['pilih'] == "kecamatan") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Kristen' AND kecamatan = '$_POST[kecamatan]'");
                } else if (@$_POST['pilih'] == "desa") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Kristen' AND desa_kelurahan = '$_POST[desa]'");
                } else {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Kristen'");
                } 
                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#90ed7d'} , 
            {y:<?php
                $koneksi = include("koneksi.php");          
                if (@$_POST['pilih'] == "provinsi") {
                   $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Katolik' AND provinsi = '$_POST[provinsi]'");
                } else if (@$_POST['pilih'] == "kabupaten") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Katolik' AND kabupaten = '$_POST[kabupaten]'");
                    
                } else if (@$_POST['pilih'] == "kecamatan") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Katolik' AND kecamatan = '$_POST[kecamatan]'");
                } else if (@$_POST['pilih'] == "desa") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Katolik' AND desa_kelurahan = '$_POST[desa]'");
                } else {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Katolik'");
                }

                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#8085e9'} ,  
            {y:<?php
                $koneksi = include("koneksi.php");           
                if (@$_POST['pilih'] == "provinsi") {
                   $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Hindu' AND provinsi = '$_POST[provinsi]'");
                } else if (@$_POST['pilih'] == "kabupaten") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Hindu' AND kabupaten = '$_POST[kabupaten]'");
                    
                } else if (@$_POST['pilih'] == "kecamatan") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Hindu' AND kecamatan = '$_POST[kecamatan]'");
                } else if (@$_POST['pilih'] == "desa") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Hindu' AND desa_kelurahan = '$_POST[desa]'");
                } else {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Hindu'");
                }

                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#f15c80'} ,  
            {y:<?php
                $koneksi = include("koneksi.php");          
                 if (@$_POST['pilih'] == "provinsi") {
                   $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Budha' AND provinsi = '$_POST[provinsi]'");
                } else if (@$_POST['pilih'] == "kabupaten") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Budha' AND kabupaten = '$_POST[kabupaten]'");
                    
                } else if (@$_POST['pilih'] == "kecamatan") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Budha' AND kecamatan = '$_POST[kecamatan]'");
                } else if (@$_POST['pilih'] == "desa") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Budha' AND desa_kelurahan = '$_POST[desa]'");
                } else {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Budha'");
                }
                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#e4d354'} ,  
            {y:<?php
                $koneksi = include("koneksi.php");          
                
                if (@$_POST['pilih'] == "provinsi") {
                   $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Konghucu' AND provinsi = '$_POST[provinsi]'");
                } else if (@$_POST['pilih'] == "kabupaten") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Konghucu' AND kabupaten = '$_POST[kabupaten]'");
                    
                } else if (@$_POST['pilih'] == "kecamatan") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Konghucu' AND kecamatan = '$_POST[kecamatan]'");
                } else if (@$_POST['pilih'] == "desa") {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Konghucu' AND desa_kelurahan = '$_POST[desa]'");
                } else {
                    $hasil = mysqli_query($koneksi,"SELECT count(*) AS jumlah FROM `master_petani` WHERE Agama='Konghucu'");
                }
                $jumlah = 0;
                if ($petani = mysqli_fetch_array($hasil)) {
                    echo $petani['jumlah'];
                } else {
                    echo "0";
                }

            ?>, color: '#2b908f'} ]

        }]
    });
});
        </script>

        
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
    <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
    ?>
        <li style="background-color: #34A853"><a href="Homei.php">Selamat Datang <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li style="background-color: #34A853"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
     </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Wilayah.php"> Pendataan Wilayah</a></li> 
                        <li><a href="Petani.php">Pendataan Petani</a></li>
                        <li><a href="KelompokTani.php">Pendataan Kelompok Tani</a></li>
                        <li><a href="StrukturOrganisasi.php">Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetani.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li class='active'>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Petani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikWilayah.php"> Berdasarkan Wilayah</a></li>
                        <li><a href="#"> Berdasarkan Agama</a>
                          <li>
                              <a href="SumarryTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <li class="active"><a href="GrafikTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a></li>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Usia</a>
                          <li>
                              <a href="SumarryTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Tk. Pendidikan</a>
                          <li>
                              <a href="SumarryTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Status Petani</a>
                          <li>
                                <a href="SumarryTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikKelTaniWilayah.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_Tani.php"> Detail Anggota Kelompok Tani</a></li> 
                        <li><a href="StatistikKelompokTani.php"> Statistik Kelompok Tani</a></li>     
                    </ul>
                </li>
        </div><!-- /#sidebar-wrapper -->

        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="design/side_menu.js"></script>

<body>
    
            <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Informasi Petani</h1>
                    </div>
                    <div style="margin-bottom:15px;" align="center">
                        <form method="post" action="">
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-4"><input type="radio" name="pilih" value="semua"  <?php if ((@$_POST['pilih'] == '') || (@$_POST['pilih'] == 'semua')) { echo " checked"; } ?> /> Seluruh Wilayah</label></div>
                                    <div class="col-md-6">
                                        
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="provinsi"  <?php if ((@$_POST['pilih'] == 'provinsi') || (@$_POST['pilih'] == 'provinsi')) { echo " checked"; } ?> /> Provinsi</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control"  name="provinsi">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from provinsi ORDER BY Nama_Provinsi");
                                            $no = 1;
                                            while($prov = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $prov['Nama_Provinsi']; ?>" <?php if ( @$_POST['provinsi'] == $prov['Nama_Provinsi']) { echo " selected"; } ?>><?php echo $prov['Nama_Provinsi']; ?></option>
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="kabupaten"  <?php if ((@$_POST['pilih'] == 'kabupaten')) { echo " checked"; } ?> /> Kabupaten</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control"  name="kabupaten">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from kabupaten ORDER BY nama_provinsi, nama_kabupaten");
                                            $no = 1;
                                            while($kab = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $kab['Nama_Kabupaten']; ?>" <?php if ( @$_POST['kabupaten'] == $kab['Nama_Kabupaten']) { echo " selected"; } ?>><?php echo $kab['Nama_Kabupaten']; ?> - <?php echo $kab['Nama_Provinsi']; ?></option>
                                        
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="kecamatan" <?php if (@$_POST['pilih'] == 'kecamatan') { echo " checked"; } ?> /> Kecamatan</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control"  name="kecamatan">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from kecamatan ORDER BY nama_provinsi, nama_kabupaten, nama_kecamatan");
                                            $no = 1;
                                            while($kab = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $kab['Nama_Kecamatan']; ?>" <?php if ( @$_POST['kecamatan'] == $kab['Nama_Kecamatan']) { echo " selected"; } ?>><?php echo $kab['Nama_Kecamatan']; ?> - <?php echo $kab['Nama_Kabupaten']; ?> - <?php echo $kab['Nama_Provinsi']; ?></option>
                                        
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="desa" <?php if (@$_POST['pilih'] == 'desa') { echo " checked"; } ?> /> Desa</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="desa">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from kelurahan_desa ORDER BY nama_provinsi, nama_kabupaten, nama_kecamatan, nama_desa");
                                            //echo "select * from kelurahan_desa ORDER BY nama_provinsi, nama_kabupaten, nama_kecamatan, nama_desa";
                                            $no = 1;
                                            while($kec = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $kec['Nama_Desa']; ?>" <?php if (@$_POST['desa'] == $kec['Nama_Desa']) { echo " selected"; } ?>><?php echo $kec['Nama_Desa']; ?> - <?php echo $kec['Nama_Kecamatan']; ?> - <?php echo $kec['Nama_Kabupaten']; ?></option>
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <input type="submit" name="cari" value="Tampil" class="btn-md btn-primary" style="padding:3px;" margin="6px;" width="50px;"  />
                         </form>


                        
                    </div>
                </div>
            </div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <style>
                        div.scroll {
                        width: 1080px;
                        overflow: auto;
                        }
                        </style>
                        <div class ="scroll">      
                        <script src="design/highcharts.js"></script>
                        <script src="highcharts/modules/exporting.js"></script>

                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>


                        <br/>
 
                     
                
            </div>
</script>
</body>
</html>