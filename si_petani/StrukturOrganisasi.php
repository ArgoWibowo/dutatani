<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADP")
  {
    header("Location:login.php?pesan=khusus untuk admin & kelompok tani");
  }
?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php                   
        $id_user = $_SESSION['ID_User'];    
        $pesan; 
        if(isset($_GET['pesan']) && !empty($_GET['pesan'])){
        $pesan=$_GET['pesan'];
        } else {
        $pesan="";
        }                              
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li style="background-color: #34A853"><a href="#">Selamat Datang <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li style="background-color: #34A853"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li class='active'>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Wilayah.php"> Pendataan Wilayah</a></li> 
                        <li><a href="Petani.php">Pendataan Petani</a></li>
                        <li><a href="KelompokTani.php">Pendataan Kelompok Tani</a></li>
                        <li class="active"><a href="StrukturOrganisasi.php">Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetani.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Petani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikWilayah.php"> Berdasarkan Wilayah</a></li>
                        <li><a href="#"> Berdasarkan Agama</a>
                          <li>
                              <a href="SumarryTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Usia</a>
                          <li>
                              <a href="SumarryTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Tk. Pendidikan</a>
                          <li>
                              <a href="SumarryTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Status Petani</a>
                          <li>
                                <a href="SumarryTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikKelTaniWilayah.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_Tani.php"> Detail Anggota Kelompok Tani</a></li> 
                        <li><a href="StatistikKelompokTani.php"> Statistik Kelompok Tani</a></li>     
                    </ul>
                </li>
                       </div><!-- /#sidebar-wrapper -->
        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="design/side_menu.js"></script>

<body>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Pendataan Struktur Organisasi</h1>
                        <a class="fa fa-2x fa-list" href="DataStrukturOrganisasi.php">  Daftar Struktur Organisasi</a>
                        <br>
                        <br>
                    </div>
                </div>
            
        <div class="row">
              
        </div>
              </br>

                <div class="row">
                    <div class="col-md-7">
                        <form role="form" action="SimpanStrukturOrganisasi.php" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label class="control-label" for="exampleInputEmail1">Id Kelompok Tani *</label>
                                        <input class="form-control autoIDKelTani" name="id_kelompoktani" id="id_kelompoktani" placeholder="Id Kelompok Tani" 
                                        type="text" maxlength="10" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label class="control-label" for="exampleInputPassword1">Tanggal Awal Jabatan *</label>
                                        <input class="form-control" name="tgl_awal" type="date" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">Tanggal Akhir Jabatan *</label>
                                            <input class="form-control" name="tgl_selesai" type="date" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label class="control-label">Nama Ketua *</label>
                                        <input class="form-control" name="nama_ketua" type="text" maxlength="100" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Telepon Ketua *</label>
                                        <input class="form-control" name="telpon_ketua" type="text" maxlength="20" required>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label class="control-label">Nama Wakil Ketua *</label>
                                        <input class="form-control" name="nama_wakilketua" type="text" maxlength="100" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Telepon Wakil Ketua *</label>
                                        <input class="form-control" name="telpon_wakilketua" type="text" maxlength="20" required>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label class="control-label">Nama Sekretaris *</label>
                                        <input class="form-control" name="nama_sekretaris" type="text" maxlength="100" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Telepon Sekretaris *</label>
                                        <input class="form-control" name="telpon_sekretaris" type="text" maxlength="20" required>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label class="control-label">Nama Bendahara *</label>
                                        <input class="form-control" name="nama_bendahara" type="text" maxlength="100" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Telepon Bendahara *</label>
                                        <input class="form-control" name="telpon_bendahara" type="text" maxlength="20" required>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label class="control-label">Scan Struktur Organisasi *</label>
                                        <input type="file" name="fileToUpload1" required>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label class="control-label">Scan Susunan Pengurus *</label>
                                        <input type="file" name="fileToUpload2" required>
                                    </div>
                                </div>
                            </div>
                            </br>
                            </br>
                            
                                <input class="btn btn-primary btn-lg" type="submit" value="Simpan">
                            <br/>
                        </form>
                    </div>
                </div>

</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
var sourceIDKelTani = [];

<?php
$koneksi = include("koneksi.php");
$hasil = mysqli_query($koneksi, "SELECT ID_Kelompok_Tani FROM master_kel_tani ORDER BY ID_Kelompok_Tani ASC");
while ($row = mysqli_fetch_array($hasil))

{
    ?>
    sourceIDKelTani.push("<?php echo $row["ID_Kelompok_Tani"]; ?>");
    <?php
}
?>

$(function() {
    $(".autoIDKelTani").autocomplete({
        source: function (request, response) {
            response($.ui.autocomplete.filter(sourceIDKelTani, request.term));
        }
    });
});
</script>