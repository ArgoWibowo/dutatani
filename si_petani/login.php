<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        
                          </ul>
                        </li>
                      </ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Wilayah.php"> Pendataan Wilayah</a></li> 
                        <li><a href="PetaniUser.php">Pendataan Petani</a></li>
                        <li><a href="KelompokTani.php">Pendataan Kelompok Tani</a></li>
                        <li><a href="StrukturOrganisasi.php">Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetani.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Petani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikWilayah.php"> Berdasarkan Wilayah</a></li>
                        <li><a href="#"> Berdasarkan Agama</a>
                          <li>
                              <a href="SumarryTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Usia</a>
                          <li>
                              <a href="SumarryTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Tk. Pendidikan</a>
                          <li>
                              <a href="SumarryTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Status Petani</a>
                          <li>
                                <a href="SumarryTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikKelTaniWilayah.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_Tani.php"> Detail Anggota Kelompok Tani</a></li>
                        <li><a href="StatistikKelompokTani_User.php"> Statistik Kelompok Tani</a></li>
                    </ul>
                </li>
          </div><!-- /#sidebar-wrapper -->
        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="design/side_menu.js"></script>

<body onload="<?php if (@$_GET['pesan'] != "") { echo "alert('$_GET[pesan]');"; } ?>">
                             <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Login</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <form role="form" action="login_user.php" enctype="multipart/form-data" method="post">
                                                 <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label" >ID User</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control" name="id_user" placeholder="ID User" required>
                                                    </div>
                                                </div>
                                                <br/>
                                                <p></p>
                                                <br>
                                                 <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label" >Password</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                                                    </div>
                                                </div>
                                                <br />
                                                <input class="btn btn-primary btn-lg" type="submit" value="Simpan">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--konten-->
                    </div>
                </div>
        <script src="design/side_menu.js"></script>
        <!--end wraper-->
</body>
</html>