<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADP")
  {
    header("Location:login.php?pesan=khusus untuk admin & petani");
  }
  //select DATEDIFF(NOW(), Tanggal_Lahir) / 365.25 as umur FROM master_petani
  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
        <style type="text/css">
${demo.css}
        </style>
     
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
    <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
    ?>
        <li style="background-color: #34A853"><a href="Homei.php">Selamat Datang <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li style="background-color: #34A853"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
     </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
               <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Wilayah.php"> Pendataan Wilayah</a></li> 
                        <li><a href="Petani.php">Pendataan Petani</a></li>
                        <li><a href="KelompokTani.php">Pendataan Kelompok Tani</a></li>
                        <li><a href="StrukturOrganisasi.php">Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetani.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Petani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikWilayah.php"> Berdasarkan Wilayah</a></li>
                        <li><a href="#"> Berdasarkan Agama</a>
                          <li>
                              <a href="SumarryTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Usia</a>
                          <li>
                              <a href="SumarryTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Tk. Pendidikan</a>
                          <li>
                              <a href="SumarryTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Status Petani</a>
                          <li>
                                <a href="SumarryTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li class='active'>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikKelTaniWilayah.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <li class="active"><a href="SumarryKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a></li>
                                <a href="GrafikKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_Tani.php"> Detail Anggota Kelompok Tani</a></li>
                        <li><a href="StatistikKelompokTani.php"> Statistik Kelompok Tani</a></li>      
                    </ul>
                </li>
        </div><!-- /#sidebar-wrapper -->

        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="design/side_menu.js"></script>

<body>
    
            <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Informasi Kelompok Tani</h1>
                        <h4>Berdasarkan Perkembangan Kelompok Tani</h4>
                        <br>
                    </div>
                    <div style="margin-bottom:15px;" align="center">
                        <form method="post" action="">
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-4"><input type="radio" name="pilih" value="semua"  <?php if ((@$_POST['pilih'] == '') || (@$_POST['pilih'] == 'semua')) { echo " checked"; } ?> /> Seluruh Wilayah</label></div>
                                    <div class="col-md-6">
                                        
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="provinsi"  <?php if ((@$_POST['pilih'] == 'provinsi') || (@$_POST['pilih'] == 'provinsi')) { echo " checked"; } ?> /> Provinsi</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control"  name="provinsi">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from provinsi ORDER BY Nama_Provinsi");
                                            $no = 1;
                                            while($prov = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $prov['Nama_Provinsi']; ?>" <?php if ( @$_POST['provinsi'] == $prov['Nama_Provinsi']) { echo " selected"; } ?>><?php echo $prov['Nama_Provinsi']; ?></option>
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="kabupaten"  <?php if ((@$_POST['pilih'] == 'kabupaten')) { echo " checked"; } ?> /> Kabupaten</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control"  name="kabupaten">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from kabupaten ORDER BY nama_provinsi, nama_kabupaten");
                                            $no = 1;
                                            while($kab = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $kab['Nama_Kabupaten']; ?>" <?php if ( @$_POST['kabupaten'] == $kab['Nama_Kabupaten']) { echo " selected"; } ?>><?php echo $kab['Nama_Kabupaten']; ?> - <?php echo $kab['Nama_Provinsi']; ?></option>
                                        
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="kecamatan" <?php if (@$_POST['pilih'] == 'kecamatan') { echo " checked"; } ?> /> Kecamatan</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control"  name="kecamatan">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from kecamatan ORDER BY nama_provinsi, nama_kabupaten, nama_kecamatan");
                                            $no = 1;
                                            while($kab = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $kab['Nama_Kecamatan']; ?>" <?php if ( @$_POST['kecamatan'] == $kab['Nama_Kecamatan']) { echo " selected"; } ?>><?php echo $kab['Nama_Kecamatan']; ?> - <?php echo $kab['Nama_Kabupaten']; ?> - <?php echo $kab['Nama_Provinsi']; ?></option>
                                        
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row" align="left">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="col-md-3"><input type="radio" name="pilih" value="desa" <?php if (@$_POST['pilih'] == 'desa') { echo " checked"; } ?> /> Desa</label></div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="desa">
                                        <?php
                                            $koneksi = include("koneksi.php");                      
                                            $hasil = mysqli_query($koneksi,"select * from kelurahan_desa ORDER BY nama_provinsi, nama_kabupaten, nama_kecamatan, nama_desa");
                                            //echo "select * from kelurahan_desa ORDER BY nama_provinsi, nama_kabupaten, nama_kecamatan, nama_desa";
                                            $no = 1;
                                            while($kec = mysqli_fetch_array($hasil)){
                                        ?> 
                                            <option value="<?php echo $kec['Nama_Desa']; ?>" <?php if (@$_POST['desa'] == $kec['Nama_Desa']) { echo " selected"; } ?>><?php echo $kec['Nama_Desa']; ?> - <?php echo $kec['Nama_Kecamatan']; ?> - <?php echo $kec['Nama_Kabupaten']; ?></option>
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <input type="submit" name="cari" value="Tampil" class="btn-md btn-primary" style="padding:3px;" margin="6px;" width="50px;"  />
                         </form>
                        
<?php
if (@$_POST['pilih'] == "provinsi") {

    ?>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8"> 
                           <table class="table"> <!-- style="width: 1000px;"> -->
                            <thead>
                            <div style="margin-bottom:15px;" align="center">
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-lg btn-primary" onclick="window.location.href='DetailKelTaniPerkemb_Prov.php?provinsi=<?php echo $_POST['provinsi'];?>'"> Cetak Daftar</button>
                                <br>
                                <br>
                            </div>
                              <tr>
                                   <th>No</th>
                                   <th>Wilayah</th>
                                   <th>Sebelum 2012</th>
                                   <th>2012 - 2014</th>
                                   <th>2015 - 2017</th>
                                   <th>Sesudah 2017</th>                             
                            </tr>
                             </thead>
                                            
                                            <tbody>
                                            <?php
                                                $koneksi = include("koneksi.php");                      
                                                $hasil = mysqli_query($koneksi,"select * from provinsi WHERE nama_provinsi = '$_POST[provinsi]' order by Nama_Provinsi");
                                                $no = 1;
                                                $total2012=0;
                                                $total2014=0;
                                                $total2017=0;
                                                $totallbh2017=0;
                                                while($provinsi = mysqli_fetch_array($hasil)){
                                            ?>  
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td> 
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                            echo $kabupaten['Nama_Kabupaten'].'<br/>';
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;".$provinsi['Nama_Provinsi']?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2012");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2012=$total2012+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2012");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2015");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2014=$total2014+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2015");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2017");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2017=$total2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi, "select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2017");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2020");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $totallbh2017=$totallbh2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2020");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>
                                                </tr>
                                                <?php
                              
                                                }

                                                ?>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>Total
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2012;
                                                    ?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2014;
                                                    ?>
                                                    </td>
                                                    <td>
                                                  <?php
                                                    echo "&nbsp;".$total2017;
                                                    ?>
                                                    </td>
                                                    <td>
                                                  <?php
                                                    echo "&nbsp;".$totallbh2017;
                                                    ?>
                                                    </td>
                                                </tr>
                                        </tbody>
                                </table>
                            </div>

<?php   
} else if (@$_POST['pilih'] == "kabupaten") {
    
    ?>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8"> 
                           <table class="table">
                             <thead>
                            <div style="margin-bottom:15px;" align="center">
                              
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-lg btn-primary" onclick="window.location.href='DetailKelTaniPerkemb_Kab.php?kabupaten=<?php echo $_POST['kabupaten'];?>'"> Cetak Daftar</button>
                                <br>
                                <br>
                            </div>

                            <tr>
                                   <th>No</th>
                                   <th>Wilayah</th>
                                   <th>Sebelum 2012</th>
                                   <th>2012 - 2014</th>
                                   <th>2015 - 2017</th>
                                   <th>Sesudah 2017</th>
                                  
                             </tr>
                             </thead>
                                            
                                            <tbody>
                                            <?php
                                                $koneksi = include("koneksi.php");                      
                                                $hasil = mysqli_query($koneksi,"select * from kabupaten WHERE nama_kabupaten = '$_POST[kabupaten]' order by Nama_Kabupaten");
                                                $no = 1;
                                                $total2012=0;
                                                $total2014=0;
                                                $total2017=0;
                                                $totallbh2017=0;

                                                while($provinsi = mysqli_fetch_array($hasil)){
                                            ?>  
                                                 <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kecamatan WHERE Nama_Kabupaten = '$provinsi[Nama_Kabupaten]' order by Nama_Kecamatan");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                            echo $kabupaten['Nama_Kecamatan'].'<br/>';
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;".$provinsi['Nama_Kabupaten']?>
                                                    </td>
                                                    
                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kabupaten as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kabupaten = '$provinsi[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2012");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2012=$total2012+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kecamatan WHERE Nama_Kabupaten = '$provinsi[Nama_Kabupaten]' order by nama_kecamatan");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$kabupaten[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2012");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kabupaten as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kabupaten = '$provinsi[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2015");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2014=$total2014+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kecamatan WHERE Nama_Kabupaten = '$provinsi[Nama_Kabupaten]' order by nama_kecamatan");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$kabupaten[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2015");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>


                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kabupaten as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kabupaten = '$provinsi[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2017");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                   
                                                    $total2017=$total2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kecamatan WHERE Nama_Kabupaten = '$provinsi[Nama_Kabupaten]' order by nama_kecamatan");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$kabupaten[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2017");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                   <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kabupaten as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kabupaten = '$provinsi[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2020");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $totallbh2017=$totallbh2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kecamatan WHERE Nama_Kabupaten = '$provinsi[Nama_Kabupaten]' order by nama_kecamatan");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$kabupaten[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2020");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>
                                                </tr>
                                                <?php
                              
                                                }

                                                ?>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>Total
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2012;
                                                    ?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2014;
                                                    ?>
                                                    </td>
                                                    <td>
                                                  <?php
                                                    echo "&nbsp;".$total2017;
                                                    ?>
                                                    </td>
                                                    <td>
                                                  <?php
                                                    echo "&nbsp;".$totallbh2017;
                                                    ?>
                                                    </td>
                                                </tr>
                                        </tbody>
                                </table>
                            </div>

<?php
    
} else if (@$_POST['pilih'] == "kecamatan") {
 
    ?>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8">     
                           <table class="table">
                             <thead>
                            <div style="margin-bottom:15px;" align="center">
                              
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-lg btn-primary" onclick="window.location.href='DetailKelTaniPerkemb_Kec.php?kecamatan=<?php echo $_POST['kecamatan'];?>'"> Cetak Daftar</button>
                                <br>
                                <br>
                            </div>
                              <tr>
                                   <th>No</th>
                                   <th>Wilayah</th>
                                   <th>Sebelum 2012</th>
                                   <th>2012 - 2014</th>
                                   <th>2015 - 2017</th>
                                   <th>Sesudah 2017</th>
                             </thead>
                                            
                                            <tbody>
                                            <?php
                                                $koneksi = include("koneksi.php");                      
                                                $hasil = mysqli_query($koneksi,"select * from kecamatan WHERE nama_kecamatan = '$_POST[kecamatan]' order by Nama_Kecamatan");
                                                $no = 1;
                                                $total2012=0;
                                                $total2014=0;
                                                $total2017=0;
                                                $totallbh2017=0;
                                                while($provinsi = mysqli_fetch_array($hasil)){
                                            ?>  
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td> 
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kelurahan_desa WHERE Nama_Kecamatan = '$provinsi[Nama_Kecamatan]' order by Nama_Desa");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                            echo $kabupaten['Nama_Desa'].'<br/>';
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;".$provinsi['Nama_Kecamatan']?>
                                                    </td>
                                                    
                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kecamatan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$provinsi[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2012");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2012=$total2012+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kelurahan_desa WHERE Nama_Kecamatan = '$provinsi[Nama_Kecamatan]' order by nama_desa");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$kabupaten[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2012 ");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                   <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kecamatan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$provinsi[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2015");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2014=$total2014+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kelurahan_desa WHERE Nama_Kecamatan = '$provinsi[Nama_Kecamatan]' order by nama_desa");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$kabupaten[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2015");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kecamatan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$provinsi[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2017");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2017=$total2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kelurahan_desa WHERE Nama_Kecamatan = '$provinsi[Nama_Kecamatan]' order by nama_desa");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$kabupaten[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2017");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                   <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT kecamatan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE kecamatan = '$provinsi[Nama_Kecamatan]' AND YEAR(Tgl_Terbentuk)<2020");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $totallbh2017=$totallbh2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kelurahan_desa WHERE Nama_Kecamatan = '$provinsi[Nama_Kecamatan]' order by nama_desa");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$kabupaten[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2020");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>
                                                 </tr>
                                                <?php
                              
                                                }

                                                ?>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>Total
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2012;
                                                    ?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2014;
                                                    ?>
                                                    </td>
                                                    <td>
                                                  <?php
                                                    echo "&nbsp;".$total2017;
                                                    ?>
                                                    </td>
                                                    <td>
                                                  <?php
                                                    echo "&nbsp;".$totallbh2017;
                                                    ?>
                                                    </td>
                                                </tr>
                                        </tbody>
                                </table>    
                            </div>

<?php
} else if (@$_POST['pilih'] == "desa") {
   
    ?>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8"> 
                           <table class="table">
                             <thead>
                            <div style="margin-bottom:15px;" align="center">
                              
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-lg btn-primary" onclick="window.location.href='DetailKelTaniPerkemb_Desa.php?desa=<?php echo $_POST['desa'];?>'"> Cetak Daftar</button>
                                <br>
                                <br>
                            </div>
                              <tr>
                                   <th>No</th>
                                   <th>Wilayah</th>
                                   <th>Sebelum 2012</th>
                                   <th>2012 - 2014</th>
                                   <th>2015 - 2017</th>
                                   <th>Sesudah 2017</th>
                              </tr>
                             </thead>
                                            
                                            <tbody>
                                            <?php
                                                $koneksi = include("koneksi.php");                      
                                                $hasil = mysqli_query($koneksi,"select * from kelurahan_desa WHERE nama_desa = '$_POST[desa]' order by Nama_Desa");
                                                $no = 1;
                                                $total2012=0;
                                                $total2014=0;
                                                $total2017=0;
                                                $totallbh2017=0;
                                                while($provinsi = mysqli_fetch_array($hasil)){
                                            ?>  
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $provinsi['Nama_Desa']?>
                                                   
                                                    </td>
                                                    
                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT desa_kelurahan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$provinsi[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2012");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    echo $data2['jumlah'];
                                                    $total2012=$total2012+$data2['jumlah'];
                                                    ?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT desa_kelurahan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$provinsi[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2015");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    echo $data2['jumlah'];
                                                    $total2014=$total2014+$data2['jumlah'];
                                                    ?>
                                                    </td>
                                                     <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT desa_kelurahan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$provinsi[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2017");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    echo $data2['jumlah'];
                                                    $total2017=$total2017+$data2['jumlah'];
                                                    ?>
                                                    </td>

                                                   <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT desa_kelurahan as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE desa_kelurahan = '$provinsi[Nama_Desa]' AND YEAR(Tgl_Terbentuk)<2020");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    echo $data2['jumlah'];
                                                    $totallbh2017=$totallbh2017+$data2['jumlah'];
                                                    ?>
                                                    </td>
                                                </tr>

                                    <?php
                              
                                    }

                                    ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>Total
                                        </td>
                                        <td>
                                       <?php
                                        echo $total2012;
                                        ?>
                                        </td>
                                        <td>
                                       <?php
                                        echo $total2014;
                                        ?>
                                        </td>
                                        <td>
                                      <?php
                                        echo $total2017;
                                        ?>
                                        </td>
                                        <td>
                                      <?php
                                        echo $totallbh2017;
                                        ?>
                                        </td>
                                    </tr>
                            </tbody>
                    </table>
                </div>

<?php
} else {
  
    ?>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8"> 

                           <table class="table" >
                             <thead>
                            <div style="margin-bottom:15px;" align="center">
                              
                            </div>
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-lg btn-primary" onclick="window.location.href='DetailKelTaniPerkemb_Seluruh.php'"> Cetak Daftar</button>
                                <br>
                                <br>
                            </div>
                              <tr>
                                   <th>No</th>
                                   <th>Wilayah</th>
                                   <th>Sebelum 2012</th>
                                   <th>2012 - 2014</th>
                                   <th>2015 - 2017</th>
                                   <th>Sesudah 2017</th>
                              </tr>
                             </thead>
                                            
                                            <tbody>
                                            <?php
                                                $koneksi = include("koneksi.php");                      
                                                $hasil = mysqli_query($koneksi,"select * from provinsi order by Nama_Provinsi");
                                                $no = 1;
                                                $total2012=0;
                                                $total2014=0;
                                                $total2017=0;
                                                $totallbh2017=0;
                                                while($provinsi = mysqli_fetch_array($hasil)){
                                            ?>  
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                            echo $kabupaten['Nama_Kabupaten'].'<br/>';
                                                        }
                                                    ?>
                                                    <?php echo  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$provinsi['Nama_Provinsi']?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2012 ");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2012=$total2012+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                        $hasil4 = mysqli_query($koneksi,"SELECT count(
                                                        ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2012 ");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(
                                                ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2015");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2014=$total2014+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(
                                                    ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2015");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(
                                                ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2017");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $total2017=$total2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(
                                                ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2017");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>

                                                    <td>
                                                   <?php
                                                   $hasil2 = mysqli_query($koneksi,"SELECT provinsi as nama, count(
                                                ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Provinsi = '$provinsi[Nama_Provinsi]' AND YEAR(Tgl_Terbentuk)<2020");
                                                    $data2 = mysqli_fetch_array($hasil2);
                                                    
                                                    $totallbh2017=$totallbh2017+$data2['jumlah'];
                                                    ?>
                                                    <?php
                                                        $hasil3 = mysqli_query($koneksi,"select * from kabupaten WHERE Nama_Provinsi = '$provinsi[Nama_Provinsi]' order by Nama_Kabupaten");
                                                        while($kabupaten = mysqli_fetch_array($hasil3)){
                                                        
                                                            $hasil4 = mysqli_query($koneksi,"SELECT count(
                                                ID_Kelompok_Tani) AS jumlah FROM `master_kel_tani` WHERE Kabupaten = '$kabupaten[Nama_Kabupaten]' AND YEAR(Tgl_Terbentuk)<2020");
                                                            $data4 = mysqli_fetch_array($hasil4);
                                                                echo $data4['jumlah'].'<br/>';
                                                        
                                                        }
                                                    ?>
                                                    <?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data2['jumlah'];?>
                                                    </td>
                                            </tr>
                                            <?php
                              
                                            }

                                            ?>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>Total
                                                    </td>
                                                    <td>
                                                     <?php
                                                    echo "&nbsp;".$total2012;
                                                    ?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2014;
                                                    ?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$total2017;
                                                    ?>
                                                    </td>
                                                    <td>
                                                   <?php
                                                    echo "&nbsp;".$totallbh2017;
                                                    ?>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                                      <?php
} 
?>
                </div>
                     
                
            </div>
</script>
</body>
</html>