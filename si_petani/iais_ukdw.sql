-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2018 at 02:22 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iais_ukdw`
--

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Bantul', 'Daerah Istimewa Yogyakarta'),
('Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Sleman', 'Daerah Istimewa Yogyakarta'),
('Kebumen', 'Jawa Tengah'),
('Temanggung', 'Jawa Tengah');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `Nama_Kecamatan` varchar(100) NOT NULL,
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`Nama_Kecamatan`, `Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Bulu', 'Temanggung', 'Jawa Tengah'),
('Dlingo', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Kandangan', 'Temanggung', 'Jawa Tengah'),
('kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Kedu', 'Temanggung', 'Jawa Tengah'),
('Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Panggang', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Pengasih', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Sempor', 'Kebumen', 'Jawa Tengah'),
('Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Turi', 'Sleman', 'Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan_desa`
--

CREATE TABLE `kelurahan_desa` (
  `Nama_Desa` varchar(100) NOT NULL,
  `Nama_Kecamatan` varchar(100) NOT NULL,
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan_desa`
--

INSERT INTO `kelurahan_desa` (`Nama_Desa`, `Nama_Kecamatan`, `Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Jagalan', 'Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Singosaren', 'Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Ngimbrang', 'Bulu', 'Temanggung', 'Jawa Tengah'),
('Terong', 'Dlingo', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Klitren', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Terban', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Ngupasan', 'Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Prawirodirjan', 'Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Malebo', 'Kandangan', 'Temanggung', 'Jawa Tengah'),
('Bangunjiwo', 'kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Tirtonirmolo', 'kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Gondang Wayang', 'Kedu', 'Temanggung', 'Jawa Tengah'),
('Tawangsari', 'Kedu', 'Temanggung', 'Jawa Tengah'),
('Donoharjo', 'Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Minomartani', 'Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Beji', 'Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Watusigar', 'Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Hargobingangun', 'Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Pakembingangun', 'Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Girimulyo', 'Panggang', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Karang Sari', 'Pengasih', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Kalitekuk', 'Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('kemejing', 'Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Selokerto', 'Sempor', 'Kebumen', 'Jawa Tengah'),
('Kaliagung', 'Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Salamrejo', 'Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Palihan', 'Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Sindutan', 'Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Wonokerto', 'Turi', 'Sleman', 'Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `log_aktifitas_pelatihan`
--

CREATE TABLE `log_aktifitas_pelatihan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_aktifitas_pelayanan`
--

CREATE TABLE `log_aktifitas_pelayanan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan_Pelayanan` varchar(10) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_biaya_keluar`
--

CREATE TABLE `log_biaya_keluar` (
  `ID_Spesies` varchar(10) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL,
  `ID_Aktivitas` varchar(10) NOT NULL,
  `ID_Petani` varchar(10) NOT NULL,
  `Tahun_Biaya` int(4) DEFAULT NULL,
  `ID_Biaya` varchar(10) NOT NULL,
  `Jumlah_Satuan` int(4) DEFAULT NULL,
  `Harga_Satuan` int(10) DEFAULT NULL,
  `Tgl_Pengeluaran` date DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_penawaran_prod`
--

CREATE TABLE `log_penawaran_prod` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_permintaan`
--

CREATE TABLE `log_permintaan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan` varchar(15) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_user_trans`
--

CREATE TABLE `log_user_trans` (
  `ID_User` varchar(10) NOT NULL,
  `Tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID_Aktivitas` varchar(10) NOT NULL,
  `Keterangan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_aktifitas_spesies`
--

CREATE TABLE `master_aktifitas_spesies` (
  `ID_Aktivitas` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Periode_Waktu` int(3) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_aktivitas`
--

CREATE TABLE `master_aktivitas` (
  `ID_Aktivitas` varchar(10) NOT NULL,
  `Nama_Aktivitas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_alat_tani`
--

CREATE TABLE `master_alat_tani` (
  `ID_Alat` varchar(10) NOT NULL,
  `Nama_Alat` varchar(50) NOT NULL,
  `Deskripsi_Alat` varchar(200) DEFAULT NULL,
  `Spesifikasi` varchar(200) DEFAULT NULL,
  `Harga_Terendah` int(12) DEFAULT NULL,
  `Harga_Tertinggi` int(12) DEFAULT NULL,
  `Fungsi` varchar(200) DEFAULT NULL,
  `ID_Kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_alat_tani`
--

INSERT INTO `master_alat_tani` (`ID_Alat`, `Nama_Alat`, `Deskripsi_Alat`, `Spesifikasi`, `Harga_Terendah`, `Harga_Tertinggi`, `Fungsi`, `ID_Kategori`) VALUES
('1', 'hhhdj', 'jjj', 'akaka', 2929, 3939, 'akak', '1'),
('2', 'kon', 'shshhs', 'jssjs', 283, 454545, 'ajajsj', '1');

-- --------------------------------------------------------

--
-- Table structure for table `master_bahan_tani`
--

CREATE TABLE `master_bahan_tani` (
  `ID_Bahan` varchar(10) NOT NULL,
  `Nama_Bahan` varchar(50) NOT NULL,
  `Deskripsi_Bahan` varchar(200) DEFAULT NULL,
  `Spesifikasi_Bahan` varchar(200) DEFAULT NULL,
  `Harga_Terendah` int(12) DEFAULT NULL,
  `Harga_Tertinggi` int(12) DEFAULT NULL,
  `Fungsi_Bahan` varchar(200) DEFAULT NULL,
  `Jenis_Bahan` varchar(100) DEFAULT NULL,
  `ID_Kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_detail_materi_ajar`
--

CREATE TABLE `master_detail_materi_ajar` (
  `ID_Materi_Ajar` varchar(10) NOT NULL,
  `ID_Sub_materi` varchar(10) NOT NULL,
  `Deskripsi_sub` varchar(200) NOT NULL,
  `file_materi` varchar(200) NOT NULL,
  `link_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_fasilitator`
--

CREATE TABLE `master_fasilitator` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Fasilitator` varchar(50) NOT NULL,
  `Alamat_Fasilitator` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Telpon` varchar(15) DEFAULT NULL,
  `Pendidikan_Terakhir` varchar(3) DEFAULT NULL,
  `Jurusan` varchar(30) DEFAULT NULL,
  `Kompetensi_Keahlian` varchar(200) DEFAULT NULL,
  `Riwayat_Pendidikan` varchar(200) DEFAULT NULL,
  `Pengalaman_Kerja` varchar(200) DEFAULT NULL,
  `Foto` varchar(200) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_jenis_sup`
--

CREATE TABLE `master_jenis_sup` (
  `ID_Jenis_Sup` varchar(10) NOT NULL,
  `Nama_Jenis_Sup` varchar(50) NOT NULL,
  `Deskripsi_Jenis_Sup` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_kategori`
--

CREATE TABLE `master_kategori` (
  `ID_Kategori` char(3) NOT NULL,
  `Nama_Kategori` varchar(40) NOT NULL,
  `Deskripsi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kategori`
--

INSERT INTO `master_kategori` (`ID_Kategori`, `Nama_Kategori`, `Deskripsi`) VALUES
('ADK', 'Admin Kelompok Tani', 'Admin Kelompok Tani'),
('ADP', 'Admin Petani', 'Admin Web'),
('PET', 'Petani', 'Anggota Petani');

-- --------------------------------------------------------

--
-- Table structure for table `master_kategori_alatbahan`
--

CREATE TABLE `master_kategori_alatbahan` (
  `ID_Kategori` varchar(10) NOT NULL,
  `Jenis_kategori` varchar(1) NOT NULL,
  `Nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kategori_alatbahan`
--

INSERT INTO `master_kategori_alatbahan` (`ID_Kategori`, `Jenis_kategori`, `Nama_kategori`) VALUES
('1', '1', 'bermesina'),
('2', '2', 'manual'),
('4', '4', 'kambing');

-- --------------------------------------------------------

--
-- Table structure for table `master_kel_tani`
--

CREATE TABLE `master_kel_tani` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `Nama_Kelompok_Tani` varchar(50) NOT NULL,
  `Alamat_Sekretariat` varchar(50) NOT NULL,
  `Kabupaten` varchar(50) NOT NULL,
  `Kecamatan` varchar(50) NOT NULL,
  `Provinsi` varchar(50) NOT NULL,
  `Desa_Kelurahan` varchar(50) NOT NULL,
  `Deskripsi` varchar(200) NOT NULL,
  `Foto1` varchar(200) NOT NULL,
  `Foto2` varchar(200) DEFAULT NULL,
  `Legalitas` varchar(100) NOT NULL,
  `Bukti_Legalitas` varchar(200) NOT NULL,
  `Kontak_Person` varchar(50) NOT NULL,
  `Nomor_Telpon` varchar(15) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Tgl_Terbentuk` date NOT NULL,
  `Email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kel_tani`
--

INSERT INTO `master_kel_tani` (`ID_Kelompok_Tani`, `Nama_Kelompok_Tani`, `Alamat_Sekretariat`, `Kabupaten`, `Kecamatan`, `Provinsi`, `Desa_Kelurahan`, `Deskripsi`, `Foto1`, `Foto2`, `Legalitas`, `Bukti_Legalitas`, `Kontak_Person`, `Nomor_Telpon`, `ID_User`, `Tgl_Terbentuk`, `Email`) VALUES
('KelTani001', 'Ngudi Lestari', 'Jalan Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', ' terletak di keamatan panggan dusun Glaheng', 'KelTani001.jpg', NULL, '00000000000', 'KelTani001.jpg', 'Roehmad', '081228953212', 'AdmKel0001', '2015-12-09', 'RoehmadSuparto@yahoo.com'),
('KelTani002', 'Ngudi Rejeki', 'Jl Gedang Sari', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '-', 'KelTani002.jpg', NULL, '00000000000', 'KelTani002.jpg', 'Sumarno', '0878839033892', 'AdmKel0002', '2012-06-02', 'sumarno_tungu@gmail.com'),
('KelTani003', 'Kelompok Perempuan Wonorojo', 'Imorejo', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '-', 'KelTani003.jpg', NULL, '00000000000', 'KelTani003.jpg', 'Ida Kristiana', '000000', 'AdmKel0003', '2014-07-15', 'belumada@belumada.com'),
('KelTani004', 'Kebokuning', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '-', 'KelTani004.jpg', NULL, '00000000000', 'KelTani004.jpg', 'Rubikem', '087838297451', 'AdmKel0004', '2013-05-05', 'belumada@belumada.com'),
('KelTani005', 'Sendang Rejeki', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '-', 'KelTani005.jpg', NULL, '00000000000', 'KelTani005.jpg', 'Yunaliyati', '000000', 'AdmKel0005', '2010-02-09', 'belumada@belumada.com'),
('KelTani006', 'kelompok tani', 'jalan dami', 'Sleman', 'Ngaglik', 'Daerah Istimewa Yogyakarta', 'Donoharjo', 'kelompok baru', 'KelTani006.jpg', NULL, '00000000000', 'KelTani006.jpg', 'Malvin', '08121584000', 'Petani0040', '2017-03-16', 'christopher.malvin@gmail.com'),
('KelTani007', 'Sari Bumi', 'Kedu Tawangsari Rt.03 Rw.08', 'Temanggung', 'Kedu', 'Jawa Tengah', 'Tawangsari', 'Menjual bibit tembakau super', 'KelTani007.jpg', NULL, '00000000000', 'KelTani007.jpg', 'Siswanto', '086576777755', 'AdmKel0007', '2016-01-10', 'Saribumi@gmail.com'),
('KelTani008', 'Lambang Bibit', 'Kalipan Timur Rt.02 Rw. 05', 'Temanggung', 'Kedu', 'Jawa Tengah', 'Gondang Wayang', 'Pusat Penjualan segala macam bibit di kabupaten temanggung', 'KelTani008.jpg', NULL, '0000000000000000', 'KelTani008.jpg', 'Haryanto', '085225224558', 'AdmKel0008', '2007-05-14', 'lambangbibit@gmail.com'),
('KelTani009', 'Dewi Sri III', 'Ketuwon Wetan', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Merupakan kelompok tani menerapkan sistem gotong royong', 'KelTani009.jpg', NULL, 'AHU-0026126.AH.02.07', 'KelTani009.jpg', 'Nurhadi Mahfuz', '081832442427', 'AdmKel0009', '2000-08-20', 'dewisritiga@gmail.com'),
('KelTani010', 'Makmur', 'Gokerten Rt.2 Rw.5', 'Temanggung', 'Bulu', 'Jawa Tengah', 'Ngimbrang', 'Sub Kelompok tani Lohjinawi', 'KelTani010.jpg', NULL, '0000000000', 'KelTani010.jpg', 'Sarwoto', '0853438434', 'AdmKel0010', '2015-02-09', 'makmur@gmail.com'),
('KelTani011', 'Manunggal', 'Karang Wetan Rt.03/Rw.03', 'Temanggung', 'Kedu', 'Jawa Tengah', 'Tawangsari', 'sub kelompok tani dari lohjinawi', 'KelTani011.jpg', NULL, '000000000', 'KelTani011.jpg', 'Adi Irawan', '08757372372', 'AdmKel0011', '2016-12-01', 'manunggal@gmail.com'),
('KelTani012', 'Lohjinawi', 'Jetis Rt.03 Rw0.1', 'Temanggung', 'Bulu', 'Jawa Tengah', 'Ngimbrang', 'Merupakan induk kelompok tani desa ngimbrang ', 'KelTani012.jpg', NULL, '000000000000', 'KelTani012.jpg', 'Aris Listiyanto', '08565906596', 'AdmKel0012', '2015-03-10', 'lohjinawi@2gmail.com'),
('KelTani013', 'Dewi Sri I', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'kelompok tani baru', 'KelTani013.jpg', NULL, '0000000000', 'KelTani013.jpg', 'Sochibah', '081677634342', 'AdmKel0013', '2000-01-05', 'dewisrisatu@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `master_kode_biaya`
--

CREATE TABLE `master_kode_biaya` (
  `ID_Biaya` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Uraian_Biaya` varchar(100) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga_satuan` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_materi_ajar`
--

CREATE TABLE `master_materi_ajar` (
  `ID_Materi_Ajar` varchar(10) NOT NULL,
  `Nama_materi` varchar(100) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `deskripsi_materi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_materi_pelatihan`
--

CREATE TABLE `master_materi_pelatihan` (
  `ID_Materi` varchar(10) NOT NULL,
  `Nama_Materi` varchar(50) NOT NULL,
  `Deskripsi_Materi` varchar(200) DEFAULT NULL,
  `Materi_Sebelumnya` varchar(10) DEFAULT NULL,
  `Kebutuhan_Spesifikasi` varchar(200) DEFAULT NULL,
  `Level` varchar(20) DEFAULT NULL,
  `Jumlah_Jam` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_morf_tanaman`
--

CREATE TABLE `master_morf_tanaman` (
  `ID_Morfologi` varchar(10) NOT NULL,
  `Nama_Morfologi_Tanaman` varchar(50) DEFAULT NULL,
  `Nama_Divisi` varchar(10) DEFAULT NULL,
  `Nama_Sub_Divisi` varchar(10) DEFAULT NULL,
  `Nama_Ordo` varchar(10) DEFAULT NULL,
  `Nama_Famili` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_org_unit`
--

CREATE TABLE `master_org_unit` (
  `Org_Unit` varchar(10) NOT NULL,
  `Nama_Organisasi` varchar(100) NOT NULL,
  `Tingkatan` varchar(30) NOT NULL,
  `Org_Unit_Atasan` varchar(10) NOT NULL,
  `Pelayanan` tinyint(1) DEFAULT NULL,
  `Pendaftaran_Anggota` tinyint(1) DEFAULT NULL,
  `Pelatihan` tinyint(1) DEFAULT NULL,
  `Konsultasi` tinyint(1) DEFAULT NULL,
  `Penawaran` tinyint(1) DEFAULT NULL,
  `Permintaan` tinyint(1) DEFAULT NULL,
  `Memberi_Informasi` tinyint(1) DEFAULT NULL,
  `Meminta_Informasi` tinyint(1) DEFAULT NULL,
  `Berbagi_Informasi` tinyint(1) DEFAULT NULL,
  `Data_Spesifik_Anggota` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_pelayanan`
--

CREATE TABLE `master_pelayanan` (
  `ID_Pelayanan` varchar(10) NOT NULL,
  `Nama_Pelayanan` varchar(50) NOT NULL,
  `Deskripsi_Pelayanan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_petani`
--

CREATE TABLE `master_petani` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Petani` varchar(50) NOT NULL,
  `Alamat_Petani` varchar(50) NOT NULL,
  `Kabupaten` varchar(50) NOT NULL,
  `Kecamatan` varchar(50) NOT NULL,
  `Provinsi` varchar(50) NOT NULL,
  `Desa_Kelurahan` varchar(50) NOT NULL,
  `Foto` varchar(200) DEFAULT NULL,
  `Nomor_Telpon` varchar(15) NOT NULL,
  `Pendidikan_Terakhir` varchar(4) NOT NULL,
  `Jumlah_Tanggungan` int(2) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Agama` varchar(10) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Deskripsi_Keahlian` varchar(200) NOT NULL,
  `Status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_petani`
--

INSERT INTO `master_petani` (`ID_User`, `Nama_Petani`, `Alamat_Petani`, `Kabupaten`, `Kecamatan`, `Provinsi`, `Desa_Kelurahan`, `Foto`, `Nomor_Telpon`, `Pendidikan_Terakhir`, `Jumlah_Tanggungan`, `Email`, `Agama`, `Tanggal_Lahir`, `Deskripsi_Keahlian`, `Status`) VALUES
('Petani0001', 'Roehmad Suparto', 'Jalan Panggang Utama', 'Kulon Progo', 'Temon', 'Daerah Istimewa Yogyakarta', 'Sindutan', 'Petani0001.jpg', '081228953212', 'SMP', 5, 'RoehmadSuparto@yahoo.com', 'Islam', '1956-02-07', 'menanam padi, jagung, cabai', 1),
('Petani0002', 'Ngatilah', 'Jalan Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0002.jpg', '082324612253', 'SMA', 0, 'ngatilah@gmail.com', 'Islam', '1973-06-05', 'menanam padi', 1),
('Petani0003', 'Pajiyanti', 'Jalan Siluk Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0003.jpg', '000000', 'SD', 10, 'belumada@belumada.com', 'Islam', '1960-01-13', 'menanam padi', 0),
('Petani0004', 'NIsa Subekti', 'Jalan Panggang Utama', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0004.jpg', '087739773682', 'SD', 5, 'nisa@yahoo.com', 'Kristen', '1988-03-03', 'Menanam padi, jagung, kacang, singkong', 1),
('Petani0005', 'Ruswantiningsih', 'Jalan Siluk Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '081392591042', 'SMA', 3, 'ruswantiningsih@gmail.com', 'Katolik', '1970-12-09', 'menanam jagung, padi, kacang, ketela', 1),
('Petani0006', 'Mujiyo', 'Pantai Gesing km. 10', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0006.jpg', '0878839082181', 'SMP', 3, 'belumada@belumada.com', 'Islam', '1978-04-10', 'menanam jagung, padi', 1),
('Petani0007', 'Supriyati', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '000000', 'SMP', 10, 'belumada@belumada.com', 'Islam', '1979-07-11', 'menanam padi, jagung', 1),
('Petani0008', 'Giyanti', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '085328129673', 'SMA', 10, 'belumada@belumada.com', 'Islam', '1980-09-17', 'menanam padi', 1),
('Petani0009', 'Wakimin', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0009.png', '081228941305', 'SMP', 7, 'belumada@belumada.com', 'Kristen', '1971-09-29', 'menanam padi', 1),
('Petani0010', 'Wagiyem', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '000000', 'SMP', 10, 'belumada@belumada.com', 'Kristen', '1966-10-02', 'menanam padi', 1),
('Petani0011', 'Sumarno', 'Jl Gedang Sari', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0011.jpg', '0878839033892', 'SMP', 6, 'sumarno_tungu@gmail.com', 'Katolik', '1974-01-14', 'menanam padi, jagung, ketela', 1),
('Petani0012', 'Suwadiman', 'Nggesing', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '000000', 'SD', 4, 'belumada@belumada.com', 'Islam', '1965-08-13', 'menanam padi, jagung, ketela, kacang, kedelai', 1),
('Petani0013', 'Suprapto', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 2, 'belumada@belumada.com', 'Islam', '1967-04-28', 'tidak memiliki keahlian', 0),
('Petani0014', 'Amroni', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SMA', 2, 'belumada@belumada.com', 'Katolik', '1970-05-21', 'menanam padi, jagung, peternak gurami', 1),
('Petani0015', 'Siti Yuningsih', 'Kopen Becici', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0015.jpg', '085701145454', 'SMA', 0, 'yuningsih.siti@yahoo.com', 'Islam', '1969-02-18', 'menanam salak, cabe, sayuran', 1),
('Petani0016', 'Ida Krisharia', 'Imorejo', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '000000', 'SMA', 0, 'belumada@belumada.com', 'Katolik', '1972-12-23', 'tidak ada', 1),
('Petani0017', 'Riyantiningsih', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0017.jpg', '087738513253', 'SMA', 3, 'riyanti@yahoo.co.id', 'Katolik', '1975-06-17', 'menanam salak', 1),
('Petani0018', 'Ismawati', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0018.jpg', '085743974065', 'SMA', 5, 'ismawati@gmail.com', 'Kristen', '1978-08-09', 'menanam salak', 1),
('Petani0019', 'Sri Hartutik', 'Selowangsan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '000000', 'SMA', 0, 'tutik_salak@yahoo.co.id', 'Islam', '1974-11-17', 'menanam salak, padi, sayuran', 1),
('Petani0021', 'Erna Kusrini', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0021.jpg', '085643222860', 'SMA', 0, 'erna_kusrini@yahoo.com', 'Kristen', '1976-10-20', 'menanam salak', 1),
('Petani0022', 'Sri Haryanti', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '000000', 'SMA', 0, 'belumada@belumada.com', 'Islam', '1977-02-08', 'tidak ada', 0),
('Petani0023', 'Kristiningsih', 'Sempu', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0023.jpg', '000000', 'SMA', 3, 'kristiningsih@gmail.com', 'Kristen', '1977-11-16', 'tidak ada', 0),
('Petani0024', 'Nursiyah', 'Sempu', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '0817273103', 'SMA', 4, 'nursiyah@gmail.com', 'Islam', '1974-07-11', 'menanam salak', 1),
('Petani0025', 'Nur Kahyatun', 'Imorejo', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0025.png', '085799382768', 'D2', 3, 'khayatun@gmail.com', 'Islam', '1969-09-10', 'menanam salak', 1),
('Petani0026', 'Rubikem', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '087838297451', 'SMP', 2, 'belumada@belumada.com', 'Islam', '1964-07-15', 'tidak memiliki keahlian', 0),
('Petani0027', 'Siti Fatimah', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '08175420109', 'SMP', 0, 'siti.fatimah@yahoo.co.id', 'Islam', '1980-09-24', 'menanam padi', 1),
('Petani0028', 'Sarbini', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SMA', 0, 'belumada@belumada.com', 'Islam', '1969-03-16', 'menanam jagung & padi', 1),
('Petani0029', 'Pradiwiyanto', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SD', 0, 'belumada@belumada.com', 'Islam', '1951-11-14', 'menanam padi, coklat, jagung, perkebunan', 1),
('Petani0030', 'Partiem', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', 'Petani0030.jpg', '000000', 'SD', 3, 'partiem@gmail.com', 'Islam', '1971-01-23', 'menanam padi, jahe, jagung, singkong', 1),
('Petani0031', 'Maryudi', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SMP', 2, 'belumada@belumada.com', 'Islam', '1952-04-07', 'menanam singkong, padi, kacang tanah', 0),
('Petani0032', 'Sismail', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SMP', 2, 'belumada@belumada.com', 'Islam', '1956-05-15', 'menanam singkong, jagung, padi', 0),
('Petani0033', 'Muhyadi', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SD', 1, 'belumada@belumada.com', 'Islam', '1951-10-27', 'menanam padi, ketela, jagung, jati, mahoni', 1),
('Petani0034', 'Asmilah', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 4, 'belumada@belumada.com', 'Islam', '1977-02-08', 'tidak memiliki keahlian', 0),
('Petani0035', 'Utamiyem', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SMP', 0, 'belumada@belumada.com', 'Islam', '1970-11-11', 'tidak memiliki keahlian', 0),
('Petani0036', 'Kastopah', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 5, 'belumada@belumada.com', 'Islam', '1977-02-14', 'tidak memiliki keahlian', 0),
('Petani0037', 'Mugirah', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 3, 'belumada@belumada.com', 'Islam', '1969-05-14', 'tidak ada keahlian', 0),
('Petani0038', 'Sunaryati', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', 'Petani0038.jpg', '000000', 'SMP', 4, 'belumada@belumada.com', 'Katolik', '1969-04-15', 'tidak memiliki keahlian', 0),
('Petani0039', 'Yunaliyati', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', 'Petani0039.jpg', '000000', 'SMA', 5, 'yunaliyati@yahoo.com', 'Katolik', '1976-08-09', 'menanam bawang merah', 1),
('Petani0040', 'Malvin', 'Jalan damai', 'Sleman', 'Ngaglik', 'Daerah Istimewa Yogyakarta', 'Donoharjo', 'Petani0040.JPG', '08121584000', 'SMA', 0, 'christopher.malvin@gmail.com', 'Kristen', '1994-03-24', 'tidak memiliki keahlian', 0),
('Petani0042', 'Sarti', 'Ketuwon kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0042.png', '085991684333', 'SMP', 2, 'Sarti@gmail.com', 'Islam', '1995-06-18', 'menanam jamur', 1),
('Petani0043', 'Setiyowati', 'Ketuwon kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0043.png', '087654890123', 'SMP', 3, 'Setyowati@gmail.com', 'Islam', '1992-02-08', 'Menanam padi, jagung dan palawija', 1),
('Petani0044', 'Tihamah', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', '', '0513986345678', 'SMP', 3, 'Tihamah@gmailcom', 'Islam', '1978-04-13', 'Menanam padi, jagung cabai, tembakau', 1),
('Petani0045', 'Tutik Sudariyah', 'Ketuwon kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0045.png', '089711456732', 'SMP', 3, 'tutik@gmail.com', 'Islam', '1983-12-31', 'menanam gembili', 1),
('Petani0046', 'Triyanti', 'Ketuwon kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0046.jpg', '085432766511', 'SMP', 4, 'triyanti@gmail.com', 'Islam', '1986-10-05', 'menanam kopi, tembakau, padi, jagung', 1),
('Petani0047', 'Hartono', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0047.jpg', '081987648176', 'SMA', 4, 'hartono@gmail.com', 'Islam', '1988-05-23', 'menanam padi', 1),
('Petani0048', 'Sariyadi', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0048.jpg', '085677890431', 'SMP', 4, 'sariyadi@gmail.com', 'Islam', '1977-11-06', 'tanam padi, jagung, kopi', 1),
('Petani0049', 'Maryanto', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0049.jpg', '087780123511', 'SMP', 6, 'maryanto@gmail.com', 'Islam', '1979-01-13', 'menanam kopi', 1),
('Petani0050', 'Wasito', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0050.jpg', '087541890768', 'SMA', 3, 'wasito@gmail.com', 'Islam', '1985-06-29', 'menanam padi dan kopi', 1),
('Petani0051', 'Siti Khalimah', 'Ketuwon Wetan', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0051.png', '081549562177', 'SMA', 2, 'khalimah@gmail.com', 'Islam', '1989-08-12', 'menanam padi dan jagung', 1),
('Petani0052', 'Nur Yasin', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0052.jpg', '089765125466', 'SMA', 2, 'nuryasin@gmail.com', 'Islam', '1989-02-14', 'menanam padi, cabai,jagung, tembakau dan kopi', 1),
('Petani0053', 'Sarimah', 'Ketuwon Wetan', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0053.png', '085789810659', 'SMA', 3, 'sarimah@gmail.com', 'Islam', '1974-09-17', 'menanam dan merawat tanaman kopi', 1),
('Petani0054', 'Sochibah', 'Ketuwon Wetan', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0054.png', '081678317099', 'SMP', 3, 'sochibah@gmail.com', 'Islam', '1975-12-09', 'menanam padi, tembakau, kopi, cabai', 1),
('Petani0055', 'Istikomah', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0055.png', '085786150799', 'SMA', 2, 'istikomah@gmail.com', 'Islam', '1978-10-27', 'tanam cabai, sledri', 1),
('Petani0056', 'Triyanti', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0056.png', '08177752190', 'SMP', 3, 'triyanti@gmail.com', 'Islam', '1986-03-21', 'menanm kopi, padi, jagung, tembakau, cabai', 1),
('Petani0057', 'Setiyowati', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0057.png', '087895120999', 'SMP', 3, 'setiyowati@gmail.com', 'Islam', '1992-05-28', 'menanam sayur dan kopi', 1),
('Petani0058', 'Sarti', 'Ketuwon Wetan', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0058.png', '081095365111', 'SMP', 2, 'sarti@gmail.com', 'Islam', '1995-12-31', 'menanam palawija', 1),
('Petani0059', 'Asmui1', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', '', '081560129741', 'SMP', 3, 'asmui@gmail.com', 'Islam', '1976-02-12', 'mencangkok, stek batang', 1),
('Petani0060', 'Paimah', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0060.png', '081776610927', 'SMP', 3, 'paimah@gmail.com', 'Islam', '1982-04-18', 'matun dan tandur', 1),
('Petani0061', 'Marwati', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0061.png', '089514096831', 'SMP', 3, 'warwati@gmail.com', 'Islam', '1988-06-19', 'menanam padi, jagung dan cabai', 1),
('Petani0062', 'Nikmah', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0062.png', '085601832207', 'SMP', 2, 'nikmah@gmail.com', 'Islam', '1987-10-09', 'matun, tandur, mengolah kopi mentah', 1),
('Petani0063', 'warohmah', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0063.png', '081509182644', 'SMP', 3, 'warohmah@gmail.com', 'Islam', '1990-03-28', 'mengolah kopi mentah', 1),
('Petani0064', 'Muslichatun', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0064.png', '082690175209', 'SMP', 3, 'muslichatun@gmail.com', 'Islam', '1993-11-01', 'mengolah kopi mentah', 1),
('Petani0065', 'Miftahudin', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0065.jpg', '081001992882', 'SMP', 3, 'miftahudin@gmail.com', 'Islam', '1985-10-07', 'mencangkul, menanam tembakau', 1),
('Petani0066', 'Siti Hidayah', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0066.png', '085007914311', 'SMP', 2, 'hidayah@gmail.com', 'Islam', '1995-06-29', 'mengolah kopi mentah', 1),
('Petani0067', 'Muji Lestari', 'Ketuwon Wetan', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0067.png', '081590127519', 'SMP', 3, 'lestari@gmail.com', 'Islam', '1992-03-19', 'mengolah kopi mentah', 1),
('Petani0068', 'Malikhah', 'Ketuwon Kulon', 'Temanggung', 'Kandangan', 'Jawa Tengah', 'Malebo', 'Petani0068.png', '085207410991', 'SMP', 4, 'malikhah@gmail.com', 'Islam', '1982-10-28', 'menanam padi, mengolah kopi mentah', 1),
('Petani0069', 'Arsita Amandalia', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0069.png', '085991726851', 'SMP', 3, 'amandalia@gmail.com', 'Islam', '1987-03-20', 'menanam padi', 1),
('Petani0070', 'Alfiyah', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0070.png', '082904728312', 'SMP', 2, 'alfiyah@gmail.com', 'Islam', '1990-11-01', 'menanam sayuran', 1),
('Petani0071', 'Supriatun', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0071.png', '087106802471', 'SMP', 4, 'supriatun@gmail.com', 'Islam', '1978-09-12', 'menanam padi', 1),
('Petani0072', 'Andriyanto', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0072.jpg', '085891470249', 'SMP', 3, 'andriyanto@gmail.com', 'Kristen', '1988-02-29', 'menanam padi, cabai  dan mencangkul', 1),
('Petani0073', 'Septian Hidayat', 'kruwed RT.03 / 02', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0073.jpg', '082819467711', 'SMP', 4, 'septianhidayat@gmail.com', 'Islam', '1993-04-17', 'mencangkul', 1),
('Petani0074', 'Idah Mahrani', 'Kruwed  Rt 01/ Rw. 01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0074.png', '082788222185', 'SMA', 4, 'idahmahrani@gmail.com', 'Kristen', '1983-02-23', 'menanam padi', 1),
('Petani0075', 'Muryani', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0075.png', '081881779914', 'SMP', 5, 'muryani@gmail.com', 'Islam', '1980-11-02', 'menanam padi', 1),
('Petani0076', 'Umika Fitri R', 'Kruwed  Rt 01/ Rw. 01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0076.png', '082444779016', 'SMA', 4, 'umikafitri@gmail.com', 'Islam', '1984-12-15', 'menanam padi', 1),
('Petani0077', 'Nina Ristiana', 'Kruwed  Rt 01/ Rw. 01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', '', '087333975412', 'SMA', 4, 'ninaristiana@gmail.com', 'Islam', '1985-03-03', 'menanam padi dan cabai', 1),
('Petani0078', 'Muh Agung Prayoga', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0078.jpg', '089736018325', 'SMA', 2, 'muhagung@gmail.com', 'Islam', '1980-09-20', 'mencangkul , mengolah lahan untuk ditanami padi', 1),
('Petani0079', 'Ilham', 'Kruwed  Rt 01/ Rw. 01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0079.jpg', '082900214729', 'SMA', 2, 'ilham@gmail.com', 'Kristen', '1990-05-27', 'mencangkul', 1),
('Petani0080', 'Miji Pamungkas', 'Kruwed  Rt 01/ Rw. 01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0080.jpg', '089881726679', 'SMA', 3, 'mijipamungkas@gmail.com', 'Kristen', '1991-12-01', 'menanam padi, mencangkul', 1),
('Petani0081', 'Laela Riyanti', 'Kruwed  Rt 01/ Rw. 01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0081.png', '081782378290', 'SMA', 3, 'laealriyanti@gmail.com', 'Islam', '1988-12-15', 'menanam padi', 1),
('Petani0082', 'Ayu Laksmi', 'Kruwed  Rt. 01/ Rw. 01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0082.png', '082091436768', 'SMA', 3, 'ayulaksmi@gmail.com', 'Islam', '1984-09-01', 'menanam ketela ', 1),
('Petani0083', 'Rizal Ade T', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0083.jpg', '085232909232', 'SMP', 3, 'rizalade@gmail.com', 'Kristen', '1991-04-18', 'menanam ubi kayu', 1),
('Petani0084', 'Fatimah', 'Kruwed 01/01', 'Kebumen', 'Sempor', 'Jawa Tengah', 'Selokerto', 'Petani0084.png', '081654986480', 'SMA', 3, 'fatimah@gmail.com', 'Islam', '1986-10-12', 'mengolah ubi kayu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_peta_lahan`
--

CREATE TABLE `master_peta_lahan` (
  `ID_Lahan` varchar(10) NOT NULL,
  `Koordinat_X` varchar(20) DEFAULT NULL,
  `Koordinat_Y` varchar(20) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `bulan` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_produk_tani`
--

CREATE TABLE `master_produk_tani` (
  `ID_Produk` varchar(10) NOT NULL,
  `Nama_Produk` varchar(200) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Deskripsi_Produk` varchar(200) DEFAULT NULL,
  `Satuan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_spesies _tanaman`
--

CREATE TABLE `master_spesies _tanaman` (
  `ID_Spesies` varchar(10) NOT NULL,
  `Jenis_Tanaman` varchar(20) DEFAULT NULL,
  `Nama_Tanaman` varchar(50) NOT NULL,
  `Nama_Latin` varchar(50) DEFAULT NULL,
  `Habitat` varchar(50) DEFAULT NULL,
  `Masa_Tanam` int(4) DEFAULT NULL,
  `Akar` varchar(200) DEFAULT NULL,
  `Batang` varchar(200) DEFAULT NULL,
  `Daun` varchar(200) DEFAULT NULL,
  `Buah` varchar(200) DEFAULT NULL,
  `Biji` varchar(200) DEFAULT NULL,
  `Perkembangbiakan` varchar(200) DEFAULT NULL,
  `Foto1` varchar(200) DEFAULT NULL,
  `Foto2` varchar(200) DEFAULT NULL,
  `Iklim` varchar(50) DEFAULT NULL,
  `Jenis_Tanah` varchar(20) DEFAULT NULL,
  `Kelembaban` varchar(20) DEFAULT NULL,
  `ID_Morfologi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_supplier`
--

CREATE TABLE `master_supplier` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Supplier` varchar(200) NOT NULL,
  `ID_Jenis_Sup` varchar(10) NOT NULL,
  `Alamat_Supplier` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Kontak_Person` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `No_Handphone` varchar(15) DEFAULT NULL,
  `No_Telpon` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_user`
--

CREATE TABLE `master_user` (
  `ID_User` varchar(10) NOT NULL,
  `Password` varchar(70) NOT NULL,
  `PIN` int(6) NOT NULL,
  `Tingkat_Priv` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_user`
--

INSERT INTO `master_user` (`ID_User`, `Password`, `PIN`, `Tingkat_Priv`) VALUES
('AdmKel0001', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0002', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0003', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0004', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0005', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0006', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0007', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0008', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0009', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0010', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0011', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0012', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0013', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0014', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0015', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmKel0016', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('AdmPet0001', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '1'),
('Petani0001', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '2'),
('Petani0002', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0003', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0004', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0005', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0006', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0007', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0008', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0009', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0010', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0011', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0012', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0013', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0014', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0015', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0016', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0017', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0018', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0019', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0020', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0021', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0022', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0023', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0024', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0025', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0026', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0027', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0028', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0029', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0030', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0031', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0032', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0033', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0034', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0035', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0036', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0037', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0038', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0039', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0040', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0041', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0042', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0043', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0044', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0045', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0046', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0047', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0048', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0049', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0050', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0051', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0052', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0053', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0054', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0055', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0056', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0057', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0058', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0059', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0060', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0061', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0062', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0063', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0064', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0065', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0066', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0067', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0068', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0069', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0070', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0071', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0072', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0073', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0074', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0075', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0076', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0077', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0078', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0079', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0080', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0081', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0082', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0083', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0084', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0085', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0086', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0087', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0088', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0089', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0090', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0091', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0092', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0093', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0094', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3'),
('Petani0095', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 123, '3');

-- --------------------------------------------------------

--
-- Table structure for table `master_user_kat`
--

CREATE TABLE `master_user_kat` (
  `ID_Kategori` char(3) NOT NULL,
  `ID_User` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_user_kat`
--

INSERT INTO `master_user_kat` (`ID_Kategori`, `ID_User`) VALUES
('ADP', 'AdmPet0001'),
('ADK', 'AdmKel0001'),
('ADK', 'AdmKel0002'),
('ADK', 'AdmKel0003'),
('ADK', 'AdmKel0004'),
('ADK', 'AdmKel0005'),
('ADK', 'AdmKel0006'),
('ADK', 'AdmKel0007'),
('PET', 'Petani0001'),
('PET', 'Petani0002'),
('PET', 'Petani0003'),
('PET', 'Petani0004'),
('PET', 'Petani0005'),
('PET', 'Petani0006'),
('PET', 'Petani0007'),
('PET', 'Petani0008'),
('PET', 'Petani0009'),
('PET', 'Petani0010'),
('PET', 'Petani0011'),
('PET', 'Petani0012'),
('PET', 'Petani0013'),
('PET', 'Petani0014'),
('PET', 'Petani0015'),
('PET', 'Petani0016'),
('PET', 'Petani0017'),
('PET', 'Petani0018'),
('PET', 'Petani0019'),
('PET', 'Petani0020'),
('PET', 'Petani0021'),
('PET', 'Petani0022'),
('PET', 'Petani0023'),
('PET', 'Petani0024'),
('PET', 'Petani0025'),
('PET', 'Petani0026'),
('PET', 'Petani0027'),
('PET', 'Petani0028'),
('PET', 'Petani0029'),
('PET', 'Petani0030'),
('PET', 'Petani0031'),
('PET', 'Petani0032'),
('PET', 'Petani0033'),
('PET', 'Petani0034'),
('PET', 'Petani0035'),
('PET', 'Petani0036'),
('PET', 'Petani0037'),
('PET', 'Petani0038'),
('PET', 'Petani0039'),
('PET', 'Petani0040'),
('PET', 'Petani0041'),
('PET', 'Petani0042'),
('PET', 'Petani0043'),
('PET', 'Petani0044'),
('PET', 'Petani0045'),
('PET', 'Petani0046'),
('PET', 'Petani0047'),
('PET', 'Petani0048'),
('PET', 'Petani0049'),
('PET', 'Petani0050'),
('PET', 'Petani0051'),
('PET', 'Petani0052'),
('PET', 'Petani0053'),
('PET', 'Petani0054'),
('PET', 'Petani0055'),
('PET', 'Petani0056'),
('PET', 'Petani0057'),
('PET', 'Petani0058'),
('PET', 'Petani0059'),
('PET', 'Petani0060'),
('PET', 'Petani0061'),
('PET', 'Petani0062'),
('PET', 'Petani0063'),
('PET', 'Petani0063'),
('PET', 'Petani0064'),
('PET', 'Petani0065'),
('PET', 'Petani0066'),
('PET', 'Petani0067'),
('PET', 'Petani0068'),
('PET', 'Petani0069'),
('PET', 'Petani0070');

-- --------------------------------------------------------

--
-- Table structure for table `master_user_org`
--

CREATE TABLE `master_user_org` (
  `NIK` varchar(10) NOT NULL,
  `Password` varchar(14) NOT NULL,
  `Nama_Karyawan` varchar(100) NOT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Alamat_Rumah` varchar(100) DEFAULT NULL,
  `No_Telpon` varchar(15) DEFAULT NULL,
  `Org_Unit` varchar(10) NOT NULL,
  `Jenis_Kelamin` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`Nama_Provinsi`) VALUES
('Daerah Istimewa Yogyakarta'),
('Jawa Tengah');

-- --------------------------------------------------------

--
-- Table structure for table `trans_aktivitas_pertanian`
--

CREATE TABLE `trans_aktivitas_pertanian` (
  `ID_aktifitas_petani` varchar(10) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL,
  `ID_Petani` varchar(10) NOT NULL,
  `Tanggal_Mulai` date DEFAULT NULL,
  `Tanggal_Selesai` date DEFAULT NULL,
  `Tahun_Aktivitas` int(4) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_anggaraan`
--

CREATE TABLE `trans_anggaraan` (
  `ID_User` varchar(10) NOT NULL,
  `ID_Aktifitas` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `ID_Biaya` varchar(10) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `periode_tanam` varchar(2) NOT NULL,
  `jumlah_satuan` int(4) NOT NULL,
  `harga_satuan` int(9) NOT NULL,
  `waktu_perkiraan_keluar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_ang_petani`
--

CREATE TABLE `trans_ang_petani` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Tgl_Gabung` date NOT NULL,
  `Tgl_Expired` date NOT NULL,
  `Keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_ang_petani`
--

INSERT INTO `trans_ang_petani` (`ID_Kelompok_Tani`, `ID_User`, `Tgl_Gabung`, `Tgl_Expired`, `Keterangan`) VALUES
('KelTani001', 'Petani0001', '2015-12-09', '2099-12-31', 'ok'),
('KelTani001', 'Petani0002', '2015-12-09', '2099-12-31', 'berlaku seumur hidup'),
('KelTani001', 'Petani0003', '2015-12-09', '2099-12-31', 'berlaku seumur hidup'),
('KelTani001', 'Petani0004', '2016-01-03', '2099-12-31', '-'),
('KelTani001', 'Petani0005', '2016-01-03', '2099-12-31', '-'),
('KelTani002', 'Petani0006', '2012-06-02', '2099-12-31', '-'),
('KelTani002', 'Petani0007', '2012-06-12', '2099-12-31', '-'),
('KelTani002', 'Petani0008', '2012-05-07', '2099-12-31', '-'),
('KelTani002', 'Petani0009', '2012-06-02', '2099-12-31', '-'),
('KelTani002', 'Petani0010', '2016-01-12', '2099-12-31', '-'),
('KelTani002', 'Petani0011', '2014-06-17', '2099-12-31', '-'),
('KelTani002', 'Petani0012', '2015-10-13', '2099-12-31', '-'),
('KelTani002', 'Petani0013', '2013-05-06', '2099-12-31', '-'),
('KelTani002', 'Petani0014', '2016-02-03', '2099-12-31', 'anggota baru'),
('KelTani003', 'Petani0004', '0333-12-12', '2099-12-31', 'OK'),
('KelTani003', 'Petani0015', '2014-06-15', '2099-12-31', '-'),
('KelTani003', 'Petani0016', '2014-01-01', '2099-12-31', '-'),
('KelTani003', 'Petani0017', '2016-05-09', '2099-12-31', '-'),
('KelTani003', 'Petani0018', '2015-11-10', '2099-12-31', '-'),
('KelTani003', 'Petani0019', '2014-01-01', '2099-12-31', '-'),
('KelTani003', 'Petani0021', '2015-01-12', '2099-12-31', '-'),
('KelTani003', 'Petani0022', '2016-11-15', '2099-12-31', 'anggota baru'),
('KelTani003', 'Petani0023', '2014-06-17', '2099-12-31', '-'),
('KelTani003', 'Petani0024', '2015-02-10', '2099-12-31', '-'),
('KelTani003', 'Petani0025', '2014-01-01', '2099-12-31', '-'),
('KelTani003', 'Petani0026', '2015-05-05', '2099-12-31', 'Ketua Kelompok Tani'),
('KelTani004', 'Petani0026', '2015-05-05', '2099-12-31', 'Ketua Kelompok Tani'),
('KelTani004', 'Petani0027', '2013-05-12', '2099-12-31', '-'),
('KelTani004', 'Petani0028', '2014-02-10', '2099-12-31', '-'),
('KelTani004', 'Petani0029', '2013-05-05', '2099-12-31', '-'),
('KelTani004', 'Petani0030', '2015-05-17', '2099-12-31', '-'),
('KelTani004', 'Petani0032', '2015-11-09', '2099-12-31', ''),
('KelTani004', 'Petani0033', '2015-12-20', '2099-12-31', '-'),
('KelTani005', 'Petani0034', '2010-02-09', '2099-12-31', 'Ketua Kelompok Tani'),
('KelTani005', 'Petani0035', '2010-02-09', '2099-12-31', '-'),
('KelTani005', 'Petani0036', '2010-03-02', '2099-12-31', '-'),
('KelTani005', 'Petani0037', '2010-03-06', '2099-12-31', '-'),
('KelTani005', 'Petani0038', '2010-03-14', '2099-12-31', '-'),
('KelTani005', 'Petani0039', '2010-02-09', '2099-12-31', '-');

-- --------------------------------------------------------

--
-- Table structure for table `trans_harga_prod`
--

CREATE TABLE `trans_harga_prod` (
  `ID_Produk` varchar(10) NOT NULL,
  `Tanggal` date NOT NULL,
  `Harga` int(15) NOT NULL,
  `ID_User` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_hasil_panen`
--

CREATE TABLE `trans_hasil_panen` (
  `ID_Petani` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Bulan_Panen` varchar(2) DEFAULT NULL,
  `Tahun_Panen` varchar(4) NOT NULL,
  `Periode_Panen` varchar(2) NOT NULL,
  `Jumlah_Hasil_Panen` int(4) DEFAULT NULL,
  `ID_Produk` varchar(10) NOT NULL,
  `Luas_lahan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_kalender_tanam`
--

CREATE TABLE `trans_kalender_tanam` (
  `ID_Spesies` varchar(10) NOT NULL,
  `Nama_Kalender` varchar(50) NOT NULL,
  `Masa_Tanam` varchar(2) NOT NULL,
  `Kabupaten` varchar(50) NOT NULL,
  `Provinsi` varchar(50) NOT NULL,
  `Tanggal_Awal` date NOT NULL,
  `Tanggal_Akhir` date NOT NULL,
  `Pengolahan_Lahan_Awal` date NOT NULL,
  `Pengolahan_Lahan_Akhir` date NOT NULL,
  `Persiapan_Lahan_Awal` date NOT NULL,
  `Masa_Penanaman_Awal` date NOT NULL,
  `Masa_Pemupukan` date NOT NULL,
  `Masa_Panen` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_pelaksanaan_pelatihan`
--

CREATE TABLE `trans_pelaksanaan_pelatihan` (
  `ID_User` varchar(10) NOT NULL,
  `Tanggal_Pelaksanaan` date DEFAULT NULL,
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `Pembahasan` varchar(200) DEFAULT NULL,
  `Sesi` varchar(1) DEFAULT NULL,
  `Jumlah_Peserta` int(3) DEFAULT NULL,
  `Bukti_Materi` varchar(200) DEFAULT NULL,
  `Bukti_Daftar_Hadir` varchar(200) DEFAULT NULL,
  `Catatan` varchar(200) DEFAULT NULL,
  `Foto_Kegiatan_1` varchar(200) DEFAULT NULL,
  `Foto_Kegiatan_2` varchar(200) DEFAULT NULL,
  `Jam_Awal` timestamp NULL DEFAULT NULL,
  `Jam_Akhir` timestamp NULL DEFAULT NULL,
  `Nomor_Pelaksanaan_pelatihan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_penawaran_prod_tani`
--

CREATE TABLE `trans_penawaran_prod_tani` (
  `ID_User` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Tanggal_Penawaran` date DEFAULT NULL,
  `Spesifikasi_Barang` varchar(200) DEFAULT NULL,
  `ID_Barang` varchar(10) NOT NULL,
  `Kondisi_Barang` varchar(30) DEFAULT NULL,
  `Merk` varchar(30) DEFAULT NULL,
  `Harga` int(12) DEFAULT NULL,
  `Tahun_Produksi` int(4) DEFAULT NULL,
  `Gambar1` varchar(200) DEFAULT NULL,
  `Gambar2` varchar(200) DEFAULT NULL,
  `Status` varchar(10) DEFAULT NULL,
  `Stok` int(3) DEFAULT NULL,
  `Satuan_Barang` varchar(10) DEFAULT NULL,
  `Validasi_Admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_permintaan`
--

CREATE TABLE `trans_permintaan` (
  `ID_Permintaan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Qty` int(3) DEFAULT NULL,
  `Harga` int(12) DEFAULT NULL,
  `Tgl_Kebutuhan` date DEFAULT NULL,
  `Tgl_Permintaan` date DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_permintaan_pelatihan`
--

CREATE TABLE `trans_permintaan_pelatihan` (
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Materi` varchar(10) NOT NULL,
  `Tanggal_Awal_Permintaan` date DEFAULT NULL,
  `Tanggal_Akhir_Permintaan` date DEFAULT NULL,
  `Tempat` varchar(50) DEFAULT NULL,
  `Alamat` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Kontak_Person` varchar(50) DEFAULT NULL,
  `Telpon` varchar(50) DEFAULT NULL,
  `Jumlah_Target_Peserta` int(3) DEFAULT NULL,
  `Surat_Permintaan` varchar(200) DEFAULT NULL,
  `Tanggal_Awal_Setuju` date DEFAULT NULL,
  `Tanggal_Akhir_Setuju` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_permintaan_pelayanan`
--

CREATE TABLE `trans_permintaan_pelayanan` (
  `ID_Permintaan_Pelayanan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Pelayanan` varchar(10) NOT NULL,
  `Tanggal_Permintaan` date DEFAULT NULL,
  `Permasalahan` varchar(200) DEFAULT NULL,
  `Solusi` varchar(200) DEFAULT NULL,
  `Tanggal_Respon` date NOT NULL,
  `Penanggungjawab` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_struk_org`
--

CREATE TABLE `trans_struk_org` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `Tgl_Awal` date NOT NULL,
  `Tgl_Selesai` date NOT NULL,
  `Nama_Ketua` varchar(100) NOT NULL,
  `Telpon_Ketua` varchar(20) NOT NULL,
  `Nama_Wakil_Ketua` varchar(100) NOT NULL,
  `Telpon_Wakil_Ketua` varchar(20) NOT NULL,
  `Nama_Sekretaris` varchar(100) NOT NULL,
  `Telpon_Sekretaris` varchar(20) NOT NULL,
  `Nama_Bendahara` varchar(100) NOT NULL,
  `Telpon_Bendahara` varchar(20) NOT NULL,
  `Scan_Struktur_Organisasi` varchar(200) NOT NULL,
  `Scan_Susunan_Pengurus` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_struk_org`
--

INSERT INTO `trans_struk_org` (`ID_Kelompok_Tani`, `Tgl_Awal`, `Tgl_Selesai`, `Nama_Ketua`, `Telpon_Ketua`, `Nama_Wakil_Ketua`, `Telpon_Wakil_Ketua`, `Nama_Sekretaris`, `Telpon_Sekretaris`, `Nama_Bendahara`, `Telpon_Bendahara`, `Scan_Struktur_Organisasi`, `Scan_Susunan_Pengurus`) VALUES
('KelTani002', '2015-06-02', '2018-06-02', 'Sumarno', '0878839033892', 'Wakimin', '081228941305', 'Giyanti', '085328129673', 'Mujiyo', '0878839082181', 'KelTani002.jpg', 'KelTani002.jpg'),
('KelTani003', '2018-03-16', '2018-07-20', 'Sarwoto', '0856588945490', 'Ai Irawan', '08564', 'Aris Listianto', '08572', 'Dwi Arina', '082', 'KelTani003.jpg', 'KelTani003.jpg'),
('KelTani004', '2019-05-06', '2022-05-06', 'Sismail', '7232308863434', 'Muhyadi', '000000', 'Sarbini', '000000', 'Partiem', '000000', 'KelTani004.jpg', 'KelTani004.jpg'),
('KelTani004', '2019-05-06', '2022-05-06', 'Sismail', '7232308863434', 'Muhyadi', '000000', 'Sarbini', '000000', 'Partiem', '000000', 'KelTani004.jpg', 'KelTani004.jpg'),
('KelTani001', '2018-02-21', '2019-02-02', 'Eve', '087797602449', 'Lina', '1899', 'Putri', '122', 'Widiasih', '2910', 'KelTani001.jpg', 'KelTani001.jpg'),
('KelTani003', '2018-03-16', '2018-07-20', 'Sarwoto', '0856588945490', 'Ai Irawan', '08564', 'Aris Listianto', '08572', 'Dwi Arina', '082', 'KelTani003.jpg', 'KelTani003.png'),
('KelTani001', '2018-02-21', '2019-02-02', 'Eve', '087797602449', 'Lina', '1899', 'Putri', '122', 'Widiasih', '2910', 'KelTani001.png', 'KelTani001.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi` (`Nama_Provinsi`),
  ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`Nama_Kecamatan`),
  ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi` (`Nama_Provinsi`),
  ADD KEY `Nama_Kecamatan` (`Nama_Kecamatan`),
  ADD KEY `Nama_Kecamatan_2` (`Nama_Kecamatan`),
  ADD KEY `Nama_Kabupaten_2` (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi_2` (`Nama_Provinsi`),
  ADD KEY `Nama_Kecamatan_3` (`Nama_Kecamatan`);

--
-- Indexes for table `kelurahan_desa`
--
ALTER TABLE `kelurahan_desa`
  ADD PRIMARY KEY (`Nama_Desa`),
  ADD KEY `Nama_Kecamatan` (`Nama_Kecamatan`,`Nama_Kabupaten`,`Nama_Provinsi`),
  ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi` (`Nama_Provinsi`),
  ADD KEY `Nama_Kecamatan_2` (`Nama_Kecamatan`),
  ADD KEY `Nama_Kabupaten_2` (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi_2` (`Nama_Provinsi`),
  ADD KEY `Nama_Desa` (`Nama_Desa`),
  ADD KEY `Nama_Kecamatan_3` (`Nama_Kecamatan`),
  ADD KEY `Nama_Kabupaten_3` (`Nama_Kabupaten`),
  ADD KEY `Nama_Kabupaten_4` (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi_3` (`Nama_Provinsi`),
  ADD KEY `Nama_Kecamatan_4` (`Nama_Kecamatan`);

--
-- Indexes for table `log_aktifitas_pelatihan`
--
ALTER TABLE `log_aktifitas_pelatihan`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Permintaan_Pelatihan` (`ID_Permintaan_Pelatihan`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_aktifitas_pelayanan`
--
ALTER TABLE `log_aktifitas_pelayanan`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Permintaan_Pelayanan` (`ID_Permintaan_Pelayanan`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_biaya_keluar`
--
ALTER TABLE `log_biaya_keluar`
  ADD KEY `ID_Aktivitas` (`ID_Aktivitas`,`ID_Petani`,`ID_Biaya`),
  ADD KEY `ID_Biaya` (`ID_Biaya`),
  ADD KEY `ID_Petani` (`ID_Petani`),
  ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `log_penawaran_prod`
--
ALTER TABLE `log_penawaran_prod`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Penawaran` (`ID_Penawaran`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_permintaan`
--
ALTER TABLE `log_permintaan`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Permintaan` (`ID_Permintaan`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_user_trans`
--
ALTER TABLE `log_user_trans`
  ADD KEY `ID_User` (`ID_User`,`ID_Aktivitas`),
  ADD KEY `ID_Aktivitas` (`ID_Aktivitas`);

--
-- Indexes for table `master_aktifitas_spesies`
--
ALTER TABLE `master_aktifitas_spesies`
  ADD PRIMARY KEY (`ID_aktifitas_spesies`),
  ADD KEY `ID_Spesies` (`ID_Spesies`),
  ADD KEY `ID_Aktivitas` (`ID_Aktivitas`),
  ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Indexes for table `master_aktivitas`
--
ALTER TABLE `master_aktivitas`
  ADD PRIMARY KEY (`ID_Aktivitas`);

--
-- Indexes for table `master_alat_tani`
--
ALTER TABLE `master_alat_tani`
  ADD PRIMARY KEY (`ID_Alat`),
  ADD KEY `ID_Kategori` (`ID_Kategori`);

--
-- Indexes for table `master_bahan_tani`
--
ALTER TABLE `master_bahan_tani`
  ADD PRIMARY KEY (`ID_Bahan`),
  ADD KEY `ID_Kategori` (`ID_Kategori`);

--
-- Indexes for table `master_detail_materi_ajar`
--
ALTER TABLE `master_detail_materi_ajar`
  ADD PRIMARY KEY (`ID_Materi_Ajar`,`ID_Sub_materi`),
  ADD KEY `ID_Materi_Ajar` (`ID_Materi_Ajar`);

--
-- Indexes for table `master_fasilitator`
--
ALTER TABLE `master_fasilitator`
  ADD PRIMARY KEY (`ID_User`);

--
-- Indexes for table `master_jenis_sup`
--
ALTER TABLE `master_jenis_sup`
  ADD PRIMARY KEY (`ID_Jenis_Sup`);

--
-- Indexes for table `master_kategori`
--
ALTER TABLE `master_kategori`
  ADD PRIMARY KEY (`ID_Kategori`);

--
-- Indexes for table `master_kategori_alatbahan`
--
ALTER TABLE `master_kategori_alatbahan`
  ADD PRIMARY KEY (`ID_Kategori`);

--
-- Indexes for table `master_kel_tani`
--
ALTER TABLE `master_kel_tani`
  ADD PRIMARY KEY (`ID_Kelompok_Tani`),
  ADD KEY `ID_User` (`ID_User`),
  ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`),
  ADD KEY `Kecamatan` (`Kecamatan`),
  ADD KEY `Kabupaten` (`Kabupaten`),
  ADD KEY `Desa_Kelurahan` (`Desa_Kelurahan`),
  ADD KEY `Desa_Kelurahan_2` (`Desa_Kelurahan`),
  ADD KEY `Kecamatan_2` (`Kecamatan`),
  ADD KEY `Provinsi` (`Provinsi`);

--
-- Indexes for table `master_kode_biaya`
--
ALTER TABLE `master_kode_biaya`
  ADD PRIMARY KEY (`ID_Biaya`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_materi_ajar`
--
ALTER TABLE `master_materi_ajar`
  ADD PRIMARY KEY (`ID_Materi_Ajar`),
  ADD KEY `ID_User` (`ID_User`),
  ADD KEY `ID_User_2` (`ID_User`);

--
-- Indexes for table `master_materi_pelatihan`
--
ALTER TABLE `master_materi_pelatihan`
  ADD PRIMARY KEY (`ID_Materi`),
  ADD KEY `ID_Materi` (`ID_Materi`),
  ADD KEY `Materi_Sebelumnya` (`Materi_Sebelumnya`);

--
-- Indexes for table `master_morf_tanaman`
--
ALTER TABLE `master_morf_tanaman`
  ADD PRIMARY KEY (`ID_Morfologi`);

--
-- Indexes for table `master_org_unit`
--
ALTER TABLE `master_org_unit`
  ADD PRIMARY KEY (`Org_Unit`),
  ADD KEY `Org_Unit_Atasan` (`Org_Unit_Atasan`);

--
-- Indexes for table `master_pelayanan`
--
ALTER TABLE `master_pelayanan`
  ADD PRIMARY KEY (`ID_Pelayanan`);

--
-- Indexes for table `master_petani`
--
ALTER TABLE `master_petani`
  ADD PRIMARY KEY (`ID_User`),
  ADD KEY `ID_User` (`ID_User`),
  ADD KEY `Kabupaten` (`Kabupaten`),
  ADD KEY `Kecamatan` (`Kecamatan`),
  ADD KEY `Provinsi` (`Provinsi`),
  ADD KEY `Desa_Kelurahan` (`Desa_Kelurahan`);

--
-- Indexes for table `master_peta_lahan`
--
ALTER TABLE `master_peta_lahan`
  ADD PRIMARY KEY (`ID_Lahan`,`bulan`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_produk_tani`
--
ALTER TABLE `master_produk_tani`
  ADD PRIMARY KEY (`ID_Produk`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_spesies _tanaman`
--
ALTER TABLE `master_spesies _tanaman`
  ADD PRIMARY KEY (`ID_Spesies`),
  ADD KEY `ID_Morfologi` (`ID_Morfologi`);

--
-- Indexes for table `master_supplier`
--
ALTER TABLE `master_supplier`
  ADD PRIMARY KEY (`ID_User`),
  ADD KEY `ID_Jenis_Sup` (`ID_Jenis_Sup`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_user`
--
ALTER TABLE `master_user`
  ADD PRIMARY KEY (`ID_User`);

--
-- Indexes for table `master_user_kat`
--
ALTER TABLE `master_user_kat`
  ADD KEY `ID_Kategori` (`ID_Kategori`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_user_org`
--
ALTER TABLE `master_user_org`
  ADD PRIMARY KEY (`NIK`),
  ADD KEY `Org_Unit` (`Org_Unit`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`Nama_Provinsi`),
  ADD KEY `Nama_Provinsi` (`Nama_Provinsi`),
  ADD KEY `Nama_Provinsi_2` (`Nama_Provinsi`);

--
-- Indexes for table `trans_aktivitas_pertanian`
--
ALTER TABLE `trans_aktivitas_pertanian`
  ADD PRIMARY KEY (`ID_aktifitas_petani`),
  ADD KEY `ID_Aktivitas` (`ID_Petani`),
  ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Indexes for table `trans_ang_petani`
--
ALTER TABLE `trans_ang_petani`
  ADD PRIMARY KEY (`ID_Kelompok_Tani`,`ID_User`),
  ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_harga_prod`
--
ALTER TABLE `trans_harga_prod`
  ADD UNIQUE KEY `ID_Produk_2` (`ID_Produk`,`Tanggal`),
  ADD KEY `ID_Produk` (`ID_Produk`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_hasil_panen`
--
ALTER TABLE `trans_hasil_panen`
  ADD PRIMARY KEY (`ID_Petani`,`ID_Spesies`,`Tahun_Panen`,`Periode_Panen`),
  ADD KEY `ID_Petani` (`ID_Petani`),
  ADD KEY `ID_Spesies` (`ID_Spesies`),
  ADD KEY `ID_Produk` (`ID_Produk`);

--
-- Indexes for table `trans_kalender_tanam`
--
ALTER TABLE `trans_kalender_tanam`
  ADD PRIMARY KEY (`ID_Spesies`,`Masa_Tanam`,`Kabupaten`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `trans_pelaksanaan_pelatihan`
--
ALTER TABLE `trans_pelaksanaan_pelatihan`
  ADD KEY `ID_Fasilitator` (`ID_Permintaan_Pelatihan`),
  ADD KEY `ID_Permintaan_Pelatihan` (`ID_Permintaan_Pelatihan`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_penawaran_prod_tani`
--
ALTER TABLE `trans_penawaran_prod_tani`
  ADD PRIMARY KEY (`ID_Penawaran`),
  ADD KEY `ID_Supplier` (`ID_User`,`ID_Barang`),
  ADD KEY `ID_Barang` (`ID_Barang`);

--
-- Indexes for table `trans_permintaan`
--
ALTER TABLE `trans_permintaan`
  ADD PRIMARY KEY (`ID_Permintaan`),
  ADD KEY `ID_User` (`ID_User`,`ID_Penawaran`),
  ADD KEY `ID_Penawaran` (`ID_Penawaran`);

--
-- Indexes for table `trans_permintaan_pelatihan`
--
ALTER TABLE `trans_permintaan_pelatihan`
  ADD PRIMARY KEY (`ID_Permintaan_Pelatihan`),
  ADD KEY `ID_User` (`ID_User`,`ID_Materi`),
  ADD KEY `ID_Materi` (`ID_Materi`),
  ADD KEY `ID_User_2` (`ID_User`),
  ADD KEY `ID_Materi_2` (`ID_Materi`);

--
-- Indexes for table `trans_permintaan_pelayanan`
--
ALTER TABLE `trans_permintaan_pelayanan`
  ADD PRIMARY KEY (`ID_Permintaan_Pelayanan`),
  ADD KEY `ID_User` (`ID_User`,`ID_Pelayanan`),
  ADD KEY `ID_Pelayanan` (`ID_Pelayanan`);

--
-- Indexes for table `trans_struk_org`
--
ALTER TABLE `trans_struk_org`
  ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD CONSTRAINT `provinsi_kabupaten` FOREIGN KEY (`Nama_Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE;

--
-- Constraints for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `kabupaten_kecamatan` FOREIGN KEY (`Nama_Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
  ADD CONSTRAINT `provinsi_kecamatan` FOREIGN KEY (`Nama_Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE;

--
-- Constraints for table `kelurahan_desa`
--
ALTER TABLE `kelurahan_desa`
  ADD CONSTRAINT `FK_keldes` FOREIGN KEY (`Nama_Kecamatan`) REFERENCES `kecamatan` (`Nama_Kecamatan`),
  ADD CONSTRAINT `kabupaten_desa` FOREIGN KEY (`Nama_Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
  ADD CONSTRAINT `provinsi_desa` FOREIGN KEY (`Nama_Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE;

--
-- Constraints for table `log_aktifitas_pelatihan`
--
ALTER TABLE `log_aktifitas_pelatihan`
  ADD CONSTRAINT `log_aktifitas_pelatihan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_aktifitas_pelatihan_ibfk_3` FOREIGN KEY (`ID_Permintaan_Pelatihan`) REFERENCES `trans_permintaan_pelatihan` (`ID_Permintaan_Pelatihan`);

--
-- Constraints for table `log_aktifitas_pelayanan`
--
ALTER TABLE `log_aktifitas_pelayanan`
  ADD CONSTRAINT `log_aktifitas_pelayanan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_aktifitas_pelayanan_ibfk_3` FOREIGN KEY (`ID_Permintaan_Pelayanan`) REFERENCES `trans_permintaan_pelayanan` (`ID_Permintaan_Pelayanan`);

--
-- Constraints for table `log_biaya_keluar`
--
ALTER TABLE `log_biaya_keluar`
  ADD CONSTRAINT `log_biaya_keluar_ibfk_1` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktifitas_spesies` (`ID_Aktivitas`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_2` FOREIGN KEY (`ID_Biaya`) REFERENCES `master_kode_biaya` (`ID_Biaya`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_3` FOREIGN KEY (`ID_Petani`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_4` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_5` FOREIGN KEY (`ID_aktifitas_spesies`) REFERENCES `master_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Constraints for table `log_penawaran_prod`
--
ALTER TABLE `log_penawaran_prod`
  ADD CONSTRAINT `log_penawaran_prod_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_penawaran_prod_ibfk_3` FOREIGN KEY (`ID_Penawaran`) REFERENCES `trans_penawaran_prod_tani` (`ID_Penawaran`),
  ADD CONSTRAINT `log_penawaran_prod_ibfk_4` FOREIGN KEY (`ID_User`) REFERENCES `master_supplier` (`ID_User`);

--
-- Constraints for table `log_permintaan`
--
ALTER TABLE `log_permintaan`
  ADD CONSTRAINT `log_permintaan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_permintaan_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `trans_permintaan` (`ID_User`),
  ADD CONSTRAINT `log_permintaan_ibfk_4` FOREIGN KEY (`ID_Permintaan`) REFERENCES `trans_permintaan` (`ID_Permintaan`);

--
-- Constraints for table `log_user_trans`
--
ALTER TABLE `log_user_trans`
  ADD CONSTRAINT `log_user_trans_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_user_trans_ibfk_2` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktivitas` (`ID_Aktivitas`);

--
-- Constraints for table `master_aktifitas_spesies`
--
ALTER TABLE `master_aktifitas_spesies`
  ADD CONSTRAINT `master_aktifitas_spesies_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
  ADD CONSTRAINT `master_aktifitas_spesies_ibfk_2` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktivitas` (`ID_Aktivitas`);

--
-- Constraints for table `master_alat_tani`
--
ALTER TABLE `master_alat_tani`
  ADD CONSTRAINT `master_alat_tani_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori_alatbahan` (`ID_Kategori`);

--
-- Constraints for table `master_bahan_tani`
--
ALTER TABLE `master_bahan_tani`
  ADD CONSTRAINT `master_bahan_tani_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori_alatbahan` (`ID_Kategori`);

--
-- Constraints for table `master_detail_materi_ajar`
--
ALTER TABLE `master_detail_materi_ajar`
  ADD CONSTRAINT `master_detail_materi_ajar_ibfk_1` FOREIGN KEY (`ID_Materi_Ajar`) REFERENCES `master_materi_ajar` (`ID_Materi_Ajar`);

--
-- Constraints for table `master_kel_tani`
--
ALTER TABLE `master_kel_tani`
  ADD CONSTRAINT `master_kel_tani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `master_kel_tani_ibfk_2` FOREIGN KEY (`Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
  ADD CONSTRAINT `master_kel_tani_ibfk_3` FOREIGN KEY (`Kecamatan`) REFERENCES `kecamatan` (`Nama_Kecamatan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `master_kel_tani_ibfk_4` FOREIGN KEY (`Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE,
  ADD CONSTRAINT `master_kel_tani_ibfk_5` FOREIGN KEY (`Desa_Kelurahan`) REFERENCES `kelurahan_desa` (`Nama_Desa`) ON UPDATE CASCADE;

--
-- Constraints for table `master_kode_biaya`
--
ALTER TABLE `master_kode_biaya`
  ADD CONSTRAINT `master_kode_biaya_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `master_materi_ajar`
--
ALTER TABLE `master_materi_ajar`
  ADD CONSTRAINT `master_materi_ajar_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_materi_pelatihan`
--
ALTER TABLE `master_materi_pelatihan`
  ADD CONSTRAINT `master_materi_pelatihan_ibfk_1` FOREIGN KEY (`Materi_Sebelumnya`) REFERENCES `master_materi_pelatihan` (`ID_Materi`);

--
-- Constraints for table `master_org_unit`
--
ALTER TABLE `master_org_unit`
  ADD CONSTRAINT `master_org_unit_ibfk_1` FOREIGN KEY (`Org_Unit_Atasan`) REFERENCES `master_org_unit` (`Org_Unit`);

--
-- Constraints for table `master_petani`
--
ALTER TABLE `master_petani`
  ADD CONSTRAINT `master_petani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `master_petani_ibfk_2` FOREIGN KEY (`Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
  ADD CONSTRAINT `master_petani_ibfk_3` FOREIGN KEY (`Kecamatan`) REFERENCES `kecamatan` (`Nama_Kecamatan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `master_petani_ibfk_4` FOREIGN KEY (`Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE,
  ADD CONSTRAINT `master_petani_ibfk_5` FOREIGN KEY (`Desa_Kelurahan`) REFERENCES `kelurahan_desa` (`Nama_Desa`) ON UPDATE CASCADE;

--
-- Constraints for table `master_peta_lahan`
--
ALTER TABLE `master_peta_lahan`
  ADD CONSTRAINT `master_peta_lahan_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `master_produk_tani`
--
ALTER TABLE `master_produk_tani`
  ADD CONSTRAINT `master_produk_tani_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `master_spesies _tanaman`
--
ALTER TABLE `master_spesies _tanaman`
  ADD CONSTRAINT `master_spesies _tanaman_ibfk_1` FOREIGN KEY (`ID_Morfologi`) REFERENCES `master_morf_tanaman` (`ID_Morfologi`);

--
-- Constraints for table `master_supplier`
--
ALTER TABLE `master_supplier`
  ADD CONSTRAINT `master_supplier_ibfk_1` FOREIGN KEY (`ID_Jenis_Sup`) REFERENCES `master_jenis_sup` (`ID_Jenis_Sup`),
  ADD CONSTRAINT `master_supplier_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_user_kat`
--
ALTER TABLE `master_user_kat`
  ADD CONSTRAINT `master_user_kat_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori` (`ID_Kategori`),
  ADD CONSTRAINT `master_user_kat_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_user_org`
--
ALTER TABLE `master_user_org`
  ADD CONSTRAINT `master_user_org_ibfk_1` FOREIGN KEY (`Org_Unit`) REFERENCES `master_org_unit` (`Org_Unit`);

--
-- Constraints for table `trans_aktivitas_pertanian`
--
ALTER TABLE `trans_aktivitas_pertanian`
  ADD CONSTRAINT `trans_aktivitas_pertanian_ibfk_1` FOREIGN KEY (`ID_Petani`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_aktivitas_pertanian_ibfk_2` FOREIGN KEY (`ID_aktifitas_spesies`) REFERENCES `master_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Constraints for table `trans_ang_petani`
--
ALTER TABLE `trans_ang_petani`
  ADD CONSTRAINT `trans_ang_petani_ibfk_1` FOREIGN KEY (`ID_Kelompok_Tani`) REFERENCES `master_kel_tani` (`ID_Kelompok_Tani`),
  ADD CONSTRAINT `trans_ang_petani_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_petani` (`ID_User`),
  ADD CONSTRAINT `trans_ang_petani_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `trans_harga_prod`
--
ALTER TABLE `trans_harga_prod`
  ADD CONSTRAINT `trans_harga_prod_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_harga_prod_ibfk_3` FOREIGN KEY (`ID_Produk`) REFERENCES `master_produk_tani` (`ID_Produk`);

--
-- Constraints for table `trans_hasil_panen`
--
ALTER TABLE `trans_hasil_panen`
  ADD CONSTRAINT `trans_hasil_panen_ibfk_1` FOREIGN KEY (`ID_Petani`) REFERENCES `master_petani` (`ID_User`),
  ADD CONSTRAINT `trans_hasil_panen_ibfk_2` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
  ADD CONSTRAINT `trans_hasil_panen_ibfk_3` FOREIGN KEY (`ID_Produk`) REFERENCES `master_produk_tani` (`ID_Produk`);

--
-- Constraints for table `trans_kalender_tanam`
--
ALTER TABLE `trans_kalender_tanam`
  ADD CONSTRAINT `trans_kalender_tanam_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `trans_pelaksanaan_pelatihan`
--
ALTER TABLE `trans_pelaksanaan_pelatihan`
  ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_2` FOREIGN KEY (`ID_Permintaan_Pelatihan`) REFERENCES `trans_permintaan_pelatihan` (`ID_Permintaan_Pelatihan`),
  ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_fasilitator` (`ID_User`),
  ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_4` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `trans_penawaran_prod_tani`
--
ALTER TABLE `trans_penawaran_prod_tani`
  ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_supplier` (`ID_User`),
  ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_2` FOREIGN KEY (`ID_Barang`) REFERENCES `master_alat_tani` (`ID_Alat`),
  ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `trans_permintaan`
--
ALTER TABLE `trans_permintaan`
  ADD CONSTRAINT `trans_permintaan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_permintaan_ibfk_2` FOREIGN KEY (`ID_Penawaran`) REFERENCES `trans_penawaran_prod_tani` (`ID_Penawaran`);

--
-- Constraints for table `trans_permintaan_pelatihan`
--
ALTER TABLE `trans_permintaan_pelatihan`
  ADD CONSTRAINT `trans_permintaan_pelatihan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_permintaan_pelatihan_ibfk_2` FOREIGN KEY (`ID_Materi`) REFERENCES `master_materi_pelatihan` (`ID_Materi`);

--
-- Constraints for table `trans_permintaan_pelayanan`
--
ALTER TABLE `trans_permintaan_pelayanan`
  ADD CONSTRAINT `trans_permintaan_pelayanan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_permintaan_pelayanan_ibfk_3` FOREIGN KEY (`ID_Pelayanan`) REFERENCES `master_pelayanan` (`ID_Pelayanan`);

--
-- Constraints for table `trans_struk_org`
--
ALTER TABLE `trans_struk_org`
  ADD CONSTRAINT `trans_struk_org_ibfk_1` FOREIGN KEY (`ID_Kelompok_Tani`) REFERENCES `master_kel_tani` (`ID_Kelompok_Tani`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
