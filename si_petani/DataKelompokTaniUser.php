<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADK")
  {
    header("Location:login.php?pesan=khusus untuk kelompok tani");
  }
  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive: true
            });
        });
        </script>
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
    <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
    ?>
        <li style="background-color: #024A0B"><a href="HomeiKelTani.php">Selamat Datang <?php echo $id_user ?></a></li>
        <li style="background-color: #024A0B"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <!-- bs-example-navbar-collapse-1 -->
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li class="active"><a href="DataKelompokTaniUser.php"> Daftar Kelompok Tani</a></li>
                        <li><a href="StrukturOrganisasiUser.php"> Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetaniUser.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikKelTaniWilayah_User.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_TaniUser.php"> Detail Anggota Kelompok Tani</a></li>
                        <li><a href="StatistikKelompokTani_User.php"> Statistik Kelompok Tani</a></li> 
                    </ul>
                </li>
        </div><!-- /#sidebar-wrapper -->
        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="design/side_menu.js"></script>
    <body onload="<?php if (@$_GET['pesan'] != "") { echo "alert('$_GET[pesan]');"; } ?>">
        <div class="section">
        <div class="container">
        <div class="row">
        <div class="col-md-11">
                        <h1>Daftar Kelompok Tani</h1>
                        <br>
                

                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kelompok Tani</th>
                                    <th>Kabupaten</th>
                                    <th>Kecamatan</th>
                                    <th>Provinsi</th>
                                    <th>Desa / kelurahan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            <?php
                                $koneksi = include("koneksi.php");                      
                                $hasil = mysqli_query($koneksi,"select * from master_kel_tani where ID_User = '$_SESSION[ID_User]'");
                                $no = 1;
                                while($kel_tani = mysqli_fetch_array($hasil)){
                            ?>  
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $kel_tani['Nama_Kelompok_Tani']?></td>
                                    <td><?php echo $kel_tani['Kabupaten']?></td>
                                    <td><?php echo $kel_tani['Kecamatan']?></td>
                                    <td><?php echo $kel_tani['Provinsi']?></td>
                                    <td><?php echo $kel_tani['Desa_Kelurahan']?></td>
                                    <td><a class="btn btn-s btn-info" href="KartuKelompokTani.php?id=<?php echo $kel_tani['ID_Kelompok_Tani'] ?>">Kartu Kelompok tani</a>&nbsp;<a class="btn btn-s btn-primary" href="DetailKelompokTaniUser.php?id=<?php 
                                    echo $kel_tani['ID_Kelompok_Tani'] ?>">Detail</a></td>
                                </tr>
                                <?php

                                }?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
        <div class="section">
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
</body>
</html>