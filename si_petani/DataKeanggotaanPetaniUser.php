<?php
  session_start();
      if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADK")
  {
    header("Location:login.php?pesan=khusus untuk admin & kelompok tani");
  }

  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

         <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive: true
            });
        });
        </script>
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
    <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
    ?>
        <li style="background-color: #34A853"><a href="HomeiKelTani.php">Selamat Datang <?php echo $id_user ?></a></li>
        <li style="background-color: #34A853"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li class='active'>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                       
                        <li><a href="StrukturOrganisasiUser.php"> Pendataan Struktur Organisasi</a></li>
                        <li class='active'><a href="KeangotaanPetaniUser.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
               <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                         <li><a href="GrafikKelTaniWilayah_User.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani_User.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_TaniUser.php"> Detail Anggota Kelompok Tani</a></li>
                        <li><a href="StatistikKelompokTani_User.php"> Statistik Kelompok Tani</a></li> 
                      </ul>
         </div><!-- /#sidebar-wrapper -->
        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="design/side_menu.js"></script>
<body onload="<?php if (@$_GET['pesan'] != "") { echo "alert('$_GET[pesan]');"; } ?>">
    <div class="section">
    <div class="container">
    <div class="row">
    <div class="col-md-10">
            <h1>Daftar Keanggotaan Petani</h1>

 
      <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">   
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kelompok Tani</th>
                  <th>Nama Petani</th>
                  <th>Tanggal Gabung</th>
                  <th>Tanggal Kadaluarsa</th>
                  <th>Keterangan</th>
                  
                </tr>
              </thead>
              
              <tbody>
              <?php
                    $koneksi = include("koneksi.php");                      
                    $hasil = mysqli_query($koneksi,"select trans_ang_petani.* from trans_ang_petani JOIN master_kel_tani ON trans_ang_petani.ID_Kelompok_Tani=master_kel_tani.ID_Kelompok_Tani where master_kel_tani.ID_User = '$_SESSION[ID_User]'");
                    $no = 1;
                    while($petani = mysqli_fetch_array($hasil)){
                    $h = mysqli_fetch_assoc(mysqli_query($koneksi, "select Nama_Kelompok_Tani from master_kel_tani where ID_Kelompok_Tani= '" . $petani['ID_Kelompok_Tani'] . "'"));
                    $p = mysqli_fetch_assoc(mysqli_query($koneksi, "select Nama_Petani from master_petani where ID_User= '" . $petani['ID_User'] . "'"));
                    $namakeltani = $h['Nama_Kelompok_Tani'];
                    $namapetani = $p['Nama_Petani'];
              ?>  
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $namakeltani ?></td>
                  <td><?php echo $namapetani ?></td>
                  <td><?php echo substr($petani['Tgl_Gabung'], 8, 2)."-".substr($petani['Tgl_Gabung'], 5, 2).'-'.substr($petani['Tgl_Gabung'], 0, 4); ?></td>
                  <td><?php 
                  //2099-12-31
                  //0123456789
                  echo substr($petani['Tgl_Expired'], 8, 2)."-".substr($petani['Tgl_Expired'], 5, 2).'-'.substr($petani['Tgl_Expired'], 0, 4);

                   ?></td>
                  <td><?php echo $petani['Keterangan']?></td>
                </tr>
              </tbody>
              <?php
              
              }?>
            </table>
          </div>
        </div>
        <div class="row">
        <div class="col-md-12">
            <a class="btn btn-lg btn-danger" href="KeangotaanPetaniUser.php">Kembali</a>
         
          </div>

        </div>
      </div>
  

</body></html>