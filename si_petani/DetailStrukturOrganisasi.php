<?php
  session_start();
  if (!isset($_SESSION ['ID_User'])){
      header("location:login.php?pesan=Mohon Login");
  }
  elseif ($_SESSION["ID_Kategori"]!="ADP")
  {
    header("Location:login.php?pesan=khusus untuk admin ");
  }
  ?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="design\coba2.css" rel="stylesheet" type="text/css">
        <link href="design\side-bar.css" rel="stylesheet">
</head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Pangkalan Data Petani dan Komunitas Tani</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
      <ul class="nav navbar-nav navbar-right">
    <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
    ?>
        <li style="background-color: #024A0B"><a href="#">Selamat Datang <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li style="background-color: #024A0B"><a href="logout.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Pangkalan Data</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Wilayah.php"> Pendataan Wilayah</a></li> 
                        <!-- <li><a href="PetaniUser.php">Pendataan Petani</a></li> -->
                        <li><a href="KelompokTani.php">Pendataan Kelompok Tani</a></li>
                        <li><a href="StrukturOrganisasi.php">Pendataan Struktur Organisasi</a></li>
                        <li><a href="KeangotaanPetani.php"> Pendataan Keanggotaan Petani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Petani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikWilayah.php"> Berdasarkan Wilayah</a></li>
                        <li><a href="#"> Berdasarkan Agama</a>
                          <li>
                              <a href="SumarryTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniAgama.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Usia</a>
                          <li>
                              <a href="SumarryTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Tk. Pendidikan</a>
                          <li>
                              <a href="SumarryTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikTaniPendidikan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Status Petani</a>
                          <li>
                                <a href="SumarryTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikTaniStatus.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                            </li>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Informasi Kelompok Tani</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="GrafikKelTaniWilayah.php"> Berdasarkan Wilayah</a></li>
                       
                        <li><a href="#"> Berdasarkan Lama Terbentuk</a>
                          <li>
                              <a href="SumarryKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                              <a href="GrafikKelTaniUmur.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="#"> Berdasarkan Perkembangan Kelompok Tani</a>
                          <li>
                                <a href="SumarryKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKelTaniPerkemb.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>              
                        <li><a href="#"> Berdasarkan Keanggotaan Petani</a>
                        <li>
                                <a href="SumarryKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-list-alt fa-stack-1x "></i></span> Tabel</a>
                                <a href="GrafikKeanggotaanTani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-signal fa-stack-1x "></i></span> Grafik</a>
                          </li>
                        </li>
                        <li><a href="Detail_Kel_Tani.php"> Detail Anggota Kelompok Tani</a></li> 
                        <li><a href="StatistikKelompokTani.php"> Statistik Kelompok Tani</a></li>
                    </ul>
                
        </div><!-- /#sidebar-wrapper -->
    
    <script src="design/side_menu.js"></script>

        <body>        
        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Detail Struktur Organisasi</h1>
                    </div>
                    <div class="col-lg-12">
                    <?php
                       $id_keltani = $_GET['id'];
                       $koneksi = include("koneksi.php");                      
                       $hasil = mysqli_query($koneksi,"SELECT * FROM `trans_struk_org` where ID_Kelompok_Tani ='$id_keltani' order by Tgl_awal desc limit 1");
                       $no = 1;
                       while($struk_org = mysqli_fetch_array($hasil)){
                    ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            Tanggal Awal  : <?php echo substr($struk_org['Tgl_Awal'], 8, 2)."-".substr($struk_org['Tgl_Awal'], 5, 2).'-'.substr($struk_org['Tgl_Awal'], 0, 4); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            Tanggal Selesai      : <?php echo substr($struk_org['Tgl_Selesai'],8,2)."-".substr($struk_org['Tgl_Selesai'], 5, 2).'-'.substr($struk_org['Tgl_Selesai'], 0, 4);?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            Nama Wakil Ketua : <?php echo $struk_org['Nama_Wakil_Ketua']
                                            ?>
                                        </div>
                                    </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        Telpon Wakil Ketua : <?php echo $struk_org['Telpon_Wakil_Ketua']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-12">
                                        Nama Sekretaris   : <?php echo $struk_org['Nama_Sekretaris']
                                        ?>
                                    </div>
                                 </div>   
                                 <div class="row">
                                    <div class="col-md-12">
                                        Telepon Sekretaris : <?php echo $struk_org['Telpon_Sekretaris']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-12">
                                        Nama Bendahara : <?php echo $struk_org['Nama_Bendahara']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-12">
                                        Telpon Bendahara : <?php echo $struk_org['Telpon_Bendahara']
                                        ?>
                                    </div>
                                 </div>
                                 <br>
                            <div class="row">
                                <div class="col-md-12">
                                    Scan Struktur Organisasi :

                                    <?php echo '<img src="img/scan_struktur_organisasi/'.$struk_org['Scan_Struktur_Organisasi'].'"class="img-rounded" width="130" />'
                                    ?>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    Scan Susunan Pengurus :

                                    <?php echo '<img src="img/scan_susunan_pengurus/'.$struk_org['Scan_Susunan_Pengurus'].'"class="img-rounded" width="130" />'
                                    ?>
                                </div>
                            </div>
                            <br>
                        </div>   
                        </div>
                            <br>
                        </div>  
                            <?php
                            }
                            ?>
                            
                        <div class="col-md-12 text-left">
                            <a class="btn btn-lg btn-danger" href="DataStrukturOrganisasi.php">Back</a>
                        </div>
                </div>
            </div>
         </div>
     </body>
</html>