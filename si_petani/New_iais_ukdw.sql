-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Mar 2017 pada 01.43
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iais_ukdw`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE IF NOT EXISTS `kabupaten` (
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kabupaten`
--

INSERT INTO `kabupaten` (`Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Bantul', 'Daerah Istimewa Yogyakarta'),
('Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Sleman', 'Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE IF NOT EXISTS `kecamatan` (
  `Nama_Kecamatan` varchar(100) NOT NULL,
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`Nama_Kecamatan`, `Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Dlingo', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Panggang', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Pengasih', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Turi', 'Sleman', 'Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan_desa`
--

CREATE TABLE IF NOT EXISTS `kelurahan_desa` (
  `Nama_Desa` varchar(100) NOT NULL,
  `Nama_Kecamatan` varchar(100) NOT NULL,
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelurahan_desa`
--

INSERT INTO `kelurahan_desa` (`Nama_Desa`, `Nama_Kecamatan`, `Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Jagalan', 'Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Singosaren', 'Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Terong', 'Dlingo', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Klitren', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Terban', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Ngupasan', 'Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Prawirodirjan', 'Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Bangunjiwo', 'kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Tirtonirmolo', 'kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Donoharjo', 'Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Minomartani', 'Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Beji', 'Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Watusigar', 'Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Hargobingangun', 'Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Pakembingangun', 'Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Girimulyo', 'Panggang', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Karang Sari', 'Pengasih', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Kalitekuk', 'Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('kemejing', 'Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Kaliagung', 'Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Salamrejo', 'Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Palihan', 'Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Sindutan', 'Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Wonokerto', 'Turi', 'Sleman', 'Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_aktifitas_pelatihan`
--

CREATE TABLE IF NOT EXISTS `log_aktifitas_pelatihan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_aktifitas_pelayanan`
--

CREATE TABLE IF NOT EXISTS `log_aktifitas_pelayanan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan_Pelayanan` varchar(10) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_biaya_keluar`
--

CREATE TABLE IF NOT EXISTS `log_biaya_keluar` (
  `ID_Spesies` varchar(10) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL,
  `ID_Aktivitas` varchar(10) NOT NULL,
  `ID_Petani` varchar(10) NOT NULL,
  `Tahun_Biaya` int(4) DEFAULT NULL,
  `ID_Biaya` varchar(10) NOT NULL,
  `Jumlah_Satuan` int(4) DEFAULT NULL,
  `Harga_Satuan` int(10) DEFAULT NULL,
  `Tgl_Pengeluaran` date DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_penawaran_prod`
--

CREATE TABLE IF NOT EXISTS `log_penawaran_prod` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_permintaan`
--

CREATE TABLE IF NOT EXISTS `log_permintaan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan` varchar(15) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_user_trans`
--

CREATE TABLE IF NOT EXISTS `log_user_trans` (
  `ID_User` varchar(10) NOT NULL,
  `Tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID_Aktivitas` varchar(10) NOT NULL,
  `Keterangan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_aktifitas_spesies`
--

CREATE TABLE IF NOT EXISTS `master_aktifitas_spesies` (
  `ID_Aktivitas` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Periode_Waktu` int(3) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_aktivitas`
--

CREATE TABLE IF NOT EXISTS `master_aktivitas` (
  `ID_Aktivitas` varchar(10) NOT NULL,
  `Nama_Aktivitas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_alat_tani`
--

CREATE TABLE IF NOT EXISTS `master_alat_tani` (
  `ID_Alat` varchar(10) NOT NULL,
  `Nama_Alat` varchar(50) NOT NULL,
  `Deskripsi_Alat` varchar(200) DEFAULT NULL,
  `Spesifikasi` varchar(200) DEFAULT NULL,
  `Harga_Terendah` int(12) DEFAULT NULL,
  `Harga_Tertinggi` int(12) DEFAULT NULL,
  `Fungsi` varchar(200) DEFAULT NULL,
  `ID_Kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_alat_tani`
--

INSERT INTO `master_alat_tani` (`ID_Alat`, `Nama_Alat`, `Deskripsi_Alat`, `Spesifikasi`, `Harga_Terendah`, `Harga_Tertinggi`, `Fungsi`, `ID_Kategori`) VALUES
('1', 'hhhdj', 'jjj', 'akaka', 2929, 3939, 'akak', '1'),
('2', 'kon', 'shshhs', 'jssjs', 283, 454545, 'ajajsj', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_bahan_tani`
--

CREATE TABLE IF NOT EXISTS `master_bahan_tani` (
  `ID_Bahan` varchar(10) NOT NULL,
  `Nama_Bahan` varchar(50) NOT NULL,
  `Deskripsi_Bahan` varchar(200) DEFAULT NULL,
  `Spesifikasi_Bahan` varchar(200) DEFAULT NULL,
  `Harga_Terendah` int(12) DEFAULT NULL,
  `Harga_Tertinggi` int(12) DEFAULT NULL,
  `Fungsi_Bahan` varchar(200) DEFAULT NULL,
  `Jenis_Bahan` varchar(100) DEFAULT NULL,
  `ID_Kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_detail_materi_ajar`
--

CREATE TABLE IF NOT EXISTS `master_detail_materi_ajar` (
  `ID_Materi_Ajar` varchar(10) NOT NULL,
  `ID_Sub_materi` varchar(10) NOT NULL,
  `Deskripsi_sub` varchar(200) NOT NULL,
  `file_materi` varchar(200) NOT NULL,
  `link_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_fasilitator`
--

CREATE TABLE IF NOT EXISTS `master_fasilitator` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Fasilitator` varchar(50) NOT NULL,
  `Alamat_Fasilitator` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Telpon` varchar(15) DEFAULT NULL,
  `Pendidikan_Terakhir` varchar(3) DEFAULT NULL,
  `Jurusan` varchar(30) DEFAULT NULL,
  `Kompetensi_Keahlian` varchar(200) DEFAULT NULL,
  `Riwayat_Pendidikan` varchar(200) DEFAULT NULL,
  `Pengalaman_Kerja` varchar(200) DEFAULT NULL,
  `Foto` varchar(200) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_jenis_sup`
--

CREATE TABLE IF NOT EXISTS `master_jenis_sup` (
  `ID_Jenis_Sup` varchar(10) NOT NULL,
  `Nama_Jenis_Sup` varchar(50) NOT NULL,
  `Deskripsi_Jenis_Sup` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kategori`
--

CREATE TABLE IF NOT EXISTS `master_kategori` (
  `ID_Kategori` char(3) NOT NULL,
  `Nama_Kategori` varchar(40) NOT NULL,
  `Deskripsi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_kategori`
--

INSERT INTO `master_kategori` (`ID_Kategori`, `Nama_Kategori`, `Deskripsi`) VALUES
('ADK', 'Admin Kelompok Tani', 'Admin Kelompok Tani'),
('ADP', 'Admin Petani', 'Admin Web'),
('PET', 'Petani', 'Anggota Petani');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kategori_alatbahan`
--

CREATE TABLE IF NOT EXISTS `master_kategori_alatbahan` (
  `ID_Kategori` varchar(10) NOT NULL,
  `Jenis_kategori` varchar(1) NOT NULL,
  `Nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_kategori_alatbahan`
--

INSERT INTO `master_kategori_alatbahan` (`ID_Kategori`, `Jenis_kategori`, `Nama_kategori`) VALUES
('1', '1', 'bermesina'),
('2', '2', 'manual'),
('4', '4', 'kambing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kel_tani`
--

CREATE TABLE IF NOT EXISTS `master_kel_tani` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `Nama_Kelompok_Tani` varchar(50) NOT NULL,
  `Alamat_Sekretariat` varchar(50) NOT NULL,
  `Kabupaten` varchar(50) NOT NULL,
  `Kecamatan` varchar(50) NOT NULL,
  `Provinsi` varchar(50) NOT NULL,
  `Desa_Kelurahan` varchar(50) NOT NULL,
  `Deskripsi` varchar(200) NOT NULL,
  `Foto1` varchar(200) NOT NULL,
  `Foto2` varchar(200) DEFAULT NULL,
  `Legalitas` varchar(100) NOT NULL,
  `Bukti_Legalitas` varchar(200) NOT NULL,
  `Kontak_Person` varchar(50) NOT NULL,
  `Nomor_Telpon` varchar(15) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Tgl_Terbentuk` date NOT NULL,
  `Email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_kel_tani`
--

INSERT INTO `master_kel_tani` (`ID_Kelompok_Tani`, `Nama_Kelompok_Tani`, `Alamat_Sekretariat`, `Kabupaten`, `Kecamatan`, `Provinsi`, `Desa_Kelurahan`, `Deskripsi`, `Foto1`, `Foto2`, `Legalitas`, `Bukti_Legalitas`, `Kontak_Person`, `Nomor_Telpon`, `ID_User`, `Tgl_Terbentuk`, `Email`) VALUES
('KelTani001', 'Ngudi Lestari', 'Jalan Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'terletak di keamatan panggan dusun Glaheng', 'KelTani001.jpg', NULL, '00000000000', 'KelTani001.jpg', 'Roehmad', '081228953212', 'AdmKel0001', '2015-12-09', 'RoehmadSuparto@yahoo.com'),
('KelTani002', 'Ngudi Rejeki', 'Jl Gedang Sari', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '-', 'KelTani002.jpg', NULL, '00000000000', 'KelTani002.jpg', 'Sumarno', '0878839033892', 'AdmKel0002', '2012-06-02', 'sumarno_tungu@gmail.com'),
('KelTani003', 'Kelompok Perempuan Wonorojo', 'Imorejo', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '-', 'KelTani003.jpg', NULL, '00000000000', 'KelTani003.jpg', 'Ida Kristiana', '000000', 'AdmKel0003', '2014-07-15', 'belumada@belumada.com'),
('KelTani004', 'Kebokuning', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '-', 'KelTani004.jpg', NULL, '00000000000', 'KelTani004.jpg', 'Rubikem', '087838297451', 'AdmKel0004', '2013-05-05', 'belumada@belumada.com'),
('KelTani005', 'Sendang Rejeki', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '-', 'KelTani005.jpg', NULL, '00000000000', 'KelTani005.jpg', 'Yunaliyati', '000000', 'AdmKel0005', '2010-02-09', 'belumada@belumada.com'),
('KelTani006', 'kelompok tani', 'jalan dami', 'Sleman', 'Ngaglik', 'Daerah Istimewa Yogyakarta', 'Donoharjo', 'kelompok baru', 'KelTani006.jpg', NULL, '00000000000', 'KelTani006.jpg', 'Malvin', '08121584000', 'Petani0040', '2017-03-16', 'christopher.malvin@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kode_biaya`
--

CREATE TABLE IF NOT EXISTS `master_kode_biaya` (
  `ID_Biaya` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Uraian_Biaya` varchar(100) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga_satuan` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_materi_ajar`
--

CREATE TABLE IF NOT EXISTS `master_materi_ajar` (
  `ID_Materi_Ajar` varchar(10) NOT NULL,
  `Nama_materi` varchar(100) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `deskripsi_materi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_materi_pelatihan`
--

CREATE TABLE IF NOT EXISTS `master_materi_pelatihan` (
  `ID_Materi` varchar(10) NOT NULL,
  `Nama_Materi` varchar(50) NOT NULL,
  `Deskripsi_Materi` varchar(200) DEFAULT NULL,
  `Materi_Sebelumnya` varchar(10) DEFAULT NULL,
  `Kebutuhan_Spesifikasi` varchar(200) DEFAULT NULL,
  `Level` varchar(20) DEFAULT NULL,
  `Jumlah_Jam` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_morf_tanaman`
--

CREATE TABLE IF NOT EXISTS `master_morf_tanaman` (
  `ID_Morfologi` varchar(10) NOT NULL,
  `Nama_Morfologi_Tanaman` varchar(50) DEFAULT NULL,
  `Nama_Divisi` varchar(10) DEFAULT NULL,
  `Nama_Sub_Divisi` varchar(10) DEFAULT NULL,
  `Nama_Ordo` varchar(10) DEFAULT NULL,
  `Nama_Famili` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_org_unit`
--

CREATE TABLE IF NOT EXISTS `master_org_unit` (
  `Org_Unit` varchar(10) NOT NULL,
  `Nama_Organisasi` varchar(100) NOT NULL,
  `Tingkatan` varchar(30) NOT NULL,
  `Org_Unit_Atasan` varchar(10) NOT NULL,
  `Pelayanan` tinyint(1) DEFAULT NULL,
  `Pendaftaran_Anggota` tinyint(1) DEFAULT NULL,
  `Pelatihan` tinyint(1) DEFAULT NULL,
  `Konsultasi` tinyint(1) DEFAULT NULL,
  `Penawaran` tinyint(1) DEFAULT NULL,
  `Permintaan` tinyint(1) DEFAULT NULL,
  `Memberi_Informasi` tinyint(1) DEFAULT NULL,
  `Meminta_Informasi` tinyint(1) DEFAULT NULL,
  `Berbagi_Informasi` tinyint(1) DEFAULT NULL,
  `Data_Spesifik_Anggota` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_pelayanan`
--

CREATE TABLE IF NOT EXISTS `master_pelayanan` (
  `ID_Pelayanan` varchar(10) NOT NULL,
  `Nama_Pelayanan` varchar(50) NOT NULL,
  `Deskripsi_Pelayanan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_petani`
--

CREATE TABLE IF NOT EXISTS `master_petani` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Petani` varchar(50) NOT NULL,
  `Alamat_Petani` varchar(50) NOT NULL,
  `Kabupaten` varchar(50) NOT NULL,
  `Kecamatan` varchar(50) NOT NULL,
  `Provinsi` varchar(50) NOT NULL,
  `Desa_Kelurahan` varchar(50) NOT NULL,
  `Foto` varchar(200) DEFAULT NULL,
  `Nomor_Telpon` varchar(15) NOT NULL,
  `Pendidikan_Terakhir` varchar(4) NOT NULL,
  `Jumlah_Tanggungan` int(2) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Agama` varchar(10) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Deskripsi_Keahlian` varchar(200) NOT NULL,
  `Status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_petani`
--

INSERT INTO `master_petani` (`ID_User`, `Nama_Petani`, `Alamat_Petani`, `Kabupaten`, `Kecamatan`, `Provinsi`, `Desa_Kelurahan`, `Foto`, `Nomor_Telpon`, `Pendidikan_Terakhir`, `Jumlah_Tanggungan`, `Email`, `Agama`, `Tanggal_Lahir`, `Deskripsi_Keahlian`, `Status`) VALUES
('Petani0001', 'Roehmad Suparto', 'Jalan Panggang Utama', 'Kulon Progo', 'Temon', 'Daerah Istimewa Yogyakarta', 'Sindutan', '', '081228953212', 'SMP', 0, 'RoehmadSuparto@yahoo.com', 'Islam', '1956-02-07', 'menanam padi, jagung, cabai', 1),
('Petani0002', 'Ngatilah', 'Jalan Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0002.jpg', '082324612253', 'SMA', 0, 'ngatilah@gmail.com', 'Islam', '1973-06-05', 'menanam padi', 1),
('Petani0003', 'Pajiyanti', 'Jalan Siluk Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0003.jpg', '000000', 'SD', 10, 'belumada@belumada.com', 'Islam', '1960-01-13', 'menanam padi', 0),
('Petani0004', 'NIsa Subekti', 'Jalan Panggang Utama', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0004.jpg', '087739773682', 'SD', 5, 'nisa@yahoo.com', 'Kristen', '1988-03-03', 'Menanam padi, jagung, kacang, singkong', 1),
('Petani0005', 'Ruswantiningsih', 'Jalan Siluk Panggang', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '081392591042', 'SMA', 3, 'ruswantiningsih@gmail.com', 'Katolik', '1970-12-09', 'menanam jagung, padi, kacang, ketela', 1),
('Petani0006', 'Mujiyo', 'Pantai Gesing km. 10', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0006.jpg', '0878839082181', 'SMP', 3, 'belumada@belumada.com', 'Islam', '1978-04-10', 'menanam jagung, padi', 1),
('Petani0007', 'Supriyati', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '000000', 'SMP', 10, 'belumada@belumada.com', 'Islam', '1979-07-11', 'menanam padi, jagung', 1),
('Petani0008', 'Giyanti', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '085328129673', 'SMA', 10, 'belumada@belumada.com', 'Islam', '1980-09-17', 'menanam padi', 1),
('Petani0009', 'Wakimin', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0009.png', '081228941305', 'SMP', 7, 'belumada@belumada.com', 'Kristen', '1971-09-29', 'menanam padi', 1),
('Petani0010', 'Wagiyem', 'Macan mati, Girimulyo', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '000000', 'SMP', 10, 'belumada@belumada.com', 'Kristen', '1966-10-02', 'menanam padi', 1),
('Petani0011', 'Sumarno', 'Jl Gedang Sari', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', 'Petani0011.jpg', '0878839033892', 'SMP', 6, 'sumarno_tungu@gmail.com', 'Katolik', '1974-01-14', 'menanam padi, jagung, ketela', 1),
('Petani0012', 'Suwadiman', 'Nggesing', 'Gunungkidul', 'Panggang', 'Daerah Istimewa Yogyakarta', 'Girimulyo', '', '000000', 'SD', 4, 'belumada@belumada.com', 'Islam', '1965-08-13', 'menanam padi, jagung, ketela, kacang, kedelai', 1),
('Petani0013', 'Suprapto', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 2, 'belumada@belumada.com', 'Islam', '1967-04-28', 'tidak memiliki keahlian', 0),
('Petani0014', 'Amroni', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SMA', 2, 'belumada@belumada.com', 'Katolik', '1970-05-21', 'menanam padi, jagung, peternak gurami', 1),
('Petani0015', 'Siti Yuningsih', 'Kopen Becici', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0015.jpg', '085701145454', 'SMA', 0, 'yuningsih.siti@yahoo.com', 'Islam', '1969-02-18', 'menanam salak, cabe, sayuran', 1),
('Petani0016', 'Ida Krisharia', 'Imorejo', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '000000', 'SMA', 0, 'belumada@belumada.com', 'Katolik', '1972-12-23', 'tidak ada', 1),
('Petani0017', 'Riyantiningsih', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0017.jpg', '087738513253', 'SMA', 3, 'riyanti@yahoo.co.id', 'Katolik', '1975-06-17', 'menanam salak', 1),
('Petani0018', 'Ismawati', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0018.jpg', '085743974065', 'SMA', 5, 'ismawati@gmail.com', 'Kristen', '1978-08-09', 'menanam salak', 1),
('Petani0019', 'Sri Hartutik', 'Selowangsan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '000000', 'SMA', 0, 'tutik_salak@yahoo.co.id', 'Islam', '1974-11-17', 'menanam salak, padi, sayuran', 1),
('Petani0021', 'Erna Kusrini', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0021.jpg', '085643222860', 'SMA', 0, 'erna_kusrini@yahoo.com', 'Kristen', '1976-10-20', 'menanam salak', 1),
('Petani0022', 'Sri Haryanti', 'Dadapan', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '000000', 'SMA', 0, 'belumada@belumada.com', 'Islam', '1977-02-08', 'tidak ada', 0),
('Petani0023', 'Kristiningsih', 'Sempu', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0023.jpg', '000000', 'SMA', 3, 'kristiningsih@gmail.com', 'Kristen', '1977-11-16', 'tidak ada', 0),
('Petani0024', 'Nursiyah', 'Sempu', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', '', '0817273103', 'SMA', 4, 'nursiyah@gmail.com', 'Islam', '1974-07-11', 'menanam salak', 1),
('Petani0025', 'Nur Kahyatun', 'Imorejo', 'Sleman', 'Turi', 'Daerah Istimewa Yogyakarta', 'Wonokerto', 'Petani0025.png', '085799382768', 'D2', 3, 'khayatun@gmail.com', 'Islam', '1969-09-10', 'menanam salak', 1),
('Petani0026', 'Rubikem', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '087838297451', 'SMP', 2, 'belumada@belumada.com', 'Islam', '1964-07-15', 'tidak memiliki keahlian', 0),
('Petani0027', 'Siti Fatimah', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '08175420109', 'SMP', 0, 'siti.fatimah@yahoo.co.id', 'Islam', '1980-09-24', 'menanam padi', 1),
('Petani0028', 'Sarbini', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SMA', 0, 'belumada@belumada.com', 'Islam', '1969-03-16', 'menanam jagung & padi', 1),
('Petani0029', 'Pradiwiyanto', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SD', 0, 'belumada@belumada.com', 'Islam', '1951-11-14', 'menanam padi, coklat, jagung, perkebunan', 1),
('Petani0030', 'Partiem', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', 'Petani0030.jpg', '000000', 'SD', 3, 'partiem@gmail.com', 'Islam', '1971-01-23', 'menanam padi, jahe, jagung, singkong', 1),
('Petani0031', 'Maryudi', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SMP', 2, 'belumada@belumada.com', 'Islam', '1952-04-07', 'menanam singkong, padi, kacang tanah', 0),
('Petani0032', 'Sismail', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SMP', 2, 'belumada@belumada.com', 'Islam', '1956-05-15', 'menanam singkong, jagung, padi', 0),
('Petani0033', 'Muhyadi', 'Kebokuning', 'Bantul', 'Dlingo', 'Daerah Istimewa Yogyakarta', 'Terong', '', '000000', 'SD', 1, 'belumada@belumada.com', 'Islam', '1951-10-27', 'menanam padi, ketela, jagung, jati, mahoni', 1),
('Petani0034', 'Asmilah', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 4, 'belumada@belumada.com', 'Islam', '1977-02-08', 'tidak memiliki keahlian', 0),
('Petani0035', 'Utamiyem', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SMP', 0, 'belumada@belumada.com', 'Islam', '1970-11-11', 'tidak memiliki keahlian', 0),
('Petani0036', 'Kastopah', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 5, 'belumada@belumada.com', 'Islam', '1977-02-14', 'tidak memiliki keahlian', 0),
('Petani0037', 'Mugirah', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', '', '000000', 'SD', 3, 'belumada@belumada.com', 'Islam', '1969-05-14', 'tidak ada keahlian', 0),
('Petani0038', 'Sunaryati', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', 'Petani0038.jpg', '000000', 'SMP', 4, 'belumada@belumada.com', 'Katolik', '1969-04-15', 'tidak memiliki keahlian', 0),
('Petani0039', 'Yunaliyati', 'Sendang', 'Kulon Progo', 'Pengasih', 'Daerah Istimewa Yogyakarta', 'Karang Sari', 'Petani0039.jpg', '000000', 'SMA', 5, 'yunaliyati@yahoo.com', 'Katolik', '1976-08-09', 'menanam bawang merah', 1),
('Petani0040', 'Malvin', 'Jalan damai', 'Sleman', 'Ngaglik', 'Daerah Istimewa Yogyakarta', 'Donoharjo', 'Petani0040.JPG', '08121584000', 'SMA', 0, 'christopher.malvin@gmail.com', 'Kristen', '1994-03-24', 'tidak memiliki keahlian', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_peta_lahan`
--

CREATE TABLE IF NOT EXISTS `master_peta_lahan` (
  `ID_Lahan` varchar(10) NOT NULL,
  `Koordinat_X` varchar(20) DEFAULT NULL,
  `Koordinat_Y` varchar(20) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `bulan` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_produk_tani`
--

CREATE TABLE IF NOT EXISTS `master_produk_tani` (
  `ID_Produk` varchar(10) NOT NULL,
  `Nama_Produk` varchar(200) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Deskripsi_Produk` varchar(200) DEFAULT NULL,
  `Satuan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_spesies _tanaman`
--

CREATE TABLE IF NOT EXISTS `master_spesies _tanaman` (
  `ID_Spesies` varchar(10) NOT NULL,
  `Jenis_Tanaman` varchar(20) DEFAULT NULL,
  `Nama_Tanaman` varchar(50) NOT NULL,
  `Nama_Latin` varchar(50) DEFAULT NULL,
  `Habitat` varchar(50) DEFAULT NULL,
  `Masa_Tanam` int(4) DEFAULT NULL,
  `Akar` varchar(200) DEFAULT NULL,
  `Batang` varchar(200) DEFAULT NULL,
  `Daun` varchar(200) DEFAULT NULL,
  `Buah` varchar(200) DEFAULT NULL,
  `Biji` varchar(200) DEFAULT NULL,
  `Perkembangbiakan` varchar(200) DEFAULT NULL,
  `Foto1` varchar(200) DEFAULT NULL,
  `Foto2` varchar(200) DEFAULT NULL,
  `Iklim` varchar(50) DEFAULT NULL,
  `Jenis_Tanah` varchar(20) DEFAULT NULL,
  `Kelembaban` varchar(20) DEFAULT NULL,
  `ID_Morfologi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_supplier`
--

CREATE TABLE IF NOT EXISTS `master_supplier` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Supplier` varchar(200) NOT NULL,
  `ID_Jenis_Sup` varchar(10) NOT NULL,
  `Alamat_Supplier` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Kontak_Person` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `No_Handphone` varchar(15) DEFAULT NULL,
  `No_Telpon` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_user`
--

CREATE TABLE IF NOT EXISTS `master_user` (
  `ID_User` varchar(10) NOT NULL,
  `Password` varchar(70) NOT NULL,
  `PIN` int(6) NOT NULL,
  `Tingkat_Priv` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_user`
--

INSERT INTO `master_user` (`ID_User`, `Password`, `PIN`, `Tingkat_Priv`) VALUES
('AdmKel0001', '767676', 878787, '2'),
('AdmKel0002', 'aslajsl', 78787, '2'),
('AdmKel0003', 'kkkllii', 909090, '2'),
('AdmKel0004', 'KeboKuning', 92832, '2'),
('AdmKel0005', 'SendangRejeki', 989898, '2'),
('AdmKel0006', 'TaniMakmur', 515151, '2'),
('AdmKel0007', 'KismoMudo', 70707, '2'),
('AdmPet0001', '8989898', 928392, '1'),
('Petani0001', '1231426', 918990, '2'),
('Petani0002', '898989', 989898, '3'),
('Petani0003', '5454545', 656565, '3'),
('Petani0004', 'ausia', 8989, '2'),
('Petani0005', 'asajs', 293892, '2'),
('Petani0006', '090909', 343434, '3'),
('Petani0007', 'asdfg', 987676, '3'),
('Petani0008', 'lkjhg', 98778, '3'),
('Petani0009', 'i lobe u', 989898, '3'),
('Petani0010', 'kelompok tani', 454531, '3'),
('Petani0011', 'namaku', 555555, '3'),
('Petani0012', 'kelompok', 444444, '3'),
('Petani0013', 'kikiki', 656565, '3'),
('Petani0014', 'pantai', 281928, '3'),
('Petani0015', 'gesing', 987654, '3'),
('Petani0016', 'lllll', 99999, '3'),
('Petani0017', 'oooooo', 0, '3'),
('Petani0018', 'kujyfhj', 878676, '3'),
('Petani0019', 'poojhfvj', 765571, '3'),
('Petani0020', 'kkkjjjjhb', 543352, '3'),
('Petani0021', 'llloooppp', 888777, '3'),
('Petani0022', 'lllpppuuuyy', 776655, '3'),
('Petani0023', 'khyugga', 656565, '3'),
('Petani0024', 'llluu', 809070, '3'),
('Petani0025', 'asja', 9087342, '3'),
('Petani0026', 'lkjhgfds', 98765, '3'),
('Petani0027', 'kuyhfnnu', 765435, '3'),
('Petani0028', 'kuiyty', 979896, '3'),
('Petani0029', 'oapsoap', 767678, '3'),
('Petani0030', 'kkkkk', 676767, '3'),
('Petani0031', 'lllll', 313131, '3'),
('Petani0032', 'opopop', 322332, '3'),
('Petani0033', 'tytyt', 333333, '3'),
('Petani0034', 'iuiuiuiu', 343443, '3'),
('Petani0035', 'lllpppo', 355334, '3'),
('Petani0036', 'ooooo', 633636, '3'),
('Petani0037', 'pppp', 98998, '3'),
('Petani0038', 'llklklk', 388338, '3'),
('Petani0039', 'uiuiui', 393939, '3'),
('Petani0040', 'kpkpkp', 40404, '3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_user_kat`
--

CREATE TABLE IF NOT EXISTS `master_user_kat` (
  `ID_Kategori` char(3) NOT NULL,
  `ID_User` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_user_kat`
--

INSERT INTO `master_user_kat` (`ID_Kategori`, `ID_User`) VALUES
('ADP', 'AdmPet0001'),
('ADK', 'AdmKel0001'),
('ADK', 'AdmKel0002'),
('ADK', 'AdmKel0003'),
('ADK', 'AdmKel0004'),
('ADK', 'AdmKel0005'),
('ADK', 'AdmKel0006'),
('ADK', 'AdmKel0007'),
('PET', 'Petani0001'),
('PET', 'Petani0002'),
('PET', 'Petani0003'),
('PET', 'Petani0004'),
('PET', 'Petani0005'),
('PET', 'Petani0006'),
('PET', 'Petani0007'),
('PET', 'Petani0008'),
('PET', 'Petani0009'),
('PET', 'Petani0010'),
('PET', 'Petani0011'),
('PET', 'Petani0012'),
('PET', 'Petani0013'),
('PET', 'Petani0014'),
('PET', 'Petani0015'),
('PET', 'Petani0016'),
('PET', 'Petani0017'),
('PET', 'Petani0018'),
('PET', 'Petani0019'),
('PET', 'Petani0020'),
('PET', 'Petani0021'),
('PET', 'Petani0022'),
('PET', 'Petani0023'),
('PET', 'Petani0024'),
('PET', 'Petani0025'),
('PET', 'Petani0026'),
('PET', 'Petani0027'),
('PET', 'Petani0028'),
('PET', 'Petani0029'),
('PET', 'Petani0030'),
('PET', 'Petani0031'),
('PET', 'Petani0032'),
('PET', 'Petani0033'),
('PET', 'Petani0034'),
('PET', 'Petani0035'),
('PET', 'Petani0036'),
('PET', 'Petani0037'),
('PET', 'Petani0038'),
('PET', 'Petani0039'),
('PET', 'Petani0040');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_user_org`
--

CREATE TABLE IF NOT EXISTS `master_user_org` (
  `NIK` varchar(10) NOT NULL,
  `Password` varchar(14) NOT NULL,
  `Nama_Karyawan` varchar(100) NOT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Alamat_Rumah` varchar(100) DEFAULT NULL,
  `No_Telpon` varchar(15) DEFAULT NULL,
  `Org_Unit` varchar(10) NOT NULL,
  `Jenis_Kelamin` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE IF NOT EXISTS `provinsi` (
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`Nama_Provinsi`) VALUES
('Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_aktivitas_pertanian`
--

CREATE TABLE IF NOT EXISTS `trans_aktivitas_pertanian` (
  `ID_aktifitas_petani` varchar(10) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL,
  `ID_Petani` varchar(10) NOT NULL,
  `Tanggal_Mulai` date DEFAULT NULL,
  `Tanggal_Selesai` date DEFAULT NULL,
  `Tahun_Aktivitas` int(4) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_anggaraan`
--

CREATE TABLE IF NOT EXISTS `trans_anggaraan` (
  `ID_User` varchar(10) NOT NULL,
  `ID_Aktifitas` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `ID_Biaya` varchar(10) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `periode_tanam` varchar(2) NOT NULL,
  `jumlah_satuan` int(4) NOT NULL,
  `harga_satuan` int(9) NOT NULL,
  `waktu_perkiraan_keluar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_ang_petani`
--

CREATE TABLE IF NOT EXISTS `trans_ang_petani` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Tgl_Gabung` date NOT NULL,
  `Tgl_Expired` date NOT NULL,
  `Keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trans_ang_petani`
--

INSERT INTO `trans_ang_petani` (`ID_Kelompok_Tani`, `ID_User`, `Tgl_Gabung`, `Tgl_Expired`, `Keterangan`) VALUES
('KelTani001', 'Petani0001', '2015-12-09', '2099-12-31', 'ok'),
('KelTani001', 'Petani0002', '2015-12-09', '2099-12-31', 'berlaku seumur hidup'),
('KelTani001', 'Petani0003', '2015-12-09', '2099-12-31', 'berlaku seumur hidup'),
('KelTani001', 'Petani0004', '2016-01-03', '2099-12-31', '-'),
('KelTani001', 'Petani0005', '2016-01-03', '2099-12-31', '-'),
('KelTani002', 'Petani0006', '2012-06-02', '2099-12-31', '-'),
('KelTani002', 'Petani0007', '2012-06-12', '2099-12-31', '-'),
('KelTani002', 'Petani0008', '2012-05-07', '2099-12-31', '-'),
('KelTani002', 'Petani0009', '2012-06-02', '2099-12-31', '-'),
('KelTani002', 'Petani0010', '2016-01-12', '2099-12-31', '-'),
('KelTani002', 'Petani0011', '2014-06-17', '2099-12-31', '-'),
('KelTani002', 'Petani0012', '2015-10-13', '2099-12-31', '-'),
('KelTani002', 'Petani0013', '2013-05-06', '2099-12-31', '-'),
('KelTani002', 'Petani0014', '2016-02-03', '2099-12-31', 'anggota baru'),
('KelTani003', 'Petani0015', '2014-06-15', '2099-12-31', '-'),
('KelTani003', 'Petani0016', '2014-01-01', '2099-12-31', '-'),
('KelTani003', 'Petani0017', '2016-05-09', '2099-12-31', '-'),
('KelTani003', 'Petani0018', '2015-11-10', '2099-12-31', '-'),
('KelTani003', 'Petani0019', '2014-01-01', '2099-12-31', '-'),
('KelTani003', 'Petani0021', '2015-01-12', '2099-12-31', '-'),
('KelTani003', 'Petani0022', '2016-11-15', '2099-12-31', 'anggota baru'),
('KelTani003', 'Petani0023', '2014-06-17', '2099-12-31', '-'),
('KelTani003', 'Petani0024', '2015-02-10', '2099-12-31', '-'),
('KelTani003', 'Petani0025', '2014-01-01', '2099-12-31', '-'),
('KelTani003', 'Petani0026', '2015-05-05', '2099-12-31', 'Ketua Kelompok Tani'),
('KelTani004', 'Petani0026', '2015-05-05', '2099-12-31', 'Ketua Kelompok Tani'),
('KelTani004', 'Petani0027', '2013-05-12', '2099-12-31', '-'),
('KelTani004', 'Petani0028', '2014-02-10', '2099-12-31', '-'),
('KelTani004', 'Petani0029', '2013-05-05', '2099-12-31', '-'),
('KelTani004', 'Petani0030', '2015-05-17', '2099-12-31', '-'),
('KelTani004', 'Petani0032', '2015-11-09', '2099-12-31', ''),
('KelTani004', 'Petani0033', '2015-12-20', '2099-12-31', '-'),
('KelTani005', 'Petani0034', '2010-02-09', '2099-12-31', 'Ketua Kelompok Tani'),
('KelTani005', 'Petani0035', '2010-02-09', '2099-12-31', '-'),
('KelTani005', 'Petani0036', '2010-03-02', '2099-12-31', '-'),
('KelTani005', 'Petani0037', '2010-03-06', '2099-12-31', '-'),
('KelTani005', 'Petani0038', '2010-03-14', '2099-12-31', '-'),
('KelTani005', 'Petani0039', '2010-02-09', '2099-12-31', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_harga_prod`
--

CREATE TABLE IF NOT EXISTS `trans_harga_prod` (
  `ID_Produk` varchar(10) NOT NULL,
  `Tanggal` date NOT NULL,
  `Harga` int(15) NOT NULL,
  `ID_User` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_hasil_panen`
--

CREATE TABLE IF NOT EXISTS `trans_hasil_panen` (
  `ID_Petani` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Bulan_Panen` varchar(2) DEFAULT NULL,
  `Tahun_Panen` varchar(4) NOT NULL,
  `Periode_Panen` varchar(2) NOT NULL,
  `Jumlah_Hasil_Panen` int(4) DEFAULT NULL,
  `ID_Produk` varchar(10) NOT NULL,
  `Luas_lahan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_kalender_tanam`
--

CREATE TABLE IF NOT EXISTS `trans_kalender_tanam` (
  `ID_Spesies` varchar(10) NOT NULL,
  `Nama_Kalender` varchar(50) NOT NULL,
  `Masa_Tanam` varchar(2) NOT NULL,
  `Kabupaten` varchar(50) NOT NULL,
  `Provinsi` varchar(50) NOT NULL,
  `Tanggal_Awal` date NOT NULL,
  `Tanggal_Akhir` date NOT NULL,
  `Pengolahan_Lahan_Awal` date NOT NULL,
  `Pengolahan_Lahan_Akhir` date NOT NULL,
  `Persiapan_Lahan_Awal` date NOT NULL,
  `Masa_Penanaman_Awal` date NOT NULL,
  `Masa_Pemupukan` date NOT NULL,
  `Masa_Panen` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_pelaksanaan_pelatihan`
--

CREATE TABLE IF NOT EXISTS `trans_pelaksanaan_pelatihan` (
  `ID_User` varchar(10) NOT NULL,
  `Tanggal_Pelaksanaan` date DEFAULT NULL,
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `Pembahasan` varchar(200) DEFAULT NULL,
  `Sesi` varchar(1) DEFAULT NULL,
  `Jumlah_Peserta` int(3) DEFAULT NULL,
  `Bukti_Materi` varchar(200) DEFAULT NULL,
  `Bukti_Daftar_Hadir` varchar(200) DEFAULT NULL,
  `Catatan` varchar(200) DEFAULT NULL,
  `Foto_Kegiatan_1` varchar(200) DEFAULT NULL,
  `Foto_Kegiatan_2` varchar(200) DEFAULT NULL,
  `Jam_Awal` timestamp NULL DEFAULT NULL,
  `Jam_Akhir` timestamp NULL DEFAULT NULL,
  `Nomor_Pelaksanaan_pelatihan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_penawaran_prod_tani`
--

CREATE TABLE IF NOT EXISTS `trans_penawaran_prod_tani` (
  `ID_User` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Tanggal_Penawaran` date DEFAULT NULL,
  `Spesifikasi_Barang` varchar(200) DEFAULT NULL,
  `ID_Barang` varchar(10) NOT NULL,
  `Kondisi_Barang` varchar(30) DEFAULT NULL,
  `Merk` varchar(30) DEFAULT NULL,
  `Harga` int(12) DEFAULT NULL,
  `Tahun_Produksi` int(4) DEFAULT NULL,
  `Gambar1` varchar(200) DEFAULT NULL,
  `Gambar2` varchar(200) DEFAULT NULL,
  `Status` varchar(10) DEFAULT NULL,
  `Stok` int(3) DEFAULT NULL,
  `Satuan_Barang` varchar(10) DEFAULT NULL,
  `Validasi_Admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_permintaan`
--

CREATE TABLE IF NOT EXISTS `trans_permintaan` (
  `ID_Permintaan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Qty` int(3) DEFAULT NULL,
  `Harga` int(12) DEFAULT NULL,
  `Tgl_Kebutuhan` date DEFAULT NULL,
  `Tgl_Permintaan` date DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_permintaan_pelatihan`
--

CREATE TABLE IF NOT EXISTS `trans_permintaan_pelatihan` (
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Materi` varchar(10) NOT NULL,
  `Tanggal_Awal_Permintaan` date DEFAULT NULL,
  `Tanggal_Akhir_Permintaan` date DEFAULT NULL,
  `Tempat` varchar(50) DEFAULT NULL,
  `Alamat` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Kontak_Person` varchar(50) DEFAULT NULL,
  `Telpon` varchar(50) DEFAULT NULL,
  `Jumlah_Target_Peserta` int(3) DEFAULT NULL,
  `Surat_Permintaan` varchar(200) DEFAULT NULL,
  `Tanggal_Awal_Setuju` date DEFAULT NULL,
  `Tanggal_Akhir_Setuju` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_permintaan_pelayanan`
--

CREATE TABLE IF NOT EXISTS `trans_permintaan_pelayanan` (
  `ID_Permintaan_Pelayanan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Pelayanan` varchar(10) NOT NULL,
  `Tanggal_Permintaan` date DEFAULT NULL,
  `Permasalahan` varchar(200) DEFAULT NULL,
  `Solusi` varchar(200) DEFAULT NULL,
  `Tanggal_Respon` date NOT NULL,
  `Penanggungjawab` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trans_struk_org`
--

CREATE TABLE IF NOT EXISTS `trans_struk_org` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `Tgl_Awal` date NOT NULL,
  `Tgl_Selesai` date NOT NULL,
  `Nama_Ketua` varchar(100) NOT NULL,
  `Telpon_Ketua` varchar(20) NOT NULL,
  `Nama_Wakil_Ketua` varchar(100) NOT NULL,
  `Telpon_Wakil_Ketua` varchar(20) NOT NULL,
  `Nama_Sekretaris` varchar(100) NOT NULL,
  `Telpon_Sekretaris` varchar(20) NOT NULL,
  `Nama_Bendahara` varchar(100) NOT NULL,
  `Telpon_Bendahara` varchar(20) NOT NULL,
  `Scan_Struktur_Organisasi` varchar(200) NOT NULL,
  `Scan_Susunan_Pengurus` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trans_struk_org`
--

INSERT INTO `trans_struk_org` (`ID_Kelompok_Tani`, `Tgl_Awal`, `Tgl_Selesai`, `Nama_Ketua`, `Telpon_Ketua`, `Nama_Wakil_Ketua`, `Telpon_Wakil_Ketua`, `Nama_Sekretaris`, `Telpon_Sekretaris`, `Nama_Bendahara`, `Telpon_Bendahara`, `Scan_Struktur_Organisasi`, `Scan_Susunan_Pengurus`) VALUES
('KelTani002', '2015-06-02', '2018-06-02', 'Sumarno', '0878839033892', 'Wakimin', '081228941305', 'Giyanti', '085328129673', 'Mujiyo', '0878839082181', 'KelTani002.jpg', 'KelTani002.jpg'),
('KelTani003', '2014-01-01', '2018-01-01', 'Ida Kristiana', '000000', '-', '000000', 'Ismawati', '085743974065', 'Nur Kahyatun', '085799382768', 'KelTani003.jpg', 'KelTani003.jpg'),
('KelTani004', '2015-05-05', '2019-05-05', 'Rubikem', '087838297451', '-', '000000', 'Sarbini', '000000', 'Sismail', '000000', 'KelTani004.jpg', 'KelTani004.jpg'),
('KelTani004', '2019-05-06', '2022-05-06', 'Sismail', '000000', 'Muhyadi', '000000', 'Sarbini', '000000', 'Partiem', '000000', 'KelTani004.jpg', 'KelTani004.jpg'),
('KelTani005', '2010-02-09', '2014-02-09', 'Asmilah', '000000', '-', '000000', 'Sunaryati', '000000', 'Kastopah', '000000', 'KelTani005.jpg', 'KelTani005.jpg'),
('KelTani005', '2014-02-10', '2018-02-10', 'Sunaryati', '000000', 'Muhyadi', '000000', 'Mugirah', '000000', 'Partiem', '000000', 'KelTani005.jpg', 'KelTani005.jpg'),
('KelTani005', '2018-02-12', '2020-02-13', 'Asmilah', '000000', '-', '000000', 'Mugirah', '000000', 'Partiem', '000000', 'KelTani005.jpg', 'KelTani005.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
 ADD PRIMARY KEY (`Nama_Kabupaten`), ADD KEY `Nama_Provinsi` (`Nama_Provinsi`), ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
 ADD PRIMARY KEY (`Nama_Kecamatan`), ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`), ADD KEY `Nama_Provinsi` (`Nama_Provinsi`), ADD KEY `Nama_Kecamatan` (`Nama_Kecamatan`), ADD KEY `Nama_Kecamatan_2` (`Nama_Kecamatan`), ADD KEY `Nama_Kabupaten_2` (`Nama_Kabupaten`), ADD KEY `Nama_Provinsi_2` (`Nama_Provinsi`), ADD KEY `Nama_Kecamatan_3` (`Nama_Kecamatan`);

--
-- Indexes for table `kelurahan_desa`
--
ALTER TABLE `kelurahan_desa`
 ADD PRIMARY KEY (`Nama_Desa`), ADD KEY `Nama_Kecamatan` (`Nama_Kecamatan`,`Nama_Kabupaten`,`Nama_Provinsi`), ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`), ADD KEY `Nama_Provinsi` (`Nama_Provinsi`), ADD KEY `Nama_Kecamatan_2` (`Nama_Kecamatan`), ADD KEY `Nama_Kabupaten_2` (`Nama_Kabupaten`), ADD KEY `Nama_Provinsi_2` (`Nama_Provinsi`), ADD KEY `Nama_Desa` (`Nama_Desa`), ADD KEY `Nama_Kecamatan_3` (`Nama_Kecamatan`), ADD KEY `Nama_Kabupaten_3` (`Nama_Kabupaten`), ADD KEY `Nama_Kabupaten_4` (`Nama_Kabupaten`), ADD KEY `Nama_Provinsi_3` (`Nama_Provinsi`), ADD KEY `Nama_Kecamatan_4` (`Nama_Kecamatan`);

--
-- Indexes for table `log_aktifitas_pelatihan`
--
ALTER TABLE `log_aktifitas_pelatihan`
 ADD PRIMARY KEY (`ID_Log`), ADD KEY `ID_Permintaan_Pelatihan` (`ID_Permintaan_Pelatihan`,`ID_User`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_aktifitas_pelayanan`
--
ALTER TABLE `log_aktifitas_pelayanan`
 ADD PRIMARY KEY (`ID_Log`), ADD KEY `ID_Permintaan_Pelayanan` (`ID_Permintaan_Pelayanan`,`ID_User`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_biaya_keluar`
--
ALTER TABLE `log_biaya_keluar`
 ADD KEY `ID_Aktivitas` (`ID_Aktivitas`,`ID_Petani`,`ID_Biaya`), ADD KEY `ID_Biaya` (`ID_Biaya`), ADD KEY `ID_Petani` (`ID_Petani`), ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`), ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `log_penawaran_prod`
--
ALTER TABLE `log_penawaran_prod`
 ADD PRIMARY KEY (`ID_Log`), ADD KEY `ID_Penawaran` (`ID_Penawaran`,`ID_User`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_permintaan`
--
ALTER TABLE `log_permintaan`
 ADD PRIMARY KEY (`ID_Log`), ADD KEY `ID_Permintaan` (`ID_Permintaan`,`ID_User`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_user_trans`
--
ALTER TABLE `log_user_trans`
 ADD KEY `ID_User` (`ID_User`,`ID_Aktivitas`), ADD KEY `ID_Aktivitas` (`ID_Aktivitas`);

--
-- Indexes for table `master_aktifitas_spesies`
--
ALTER TABLE `master_aktifitas_spesies`
 ADD PRIMARY KEY (`ID_aktifitas_spesies`), ADD KEY `ID_Spesies` (`ID_Spesies`), ADD KEY `ID_Aktivitas` (`ID_Aktivitas`), ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Indexes for table `master_aktivitas`
--
ALTER TABLE `master_aktivitas`
 ADD PRIMARY KEY (`ID_Aktivitas`);

--
-- Indexes for table `master_alat_tani`
--
ALTER TABLE `master_alat_tani`
 ADD PRIMARY KEY (`ID_Alat`), ADD KEY `ID_Kategori` (`ID_Kategori`);

--
-- Indexes for table `master_bahan_tani`
--
ALTER TABLE `master_bahan_tani`
 ADD PRIMARY KEY (`ID_Bahan`), ADD KEY `ID_Kategori` (`ID_Kategori`);

--
-- Indexes for table `master_detail_materi_ajar`
--
ALTER TABLE `master_detail_materi_ajar`
 ADD PRIMARY KEY (`ID_Materi_Ajar`,`ID_Sub_materi`), ADD KEY `ID_Materi_Ajar` (`ID_Materi_Ajar`);

--
-- Indexes for table `master_fasilitator`
--
ALTER TABLE `master_fasilitator`
 ADD PRIMARY KEY (`ID_User`);

--
-- Indexes for table `master_jenis_sup`
--
ALTER TABLE `master_jenis_sup`
 ADD PRIMARY KEY (`ID_Jenis_Sup`);

--
-- Indexes for table `master_kategori`
--
ALTER TABLE `master_kategori`
 ADD PRIMARY KEY (`ID_Kategori`);

--
-- Indexes for table `master_kategori_alatbahan`
--
ALTER TABLE `master_kategori_alatbahan`
 ADD PRIMARY KEY (`ID_Kategori`);

--
-- Indexes for table `master_kel_tani`
--
ALTER TABLE `master_kel_tani`
 ADD PRIMARY KEY (`ID_Kelompok_Tani`), ADD KEY `ID_User` (`ID_User`), ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`), ADD KEY `Kecamatan` (`Kecamatan`), ADD KEY `Kabupaten` (`Kabupaten`), ADD KEY `Desa_Kelurahan` (`Desa_Kelurahan`), ADD KEY `Desa_Kelurahan_2` (`Desa_Kelurahan`), ADD KEY `Kecamatan_2` (`Kecamatan`), ADD KEY `Provinsi` (`Provinsi`);

--
-- Indexes for table `master_kode_biaya`
--
ALTER TABLE `master_kode_biaya`
 ADD PRIMARY KEY (`ID_Biaya`), ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_materi_ajar`
--
ALTER TABLE `master_materi_ajar`
 ADD PRIMARY KEY (`ID_Materi_Ajar`), ADD KEY `ID_User` (`ID_User`), ADD KEY `ID_User_2` (`ID_User`);

--
-- Indexes for table `master_materi_pelatihan`
--
ALTER TABLE `master_materi_pelatihan`
 ADD PRIMARY KEY (`ID_Materi`), ADD KEY `ID_Materi` (`ID_Materi`), ADD KEY `Materi_Sebelumnya` (`Materi_Sebelumnya`);

--
-- Indexes for table `master_morf_tanaman`
--
ALTER TABLE `master_morf_tanaman`
 ADD PRIMARY KEY (`ID_Morfologi`);

--
-- Indexes for table `master_org_unit`
--
ALTER TABLE `master_org_unit`
 ADD PRIMARY KEY (`Org_Unit`), ADD KEY `Org_Unit_Atasan` (`Org_Unit_Atasan`);

--
-- Indexes for table `master_pelayanan`
--
ALTER TABLE `master_pelayanan`
 ADD PRIMARY KEY (`ID_Pelayanan`);

--
-- Indexes for table `master_petani`
--
ALTER TABLE `master_petani`
 ADD PRIMARY KEY (`ID_User`), ADD KEY `ID_User` (`ID_User`), ADD KEY `Kabupaten` (`Kabupaten`), ADD KEY `Kecamatan` (`Kecamatan`), ADD KEY `Provinsi` (`Provinsi`), ADD KEY `Desa_Kelurahan` (`Desa_Kelurahan`);

--
-- Indexes for table `master_peta_lahan`
--
ALTER TABLE `master_peta_lahan`
 ADD PRIMARY KEY (`ID_Lahan`,`bulan`), ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_produk_tani`
--
ALTER TABLE `master_produk_tani`
 ADD PRIMARY KEY (`ID_Produk`), ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_spesies _tanaman`
--
ALTER TABLE `master_spesies _tanaman`
 ADD PRIMARY KEY (`ID_Spesies`), ADD KEY `ID_Morfologi` (`ID_Morfologi`);

--
-- Indexes for table `master_supplier`
--
ALTER TABLE `master_supplier`
 ADD PRIMARY KEY (`ID_User`), ADD KEY `ID_Jenis_Sup` (`ID_Jenis_Sup`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_user`
--
ALTER TABLE `master_user`
 ADD PRIMARY KEY (`ID_User`);

--
-- Indexes for table `master_user_kat`
--
ALTER TABLE `master_user_kat`
 ADD KEY `ID_Kategori` (`ID_Kategori`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_user_org`
--
ALTER TABLE `master_user_org`
 ADD PRIMARY KEY (`NIK`), ADD KEY `Org_Unit` (`Org_Unit`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
 ADD PRIMARY KEY (`Nama_Provinsi`), ADD KEY `Nama_Provinsi` (`Nama_Provinsi`), ADD KEY `Nama_Provinsi_2` (`Nama_Provinsi`);

--
-- Indexes for table `trans_aktivitas_pertanian`
--
ALTER TABLE `trans_aktivitas_pertanian`
 ADD PRIMARY KEY (`ID_aktifitas_petani`), ADD KEY `ID_Aktivitas` (`ID_Petani`), ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Indexes for table `trans_ang_petani`
--
ALTER TABLE `trans_ang_petani`
 ADD PRIMARY KEY (`ID_Kelompok_Tani`,`ID_User`), ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`,`ID_User`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_harga_prod`
--
ALTER TABLE `trans_harga_prod`
 ADD UNIQUE KEY `ID_Produk_2` (`ID_Produk`,`Tanggal`), ADD KEY `ID_Produk` (`ID_Produk`,`ID_User`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_hasil_panen`
--
ALTER TABLE `trans_hasil_panen`
 ADD PRIMARY KEY (`ID_Petani`,`ID_Spesies`,`Tahun_Panen`,`Periode_Panen`), ADD KEY `ID_Petani` (`ID_Petani`), ADD KEY `ID_Spesies` (`ID_Spesies`), ADD KEY `ID_Produk` (`ID_Produk`);

--
-- Indexes for table `trans_kalender_tanam`
--
ALTER TABLE `trans_kalender_tanam`
 ADD PRIMARY KEY (`ID_Spesies`,`Masa_Tanam`,`Kabupaten`), ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `trans_pelaksanaan_pelatihan`
--
ALTER TABLE `trans_pelaksanaan_pelatihan`
 ADD KEY `ID_Fasilitator` (`ID_Permintaan_Pelatihan`), ADD KEY `ID_Permintaan_Pelatihan` (`ID_Permintaan_Pelatihan`), ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_penawaran_prod_tani`
--
ALTER TABLE `trans_penawaran_prod_tani`
 ADD PRIMARY KEY (`ID_Penawaran`), ADD KEY `ID_Supplier` (`ID_User`,`ID_Barang`), ADD KEY `ID_Barang` (`ID_Barang`);

--
-- Indexes for table `trans_permintaan`
--
ALTER TABLE `trans_permintaan`
 ADD PRIMARY KEY (`ID_Permintaan`), ADD KEY `ID_User` (`ID_User`,`ID_Penawaran`), ADD KEY `ID_Penawaran` (`ID_Penawaran`);

--
-- Indexes for table `trans_permintaan_pelatihan`
--
ALTER TABLE `trans_permintaan_pelatihan`
 ADD PRIMARY KEY (`ID_Permintaan_Pelatihan`), ADD KEY `ID_User` (`ID_User`,`ID_Materi`), ADD KEY `ID_Materi` (`ID_Materi`), ADD KEY `ID_User_2` (`ID_User`), ADD KEY `ID_Materi_2` (`ID_Materi`);

--
-- Indexes for table `trans_permintaan_pelayanan`
--
ALTER TABLE `trans_permintaan_pelayanan`
 ADD PRIMARY KEY (`ID_Permintaan_Pelayanan`), ADD KEY `ID_User` (`ID_User`,`ID_Pelayanan`), ADD KEY `ID_Pelayanan` (`ID_Pelayanan`);

--
-- Indexes for table `trans_struk_org`
--
ALTER TABLE `trans_struk_org`
 ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
ADD CONSTRAINT `provinsi_kabupaten` FOREIGN KEY (`Nama_Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
ADD CONSTRAINT `kabupaten_kecamatan` FOREIGN KEY (`Nama_Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
ADD CONSTRAINT `provinsi_kecamatan` FOREIGN KEY (`Nama_Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelurahan_desa`
--
ALTER TABLE `kelurahan_desa`
ADD CONSTRAINT `FK_keldes` FOREIGN KEY (`Nama_Kecamatan`) REFERENCES `kecamatan` (`Nama_Kecamatan`),
ADD CONSTRAINT `kabupaten_desa` FOREIGN KEY (`Nama_Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
ADD CONSTRAINT `provinsi_desa` FOREIGN KEY (`Nama_Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `log_aktifitas_pelatihan`
--
ALTER TABLE `log_aktifitas_pelatihan`
ADD CONSTRAINT `log_aktifitas_pelatihan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `log_aktifitas_pelatihan_ibfk_3` FOREIGN KEY (`ID_Permintaan_Pelatihan`) REFERENCES `trans_permintaan_pelatihan` (`ID_Permintaan_Pelatihan`);

--
-- Ketidakleluasaan untuk tabel `log_aktifitas_pelayanan`
--
ALTER TABLE `log_aktifitas_pelayanan`
ADD CONSTRAINT `log_aktifitas_pelayanan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `log_aktifitas_pelayanan_ibfk_3` FOREIGN KEY (`ID_Permintaan_Pelayanan`) REFERENCES `trans_permintaan_pelayanan` (`ID_Permintaan_Pelayanan`);

--
-- Ketidakleluasaan untuk tabel `log_biaya_keluar`
--
ALTER TABLE `log_biaya_keluar`
ADD CONSTRAINT `log_biaya_keluar_ibfk_1` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktifitas_spesies` (`ID_Aktivitas`),
ADD CONSTRAINT `log_biaya_keluar_ibfk_2` FOREIGN KEY (`ID_Biaya`) REFERENCES `master_kode_biaya` (`ID_Biaya`),
ADD CONSTRAINT `log_biaya_keluar_ibfk_3` FOREIGN KEY (`ID_Petani`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `log_biaya_keluar_ibfk_4` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
ADD CONSTRAINT `log_biaya_keluar_ibfk_5` FOREIGN KEY (`ID_aktifitas_spesies`) REFERENCES `master_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Ketidakleluasaan untuk tabel `log_penawaran_prod`
--
ALTER TABLE `log_penawaran_prod`
ADD CONSTRAINT `log_penawaran_prod_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `log_penawaran_prod_ibfk_3` FOREIGN KEY (`ID_Penawaran`) REFERENCES `trans_penawaran_prod_tani` (`ID_Penawaran`),
ADD CONSTRAINT `log_penawaran_prod_ibfk_4` FOREIGN KEY (`ID_User`) REFERENCES `master_supplier` (`ID_User`);

--
-- Ketidakleluasaan untuk tabel `log_permintaan`
--
ALTER TABLE `log_permintaan`
ADD CONSTRAINT `log_permintaan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `log_permintaan_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `trans_permintaan` (`ID_User`),
ADD CONSTRAINT `log_permintaan_ibfk_4` FOREIGN KEY (`ID_Permintaan`) REFERENCES `trans_permintaan` (`ID_Permintaan`);

--
-- Ketidakleluasaan untuk tabel `log_user_trans`
--
ALTER TABLE `log_user_trans`
ADD CONSTRAINT `log_user_trans_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `log_user_trans_ibfk_2` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktivitas` (`ID_Aktivitas`);

--
-- Ketidakleluasaan untuk tabel `master_aktifitas_spesies`
--
ALTER TABLE `master_aktifitas_spesies`
ADD CONSTRAINT `master_aktifitas_spesies_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
ADD CONSTRAINT `master_aktifitas_spesies_ibfk_2` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktivitas` (`ID_Aktivitas`);

--
-- Ketidakleluasaan untuk tabel `master_alat_tani`
--
ALTER TABLE `master_alat_tani`
ADD CONSTRAINT `master_alat_tani_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori_alatbahan` (`ID_Kategori`);

--
-- Ketidakleluasaan untuk tabel `master_bahan_tani`
--
ALTER TABLE `master_bahan_tani`
ADD CONSTRAINT `master_bahan_tani_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori_alatbahan` (`ID_Kategori`);

--
-- Ketidakleluasaan untuk tabel `master_detail_materi_ajar`
--
ALTER TABLE `master_detail_materi_ajar`
ADD CONSTRAINT `master_detail_materi_ajar_ibfk_1` FOREIGN KEY (`ID_Materi_Ajar`) REFERENCES `master_materi_ajar` (`ID_Materi_Ajar`);

--
-- Ketidakleluasaan untuk tabel `master_kel_tani`
--
ALTER TABLE `master_kel_tani`
ADD CONSTRAINT `master_kel_tani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `master_kel_tani_ibfk_2` FOREIGN KEY (`Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
ADD CONSTRAINT `master_kel_tani_ibfk_3` FOREIGN KEY (`Kecamatan`) REFERENCES `kecamatan` (`Nama_Kecamatan`) ON UPDATE CASCADE,
ADD CONSTRAINT `master_kel_tani_ibfk_4` FOREIGN KEY (`Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE,
ADD CONSTRAINT `master_kel_tani_ibfk_5` FOREIGN KEY (`Desa_Kelurahan`) REFERENCES `kelurahan_desa` (`Nama_Desa`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `master_kode_biaya`
--
ALTER TABLE `master_kode_biaya`
ADD CONSTRAINT `master_kode_biaya_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Ketidakleluasaan untuk tabel `master_materi_ajar`
--
ALTER TABLE `master_materi_ajar`
ADD CONSTRAINT `master_materi_ajar_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Ketidakleluasaan untuk tabel `master_materi_pelatihan`
--
ALTER TABLE `master_materi_pelatihan`
ADD CONSTRAINT `master_materi_pelatihan_ibfk_1` FOREIGN KEY (`Materi_Sebelumnya`) REFERENCES `master_materi_pelatihan` (`ID_Materi`);

--
-- Ketidakleluasaan untuk tabel `master_org_unit`
--
ALTER TABLE `master_org_unit`
ADD CONSTRAINT `master_org_unit_ibfk_1` FOREIGN KEY (`Org_Unit_Atasan`) REFERENCES `master_org_unit` (`Org_Unit`);

--
-- Ketidakleluasaan untuk tabel `master_petani`
--
ALTER TABLE `master_petani`
ADD CONSTRAINT `master_petani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `master_petani_ibfk_2` FOREIGN KEY (`Kabupaten`) REFERENCES `kabupaten` (`Nama_Kabupaten`) ON UPDATE CASCADE,
ADD CONSTRAINT `master_petani_ibfk_3` FOREIGN KEY (`Kecamatan`) REFERENCES `kecamatan` (`Nama_Kecamatan`) ON UPDATE CASCADE,
ADD CONSTRAINT `master_petani_ibfk_4` FOREIGN KEY (`Provinsi`) REFERENCES `provinsi` (`Nama_Provinsi`) ON UPDATE CASCADE,
ADD CONSTRAINT `master_petani_ibfk_5` FOREIGN KEY (`Desa_Kelurahan`) REFERENCES `kelurahan_desa` (`Nama_Desa`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `master_peta_lahan`
--
ALTER TABLE `master_peta_lahan`
ADD CONSTRAINT `master_peta_lahan_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Ketidakleluasaan untuk tabel `master_produk_tani`
--
ALTER TABLE `master_produk_tani`
ADD CONSTRAINT `master_produk_tani_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Ketidakleluasaan untuk tabel `master_spesies _tanaman`
--
ALTER TABLE `master_spesies _tanaman`
ADD CONSTRAINT `master_spesies _tanaman_ibfk_1` FOREIGN KEY (`ID_Morfologi`) REFERENCES `master_morf_tanaman` (`ID_Morfologi`);

--
-- Ketidakleluasaan untuk tabel `master_supplier`
--
ALTER TABLE `master_supplier`
ADD CONSTRAINT `master_supplier_ibfk_1` FOREIGN KEY (`ID_Jenis_Sup`) REFERENCES `master_jenis_sup` (`ID_Jenis_Sup`),
ADD CONSTRAINT `master_supplier_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Ketidakleluasaan untuk tabel `master_user_kat`
--
ALTER TABLE `master_user_kat`
ADD CONSTRAINT `master_user_kat_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori` (`ID_Kategori`),
ADD CONSTRAINT `master_user_kat_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Ketidakleluasaan untuk tabel `master_user_org`
--
ALTER TABLE `master_user_org`
ADD CONSTRAINT `master_user_org_ibfk_1` FOREIGN KEY (`Org_Unit`) REFERENCES `master_org_unit` (`Org_Unit`);

--
-- Ketidakleluasaan untuk tabel `trans_aktivitas_pertanian`
--
ALTER TABLE `trans_aktivitas_pertanian`
ADD CONSTRAINT `trans_aktivitas_pertanian_ibfk_1` FOREIGN KEY (`ID_Petani`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `trans_aktivitas_pertanian_ibfk_2` FOREIGN KEY (`ID_aktifitas_spesies`) REFERENCES `master_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Ketidakleluasaan untuk tabel `trans_ang_petani`
--
ALTER TABLE `trans_ang_petani`
ADD CONSTRAINT `trans_ang_petani_ibfk_1` FOREIGN KEY (`ID_Kelompok_Tani`) REFERENCES `master_kel_tani` (`ID_Kelompok_Tani`),
ADD CONSTRAINT `trans_ang_petani_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_petani` (`ID_User`),
ADD CONSTRAINT `trans_ang_petani_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Ketidakleluasaan untuk tabel `trans_harga_prod`
--
ALTER TABLE `trans_harga_prod`
ADD CONSTRAINT `trans_harga_prod_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `trans_harga_prod_ibfk_3` FOREIGN KEY (`ID_Produk`) REFERENCES `master_produk_tani` (`ID_Produk`);

--
-- Ketidakleluasaan untuk tabel `trans_hasil_panen`
--
ALTER TABLE `trans_hasil_panen`
ADD CONSTRAINT `trans_hasil_panen_ibfk_1` FOREIGN KEY (`ID_Petani`) REFERENCES `master_petani` (`ID_User`),
ADD CONSTRAINT `trans_hasil_panen_ibfk_2` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
ADD CONSTRAINT `trans_hasil_panen_ibfk_3` FOREIGN KEY (`ID_Produk`) REFERENCES `master_produk_tani` (`ID_Produk`);

--
-- Ketidakleluasaan untuk tabel `trans_kalender_tanam`
--
ALTER TABLE `trans_kalender_tanam`
ADD CONSTRAINT `trans_kalender_tanam_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Ketidakleluasaan untuk tabel `trans_pelaksanaan_pelatihan`
--
ALTER TABLE `trans_pelaksanaan_pelatihan`
ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_2` FOREIGN KEY (`ID_Permintaan_Pelatihan`) REFERENCES `trans_permintaan_pelatihan` (`ID_Permintaan_Pelatihan`),
ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_fasilitator` (`ID_User`),
ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_4` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Ketidakleluasaan untuk tabel `trans_penawaran_prod_tani`
--
ALTER TABLE `trans_penawaran_prod_tani`
ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_supplier` (`ID_User`),
ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_2` FOREIGN KEY (`ID_Barang`) REFERENCES `master_alat_tani` (`ID_Alat`),
ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Ketidakleluasaan untuk tabel `trans_permintaan`
--
ALTER TABLE `trans_permintaan`
ADD CONSTRAINT `trans_permintaan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `trans_permintaan_ibfk_2` FOREIGN KEY (`ID_Penawaran`) REFERENCES `trans_penawaran_prod_tani` (`ID_Penawaran`);

--
-- Ketidakleluasaan untuk tabel `trans_permintaan_pelatihan`
--
ALTER TABLE `trans_permintaan_pelatihan`
ADD CONSTRAINT `trans_permintaan_pelatihan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `trans_permintaan_pelatihan_ibfk_2` FOREIGN KEY (`ID_Materi`) REFERENCES `master_materi_pelatihan` (`ID_Materi`);

--
-- Ketidakleluasaan untuk tabel `trans_permintaan_pelayanan`
--
ALTER TABLE `trans_permintaan_pelayanan`
ADD CONSTRAINT `trans_permintaan_pelayanan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
ADD CONSTRAINT `trans_permintaan_pelayanan_ibfk_3` FOREIGN KEY (`ID_Pelayanan`) REFERENCES `master_pelayanan` (`ID_Pelayanan`);

--
-- Ketidakleluasaan untuk tabel `trans_struk_org`
--
ALTER TABLE `trans_struk_org`
ADD CONSTRAINT `trans_struk_org_ibfk_1` FOREIGN KEY (`ID_Kelompok_Tani`) REFERENCES `master_kel_tani` (`ID_Kelompok_Tani`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
