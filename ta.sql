-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2018 at 03:21 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iais_ukdw`
--

-- --------------------------------------------------------

--
-- Table structure for table `ta`
--

CREATE TABLE IF NOT EXISTS `ta` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `icon` varchar(225) NOT NULL,
  `is_top` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `ta`
--

INSERT INTO `ta` (`id`, `position`, `parent_id`, `menu_id`, `title`, `link`, `icon`, `is_top`) VALUES
(1, 1, 0, 1, 'Beranda', 'http://localhost/dutatani/', 'glyphicon glyphicon-home', 0),
(2, 2, 0, 1, 'Tentang Kami\n', 'http://localhost/dutatani/about', 'glyphicon glyphicon-info-sign', 0),
(3, 3, 0, 1, 'Forum Diskusi', 'http://localhost/dutatani/forum', 'glyphicon glyphicon-question-sign\r\n', 0),
(4, 4, 0, 1, 'Info Pertanian', '', 'glyphicon glyphicon-tree-conifer\r\n', 0),
(5, 5, 0, 1, 'Sistem Pertanian', '', 'glyphicon glyphicon-tree-conifer', 0),
(7, 6, 4, 1, 'Tanaman', 'http://localhost/dutatani/lib/coba', 'glyphicon glyphicon-tree-deciduous', 0),
(8, 7, 4, 1, 'Lahan', '', 'glyphicon glyphicon-th-large', 0),
(9, 3, 0, 1, 'Informasi', 'youtube.com', 'glyphicon glyphicon-info-sign', 1),
(13, 8, 7, 0, 'Kiri', 'ok', 'glyphicon glyphicon-certificate', 0),
(16, 9, 7, 0, 'BE', '123', 'glyphicon glyphicon-certificate', 0),
(21, 10, 22, 0, 'SEM', 'sembilan', 'glyphicon glyphicon-camera\r\n', 1),
(22, 11, 9, 1, 'ANAK', 'asd', 'glyphicon glyphicon-plus-sign\r\n', 1),
(29, 9, 9, 1, 'MANTAB', '123', 'glyphicon glyphicon-off\r\n', 1),
(35, 3, 0, 0, 'TAMBAH', 'link', 'glyphicon glyphicon-asterisk\r\n', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
