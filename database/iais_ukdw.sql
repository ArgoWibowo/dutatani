-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2018 at 06:29 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iais_ukdw`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `tentang` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`tentang`) VALUES
('<div>Organisasi pengembangan Sistem\\Informasi Pertanian Terintegrasi (SIPT) merupakan sebuah lembaga pendidikan tinggi yang ingin memberikan kontribusi nyata bagi peningkatan produktifitas\\r\\ndan kesejahteraan masyarakat tani melalui penerapan teknologi informasi dan komunikasi. Hal tersebut menjadi visi utama bagi pengembangan SIPT. Sementara\\r\\nitu misi yang diemban organisasi meliputi:</div><div><br></div><div>1. &nbsp; &nbsp; Memberikan layanan untuk pengembangan kapasitas petani dalam menggunakan TIK.</div><div><br></div><div>2. &nbsp; &nbsp; Mengembangkan sebuah blueprint untuk SIPT.</div><div><br></div><div>3. &nbsp; &nbsp; Mendukung perkembangan pertanian modern melalui npembangunan aplikasi computer untuk berbagi proses bisnis dalam pertanian.</div><div><br></div><div>4. &nbsp; &nbsp; Mendukung pertanian presisi melalui pembangunan system berbasis pengetahuan.</div><div><br></div><div>5. &nbsp; &nbsp; Membentuk dan menghubungkan jejaring untuk semua stakeholder dibidang pertanian melalui pengembangan system pertanian yang terintegrasi.</div><div><br></div><div>6. &nbsp; &nbsp;Menyediakan informasi yang terpercaya bagi petani terkait bisnis pertanian yang dijalankan.</div>'),
('<div>Organisasi pengembangan Sistem\\Informasi Pertanian Terintegrasi (SIPT) merupakan sebuah lembaga pendidikan tinggi yang ingin memberikan kontribusi nyata bagi peningkatan produktifitas\\r\\ndan kesejahteraan masyarakat tani melalui penerapan teknologi informasi dan komunikasi. Hal tersebut menjadi visi utama bagi pengembangan SIPT. Sementara\\r\\nitu misi yang diemban organisasi meliputi:</div><div><br></div><div>1. &nbsp; &nbsp; Memberikan layanan untuk pengembangan kapasitas petani dalam menggunakan TIK.</div><div><br></div><div>2. &nbsp; &nbsp; Mengembangkan sebuah blueprint untuk SIPT.</div><div><br></div><div>3. &nbsp; &nbsp; Mendukung perkembangan pertanian modern melalui npembangunan aplikasi computer untuk berbagi proses bisnis dalam pertanian.</div><div><br></div><div>4. &nbsp; &nbsp; Mendukung pertanian presisi melalui pembangunan system berbasis pengetahuan.</div><div><br></div><div>5. &nbsp; &nbsp; Membentuk dan menghubungkan jejaring untuk semua stakeholder dibidang pertanian melalui pengembangan system pertanian yang terintegrasi.</div><div><br></div><div>6. &nbsp; &nbsp;Menyediakan informasi yang terpercaya bagi petani terkait bisnis pertanian yang dijalankan.</div>');

-- --------------------------------------------------------

--
-- Table structure for table `icon`
--

CREATE TABLE `icon` (
  `id` int(3) NOT NULL,
  `text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `icon`
--

INSERT INTO `icon` (`id`, `text`) VALUES
(1, 'glyphicon glyphicon-asterisk\r\n'),
(2, 'glyphicon glyphicon-plus\r\n'),
(3, 'glyphicon glyphicon-minus\r\n'),
(4, 'glyphicon glyphicon-euro\r\n'),
(5, 'glyphicon glyphicon-cloud\r\n'),
(6, 'glyphicon glyphicon-envelope\r\n'),
(7, 'glyphicon glyphicon-pencil\r\n'),
(8, 'glyphicon glyphicon-glass\r\n'),
(9, 'glyphicon glyphicon-music\r\n'),
(10, 'glyphicon glyphicon-search\r\n'),
(11, 'glyphicon glyphicon-heart\r\n'),
(12, 'glyphicon glyphicon-star\r\n'),
(13, 'glyphicon glyphicon-star-empty\r\n'),
(14, 'glyphicon glyphicon-user\r\n'),
(15, 'glyphicon glyphicon-film\r\n'),
(16, 'glyphicon glyphicon-th-large\r\n'),
(17, 'glyphicon glyphicon-th\r\n'),
(18, 'glyphicon glyphicon-th-list\r\n'),
(19, 'glyphicon glyphicon-ok\r\n'),
(20, 'glyphicon glyphicon-remove\r\n'),
(21, 'glyphicon glyphicon-zoom-in\r\n'),
(22, 'glyphicon glyphicon-zoom-out\r\n'),
(23, 'glyphicon glyphicon-off\r\n'),
(24, 'glyphicon glyphicon-signal\r\n'),
(25, 'glyphicon glyphicon-cog\r\n'),
(26, 'glyphicon glyphicon-trash\r\n'),
(27, 'glyphicon glyphicon-home\r\n'),
(28, 'glyphicon glyphicon-file\r\n'),
(29, 'glyphicon glyphicon-time\r\n'),
(30, 'glyphicon glyphicon-road\r\n'),
(31, 'glyphicon glyphicon-download-alt\r\n'),
(32, 'glyphicon glyphicon-download\r\n'),
(33, 'glyphicon glyphicon-upload\r\n'),
(34, 'glyphicon glyphicon-inbox\r\n'),
(35, 'glyphicon glyphicon-play-circle\r\n'),
(36, 'glyphicon glyphicon-repeat\r\n'),
(37, 'glyphicon glyphicon-refresh\r\n'),
(38, 'glyphicon glyphicon-list-alt\r\n'),
(39, 'glyphicon glyphicon-lock\r\n'),
(40, 'glyphicon glyphicon-flag\r\n'),
(41, 'glyphicon glyphicon-headphones\r\n'),
(42, 'glyphicon glyphicon-volume-off\r\n'),
(43, 'glyphicon glyphicon-volume-down\r\n'),
(44, 'glyphicon glyphicon-volume-up\r\n'),
(45, 'glyphicon glyphicon-qrcode\r\n'),
(46, 'glyphicon glyphicon-barcode\r\n'),
(47, 'glyphicon glyphicon-tag\r\n'),
(48, 'glyphicon glyphicon-tags\r\n'),
(49, 'glyphicon glyphicon-book\r\n'),
(50, 'glyphicon glyphicon-bookmark\r\n'),
(51, 'glyphicon glyphicon-print\r\n'),
(52, 'glyphicon glyphicon-camera\r\n'),
(53, 'glyphicon glyphicon-font\r\n'),
(54, 'glyphicon glyphicon-bold\r\n'),
(55, 'glyphicon glyphicon-italic\r\n'),
(56, 'glyphicon glyphicon-text-height\r\n'),
(57, 'glyphicon glyphicon-text-width\r\n'),
(58, 'glyphicon glyphicon-align-left\r\n'),
(59, 'glyphicon glyphicon-align-center\r\n'),
(60, 'glyphicon glyphicon-align-right\r\n'),
(61, 'glyphicon glyphicon-align-justify\r\n'),
(62, 'glyphicon glyphicon-list\r\n'),
(63, 'glyphicon glyphicon-indent-left\r\n'),
(64, 'glyphicon glyphicon-indent-right\r\n'),
(65, 'glyphicon glyphicon-facetime-video\r\n'),
(66, 'glyphicon glyphicon-picture\r\n'),
(67, 'glyphicon glyphicon-map-marker\r\n'),
(68, 'glyphicon glyphicon-adjust\r\n'),
(69, 'glyphicon glyphicon-tint\r\n'),
(70, 'glyphicon glyphicon-edit\r\n'),
(71, 'glyphicon glyphicon-share\r\n'),
(72, 'glyphicon glyphicon-check\r\n'),
(73, 'glyphicon glyphicon-move\r\n'),
(74, 'glyphicon glyphicon-step-backward\r\n'),
(75, 'glyphicon glyphicon-fast-backward\r\n'),
(76, 'glyphicon glyphicon-backward\r\n'),
(77, 'glyphicon glyphicon-play\r\n'),
(78, 'glyphicon glyphicon-pause\r\n'),
(79, 'glyphicon glyphicon-stop\r\n'),
(80, 'glyphicon glyphicon-forward\r\n'),
(81, 'glyphicon glyphicon-fast-forward\r\n'),
(82, 'glyphicon glyphicon-step-forward\r\n'),
(83, 'glyphicon glyphicon-eject\r\n'),
(84, 'glyphicon glyphicon-chevron-left\r\n'),
(85, 'glyphicon glyphicon-chevron-right\r\n'),
(86, 'glyphicon glyphicon-plus-sign\r\n'),
(87, 'glyphicon glyphicon-minus-sign\r\n'),
(88, 'glyphicon glyphicon-remove-sign\r\n'),
(89, 'glyphicon glyphicon-ok-sign\r\n'),
(90, 'glyphicon glyphicon-question-sign\r\n'),
(91, 'glyphicon glyphicon-info-sign\r\n'),
(92, 'glyphicon glyphicon-screenshot\r\n'),
(93, 'glyphicon glyphicon-remove-circle\r\n'),
(94, 'glyphicon glyphicon-ok-circle\r\n'),
(95, 'glyphicon glyphicon-ban-circle\r\n'),
(96, 'glyphicon glyphicon-arrow-left\r\n'),
(97, 'glyphicon glyphicon-arrow-right\r\n'),
(98, 'glyphicon glyphicon-arrow-up\r\n'),
(99, 'glyphicon glyphicon-arrow-down\r\n'),
(100, 'glyphicon glyphicon-share-alt\r\n'),
(101, 'glyphicon glyphicon-resize-full\r\n'),
(102, 'glyphicon glyphicon-resize-small\r\n'),
(103, 'glyphicon glyphicon-exclamation-sign\r\n'),
(104, 'glyphicon glyphicon-gift\r\n'),
(105, 'glyphicon glyphicon-leaf\r\n'),
(106, 'glyphicon glyphicon-fire\r\n'),
(107, 'glyphicon glyphicon-eye-open\r\n'),
(108, 'glyphicon glyphicon-eye-close\r\n'),
(109, 'glyphicon glyphicon-warning-sign\r\n'),
(110, 'glyphicon glyphicon-plane\r\n'),
(111, 'glyphicon glyphicon-calendar\r\n'),
(112, 'glyphicon glyphicon-random\r\n'),
(113, 'glyphicon glyphicon-comment\r\n'),
(114, 'glyphicon glyphicon-magnet\r\n'),
(115, 'glyphicon glyphicon-chevron-up\r\n'),
(116, 'glyphicon glyphicon-chevron-down\r\n'),
(117, 'glyphicon glyphicon-retweet\r\n'),
(118, 'glyphicon glyphicon-shopping-cart\r\n'),
(119, 'glyphicon glyphicon-folder-close\r\n'),
(120, 'glyphicon glyphicon-folder-open\r\n'),
(121, 'glyphicon glyphicon-resize-vertical\r\n'),
(122, 'glyphicon glyphicon-resize-horizontal\r\n'),
(123, 'glyphicon glyphicon-hdd\r\n'),
(124, 'glyphicon glyphicon-bullhorn\r\n'),
(125, 'glyphicon glyphicon-bell\r\n'),
(126, 'glyphicon glyphicon-certificate\r\n'),
(127, 'glyphicon glyphicon-thumbs-up\r\n'),
(128, 'glyphicon glyphicon-thumbs-down\r\n'),
(129, 'glyphicon glyphicon-hand-right\r\n'),
(130, 'glyphicon glyphicon-hand-left\r\n'),
(131, 'glyphicon glyphicon-hand-up\r\n'),
(132, 'glyphicon glyphicon-hand-down\r\n'),
(133, 'glyphicon glyphicon-circle-arrow-right\r\n'),
(134, 'glyphicon glyphicon-circle-arrow-left\r\n'),
(135, 'glyphicon glyphicon-circle-arrow-up\r\n'),
(136, 'glyphicon glyphicon-circle-arrow-down\r\n'),
(137, 'glyphicon glyphicon-globe\r\n'),
(138, 'glyphicon glyphicon-wrench\r\n'),
(139, 'glyphicon glyphicon-tasks\r\n'),
(140, 'glyphicon glyphicon-filter\r\n'),
(141, 'glyphicon glyphicon-briefcase\r\n'),
(142, 'glyphicon glyphicon-fullscreen\r\n'),
(143, 'glyphicon glyphicon-dashboard\r\n'),
(144, 'glyphicon glyphicon-paperclip\r\n'),
(145, 'glyphicon glyphicon-heart-empty\r\n'),
(146, 'glyphicon glyphicon-link\r\n'),
(147, 'glyphicon glyphicon-phone\r\n'),
(148, 'glyphicon glyphicon-pushpin\r\n'),
(149, 'glyphicon glyphicon-usd\r\n'),
(150, 'glyphicon glyphicon-gbp\r\n'),
(151, 'glyphicon glyphicon-sort\r\n'),
(152, 'glyphicon glyphicon-sort-by-alphabet\r\n'),
(153, 'glyphicon glyphicon-sort-by-alphabet-alt\r\n'),
(154, 'glyphicon glyphicon-sort-by-order\r\n'),
(155, 'glyphicon glyphicon-sort-by-order-alt\r\n'),
(156, 'glyphicon glyphicon-sort-by-attributes\r\n'),
(157, 'glyphicon glyphicon-sort-by-attributes-alt\r\n'),
(158, 'glyphicon glyphicon-unchecked\r\n'),
(159, 'glyphicon glyphicon-expand\r\n'),
(160, 'glyphicon glyphicon-collapse-down\r\n'),
(161, 'glyphicon glyphicon-collapse-up\r\n'),
(162, 'glyphicon glyphicon-log-in\r\n'),
(163, 'glyphicon glyphicon-flash\r\n'),
(164, 'glyphicon glyphicon-log-out\r\n'),
(165, 'glyphicon glyphicon-new-window\r\n'),
(166, 'glyphicon glyphicon-record\r\n'),
(167, 'glyphicon glyphicon-save\r\n'),
(168, 'glyphicon glyphicon-open\r\n'),
(169, 'glyphicon glyphicon-saved\r\n'),
(170, 'glyphicon glyphicon-import\r\n'),
(171, 'glyphicon glyphicon-export\r\n'),
(172, 'glyphicon glyphicon-send\r\n'),
(173, 'glyphicon glyphicon-floppy-disk\r\n'),
(174, 'glyphicon glyphicon-floppy-saved\r\n'),
(175, 'glyphicon glyphicon-floppy-remove\r\n'),
(176, 'glyphicon glyphicon-floppy-save\r\n'),
(177, 'glyphicon glyphicon-floppy-open\r\n'),
(178, 'glyphicon glyphicon-credit-card\r\n'),
(179, 'glyphicon glyphicon-transfer\r\n'),
(180, 'glyphicon glyphicon-cutlery\r\n'),
(181, 'glyphicon glyphicon-header\r\n'),
(182, 'glyphicon glyphicon-compressed\r\n'),
(183, 'glyphicon glyphicon-earphone\r\n'),
(184, 'glyphicon glyphicon-phone-alt\r\n'),
(185, 'glyphicon glyphicon-tower\r\n'),
(186, 'glyphicon glyphicon-stats\r\n'),
(187, 'glyphicon glyphicon-sd-video\r\n'),
(188, 'glyphicon glyphicon-hd-video\r\n'),
(189, 'glyphicon glyphicon-subtitles\r\n'),
(190, 'glyphicon glyphicon-sound-stereo\r\n'),
(191, 'glyphicon glyphicon-sound-dolby\r\n'),
(192, 'glyphicon glyphicon-sound-5-1\r\n'),
(193, 'glyphicon glyphicon-sound-6-1\r\n'),
(194, 'glyphicon glyphicon-sound-7-1\r\n'),
(195, 'glyphicon glyphicon-copyright-mark\r\n'),
(196, 'glyphicon glyphicon-registration-mark\r\n'),
(197, 'glyphicon glyphicon-cloud-download\r\n'),
(198, 'glyphicon glyphicon-cloud-upload\r\n'),
(199, 'glyphicon glyphicon-tree-conifer\r\n'),
(200, 'glyphicon glyphicon-tree-deciduous\r\n'),
(201, 'glyphicon glyphicon-cd\r\n'),
(202, 'glyphicon glyphicon-save-file\r\n'),
(203, 'glyphicon glyphicon-open-file\r\n'),
(204, 'glyphicon glyphicon-level-up\r\n'),
(205, 'glyphicon glyphicon-copy\r\n'),
(206, 'glyphicon glyphicon-paste\r\n'),
(207, 'glyphicon glyphicon-alert\r\n'),
(208, 'glyphicon glyphicon-equalizer\r\n'),
(209, 'glyphicon glyphicon-king\r\n'),
(210, 'glyphicon glyphicon-queen\r\n'),
(211, 'glyphicon glyphicon-pawn\r\n'),
(212, 'glyphicon glyphicon-bishop\r\n'),
(213, 'glyphicon glyphicon-knight\r\n'),
(214, 'glyphicon glyphicon-baby-formula\r\n'),
(215, 'glyphicon glyphicon-tent\r\n'),
(216, 'glyphicon glyphicon-blackboard\r\n'),
(217, 'glyphicon glyphicon-bed\r\n'),
(218, 'glyphicon glyphicon-apple\r\n'),
(219, 'glyphicon glyphicon-erase\r\n'),
(220, 'glyphicon glyphicon-hourglass\r\n'),
(221, 'glyphicon glyphicon-lamp\r\n'),
(222, 'glyphicon glyphicon-duplicate\r\n'),
(223, 'glyphicon glyphicon-piggy-bank\r\n'),
(224, 'glyphicon glyphicon-scissors\r\n'),
(225, 'glyphicon glyphicon-bitcoin\r\n'),
(226, 'glyphicon glyphicon-yen\r\n'),
(227, 'glyphicon glyphicon-ruble\r\n'),
(228, 'glyphicon glyphicon-scale\r\n'),
(229, 'glyphicon glyphicon-ice-lolly\r\n'),
(230, 'glyphicon glyphicon-ice-lolly-tasted\r\n'),
(231, 'glyphicon glyphicon-education\r\n'),
(232, 'glyphicon glyphicon-option-horizontal\r\n'),
(233, 'glyphicon glyphicon-option-vertical\r\n'),
(234, 'glyphicon glyphicon-menu-hamburger\r\n'),
(235, 'glyphicon glyphicon-modal-window\r\n'),
(236, 'glyphicon glyphicon-oil\r\n'),
(237, 'glyphicon glyphicon-grain\r\n'),
(238, 'glyphicon glyphicon-sunglasses\r\n'),
(239, 'glyphicon glyphicon-text-size\r\n'),
(240, 'glyphicon glyphicon-text-color\r\n'),
(241, 'glyphicon glyphicon-text-background\r\n'),
(242, 'glyphicon glyphicon-object-align-top\r\n'),
(243, 'glyphicon glyphicon-object-align-bottom\r\n'),
(244, 'glyphicon glyphicon-object-align-horizontal\r\n'),
(245, 'glyphicon glyphicon-object-align-left\r\n'),
(246, 'glyphicon glyphicon-object-align-vertical\r\n'),
(247, 'glyphicon glyphicon-object-align-right\r\n'),
(248, 'glyphicon glyphicon-triangle-right\r\n'),
(249, 'glyphicon glyphicon-triangle-left\r\n'),
(250, 'glyphicon glyphicon-triangle-bottom\r\n'),
(251, 'glyphicon glyphicon-triangle-top\r\n'),
(252, 'glyphicon glyphicon-superscript\r\n'),
(253, 'glyphicon glyphicon-subscript\r\n'),
(254, 'glyphicon glyphicon-menu-left\r\n'),
(255, 'glyphicon glyphicon-menu-right\r\n'),
(256, 'glyphicon glyphicon-menu-down\r\n'),
(257, 'glyphicon glyphicon-menu-up\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Bantul', 'Daerah Istimewa Yogyakarta'),
('Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Sleman', 'Daerah Istimewa Yogyakarta'),
('magelang', 'Jawa Tengah'),
('Semarang', 'Jawa Tengah');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `Nama_Kecamatan` varchar(100) NOT NULL,
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`Nama_Kecamatan`, `Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Banyumanik', 'Semarang', 'Jawa Tengah'),
('Candisari', 'Semarang', 'Jawa Tengah'),
('Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Grabag', 'magelang', 'Jawa Tengah'),
('kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Secang', 'magelang', 'Jawa Tengah'),
('Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan_desa`
--

CREATE TABLE `kelurahan_desa` (
  `Nama_Desa` varchar(100) NOT NULL,
  `Nama_Kecamatan` varchar(100) NOT NULL,
  `Nama_Kabupaten` varchar(100) NOT NULL,
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan_desa`
--

INSERT INTO `kelurahan_desa` (`Nama_Desa`, `Nama_Kecamatan`, `Nama_Kabupaten`, `Nama_Provinsi`) VALUES
('Jagalan', 'Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Singosaren', 'Banguntapan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Gedawang', 'Banyumanik', 'Semarang', 'Jawa Tengah'),
('PudakPayung', 'Banyumanik', 'Semarang', 'Jawa Tengah'),
('Candi', 'Candisari', 'Semarang', 'Jawa Tengah'),
('Jomblang', 'Candisari', 'Semarang', 'Jawa Tengah'),
('Klitren', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Terban', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Ngupasan', 'Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Prawirodirjan', 'Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta'),
('Baleagung', 'Grabag', 'magelang', 'Jawa Tengah'),
('Banaran', 'Grabag', 'magelang', 'Jawa Tengah'),
('Bangunjiwo', 'kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Tirtonirmolo', 'kasihan', 'Bantul', 'Daerah Istimewa Yogyakarta'),
('Donoharjo', 'Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Minomartani', 'Ngaglik', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Beji', 'Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Watusigar', 'Ngawen', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Hargobingangun', 'Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Pakembingangun', 'Pakem', 'Sleman', 'Daerah Istimewa Yogyakarta'),
('Candiretno', 'Secang', 'magelang', 'Jawa Tengah'),
('Candisari', 'Secang', 'magelang', 'Jawa Tengah'),
('Kalitekuk', 'Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('kemejing', 'Semin', 'Gunungkidul', 'Daerah Istimewa Yogyakarta'),
('Kaliagung', 'Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Salamrejo', 'Sentolo', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Palihan', 'Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta'),
('Sindutan', 'Temon', 'Kulon Progo', 'Daerah Istimewa Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `log_aktifitas_pelatihan`
--

CREATE TABLE `log_aktifitas_pelatihan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_aktifitas_pelayanan`
--

CREATE TABLE `log_aktifitas_pelayanan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan_Pelayanan` varchar(10) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_biaya_keluar`
--

CREATE TABLE `log_biaya_keluar` (
  `ID_Spesies` varchar(10) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL,
  `ID_Aktivitas` varchar(10) NOT NULL,
  `ID_Petani` varchar(10) NOT NULL,
  `Tahun_Biaya` int(4) DEFAULT NULL,
  `ID_Biaya` varchar(10) NOT NULL,
  `Jumlah_Satuan` int(4) DEFAULT NULL,
  `Harga_Satuan` int(10) DEFAULT NULL,
  `Tgl_Pengeluaran` date DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_penawaran_prod`
--

CREATE TABLE `log_penawaran_prod` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_permintaan`
--

CREATE TABLE `log_permintaan` (
  `ID_Log` varchar(10) NOT NULL,
  `ID_Permintaan` varchar(10) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Aktivitas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_user_trans`
--

CREATE TABLE `log_user_trans` (
  `ID_User` varchar(10) NOT NULL,
  `Tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID_Aktivitas` varchar(10) NOT NULL,
  `Keterangan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_aktifitas_spesies`
--

CREATE TABLE `master_aktifitas_spesies` (
  `ID_Aktivitas` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Periode_Waktu` varchar(2) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `ID_aktifitas_spesies` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_aktivitas`
--

CREATE TABLE `master_aktivitas` (
  `ID_Aktivitas` varchar(10) NOT NULL,
  `Nama_Aktivitas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_aktivitas`
--

INSERT INTO `master_aktivitas` (`ID_Aktivitas`, `Nama_Aktivitas`) VALUES
('1', 'Membuat Diskusi Baru'),
('2', 'MMengomentari'),
('3', 'LogIn');

-- --------------------------------------------------------

--
-- Table structure for table `master_alat_tani`
--

CREATE TABLE `master_alat_tani` (
  `ID_Alat` varchar(10) NOT NULL,
  `Nama_Alat` varchar(50) NOT NULL,
  `Deskripsi_Alat` varchar(200) DEFAULT NULL,
  `Spesifikasi` varchar(200) DEFAULT NULL,
  `Harga_Terendah` int(12) DEFAULT NULL,
  `Harga_Tertinggi` int(12) DEFAULT NULL,
  `Fungsi` varchar(200) DEFAULT NULL,
  `ID_Kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_bahan_tani`
--

CREATE TABLE `master_bahan_tani` (
  `ID_Bahan` varchar(10) NOT NULL,
  `Nama_Bahan` varchar(50) NOT NULL,
  `Deskripsi_Bahan` varchar(200) DEFAULT NULL,
  `Spesifikasi_Bahan` varchar(200) DEFAULT NULL,
  `Harga_Terendah` int(12) DEFAULT NULL,
  `Harga_Tertinggi` int(12) DEFAULT NULL,
  `Fungsi_Bahan` varchar(200) DEFAULT NULL,
  `Jenis_Bahan` varchar(100) DEFAULT NULL,
  `ID_Kategori` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_berita`
--

CREATE TABLE `master_berita` (
  `id` int(100) NOT NULL,
  `link` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `Id_User` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_berita_informasi`
--

CREATE TABLE `master_berita_informasi` (
  `id` int(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `NIK` varchar(10) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_berita_informasi`
--

INSERT INTO `master_berita_informasi` (`id`, `judul`, `isi`, `NIK`, `foto`, `tanggal`) VALUES
(7, 'Sambut Pagi dan Kisah Sukses Petani Sayur di Tengah Kota', '<p><span style=\"font-weight: 700; color: rgb(246, 118, 56); font-family: AcuminPro, arial, helvetica, sans-serif;\">Liputan6.com, Kupang -</span><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"> Tingginya kebutuhan hidup di kota dan tak memiliki lahan untuk berkebun, tak membuat pasangan suami istri (pasutri), Dominggus Leba (54) dan Regina Leba Bara (53), menyerah. Dengan sistem menyewa lahan kosong di Kelurahan Fatululi, Kecamatan Oebobo, Kota </span><a href=\"http://regional.liputan6.com/read/2830337/temuan-pemkab-kupang-bikin-siswa-smpn-3-kupang-bersekolah-lagi\" title=\"Kupang\" style=\"background-color: rgb(255, 255, 255); color: rgb(246, 118, 56); font-family: AcuminPro, arial, helvetica, sans-serif;\">Kupang</a><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">, Nusa Tenggara Timur, pasangan ini memanfaatkan lahan yang ada untuk menanam sayur-sayuran.</span><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">Meski hanya menyewa beberapa petak lahan, pasutri asal Kabupaten Sabu Raijua ini mampu menyekolahkan anak mereka hingga perguruan tinggi.</span><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">Kerasnya kehidupan kota menuntut keduanya harus berjuang keras. Lahan kosong berbatu disulap menjadi \"surga kecil\" yang menghijau. Bedeng-bedeng sayur dibangun berderet membentuk terasering.</span><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">\"Lahan ini kami sewa dari tahun 1999 dan menjadi sumber kehidupan kami di Kota Kupang. Satu bulan kami harus bayar di pemilik lahan sebesar Rp 280 ribu,\" ucap Regina kepada </span><span style=\"font-weight: 700; color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><a href=\"http://regional.liputan6.com/\" style=\"color: rgb(246, 118, 56);\">Liputan6.com</a></span><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">, Minggu pagi, 19 Februari 2017.</span></p><p style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">Menurut Regina, dahulu lahan yang disewanya itu dipenuhi rumput dan batu karang. Namun, dengan keuletan suaminya, tempat itu kini menjadi sawah kecil dan menjadi satu-satunya sumber penghasilan mereka.<br><br>Di samping lahan, terdapat sebuah sumur tua yang menjadi harapan Ibu Regina dan petani sayur lainnya untuk menyiram tanaman mereka.<br><br>\"Setiap pagi dan sore kami pikul air dari sumur untuk siram tanaman. Hasil dari kebun sayur kami jual ke pasar dan hasilnya buat biaya pendidikan anak-anak,\" Regina menuturkan.</p><p><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">Dari hasil menyewa lahan itu, anak sulungnya saat ini sudah lulus kuliah dan sudah bekerja. \"Mereka kami biayai dari hasil kebun kecil ini. Yang sulung sudah habis kuliah dan sudah kerja di puskesmas. Satu masih kuliah dan bungsu masih SMA,\" Dominggus menambahkan.</span><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">Di balik kesuksesan menyekolahkan anak, ada segudang perjuangan yang melelahkan tak pernah ditunjukkan. Dominggus selalu tersenyum walau terbakar teriknya matahari. Di hadapan anak-anaknya, ia selalu ceria walau sebenarnya menyimpan sejuta lelah. Kerutan di wajahnya menggambarkan perjuangan yang tak mudah.</span><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">Di balik kepenatannya, ia berharap pemerintah sudi membantu peralatan pertanian guna meringankan bebannya, sehingga dapat meningkatkan hasil </span><a href=\"http://regional.liputan6.com/read/2852655/buton-utara-jadi-kabupaten-pertanian-organik\" title=\"pertanian\" style=\"background-color: rgb(255, 255, 255); color: rgb(246, 118, 56); font-family: AcuminPro, arial, helvetica, sans-serif;\">pertanian</a><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"> yang bermuara pada peningkatan ekonomi.</span><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><br style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\">\"Kalau bisa pemerintah bantu alat semprot, bibit, obat hama dan pupuk. Soalnya harga pupuk saja mahal, belum lagi obat hama,\" ucap Dominggus.</span></p><p><span style=\"color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif;\"><br></span><br></p><figure class=\"read-page--photo-gallery--item\" id=\"gallery-inline-image-0\" data-image=\"https://cdn0-a.production.liputan6.static6.com/medias/1512203/big/034097500_1487516204-20170220-salam_pagi-petani2_kota-kupang.jpg\" data-description=\"Sepasang suami istri menyewa lahan kosong di Kota Kupang, NTT, untuk menanam sayur-sayuran. (Liputan6.com/Ola Keda)\" data-share-url=\"http://regional.liputan6.com/read/2862529/sambut-pagi-dan-kisah-sukses-petani-sayur-di-tengah-kota\" data-copy-link-url=\"http://regional.liputan6.com/read/2862529/sambut-pagi-dan-kisah-sukses-petani-sayur-di-tengah-kota\" data-title=\"Sepasang suami istri menyewa lahan kosong di Kota Kupang, NTT, untuk menanam sayur-sayuran. (Liputan6.com/Ola Keda)\" data-component=\"desktop:read-page:photo-gallery:item\" style=\"margin-bottom: 30px; padding: 0px; clear: both;\"><div class=\"read-page--photo-gallery--item__content js-gallery-content\" style=\"overflow: hidden; position: relative; background-color: rgb(221, 221, 221);\"><a href=\"http://regional.liputan6.com/read/2862529/sambut-pagi-dan-kisah-sukses-petani-sayur-di-tengah-kota#\" class=\"read-page--photo-gallery--item__link\" style=\"background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(246, 118, 56); display: block;\"></a></div></figure><p style=\"box-sizing: border-box; color: rgb(74, 74, 74); font-family: AcuminPro, arial, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\"></p>', '71130094', '', '2017-03-01 19:43:58'),
(8, 'Uji Coba Aplikasi', '<p><img src=\"&#10;http://localhost/dutatani/tentang/744b8144030f96d074ede04006852c54.PNG\" style=\"width: 25%;\"></p><p>UJi Coba aplikasi</p>', 'admin', 'tentang/744b8144030f96d074ede04006852c54.PNG', '2017-03-15 22:52:02');

-- --------------------------------------------------------

--
-- Table structure for table `master_detail_materi_ajar`
--

CREATE TABLE `master_detail_materi_ajar` (
  `ID_Materi_Ajar` varchar(10) NOT NULL,
  `ID_Sub_materi` varchar(10) NOT NULL,
  `Deskripsi_sub` varchar(200) NOT NULL,
  `file_materi` varchar(200) NOT NULL,
  `link_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_detail_user`
--

CREATE TABLE `master_detail_user` (
  `ID_User` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` tinyint(4) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `keluran_desa` varchar(50) NOT NULL,
  `nomor_telpon` varchar(15) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_detail_user`
--

INSERT INTO `master_detail_user` (`ID_User`, `nama`, `jenis_kelamin`, `tanggal_lahir`, `alamat`, `provinsi`, `kabupaten`, `kecamatan`, `keluran_desa`, `nomor_telpon`, `Email`, `Foto`) VALUES
('1234567', 'a', 1, '2016-11-16', 'a', 'Daerah Istimewa Yogyakarta', 'kota Yogyakarta', 'Gondokusuman', 'Terban', 'sw', 'as', 'adc5e881fc4e033f001d01a92aeed60a.PNG'),
('71130120', 'aa', 1, '2016-10-30', 'aa', 'Daerah Istimewa Yogyakarta', 'Gunungkidul', 'Ngawen', 'Beji', 'aa', 'aa@yahoo.com', 'fce54cba532517e1ca955d5dd6398b23.PNG'),
('71130121', 'a', 2, '2016-10-30', 'a', 'Daerah Istimewa Yogyakarta', 'kota Yogyakarta', 'Gondomanan', 'Prawirodirjan', 'a', 'a@yahoo.com', '17f83131fbde0f9ac95aee73f9e58239.PNG'),
('71130122', 'Dennis Markus', 1, '2016-10-30', 'Ddd', 'Daerah Istimewa Yogyakarta', 'Bantul', 'Banguntapan', 'Singosaren', 'da', 'dsas@yahoo.com', 'aa2627c3971eae64f01f98126d170834.jpg'),
('71130123', 'a', 1, '0000-00-00', 'a', 'Daerah Istimewa Yogyakarta', '', 'Banguntapan', 'Jagalan', 'a', 'aa@yahoo.com', 'cf35c3923c92941a3c41ddf25d088614.PNG'),
('ambarnur', 'ambar nur kustinari', 1, '2017-03-10', 'Kenekan PB 1 153 Yogyakarta', 'Daerah Istimewa Yogyakarta', 'kota Yogyakarta', 'Gondomanan', 'Prawirodirjan', '', '', '5a3e5ead4cd3f2842a43c8350026a62c.jpg'),
('dmarkus', 'Dennis Markus', 1, '2017-03-22', 'klitren lor', 'Daerah Istimewa Yogyakarta', 'kota Yogyakarta', 'Gondokusuman', 'Klitren', '081575741', 'dennis.markus@ti.ukdw.ac.id', 'a6759ba18bce2c194c586e1e7b7cc805.jpg'),
('fassatu', 'Rosa', 2, '2017-03-15', 'dutawacana ukdw', 'Daerah Istimewa Yogyakarta', 'kota Yogyakarta', 'Gondokusuman', 'Terban', '081081', 'rosadelima@staff.ukdw.ac.id', 'maxresdefault.jpg'),
('frmaya', 'Fransiska Maya', 2, '0000-00-00', 'Kenekan PB 1 153 Yogyakarta', 'Daerah Istimewa Yogyakarta', '', 'Gondokusuman', 'Klitren', '081904067865', 'fransiskamaya@ti.ukdw.ac.id', 'edb79c63322fff54340decebbc1798c5.jpg'),
('mayafraya', 'Fransiska Maya', 2, '2016-12-13', 'Godean', 'Jawa Tengah', 'magelang', 'Secang', 'Candiretno', '089671044525', 'Gak@tahu.com', 'maxresdefault.jpg'),
('petani2', 'dadang', 1, '1994-08-11', 'JL. Beo', 'Jawa Tengah', '=== Kabupaten/Kota ===', '=== Kecamatan ===', '=== Desa/Kelurahan ===', '8080808080', 'gg@gmail.com', 'maxresdefault.jpg'),
('uji_coba1', 'Dennis Markus', 1, '0000-00-00', '', 'Daerah Istimewa Yogyakarta', 'Bantul', 'Banguntapan', 'Singosaren', '', '', 'maxresdefault.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `master_fasilitator`
--

CREATE TABLE `master_fasilitator` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Fasilitator` varchar(50) NOT NULL,
  `Alamat_Fasilitator` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Telpon` varchar(15) DEFAULT NULL,
  `Pendidikan_Terakhir` varchar(3) DEFAULT NULL,
  `Jurusan` varchar(30) DEFAULT NULL,
  `Kompetensi_Keahlian` varchar(200) DEFAULT NULL,
  `Riwayat_Pendidikan` varchar(200) DEFAULT NULL,
  `Pengalaman_Kerja` varchar(200) DEFAULT NULL,
  `Foto` varchar(200) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date NOT NULL,
  `jns_kelamin` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_fasilitator`
--

INSERT INTO `master_fasilitator` (`ID_User`, `Nama_Fasilitator`, `Alamat_Fasilitator`, `Desa`, `Kecamatan`, `Kabupaten`, `Provinsi`, `Telpon`, `Pendidikan_Terakhir`, `Jurusan`, `Kompetensi_Keahlian`, `Riwayat_Pendidikan`, `Pengalaman_Kerja`, `Foto`, `Email`, `tanggal_lahir`, `jns_kelamin`) VALUES
('71130121', 'a', 'a', 'Prawirodirjan', 'Gondomanan', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta', 'a', 'a', 'a', 'a', 'a', 'a', '17f83131fbde0f9ac95aee73f9e58239.PNG', 'a@yahoo.com', '2016-10-30', 2),
('71130122', '', '', '', '', '', '', '', 'kaj', '', '', 'kaj', 'dnjalasdnj', '', '', '0000-00-00', 0),
('dmarkus', 'Dennis Markus', 'klitren lor', 'Klitren', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta', '081575741', 'SMP', 'TI', '', '', '', 'a6759ba18bce2c194c586e1e7b7cc805.jpg', 'dennis.markus@ti.ukdw.ac.id', '2017-03-22', 1),
('fassatu', 'Rosa', 'dutawacana ukdw', 'Terban', 'Gondokusuman', 'kota Yogyakarta', 'Daerah Istimewa Yogyakarta', '081081', 'S2', 'TI', 'Ti', 'TI', 'TI', 'maxresdefault.jpg', 'rosadelima@staff.ukdw.ac.id', '2017-03-15', 2);

-- --------------------------------------------------------

--
-- Table structure for table `master_jenis_sup`
--

CREATE TABLE `master_jenis_sup` (
  `ID_Jenis_Sup` varchar(10) NOT NULL,
  `Nama_Jenis_Sup` varchar(50) NOT NULL,
  `Deskripsi_Jenis_Sup` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_jenis_sup`
--

INSERT INTO `master_jenis_sup` (`ID_Jenis_Sup`, `Nama_Jenis_Sup`, `Deskripsi_Jenis_Sup`) VALUES
('1', 'Ra mudeng', 'Gak tau'),
('2', 'Mboh', 'Lali aku\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `master_kategori`
--

CREATE TABLE `master_kategori` (
  `ID_Kategori` char(3) NOT NULL,
  `Nama_Kategori` varchar(40) NOT NULL,
  `Deskripsi` varchar(200) DEFAULT NULL,
  `aktif` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kategori`
--

INSERT INTO `master_kategori` (`ID_Kategori`, `Nama_Kategori`, `Deskripsi`, `aktif`) VALUES
('760', 'Pemerintah Pusat', 'Siapa tau ada pemerintah pusat pengen gabung', 0),
('Adm', 'Admin Petanoi', 'Admin Petani', 1),
('FAS', 'Fasilitator', 'Fasilitator', 1),
('Mba', 'Pengunjung biasa', '', 0),
('Pem', 'Pemerintah Pusat', 'Siapa tau ada pemerintah pusat pengen gabung', 1),
('PET', 'Petani', 'Ini adalah petani', 1),
('PMT', 'Pemerintahan', 'Pemerintahan', 1),
('SUP', 'Suplayer', 'Ini Suplayer', 1),
('UBS', 'User Biasa', 'User Biasa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_kategori_alatbahan`
--

CREATE TABLE `master_kategori_alatbahan` (
  `ID_Kategori` varchar(10) NOT NULL,
  `Jenis_kategori` varchar(1) NOT NULL,
  `Nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_kategori_topik`
--

CREATE TABLE `master_kategori_topik` (
  `ID_kategori_topik` varchar(10) NOT NULL,
  `Judul_kategori_topik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kategori_topik`
--

INSERT INTO `master_kategori_topik` (`ID_kategori_topik`, `Judul_kategori_topik`) VALUES
('21c3ab2cf3', 'Adakah Pakbun Disini Yang Pernah Berkebun Jati ?'),
('484c9bf6c6', 'Testing'),
('5ef82a0851', 'Cara Menanam Mentimun Yang Baik Dan Benar'),
('6c316a7cf2', 'Uji Coba Aplikasi'),
('8a18c68f2f', 'Cara Menanam Pakcoy/pak Choi Yang Baik Dan Benar'),
('c1ca133853', '[ASK] Pengiriman Tanaman / Bonsai Ke Luar Negeri'),
('cf7bf6195b', 'mengendalikan hama padi dengan bio urine manusia'),
('d62272b90c', 'kucai');

-- --------------------------------------------------------

--
-- Table structure for table `master_kel_tani`
--

CREATE TABLE `master_kel_tani` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `Nama_Kelompok_Tani` varchar(50) NOT NULL,
  `Alamat_Sekretariat` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Desa_Kelurahan` varchar(50) DEFAULT NULL,
  `Deskripsi` varchar(200) DEFAULT NULL,
  `Foto1` varchar(200) DEFAULT NULL,
  `Foto2` varchar(200) DEFAULT NULL,
  `Legalitas` varchar(100) DEFAULT NULL,
  `Bukti_Legalitas` tinyint(1) DEFAULT NULL,
  `Kontak_Person` varchar(50) DEFAULT NULL,
  `Nomor_Telpon` varchar(15) DEFAULT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Tgl_Terbentuk` date DEFAULT NULL,
  `Email` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_kode_biaya`
--

CREATE TABLE `master_kode_biaya` (
  `ID_Biaya` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Uraian_Biaya` varchar(100) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga_satuan` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_materi_ajar`
--

CREATE TABLE `master_materi_ajar` (
  `ID_Materi_Ajar` varchar(10) NOT NULL,
  `Nama_materi` varchar(100) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `deskripsi_materi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_materi_pelatihan`
--

CREATE TABLE `master_materi_pelatihan` (
  `ID_Materi` varchar(10) NOT NULL,
  `Nama_Materi` varchar(50) NOT NULL,
  `Deskripsi_Materi` varchar(200) DEFAULT NULL,
  `Materi_Sebelumnya` varchar(10) DEFAULT NULL,
  `Kebutuhan_Spesifikasi` varchar(200) DEFAULT NULL,
  `Level` varchar(20) DEFAULT NULL,
  `Jumlah_Jam` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_morf_tanaman`
--

CREATE TABLE `master_morf_tanaman` (
  `ID_Morfologi` varchar(10) NOT NULL,
  `Nama_Morfologi_Tanaman` varchar(50) DEFAULT NULL,
  `Nama_Divisi` varchar(10) DEFAULT NULL,
  `Nama_Sub_Divisi` varchar(10) DEFAULT NULL,
  `Nama_Ordo` varchar(10) DEFAULT NULL,
  `Nama_Famili` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_org_unit`
--

CREATE TABLE `master_org_unit` (
  `Org_Unit` varchar(10) NOT NULL,
  `Nama_Organisasi` varchar(100) NOT NULL,
  `Tingkatan` varchar(30) NOT NULL,
  `Org_Unit_Atasan` varchar(10) NOT NULL,
  `Pelayanan` tinyint(1) DEFAULT NULL,
  `Pendaftaran_Anggota` tinyint(1) DEFAULT NULL,
  `Pelatihan` tinyint(1) DEFAULT NULL,
  `Konsultasi` tinyint(1) DEFAULT NULL,
  `Penawaran` tinyint(1) DEFAULT NULL,
  `Permintaan` tinyint(1) DEFAULT NULL,
  `Memberi_Informasi` tinyint(1) DEFAULT NULL,
  `Meminta_Informasi` tinyint(1) DEFAULT NULL,
  `Berbagi_Informasi` tinyint(1) DEFAULT NULL,
  `Data_Spesifik_Anggota` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_org_unit`
--

INSERT INTO `master_org_unit` (`Org_Unit`, `Nama_Organisasi`, `Tingkatan`, `Org_Unit_Atasan`, `Pelayanan`, `Pendaftaran_Anggota`, `Pelatihan`, `Konsultasi`, `Penawaran`, `Permintaan`, `Memberi_Informasi`, `Meminta_Informasi`, `Berbagi_Informasi`, `Data_Spesifik_Anggota`) VALUES
('1', 'Super Admin', '5', '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
('545e97834c', 'Unit Lama', '5', '1', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0),
('a96f313ccb', 'Pelayanan', '1', '1', 1, 0, 0, 0, 0, 0, 1, 1, 1, 1),
('d2d4bc2280', 'Pengelola Petani', '5', '545e97834c', 0, 1, 0, 1, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_pelayanan`
--

CREATE TABLE `master_pelayanan` (
  `ID_Pelayanan` varchar(10) NOT NULL,
  `Nama_Pelayanan` varchar(50) NOT NULL,
  `Deskripsi_Pelayanan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_petani`
--

CREATE TABLE `master_petani` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Petani` varchar(50) NOT NULL,
  `Alamat_Petani` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Desa_Kelurahan` varchar(50) DEFAULT NULL,
  `Foto` varchar(200) DEFAULT NULL,
  `Nomor_Telpon` varchar(15) DEFAULT NULL,
  `Pendidikan_Terakhir` varchar(4) DEFAULT NULL,
  `Jumlah_Tanggungan` int(2) DEFAULT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Agama` varchar(10) NOT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Deskripsi_Keahlian` varchar(200) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL,
  `jns_kelamin` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_petani`
--

INSERT INTO `master_petani` (`ID_User`, `Nama_Petani`, `Alamat_Petani`, `Kabupaten`, `Kecamatan`, `Provinsi`, `Desa_Kelurahan`, `Foto`, `Nomor_Telpon`, `Pendidikan_Terakhir`, `Jumlah_Tanggungan`, `Email`, `Agama`, `Tanggal_Lahir`, `Deskripsi_Keahlian`, `Status`, `jns_kelamin`) VALUES
('71130122', '', '', '', '', '', '', '', '', NULL, 23424, '', '', '0000-00-00', '', 1, 0),
('ambarnur', 'ambar nur kustinari', 'Kenekan PB 1 153 Yogyakarta', 'kota Yogyakarta', 'Gondomanan', 'Daerah Istimewa Yogyakarta', 'Prawirodirjan', '5a3e5ead4cd3f2842a43c8350026a62c.jpg', '', NULL, 2, '', 'katolik', '2017-03-10', '', 1, 1),
('petani2', 'dadang', 'JL. Beo', '=== Kabupaten/Kota ===', '=== Kecamatan ===', 'Jawa Tengah', '=== Desa/Kelurahan ===', 'maxresdefault.jpg', '8080808080', NULL, 1, 'gg@gmail.com', 'Kristen', '1994-08-11', 'bertanam', 1, 1),
('uji_coba1', 'Dennis Markus', '', 'Bantul', 'Banguntapan', 'Daerah Istimewa Yogyakarta', 'Singosaren', 'maxresdefault.jpg', '', NULL, 0, '', '', '0000-00-00', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_peta_lahan`
--

CREATE TABLE `master_peta_lahan` (
  `ID_Lahan` varchar(10) NOT NULL,
  `Koordinat_X` varchar(20) DEFAULT NULL,
  `Koordinat_Y` varchar(20) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `bulan_akhir` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_produk_tani`
--

CREATE TABLE `master_produk_tani` (
  `ID_Produk` varchar(10) NOT NULL,
  `Nama_Produk` varchar(200) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Deskripsi_Produk` varchar(200) DEFAULT NULL,
  `Satuan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_spesies _tanaman`
--

CREATE TABLE `master_spesies _tanaman` (
  `ID_Spesies` varchar(10) NOT NULL,
  `Jenis_Tanaman` varchar(20) DEFAULT NULL,
  `Nama_Tanaman` varchar(50) NOT NULL,
  `Nama_Latin` varchar(50) DEFAULT NULL,
  `Habitat` varchar(50) DEFAULT NULL,
  `Masa_Tanam` int(4) DEFAULT NULL,
  `Akar` varchar(200) DEFAULT NULL,
  `Batang` varchar(200) DEFAULT NULL,
  `Daun` varchar(200) DEFAULT NULL,
  `Buah` varchar(200) DEFAULT NULL,
  `Biji` varchar(200) DEFAULT NULL,
  `Perkembangbiakan` varchar(200) DEFAULT NULL,
  `Foto1` varchar(200) DEFAULT NULL,
  `Foto2` varchar(200) DEFAULT NULL,
  `Iklim` varchar(50) DEFAULT NULL,
  `Jenis_Tanah` varchar(20) DEFAULT NULL,
  `Kelembaban` varchar(20) DEFAULT NULL,
  `ID_Morfologi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_supplier`
--

CREATE TABLE `master_supplier` (
  `ID_User` varchar(10) NOT NULL,
  `Nama_Supplier` varchar(200) NOT NULL,
  `ID_Jenis_Sup` varchar(10) NOT NULL,
  `Alamat_Supplier` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Kontak_Person` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `No_Handphone` varchar(15) DEFAULT NULL,
  `No_Telpon` varchar(15) DEFAULT NULL,
  `Foto` text NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jns_kelamin` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_supplier`
--

INSERT INTO `master_supplier` (`ID_User`, `Nama_Supplier`, `ID_Jenis_Sup`, `Alamat_Supplier`, `Desa`, `Kecamatan`, `Kabupaten`, `Provinsi`, `Kontak_Person`, `Email`, `No_Handphone`, `No_Telpon`, `Foto`, `tanggal_lahir`, `jns_kelamin`) VALUES
('71130122', '', '2', '', '', '', '', '', '', '', '', '', '', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_user`
--

CREATE TABLE `master_user` (
  `ID_User` varchar(10) NOT NULL,
  `Password` varchar(70) NOT NULL,
  `PIN` int(6) NOT NULL,
  `Tingkat_Priv` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_user`
--

INSERT INTO `master_user` (`ID_User`, `Password`, `PIN`, `Tingkat_Priv`) VALUES
('1234567', '7c222fb2927d828af22f592134e8932480637c0d', 123456, '0'),
('71130120', '71130120', 123456, '1'),
('71130121', '71130121', 123456, '0'),
('71130122', '1ff464f17d137405fee2de369ed815016c191a65', 711300, '0'),
('71130123', '1234567', 711300, '0'),
('ambarnur', 'b17491b6235ac54e3cf5b933bcea46dbb2a9d98c', 123456, '1'),
('dmarkus', '0ddf626282043d4ee9dc6aa57f6eee94fd8a31a1', 123456, '0'),
('fassatu', '533619b83b1caea72dfe18d24b545202aae49de9', 123456, '1'),
('frmaya', '92492f384642f5322a0a639ecdaa96782674e194', 123456, '1'),
('mayafraya', '143ad9c15d88354aba49ab5123fe785bc1bb0db2', 123456, '0'),
('petani2', '7c4a8d09ca3762af61e59520943dc26494f8941b', 123456, '1'),
('uji_coba', '0c760bcd51cc46b768e3539e76cd9bcd22462b11', 123456, '1'),
('uji_coba1', '4c8e61873819e65058e67a400b84cce9cb67120f', 123456, '1');

-- --------------------------------------------------------

--
-- Table structure for table `master_user_kat`
--

CREATE TABLE `master_user_kat` (
  `ID_Kategori` char(3) NOT NULL,
  `ID_User` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_user_kat`
--

INSERT INTO `master_user_kat` (`ID_Kategori`, `ID_User`) VALUES
('FAS', '71130121'),
('UBS', '71130123'),
('760', '1234567'),
('PMT', '71130122'),
('FAS', '71130122'),
('PET', '71130122'),
('Mba', '71130122'),
('SUP', '71130122'),
('760', 'mayafraya'),
('760', 'frmaya'),
('FAS', 'dmarkus'),
('PET', 'ambarnur'),
('FAS', 'fassatu'),
('PET', 'uji_coba'),
('PET', 'uji_coba1'),
('PET', 'petani2');

-- --------------------------------------------------------

--
-- Table structure for table `master_user_org`
--

CREATE TABLE `master_user_org` (
  `NIK` varchar(10) NOT NULL,
  `kelola_website` tinyint(1) NOT NULL,
  `kelola_user` tinyint(1) NOT NULL,
  `Kelola_forum` tinyint(1) NOT NULL,
  `Password` varchar(70) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Nama_Karyawan` varchar(100) NOT NULL,
  `Tanggal_Lahir` date DEFAULT NULL,
  `Alamat_Rumah` varchar(100) DEFAULT NULL,
  `No_Telpon` varchar(15) DEFAULT NULL,
  `Org_Unit` varchar(10) NOT NULL,
  `Jenis_Kelamin` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_user_org`
--

INSERT INTO `master_user_org` (`NIK`, `kelola_website`, `kelola_user`, `Kelola_forum`, `Password`, `Email`, `Nama_Karyawan`, `Tanggal_Lahir`, `Alamat_Rumah`, `No_Telpon`, `Org_Unit`, `Jenis_Kelamin`) VALUES
('505', 1, 1, 1, '3ead28f890ec0f5b363587e15d61e0b4dca2ee6d', 'duta@yahoo.com', 'Uji', '2017-03-15', 'Dutawacana', '000', '545e97834c', 'L'),
('71130', 0, 1, 0, 'bf848135da46920f94a182a4fa50890119035c3f', 'oktadennis0910@gmail.com', 'Dennis Markus', '2017-03-14', 'Klitren', '089671044525', '545e97834c', 'L'),
('71130094', 1, 1, 1, '1ff464f17d137405fee2de369ed815016c191a65', 'a@yahoo.com', 'Fransiska Maya', '2016-11-15', 'a', 'a', 'a96f313ccb', 'P'),
('admin', 1, 1, 1, '6e67a99cf4a4ca4bc3a86e1a238f72e9e56b0f45', 'dennis.markus@ti.ukdw.ac.id', 'Dennis Markus', '2016-11-14', 'Gak punya dui', '0896710744525', '1', 'L');

-- --------------------------------------------------------

--
-- Table structure for table `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `id` int(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Isi` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemberitahuan`
--

INSERT INTO `pemberitahuan` (`id`, `link`, `ID_User`, `Isi`, `status`, `tanggal`) VALUES
(1, 'forum/komentar/d62272b90c', 'ambarnur', 'Ada yang mengomentari diskusi anda', 0, '0000-00-00'),
(2, 'forum/komentar/6c316a7cf2', '71130122', 'Ada yang mengomentari diskusi anda', 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal_jam` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `nama`, `email`, `pesan`, `tanggal_jam`, `status`) VALUES
(1, 'Dennis Markus', 'dennis.markus@ti.ukdw.ac.id', 'Tidak ada menapilkan tentang kami', '2017-03-14 21:22:14', 0),
(2, 'Dennis Markus', 'dennis.markus@yahoo.com', 'sudah bagus kok webnya', '2017-03-15 22:29:30', 0),
(3, 'Dennis', 'dennis.markus@ti.ukdw.ac.id', 'Pesan uji coba', '2017-03-24 10:59:18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `Nama_Provinsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`Nama_Provinsi`) VALUES
('Daerah Istimewa Yogyakarta'),
('Jawa Tengah');

-- --------------------------------------------------------

--
-- Table structure for table `ta`
--

CREATE TABLE `ta` (
  `id` int(11) UNSIGNED NOT NULL,
  `position` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `icon` varchar(225) NOT NULL,
  `is_top` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ta`
--

INSERT INTO `ta` (`id`, `position`, `parent_id`, `menu_id`, `title`, `link`, `icon`, `is_top`) VALUES
(1, 1, 0, 1, 'Beranda', 'http://localhost/dutatani/', 'glyphicon glyphicon-home', 0),
(2, 2, 0, 1, 'Tentang Kami\n', 'http://localhost/dutatani/about', 'glyphicon glyphicon-info-sign', 0),
(3, 3, 0, 1, 'Forum Diskusi', 'http://localhost/dutatani/forum', 'glyphicon glyphicon-question-sign\r\n', 0),
(4, 4, 0, 1, 'Info Pertanian', '', 'glyphicon glyphicon-tree-conifer\r\n', 0),
(5, 5, 0, 1, 'Sistem Pertanian', '', 'glyphicon glyphicon-tree-conifer', 0),
(7, 6, 4, 1, 'Tanaman', 'http://localhost/dutatani/lib/coba', 'glyphicon glyphicon-tree-deciduous', 0),
(8, 7, 4, 1, 'Lahan', '', 'glyphicon glyphicon-th-large', 0),
(9, 3, 0, 1, 'Informasi', 'youtube.com', 'glyphicon glyphicon-info-sign', 1),
(13, 8, 5, 0, 'Sistem Informasi Pertanian', 'http://localhost/dutatani/si_petani/login.php', 'glyphicon glyphicon-certificate', 0),
(16, 9, 5, 0, 'Sistem Informasi Aktivitas', 'http://localhost/dutatani/si_aktivitas/login.php', 'glyphicon glyphicon-certificate', 0),
(21, 10, 9, 0, 'SEM', 'sembilan', 'glyphicon glyphicon-camera\r\n', 1),
(22, 11, 8, 1, 'ANAK', 'asd', 'glyphicon glyphicon-plus-sign\r\n', 0),
(29, 9, 8, 1, 'MANTAB', '123', 'glyphicon glyphicon-off\r\n', 0),
(35, 3, 21, 0, 'TAMBAH', 'link', 'glyphicon glyphicon-asterisk\r\n', 1),
(36, 31, 0, 0, 'KIRI', 'as', 'glyphicon glyphicon-cd\r\n', 0),
(37, 7, 0, 0, 'ATAS', '123', 'glyphicon glyphicon-piggy-bank\r\n', 1),
(38, 8, 0, 0, 'About us', '123', 'glyphicon glyphicon-file\r\n', 1),
(39, 9, 5, 0, 'E-commerce Pertanian', '123', 'glyphicon glyphicon-usd\r\n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trans_aktivitas_pertanian`
--

CREATE TABLE `trans_aktivitas_pertanian` (
  `ID_aktifitas_petani` varchar(36) NOT NULL,
  `ID_aktifitas_spesies` varchar(10) NOT NULL,
  `ID_Petani` varchar(10) NOT NULL,
  `Tanggal_Mulai` date DEFAULT NULL,
  `Tanggal_Selesai` date DEFAULT NULL,
  `Tahun_Aktivitas` int(4) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_anggaraan`
--

CREATE TABLE `trans_anggaraan` (
  `ID_anggaran` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Aktifitas` varchar(10) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `periode_tanam` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_ang_petani`
--

CREATE TABLE `trans_ang_petani` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `Tgl_Gabung` date DEFAULT NULL,
  `Tgl_Expired` date DEFAULT NULL,
  `Keterangan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_detail_anggaran`
--

CREATE TABLE `trans_detail_anggaran` (
  `ID_detail_anggaran` varchar(10) NOT NULL,
  `ID_anggaran` varchar(10) NOT NULL,
  `ID_biaya` varchar(10) NOT NULL,
  `jumlah_anggaran` int(12) NOT NULL,
  `waktu` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_harga_prod`
--

CREATE TABLE `trans_harga_prod` (
  `ID_Produk` varchar(10) NOT NULL,
  `Tanggal` date NOT NULL,
  `Harga` int(15) NOT NULL,
  `ID_User` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_hasil_panen`
--

CREATE TABLE `trans_hasil_panen` (
  `ID_Petani` varchar(10) NOT NULL,
  `ID_Spesies` varchar(10) NOT NULL,
  `Bulan_Panen` varchar(2) DEFAULT NULL,
  `Tahun_Panen` varchar(4) NOT NULL,
  `Periode_Panen` varchar(2) NOT NULL,
  `Jumlah_Hasil_Panen` int(4) DEFAULT NULL,
  `ID_Produk` varchar(10) NOT NULL,
  `Luas_lahan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_kalender_tanam`
--

CREATE TABLE `trans_kalender_tanam` (
  `ID_Spesies` varchar(10) NOT NULL,
  `Nama_Kalender` varchar(50) NOT NULL,
  `Masa_Tanam` varchar(2) NOT NULL,
  `Kabupaten` varchar(50) NOT NULL,
  `Provinsi` varchar(50) NOT NULL,
  `Tanggal_Awal` date NOT NULL,
  `Tanggal_Akhir` date NOT NULL,
  `Pengolahan_Lahan_Awal` date NOT NULL,
  `Pengolahan_Lahan_Akhir` date NOT NULL,
  `Persiapan_Lahan_Awal` date NOT NULL,
  `Masa_Penanaman_Awal` date NOT NULL,
  `Masa_Pemupukan` date NOT NULL,
  `Masa_Panen` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_komentar_diskusi`
--

CREATE TABLE `trans_komentar_diskusi` (
  `nomor_komentar` int(100) NOT NULL,
  `ID_topik` varchar(10) NOT NULL,
  `ID_user` varchar(10) NOT NULL,
  `Komentar` text NOT NULL,
  `Tanggal` date NOT NULL,
  `Waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_komentar_diskusi`
--

INSERT INTO `trans_komentar_diskusi` (`nomor_komentar`, `ID_topik`, `ID_user`, `Komentar`, `Tanggal`, `Waktu`) VALUES
(10, 'cf7bf6195b', 'dmarkus', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">mantap infonya nih min,, baru tau ternyata bisa dipake buat pengendalian hama. Kapan2 aku mau coba deh,,, biat pesing yg penting tanaman aman terkendali....</span>                        ', '2018-03-01', '2017-03-24 18:47:41'),
(11, '5ef82a0851', 'dmarkus', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Terimakasih tips menanam mentimunnya mas.</span>                        ', '2017-03-01', '2017-03-01 20:19:02'),
(12, '8a18c68f2f', 'dmarkus', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Terimakasih untuk infonx</span><img src=\"http://www.kebunpedia.com/styles/default/xenforo/smilies/happy.png\" class=\"mceSmilie\" alt=\":happy:\" title=\"happy    :happy:\" style=\"vertical-align: text-bottom; margin: 0px 1px; max-width: 100%; color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">                        ', '2017-03-01', '2017-03-01 20:19:27'),
(13, 'c1ca133853', 'ambarnur', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Memasukkan dan mengirim keluar&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/tanaman/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">tanaman</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;ke LN, memanv tidak mudah. Itu menyangkut kekhawatiran negara lain juga Indonesia bahwa impor tanaman bisa membanyak hama penyakit yg dapat merajalela di negara masing2. Selain itu ditakutkan tanaman yg datang sebagai pendatang tersebut ternyata bersifat dominan dan mengalahkan tanaman endemik lokal. Alasan tersebut di atas menyebabkan tiap negara mensyaratkan peraturan yg bermacam2. Dari karantina sampai phytosanitary certificate.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Mungkin pak bun bisa kontak dan bicara dgn bagian karantina di bandara utk mendapat info lebih detail.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Jalan lain yg memungkinkan :</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">1. Menjual di DN dan meminta pembeli luar yg mengurus dokumen eksportnya.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">2. Kerjasama dgn perush yg sdh biasa melakukan eksport tanaman ke luar.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Semoga bermanfaat</span>                        ', '2017-03-01', '2017-03-01 20:32:05'),
(14, '21c3ab2cf3', 'ambarnur', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Target marketnya untuk pohon jati biasanya toko furniture&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Kalau saya lihat di daerah saya ..pohon jati umumnya ditanam secara perorangan tanpa ada kerja sama dgn pihak terkait&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Karena harga pohon jati itu sangat mahal jd masyarakat banyak menanam diperkarangan rumah .</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Jikalau ada lahan ditanam serentak itupun lahan yang susah air terutama lahan diperbukitan ...&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Begitu setahu saya jd system jual perpohon dan negosiasi oleh pembeli ... deal angkut ...</span>                        ', '2017-02-01', '2017-03-24 18:40:19'),
(15, 'cf7bf6195b', 'ambarnur', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Wahh, perlu dicoba ini...tanaman padiku ludes diserang tikus...!!</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Makasih infonya...</span>                        ', '2017-03-01', '2017-03-01 20:32:43'),
(16, '8a18c68f2f', 'ambarnur', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">kok loyo gitu ya, waktu pindah akar sehat tidak, keliatan tidak siram aja hehehe...</span>                        ', '2017-03-01', '2017-03-01 20:35:05'),
(17, '5ef82a0851', 'ambarnur', '<span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">ternyata threadnya sudah ada yah.</span><br style=\"margin-top: 0px; color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">disini ada tambahan tipsnya min, udah ada threadnya jg&nbsp;</span><img src=\"http://www.kebunpedia.com/styles/default/xenforo/clear.png\" class=\"mceSmilieSprite mceSmilie8\" alt=\":D\" title=\"Big Grin    :D\" style=\"vertical-align: text-bottom; margin: 0px 1px; max-width: 100%; width: 18px; height: 18px; background: url(&quot;styles/default/xenforo/xenforo-smilies-sprite.png&quot;) -20px 0px no-repeat rgb(255, 254, 252); color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px;\">                        ', '2017-03-01', '2017-03-01 20:35:17'),
(18, 'd62272b90c', '71130122', 'Terima kasih infonya gan', '2017-03-15', '2017-03-16 05:32:22');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pelaksanaan_pelatihan`
--

CREATE TABLE `trans_pelaksanaan_pelatihan` (
  `ID_User` varchar(10) NOT NULL,
  `Tanggal_Pelaksanaan` date DEFAULT NULL,
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `Pembahasan` varchar(200) DEFAULT NULL,
  `Sesi` varchar(1) DEFAULT NULL,
  `Jumlah_Peserta` int(3) DEFAULT NULL,
  `Bukti_Materi` varchar(200) DEFAULT NULL,
  `Bukti_Daftar_Hadir` varchar(200) DEFAULT NULL,
  `Catatan` varchar(200) DEFAULT NULL,
  `Foto_Kegiatan_1` varchar(200) DEFAULT NULL,
  `Foto_Kegiatan_2` varchar(200) DEFAULT NULL,
  `Jam_Awal` timestamp NULL DEFAULT NULL,
  `Jam_Akhir` timestamp NULL DEFAULT NULL,
  `Nomor_Pelaksanaan_pelatihan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_penawaran_prod_tani`
--

CREATE TABLE `trans_penawaran_prod_tani` (
  `ID_User` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Tanggal_Penawaran` date DEFAULT NULL,
  `Spesifikasi_Barang` varchar(200) DEFAULT NULL,
  `ID_Barang` varchar(10) NOT NULL,
  `Kondisi_Barang` varchar(30) DEFAULT NULL,
  `Merk` varchar(30) DEFAULT NULL,
  `Harga` int(12) DEFAULT NULL,
  `Tahun_Produksi` int(4) DEFAULT NULL,
  `Gambar1` varchar(200) DEFAULT NULL,
  `Gambar2` varchar(200) DEFAULT NULL,
  `Status` varchar(10) DEFAULT NULL,
  `Stok` int(3) DEFAULT NULL,
  `Satuan_Barang` varchar(10) DEFAULT NULL,
  `Validasi_Admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_permintaan`
--

CREATE TABLE `trans_permintaan` (
  `ID_Permintaan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Penawaran` varchar(10) NOT NULL,
  `Qty` int(3) DEFAULT NULL,
  `Harga` int(12) DEFAULT NULL,
  `Tgl_Kebutuhan` date DEFAULT NULL,
  `Tgl_Permintaan` date DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_permintaan_pelatihan`
--

CREATE TABLE `trans_permintaan_pelatihan` (
  `ID_Permintaan_Pelatihan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Materi` varchar(10) NOT NULL,
  `Tanggal_Awal_Permintaan` date DEFAULT NULL,
  `Tanggal_Akhir_Permintaan` date DEFAULT NULL,
  `Tempat` varchar(50) DEFAULT NULL,
  `Alamat` varchar(200) DEFAULT NULL,
  `Desa` varchar(50) DEFAULT NULL,
  `Kecamatan` varchar(50) DEFAULT NULL,
  `Kabupaten` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `Kontak_Person` varchar(50) DEFAULT NULL,
  `Telpon` varchar(50) DEFAULT NULL,
  `Jumlah_Target_Peserta` int(3) DEFAULT NULL,
  `Surat_Permintaan` varchar(200) DEFAULT NULL,
  `Tanggal_Awal_Setuju` date DEFAULT NULL,
  `Tanggal_Akhir_Setuju` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_permintaan_pelayanan`
--

CREATE TABLE `trans_permintaan_pelayanan` (
  `ID_Permintaan_Pelayanan` varchar(10) NOT NULL,
  `ID_User` varchar(10) NOT NULL,
  `ID_Pelayanan` varchar(10) NOT NULL,
  `Tanggal_Permintaan` date DEFAULT NULL,
  `Permasalahan` varchar(200) DEFAULT NULL,
  `Solusi` varchar(200) DEFAULT NULL,
  `Tanggal_Respon` date NOT NULL,
  `Penanggungjawab` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_realisasi_biaya`
--

CREATE TABLE `trans_realisasi_biaya` (
  `ID_detail_anggaran` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` varchar(10) NOT NULL,
  `harga` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_struk_org`
--

CREATE TABLE `trans_struk_org` (
  `ID_Kelompok_Tani` varchar(10) NOT NULL,
  `Tgl_Awal` date DEFAULT NULL,
  `Tgl_Selesai` date DEFAULT NULL,
  `Nama_Ketua` varchar(100) DEFAULT NULL,
  `Telpon_Ketua` varchar(20) DEFAULT NULL,
  `Nama_Wakil_Ketua` varchar(100) DEFAULT NULL,
  `Telpon_Wakil_Ketua` varchar(20) DEFAULT NULL,
  `Nama_Sekretaris` varchar(100) DEFAULT NULL,
  `Telpon_Sekretaris` varchar(20) DEFAULT NULL,
  `Nama_Bendahara` varchar(100) DEFAULT NULL,
  `Telpon_Bendahara` varchar(20) DEFAULT NULL,
  `Scan_Struktur_Organisasi` varchar(200) DEFAULT NULL,
  `Scan_Susunan_Pengurus` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_topik_diskusi`
--

CREATE TABLE `trans_topik_diskusi` (
  `ID_topik` varchar(10) NOT NULL,
  `ID_kategori_topik` varchar(10) NOT NULL,
  `ID_user` varchar(10) NOT NULL,
  `Judul_Topik` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isi_diskusi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_topik_diskusi`
--

INSERT INTO `trans_topik_diskusi` (`ID_topik`, `ID_kategori_topik`, `ID_user`, `Judul_Topik`, `tanggal`, `waktu`, `isi_diskusi`) VALUES
('21c3ab2cf3', '21c3ab2cf3', 'dmarkus', 'Adakah Pakbun Disini Yang Pernah Berkebun Jati ?', '2018-03-01', '2017-03-24 18:51:56', '<p><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Selamat siang pakbun dan bubun</span><br style=\"margin-top: 0px; color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Saya anak bawang mau tanya nih,&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Saya bekerja di sebuah perusahaan bergerak di bidang agroforestry, yaitu kerjasama penanaman pohon&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/jati/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">jati</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">.&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Bagi pak bun dan bubun yang pernah menjalani kerjasama seperti ini atau memiliki lahan jati,&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Biasanya siapa sih yang berminat menanam kayu jati untuk kerjasama ?&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Atau lebih tepatnya siapa sih target market untuk kerjasama penanaman pohon jati ?&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Mungkin ada master gardener yang bersedia sharing, silahkan.&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Terima kasih&nbsp;</span><br></p>'),
('484c9bf6c6', '484c9bf6c6', 'petani2', 'Testing', '2018-08-31', '2018-08-31 01:48:44', '<p>testing</p>'),
('5ef82a0851', '5ef82a0851', 'frmaya', 'Cara Menanam Mentimun Yang Baik Dan Benar', '2017-03-01', '2017-03-01 20:13:29', '<p><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Melihat banyaknya manfaat dari mentimun wajar jika akhirnya permintaan untuk buah/sayuran ini selalu tinggi. Jika Anda tertarik untuk membudidayakan mentimun, berikut kami tampilkan ulasan mengenai&nbsp;</span><b style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">cara menanam mentimun yang baik dan benar</b><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">:</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">1. Syarat tanam</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Mentimun umumnya bisa tumbuh diberbagai daerah dengan berbagai ketinggian dan suhu, namun ketinggian antara 1000 sampai dengan 1200 mdpl dengan suhu antara 21-27oC adalah tanah yang ideal. Media lahan haruslah tersinari matahari dengan baik, karena tanaman ini termasuk ke dalam tanaman yang rentan sehingga perlu memiliki perawatan yang baik. Tingkat keasaman tanah yang disarankan adalah 6 sampai dengan 7 pH.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">2. Persiapan lahan</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Lahan yang baik untuk penanaman mentimun adalah tanah yang memiliki unsur hara yang masih baik, tanah harus digemburkan dahulu dengan cara di cangkul atau dibajak sedalam kurang lebih 20-30 cm untuk membalikan posisi tanah dari bawah ke atas. Setelah tanah siap kemudian dibuat bedengan dengan spesifikasi sebagai berikut ;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Buat ukuran bedengan dengan ukuran lebar 1 meter serta tinggi sekitar 20-30 cm</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Panjang bedengan sesuai dengan panjang lahan</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Jarak antara bedengan adalah sekitar 20 cm</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Buat parit diantara bedeng dengan rapi untuk drainase</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Setelah tanah bedengan siap kemudian tutup bedengan dengan mulsa plastik pada waktu siang hari atau saat cuaca panas agar panjang dan ketahanan mulsa berada pada kondisi maksimal.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Lubangi bedengan yang telah ditutupi mulsa dengan diameter 10 cm</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Jarak antara lubang bedengan satu baris mendatar adalah 40 cm</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Sementara jarak antar lubang bedengan satu baris ke bawah aadalah 50-60cm</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Setelah bedengan siap langkah selanjutnya adalah dengan memberikan pupuk alami yang berasal dari kotoran hewan, pada setiap lubang bedeng.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">3. Penanaman</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Momen yang paling baik untuk menanam mentimun adalah saat akhir musim hujan, karena musim kering adalah waktu yang paling cocok untuk menanam mentimun. Gunakanlah bibit mentimun yang bagus, agar hasil tanaman juga bagus. Penanaman bibit dengan cara ditugal bisa dengan menggunakan tugal manual maupun otomatis, ke dalam tugal adalah 5 sampai dengan 7 cm. Dalam satu lubang tugal idealnya diisi dengan 2 sampai dengan 3 buah bibit mentimun. Kemudian tutup lubang dengan rapi, namun jangan terlalu dipadatkan karena akan menekan bibit.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">4. Perawatan</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Setelah bibit mentimun ditanam maka tahap selanjutnya adalah merawat atau memelihara mentimun agar tumbuh dengan baik. Terdapat beberapa bagian dalam perawatan mentimun seperti ;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Pengairan</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Pengairan bisa dilakukan setelah tanaman berkecambah diumur 1-4 hari, lalu saat tanaman memiliki batang atau daun pada umur 15 sampai dengan 20 hari. Tanah jangan sampai terlihat lembab, karena akan tidak baik untuk pertumbuhan tanaman mentimun, pengairan sampai tanah terlihat basah.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Pemupukan</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Pupuk yang digunakan untuk pertumbuhan tanaman mentimun adalah pupuk organik, UREA, TSP serta KCL.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">a. Pupuk organik diberikan saat tahap pengolahan tanah</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">b. Pupuk Urea yang digunakan sekitar 75 Kg/ha, dengan pembagian sekitar 25 - 35 Kg diberikan pada waktu tanam mentimun sementara yang lainnya diberikan setelah penyiangan pertama atau saat waktu tanam berada di 15-20 hari.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">c. Pupuk TSP dan KCL diberikan saat waktu tanam dengan disebar secara merata, komposisinya 40 Kg/ha TSP dan 20 kg/ha KCL.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Pengendalian hama dan penyakit</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Hama dan penyakit yang menyerang mentimun biasanya adalah Gulma, hama, kepik hijau dan karat daun.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Untuk Gulma pengendaliannya dengan pestisida Roundup yang diberikan saat tanam maupun saat tanaman mulai berbunga.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Kepik</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Kepik pengendaliannya bisa dengan pemusnahan telur secara manual atau menggunakan insektisida seperti Surecida 20 EC, Dursban 20 EC, 35 EC, atau Azodrin 15 WCS di berikan saat tanaman berumur 20 hari.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Hama</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Untuk hama pengendaliannya bisa dengan insektisida yang dilakukan pada saat tanaman mentimun berumur 7-8 hari.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">- Karat Daun</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Pengendaliannya dengan Fungisida Dithane M-45 yang diberikan pada saat tanaman berumur 20 hari. Selain itu bisa juga dengan pembasmian manual dengan mencabut secara rapi kemudian dibakar.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">5. Waktu panen</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Mentimun sudah memasuki masa siap panen saat berumur 75 hari dari waktu tanam. Panen bisa dilakukan setiap hari secara bertahap dengan mengambil terlebih dahulu buah yang sudah siap panen. Setelah panen buah mentimun harus dikemas dalam kemasan yang baik untuk menghindari kerusakan, karena buah ini memiliki kulit yang rentan dan gampang rusak.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Selain itu setelah dipanen, mentimun juga harus diletakan dalam suhu yang sejuk karena buah ini rentan sekali kehilangan cairan jika ditempatkan dalam cuaca yang panas, sehingga agar buah tetap terjaga kesegarannya harus ditempatkan dalam cuaca yang baik.</span><br></p>'),
('6c316a7cf2', '6c316a7cf2', '71130122', 'Uji Coba Aplikasi', '2017-02-15', '2017-03-24 18:31:30', '<p>Saat ini sedang uji coba aplikasi</p>'),
('8a18c68f2f', '8a18c68f2f', 'frmaya', 'Cara Menanam Pakcoy/pak Choi Yang Baik Dan Benar', '2017-03-01', '2017-03-01 20:12:21', '<p><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Jika Anda tertarik untuk membudidayakan sayuran ini maka berikut kami tampilkan beberapa tips dan&nbsp;</span><b style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">cara menanam Pakcoy/Pak Choi&nbsp;yang baik</b><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;agar memiliki hasil yang optimal&nbsp;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>1.Syarat Tumbuh</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Sayuran Pak Choy bisa hidup dalam berbagai tempat tropis maupun subtropis, daerah baik dataran tinggi maupun rendah, namun yang perlu diperhatikan untuk media pertumbuhan Pak Choy adalah media tanah haruslah subur, setidaknya memiliki unsur hara yang masih cukup baik. Tanah harus memiliki pancaran &nbsp;sinar matahari yang baik serta cukup air.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>2. Persiapan Lahan</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Gemburkan tanah yang akan menjadi media penanaman Pak Choy, gali tanah dengan kedalaman antara 20 sampai dengan 30 cm. Lalu buatlah sebuah bedengan yang nanti akan menjadi media tanam Pak Choy, bedengan yang akan dibuat haruslah berupa bedengan dengan bedengan dengan lebar 2 meter, dengan panjang menyesuaikan dengan panjangnya lahan.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Di antara sela bedengan buatlah sebuah saluran air atau drainase yang memiliki lebar kurang lebih 50 cm untuk kelancaran air agar tidak menggenangi bedengan saat musim hujan. Setelah bedengan selesai dibuat maka ratakan bagian permukaan bedengan kemudian berikan pupuk kandang pada media tanam agar tanah menjadi lebih subur. Perhitungan pupuk kandang yang digunakan adalah 7 sampai dengan 10 ton pupuk untuk media lahan seluas satu hektar. Setelah diberi pupuk berikan air agar tanah menjadi padat dan pupuk bisa menyerap dengan baik.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>3. Proses penanaman</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">a. Persemaian</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"></b><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Untuk menaman sayuran Pak Choi bisa dengan bibit atau disemaikan terlebih dahulu, jika disemaikan maka digunakan bedengan terpisah dari bedengan utama untuk menyemaikan bibit hingga berdaun. Media tanam bedengan adalah tanah yang halus dan gembur dicampur dengan pupuk kompos dengan komposisi 1:1.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Agar lebih maksimal sebelum benih ditabur untuk persemaian, bibit harus direndam terlebih dahulu dalam sebuah larutan yang berasal dari Previkur N dengan konsentrasi 0.1. Rendam bibit dalam larutan kurang lebih selama 2 jam lalau bibit yang sudah direndam kemudian dikeringkan.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>b. Bibit</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Jika akan menanam sayuran Pak Choy langsung dengan bibit maka dibutuhkan bibit Pak Choy dengan kisaran antara 1.5 sampai dengan 2 Kg per hektarnya. Penanaman dilakukan dengan cara menaburkan benih dengan jarak sekitar 9x9cm. Dalam setiap titik tanam jangan hanya menaburkan satu bibit namun bisa juga dengan memberi 2-3 biji tanam, hal ini untuk menghindari kurang berkembangnya satu benih sehingga dengan memberikan 2-3 bibit maka jika ada satu benih yang kurang berkembang masih bisa digantikan oleh bibit lainnya.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>4. Perawatan Tanaman</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Perawatan tanaman untuk sayuran Pak Choy hampir sama dengan sayuran pada umumnya, seperti ;</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>a. Pemupukan</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Pemupukan pada tanaman Pak Choy dilakukan dengan menggunakan pupuk NPK dengan porsi sebanyak 300 kg per hektar. Pupuk ini ditaburkan saat tanaman berumur 12 hari sejak awal tanam.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>b. Pengendalian hama dan penyakit</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Berbagai hama yang biasa datang menghinggapi Pak Choy adalah ulat, kutu loncat, kumbang, Siput. Pengendaliannya dengan cara menyemprotkan pestisida yang aman untuk tanaman.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>c. Pengairan</b></span></p><p><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;</span><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Agar tanaman Pak Choy selalu segar dan tumbuh dengan subur maka Pak Choy perlu untuk disiram dengan kisaran waktu teratur dan secukupnya.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>d. Perawatan lainnya</b></span></p><p><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Perawatan lainnya meliputi perawatan dari gulma, perawatan bedengan dan drainase agar tetap menjadi media lahan yang baik bagi Pak Choy.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><b>5. Panen</b></span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Sayuran Pak Choy sudah bisa dipanen dengan baik saat tanaman berumur 28 sampai dengan 30 hari dari waktu penanaman awal. Namun semua tergantung dari perawatan, cuaca dan bibit. Sayuran Pak Choy yang sudah memiliki syarat untuk dipanen memiliki bagian pangkal sehat, daun tumbuh subur dan hijau serta tanaman menunjukkan pertumbuhan yang serempak dan merata.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Untuk memanen bisa dengan mencabut dari akar dengan hati-hati atau bisa juga dengan mengambil sebagian pangkal. Hati-hati saat akan dipanen karena bagian pangkal dan daun akan rawan rusak, daun dan pangkal yang mulus akan memiliki nilai ekonomis lebih.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Sementara pangkal dan daun yang rusak atau tergores memiliki nilai ekonomis yang berkurang, jadi harap berhati-hati dalam memanen dan menyimpan Pak Choy. Pak Choy yang sudah dipanen dikumpulkan di tempat yang teduh, jangan terlalu lama disimpan di ladang namun segera bawa ke tempat penyimpanan yang memiliki iklim yang sejuk, dan tidak terkena sinar matahari langsung yang dapat merusak kesegaran Pak Choy.</span><br></p>');
INSERT INTO `trans_topik_diskusi` (`ID_topik`, `ID_kategori_topik`, `ID_user`, `Judul_Topik`, `tanggal`, `waktu`, `isi_diskusi`) VALUES
('c1ca133853', 'c1ca133853', 'dmarkus', '[ASK] Pengiriman Tanaman / Bonsai Ke Luar Negeri', '2017-03-01', '2017-03-01 20:30:25', '<p><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Hi, Salam kenal, saya Ramdani dari Pesona&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/bonsai/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">Bonsai</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">. Ini adalah postingan ke 2 saya setelah saya menuliskan secercah salam perkenalan dii thread perkenalan. Saya sangat berterima kasih kepada KebunPedia karena ternyata setelah saya membaca materi di forum ini banyak ilmu dan wawasan seputar&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/tanaman/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">tanaman</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">, hampir seluruh tanaman. Pertama kali aya mengenal forum ini dari Link yang saya lihat di KebunBibit dan salah satu forum besar lain nya yaitu Ads-id . Ketika saya melihat forum ini saya langsung terpikat dan bergabung menjadi salah satu anggota yang masih newbie di forum ini, jadi di mohon untuk para master dan suhu untuk memberikan bimibingan nya&nbsp;</span><img src=\"http://www.kebunpedia.com/styles/default/xenforo/clear.png\" class=\"mceSmilieSprite mceSmilie1\" alt=\":)\" title=\"Smile    :)\" style=\"vertical-align: text-bottom; margin: 0px 1px; max-width: 100%; width: 18px; height: 18px; background: url(&quot;styles/default/xenforo/xenforo-smilies-sprite.png&quot;) 0px 0px no-repeat rgb(255, 254, 252); color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px;\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Next topik, Balik lagi ke laptop, eh ..... ke topik hhheee, sesuai judul yang saya tulis di atas, ada pertanyaan besar yang belum bisa saya jawab sampai saat ini baik secara implementasi di lapangan/fakta ataupun secara teori searhing di mbah google,,, idih.... apaan sih bahasanya ko ga nyambung ya&nbsp;</span><img src=\"http://www.kebunpedia.com/styles/default/xenforo/clear.png\" class=\"mceSmilieSprite mceSmilie8\" alt=\":D\" title=\"Big Grin    :D\" style=\"vertical-align: text-bottom; margin: 0px 1px; max-width: 100%; width: 18px; height: 18px; background: url(&quot;styles/default/xenforo/xenforo-smilies-sprite.png&quot;) -20px 0px no-repeat rgb(255, 254, 252); color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px;\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;.....</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Langsung ya ke pokok bahasan, saya adalah salah satu owner perusahaan Bonsai yang memiliki sekitar 1.000 lebih Pohon Bonsai.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Sekilas cerita : Kami mendirikan Toko Bonsai sejak Bulan Juni 1984 di kota hujan Bogor, Awalnya kami melakukan penjualan Bonsai hanya melalui offline dan belum menjamah online, karena dengan perkembangan zaman dan perkembangan tekhnologi digital, kami mencoba untuk memasarkan Bonsai secara digital/online, dan hasilnya ternyata sungguh luar biasa, kami sering melakukan&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/pengiriman/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">pengiriman</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;ke berbagai sudut Nusantara. Omset pun kiat menanjak, Alhamdulillah saya bersyukur dengan karunia tersebut, akan tetapi banyak yang perlu kami persiapkan karena ini baru awal dan masih banyak tantangan yang harus di hadapi karena dalam marketing pemasaran adalah pertarungan yang sengit hhhee. Jadi kami belum menjadi apa-apa dan masih harus banyak melakukan perbaikan secara kualitas, kuantitas dan hal tersebut harus berkesinambungan serta konsisten&nbsp;</span><img src=\"http://www.kebunpedia.com/styles/default/xenforo/clear.png\" class=\"mceSmilieSprite mceSmilie1\" alt=\":)\" title=\"Smile    :)\" style=\"vertical-align: text-bottom; margin: 0px 1px; max-width: 100%; width: 18px; height: 18px; background: url(&quot;styles/default/xenforo/xenforo-smilies-sprite.png&quot;) 0px 0px no-repeat rgb(255, 254, 252); color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px;\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Kendala yang saya alami sekarang adalah tentang masalah pengiriman tanaman, untuk pengiriman secara nasional sudah terpecahkan karena kini kami sering melakukan pengiriman tanaman ke seluruh nusantara dengan mudah, Alhamdulilah patut di syukuri. Jadi apa yang menjadi masalah dan belum terpecahkan ?</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">\"Pengiriman Tanaman Ke Luar Negeri\" . Yang kami alami sekarang adalah kesulitan untuk melakukan pengiriman tanaman ke luar negeri. Prioritas kami adalah Bonsai, kedepannya mudah-mudahan bisa merambah lebih jauh ke&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/tanaman-hias/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">tanaman hias</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;dan&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/bibit/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">bibit</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">, semoga untuk bibit bisa bekerjasama dengan kebun bibit (Mudah-mudahan owner kebunbibit baca) heheee....</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Banyaknya permintaan order dari luar negeri membuat saya berpikir keras dan mencari solusi agar apa yang saya cari bisa terjawab yaitu problem kesulitan pengiriman ke luar negeri.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Saat saya mencari referensi tentang&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/ekspedisi/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">ekspedisi</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">&nbsp;yang melayani&nbsp;</span><a href=\"http://www.kebunpedia.com/tags/ekspor/\" class=\"Tinhte_XenTag_TagLink\" title=\"\" style=\"color: rgb(29, 122, 27); border-radius: 5px; padding-top: 0px; padding-bottom: 0px; padding-left: 3px; margin: 0px -3px; background: url(&quot;styles/default/Tinhte/XenTag/tag.png&quot;) right bottom no-repeat rgb(255, 254, 252); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; padding-right: 12px !important;\">ekspor</a><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">, diantaranya DHL dan EMS Pos Indonesia, saya langsung menghubungi call center mereka, akan tetapi jawaban yang sya dapatkan adalah : Tanaman tidak bisa di kirim ke luar negeri.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Beberapa calon pelanggan telah menghubungi saya mulai dari USA, Australia, UK, Saudi Arabia, Malaysia, Singapore, Srilanka, dll . Calon pelanggan tersebut terbuang secara percuma karena ketidak sanggupan saya untuk melakukan pengiriman ke luar negara.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Dari problem saya diatas, mudah-mudahan di forum ini ada yang bisa sharing dan berbagi ilmu kepada saya dan memberikan solusinya.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Sekian dari saya, maaf agal banyak cerita kaya cerpen hhheee.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Terima kasih, mohon maaf jika ada salah kata, Salam hangat - Pesona Bonsai.</span><br></p>'),
('cf7bf6195b', 'cf7bf6195b', 'frmaya', 'mengendalikan hama padi dengan bio urine manusia', '2017-03-01', '2017-03-01 20:14:01', '<p><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Tikus dan penggerek batang menjadi hama utama pada tanaman padi. Petanipun tidak kehabisan akal untuk mengendalikan kedua jenis hama padi tersebut. Salah satu cara yang dilakukan petani Pinrang adalah mengusir tikus dengan bau urine manusia.</span><br style=\"margin-top: 0px; color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">System repelant memiliki prinsip yaitu menciptakan suasana yang tidak disukai oleh tikus lewat bau yang ditimbulkan oleh suatu bahan tambahan yang diberikan pada tanaman padi. Bahan bisa dibuat dari buah, daun atau rempah-rempah. Tapi bahan yang sudah dicoba dan terbukti ampuh yaitu urine manusia yang telah difermentasi dengan menggunakan EM4.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Cara pembuatannya pun sangat mudah, yaitu setelah urine terkumpul sebanyak 1 liter lalu dicampur dengan air tajin sebanyak 1 liter dan ditambah EM4 sebanyak 250 ml. setelah itu difermentasi selama 7 hari. Aplikasinya yaitu untuk serangan ringan dengan mencampur Bio urine sebanyak 250ml / tangki (15 liter air) dan untuk serangan berat dosisnya bisa ditambah menjadi 500ml / tangki dan diulangi setiap 10 hari.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Dari hasil aplikasi yang telah dilakukan, ternyata mampu menghentikan aktivitas makan hama tikus setelah dilakukan penyemprotan. Hasil fermentasi bio urine ini memiliki bau yang sangat pesing / tajam sehingga akan membuat hama tikus yang memiliki penciuman yang baik tidak betah lalu pergi. Bio urine ini juga mampu mengapus jejak tikus sehingga tikus tidak memiliki lagi petunjuk jalan dalam melakukan aktivitasnya. Seperti diketahui bahwa tikus akan selalu berjalan dengan rute yang sama yang mana sebelumnya telah ia tandai dengan bau urinenya sendiri.</span><br style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\"><span style=\"color: rgb(20, 20, 20); font-family: Tahoma, Geneva, sans-serif; font-size: 14.6667px; background-color: rgb(255, 254, 252);\">Selain hama tikus bio urine manusia juga mampu mengendalikan hama penggerek batang padi yang mana hama ini juga termasuk hama utama dengan tingkat kerusakan yang ditimbulkan juga sangat tinggi. Bio urine bekerja dengan cara mengacaukan system perkawinan ngengat penggerek batang. Ngengat jantan dan ngengat betina berkomunikasi dengan bau feromon sex yang dikeluarkan oleh betina, dengan dilakukannya penyemprotan bio urine yang memilki bau khas maka akan menutupi bau feromon sex betina sehingga tidak akan terjaadi perkawinan. (Mubarak Harun)</span><br></p>'),
('d62272b90c', 'd62272b90c', 'ambarnur', 'kucai', '2017-03-01', '2017-03-01 20:36:36', '<div class=\"messageContent\" style=\"margin: 0px; padding: 0px 0px 2px; min-height: 100px; overflow: hidden; color: rgb(20, 20, 20); font-family: &quot;Trebuchet MS&quot;, Helvetica, Arial, sans-serif; font-size: 13px;\"><article><blockquote class=\"messageText SelectQuoteContainer ugc baseHtml\" style=\"margin-bottom: 0px; padding: 0px; font-size: 11pt; font-family: Tahoma, Geneva, sans-serif; line-height: 1.4;\">Alo salam kenal semuanya,<br style=\"margin-top: 0px;\"><br>saya anggota baru disini. mau berguru dari para senior disini. Oh ya saya tinggal di Purwokerto, Banyumas, Jateng<br><br><br>Saya ingin mengetahui menanam kucai agar dapat menghasilkan panen optimal. karena saya melihat kucai peluangnya besar.<br>terima kasih untuk sharingnya<div class=\"messageTextEndMarker\" style=\"margin: 0px; padding: 0px; height: 0px; font-size: 0px;\">&nbsp;</div></blockquote></article></div><div class=\"messageMeta ToggleTriggerAnchor\" style=\"margin: -5px; padding: 15px 5px 5px; font-size: 11px; overflow: hidden; zoom: 1; color: rgb(20, 20, 20); font-family: &quot;Trebuchet MS&quot;, Helvetica, Arial, sans-serif;\"><div class=\"privateControls\" style=\"margin: 0px; padding: 0px; float: left;\"></div></div>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `icon`
--
ALTER TABLE `icon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi` (`Nama_Provinsi`),
  ADD KEY `Nama_Provinsi_2` (`Nama_Provinsi`);
ALTER TABLE `kabupaten` ADD FULLTEXT KEY `Nama_Kabupaten` (`Nama_Kabupaten`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`Nama_Kecamatan`),
  ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi` (`Nama_Provinsi`);

--
-- Indexes for table `kelurahan_desa`
--
ALTER TABLE `kelurahan_desa`
  ADD PRIMARY KEY (`Nama_Desa`),
  ADD UNIQUE KEY `Nama_Desa` (`Nama_Desa`,`Nama_Kecamatan`,`Nama_Kabupaten`,`Nama_Provinsi`),
  ADD KEY `Nama_Kecamatan` (`Nama_Kecamatan`,`Nama_Kabupaten`,`Nama_Provinsi`),
  ADD KEY `Nama_Kabupaten` (`Nama_Kabupaten`),
  ADD KEY `Nama_Provinsi` (`Nama_Provinsi`);

--
-- Indexes for table `log_aktifitas_pelatihan`
--
ALTER TABLE `log_aktifitas_pelatihan`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Permintaan_Pelatihan` (`ID_Permintaan_Pelatihan`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_aktifitas_pelayanan`
--
ALTER TABLE `log_aktifitas_pelayanan`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Permintaan_Pelayanan` (`ID_Permintaan_Pelayanan`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_biaya_keluar`
--
ALTER TABLE `log_biaya_keluar`
  ADD KEY `ID_Aktivitas` (`ID_Aktivitas`,`ID_Petani`,`ID_Biaya`),
  ADD KEY `ID_Biaya` (`ID_Biaya`),
  ADD KEY `ID_Petani` (`ID_Petani`),
  ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `log_penawaran_prod`
--
ALTER TABLE `log_penawaran_prod`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Penawaran` (`ID_Penawaran`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_permintaan`
--
ALTER TABLE `log_permintaan`
  ADD PRIMARY KEY (`ID_Log`),
  ADD KEY `ID_Permintaan` (`ID_Permintaan`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `log_user_trans`
--
ALTER TABLE `log_user_trans`
  ADD KEY `ID_User` (`ID_User`,`ID_Aktivitas`),
  ADD KEY `ID_Aktivitas` (`ID_Aktivitas`);

--
-- Indexes for table `master_aktifitas_spesies`
--
ALTER TABLE `master_aktifitas_spesies`
  ADD PRIMARY KEY (`ID_aktifitas_spesies`),
  ADD KEY `ID_Spesies` (`ID_Spesies`),
  ADD KEY `ID_Aktivitas` (`ID_Aktivitas`),
  ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Indexes for table `master_aktivitas`
--
ALTER TABLE `master_aktivitas`
  ADD PRIMARY KEY (`ID_Aktivitas`);

--
-- Indexes for table `master_alat_tani`
--
ALTER TABLE `master_alat_tani`
  ADD PRIMARY KEY (`ID_Alat`),
  ADD KEY `ID_Kategori` (`ID_Kategori`);

--
-- Indexes for table `master_bahan_tani`
--
ALTER TABLE `master_bahan_tani`
  ADD PRIMARY KEY (`ID_Bahan`),
  ADD KEY `ID_Kategori` (`ID_Kategori`);

--
-- Indexes for table `master_berita`
--
ALTER TABLE `master_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_berita_informasi`
--
ALTER TABLE `master_berita_informasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NIK` (`NIK`);

--
-- Indexes for table `master_detail_materi_ajar`
--
ALTER TABLE `master_detail_materi_ajar`
  ADD PRIMARY KEY (`ID_Materi_Ajar`,`ID_Sub_materi`),
  ADD KEY `ID_Materi_Ajar` (`ID_Materi_Ajar`);

--
-- Indexes for table `master_detail_user`
--
ALTER TABLE `master_detail_user`
  ADD PRIMARY KEY (`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_fasilitator`
--
ALTER TABLE `master_fasilitator`
  ADD PRIMARY KEY (`ID_User`);

--
-- Indexes for table `master_jenis_sup`
--
ALTER TABLE `master_jenis_sup`
  ADD PRIMARY KEY (`ID_Jenis_Sup`);

--
-- Indexes for table `master_kategori`
--
ALTER TABLE `master_kategori`
  ADD PRIMARY KEY (`ID_Kategori`);

--
-- Indexes for table `master_kategori_alatbahan`
--
ALTER TABLE `master_kategori_alatbahan`
  ADD PRIMARY KEY (`ID_Kategori`);

--
-- Indexes for table `master_kategori_topik`
--
ALTER TABLE `master_kategori_topik`
  ADD PRIMARY KEY (`ID_kategori_topik`);

--
-- Indexes for table `master_kel_tani`
--
ALTER TABLE `master_kel_tani`
  ADD PRIMARY KEY (`ID_Kelompok_Tani`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_kode_biaya`
--
ALTER TABLE `master_kode_biaya`
  ADD PRIMARY KEY (`ID_Biaya`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_materi_ajar`
--
ALTER TABLE `master_materi_ajar`
  ADD PRIMARY KEY (`ID_Materi_Ajar`),
  ADD KEY `ID_User` (`ID_User`),
  ADD KEY `ID_User_2` (`ID_User`);

--
-- Indexes for table `master_materi_pelatihan`
--
ALTER TABLE `master_materi_pelatihan`
  ADD PRIMARY KEY (`ID_Materi`),
  ADD KEY `ID_Materi` (`ID_Materi`),
  ADD KEY `Materi_Sebelumnya` (`Materi_Sebelumnya`);

--
-- Indexes for table `master_morf_tanaman`
--
ALTER TABLE `master_morf_tanaman`
  ADD PRIMARY KEY (`ID_Morfologi`);

--
-- Indexes for table `master_org_unit`
--
ALTER TABLE `master_org_unit`
  ADD PRIMARY KEY (`Org_Unit`),
  ADD KEY `Org_Unit_Atasan` (`Org_Unit_Atasan`);

--
-- Indexes for table `master_pelayanan`
--
ALTER TABLE `master_pelayanan`
  ADD PRIMARY KEY (`ID_Pelayanan`);

--
-- Indexes for table `master_petani`
--
ALTER TABLE `master_petani`
  ADD PRIMARY KEY (`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_peta_lahan`
--
ALTER TABLE `master_peta_lahan`
  ADD PRIMARY KEY (`ID_Lahan`,`bulan`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_produk_tani`
--
ALTER TABLE `master_produk_tani`
  ADD PRIMARY KEY (`ID_Produk`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `master_spesies _tanaman`
--
ALTER TABLE `master_spesies _tanaman`
  ADD PRIMARY KEY (`ID_Spesies`),
  ADD KEY `ID_Morfologi` (`ID_Morfologi`);

--
-- Indexes for table `master_supplier`
--
ALTER TABLE `master_supplier`
  ADD PRIMARY KEY (`ID_User`),
  ADD KEY `ID_Jenis_Sup` (`ID_Jenis_Sup`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_user`
--
ALTER TABLE `master_user`
  ADD PRIMARY KEY (`ID_User`);

--
-- Indexes for table `master_user_kat`
--
ALTER TABLE `master_user_kat`
  ADD KEY `ID_Kategori` (`ID_Kategori`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `master_user_org`
--
ALTER TABLE `master_user_org`
  ADD PRIMARY KEY (`NIK`),
  ADD UNIQUE KEY `NIK_2` (`NIK`),
  ADD KEY `Org_Unit` (`Org_Unit`),
  ADD KEY `NIK` (`NIK`);

--
-- Indexes for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `ta`
--
ALTER TABLE `ta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trans_aktivitas_pertanian`
--
ALTER TABLE `trans_aktivitas_pertanian`
  ADD PRIMARY KEY (`ID_aktifitas_petani`),
  ADD KEY `ID_Aktivitas` (`ID_Petani`),
  ADD KEY `ID_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Indexes for table `trans_anggaraan`
--
ALTER TABLE `trans_anggaraan`
  ADD PRIMARY KEY (`ID_anggaran`),
  ADD KEY `ID_User` (`ID_User`,`ID_Aktifitas`),
  ADD KEY `ID_Aktifitas` (`ID_Aktifitas`);

--
-- Indexes for table `trans_ang_petani`
--
ALTER TABLE `trans_ang_petani`
  ADD PRIMARY KEY (`ID_Kelompok_Tani`,`ID_User`),
  ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_detail_anggaran`
--
ALTER TABLE `trans_detail_anggaran`
  ADD PRIMARY KEY (`ID_detail_anggaran`),
  ADD KEY `ID_detail_anggaran` (`ID_anggaran`,`ID_biaya`);

--
-- Indexes for table `trans_harga_prod`
--
ALTER TABLE `trans_harga_prod`
  ADD UNIQUE KEY `ID_Produk_2` (`ID_Produk`,`Tanggal`),
  ADD KEY `ID_Produk` (`ID_Produk`,`ID_User`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_hasil_panen`
--
ALTER TABLE `trans_hasil_panen`
  ADD PRIMARY KEY (`ID_Petani`,`ID_Spesies`,`Tahun_Panen`,`Periode_Panen`),
  ADD KEY `ID_Petani` (`ID_Petani`),
  ADD KEY `ID_Spesies` (`ID_Spesies`),
  ADD KEY `ID_Produk` (`ID_Produk`);

--
-- Indexes for table `trans_kalender_tanam`
--
ALTER TABLE `trans_kalender_tanam`
  ADD PRIMARY KEY (`ID_Spesies`,`Masa_Tanam`,`Kabupaten`),
  ADD KEY `ID_Spesies` (`ID_Spesies`);

--
-- Indexes for table `trans_komentar_diskusi`
--
ALTER TABLE `trans_komentar_diskusi`
  ADD PRIMARY KEY (`nomor_komentar`),
  ADD KEY `ID_topik` (`ID_topik`);

--
-- Indexes for table `trans_pelaksanaan_pelatihan`
--
ALTER TABLE `trans_pelaksanaan_pelatihan`
  ADD KEY `ID_Fasilitator` (`ID_Permintaan_Pelatihan`),
  ADD KEY `ID_Permintaan_Pelatihan` (`ID_Permintaan_Pelatihan`),
  ADD KEY `ID_User` (`ID_User`);

--
-- Indexes for table `trans_penawaran_prod_tani`
--
ALTER TABLE `trans_penawaran_prod_tani`
  ADD PRIMARY KEY (`ID_Penawaran`),
  ADD KEY `ID_Supplier` (`ID_User`,`ID_Barang`),
  ADD KEY `ID_Barang` (`ID_Barang`);

--
-- Indexes for table `trans_permintaan`
--
ALTER TABLE `trans_permintaan`
  ADD PRIMARY KEY (`ID_Permintaan`),
  ADD KEY `ID_User` (`ID_User`,`ID_Penawaran`),
  ADD KEY `ID_Penawaran` (`ID_Penawaran`);

--
-- Indexes for table `trans_permintaan_pelatihan`
--
ALTER TABLE `trans_permintaan_pelatihan`
  ADD PRIMARY KEY (`ID_Permintaan_Pelatihan`),
  ADD KEY `ID_User` (`ID_User`,`ID_Materi`),
  ADD KEY `ID_Materi` (`ID_Materi`),
  ADD KEY `ID_User_2` (`ID_User`),
  ADD KEY `ID_Materi_2` (`ID_Materi`);

--
-- Indexes for table `trans_permintaan_pelayanan`
--
ALTER TABLE `trans_permintaan_pelayanan`
  ADD PRIMARY KEY (`ID_Permintaan_Pelayanan`),
  ADD KEY `ID_User` (`ID_User`,`ID_Pelayanan`),
  ADD KEY `ID_Pelayanan` (`ID_Pelayanan`);

--
-- Indexes for table `trans_realisasi_biaya`
--
ALTER TABLE `trans_realisasi_biaya`
  ADD KEY `ID_detail_anggaran` (`ID_detail_anggaran`);

--
-- Indexes for table `trans_struk_org`
--
ALTER TABLE `trans_struk_org`
  ADD KEY `ID_Kelompok_Tani` (`ID_Kelompok_Tani`);

--
-- Indexes for table `trans_topik_diskusi`
--
ALTER TABLE `trans_topik_diskusi`
  ADD PRIMARY KEY (`ID_topik`),
  ADD KEY `ID_kategori_topik` (`ID_kategori_topik`,`ID_user`),
  ADD KEY `ID_user` (`ID_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `icon`
--
ALTER TABLE `icon`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `master_berita`
--
ALTER TABLE `master_berita`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_berita_informasi`
--
ALTER TABLE `master_berita_informasi`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ta`
--
ALTER TABLE `ta`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `trans_komentar_diskusi`
--
ALTER TABLE `trans_komentar_diskusi`
  MODIFY `nomor_komentar` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `log_aktifitas_pelatihan`
--
ALTER TABLE `log_aktifitas_pelatihan`
  ADD CONSTRAINT `log_aktifitas_pelatihan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_aktifitas_pelatihan_ibfk_3` FOREIGN KEY (`ID_Permintaan_Pelatihan`) REFERENCES `trans_permintaan_pelatihan` (`ID_Permintaan_Pelatihan`);

--
-- Constraints for table `log_aktifitas_pelayanan`
--
ALTER TABLE `log_aktifitas_pelayanan`
  ADD CONSTRAINT `log_aktifitas_pelayanan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_aktifitas_pelayanan_ibfk_3` FOREIGN KEY (`ID_Permintaan_Pelayanan`) REFERENCES `trans_permintaan_pelayanan` (`ID_Permintaan_Pelayanan`);

--
-- Constraints for table `log_biaya_keluar`
--
ALTER TABLE `log_biaya_keluar`
  ADD CONSTRAINT `log_biaya_keluar_ibfk_1` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktifitas_spesies` (`ID_Aktivitas`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_2` FOREIGN KEY (`ID_Biaya`) REFERENCES `master_kode_biaya` (`ID_Biaya`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_3` FOREIGN KEY (`ID_Petani`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_4` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
  ADD CONSTRAINT `log_biaya_keluar_ibfk_5` FOREIGN KEY (`ID_aktifitas_spesies`) REFERENCES `master_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Constraints for table `log_penawaran_prod`
--
ALTER TABLE `log_penawaran_prod`
  ADD CONSTRAINT `log_penawaran_prod_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_penawaran_prod_ibfk_3` FOREIGN KEY (`ID_Penawaran`) REFERENCES `trans_penawaran_prod_tani` (`ID_Penawaran`),
  ADD CONSTRAINT `log_penawaran_prod_ibfk_4` FOREIGN KEY (`ID_User`) REFERENCES `master_supplier` (`ID_User`);

--
-- Constraints for table `log_permintaan`
--
ALTER TABLE `log_permintaan`
  ADD CONSTRAINT `log_permintaan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_permintaan_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `trans_permintaan` (`ID_User`);

--
-- Constraints for table `log_user_trans`
--
ALTER TABLE `log_user_trans`
  ADD CONSTRAINT `log_user_trans_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `log_user_trans_ibfk_2` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktivitas` (`ID_Aktivitas`);

--
-- Constraints for table `master_aktifitas_spesies`
--
ALTER TABLE `master_aktifitas_spesies`
  ADD CONSTRAINT `master_aktifitas_spesies_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
  ADD CONSTRAINT `master_aktifitas_spesies_ibfk_2` FOREIGN KEY (`ID_Aktivitas`) REFERENCES `master_aktivitas` (`ID_Aktivitas`);

--
-- Constraints for table `master_alat_tani`
--
ALTER TABLE `master_alat_tani`
  ADD CONSTRAINT `master_alat_tani_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori_alatbahan` (`ID_Kategori`);

--
-- Constraints for table `master_bahan_tani`
--
ALTER TABLE `master_bahan_tani`
  ADD CONSTRAINT `master_bahan_tani_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori_alatbahan` (`ID_Kategori`);

--
-- Constraints for table `master_berita_informasi`
--
ALTER TABLE `master_berita_informasi`
  ADD CONSTRAINT `master_berita_informasi_ibfk_1` FOREIGN KEY (`NIK`) REFERENCES `master_user_org` (`NIK`);

--
-- Constraints for table `master_detail_materi_ajar`
--
ALTER TABLE `master_detail_materi_ajar`
  ADD CONSTRAINT `master_detail_materi_ajar_ibfk_1` FOREIGN KEY (`ID_Materi_Ajar`) REFERENCES `master_materi_ajar` (`ID_Materi_Ajar`);

--
-- Constraints for table `master_detail_user`
--
ALTER TABLE `master_detail_user`
  ADD CONSTRAINT `master_detail_user_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_kel_tani`
--
ALTER TABLE `master_kel_tani`
  ADD CONSTRAINT `master_kel_tani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_kode_biaya`
--
ALTER TABLE `master_kode_biaya`
  ADD CONSTRAINT `master_kode_biaya_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `master_materi_ajar`
--
ALTER TABLE `master_materi_ajar`
  ADD CONSTRAINT `master_materi_ajar_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_materi_pelatihan`
--
ALTER TABLE `master_materi_pelatihan`
  ADD CONSTRAINT `master_materi_pelatihan_ibfk_1` FOREIGN KEY (`Materi_Sebelumnya`) REFERENCES `master_materi_pelatihan` (`ID_Materi`);

--
-- Constraints for table `master_org_unit`
--
ALTER TABLE `master_org_unit`
  ADD CONSTRAINT `master_org_unit_ibfk_1` FOREIGN KEY (`Org_Unit_Atasan`) REFERENCES `master_org_unit` (`Org_Unit`);

--
-- Constraints for table `master_petani`
--
ALTER TABLE `master_petani`
  ADD CONSTRAINT `master_petani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_peta_lahan`
--
ALTER TABLE `master_peta_lahan`
  ADD CONSTRAINT `master_peta_lahan_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `master_produk_tani`
--
ALTER TABLE `master_produk_tani`
  ADD CONSTRAINT `master_produk_tani_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `master_spesies _tanaman`
--
ALTER TABLE `master_spesies _tanaman`
  ADD CONSTRAINT `master_spesies _tanaman_ibfk_1` FOREIGN KEY (`ID_Morfologi`) REFERENCES `master_morf_tanaman` (`ID_Morfologi`);

--
-- Constraints for table `master_supplier`
--
ALTER TABLE `master_supplier`
  ADD CONSTRAINT `master_supplier_ibfk_1` FOREIGN KEY (`ID_Jenis_Sup`) REFERENCES `master_jenis_sup` (`ID_Jenis_Sup`),
  ADD CONSTRAINT `master_supplier_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_user_kat`
--
ALTER TABLE `master_user_kat`
  ADD CONSTRAINT `master_user_kat_ibfk_1` FOREIGN KEY (`ID_Kategori`) REFERENCES `master_kategori` (`ID_Kategori`),
  ADD CONSTRAINT `master_user_kat_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `master_user_org`
--
ALTER TABLE `master_user_org`
  ADD CONSTRAINT `master_user_org_ibfk_1` FOREIGN KEY (`Org_Unit`) REFERENCES `master_org_unit` (`Org_Unit`);

--
-- Constraints for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD CONSTRAINT `pemberitahuan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `trans_aktivitas_pertanian`
--
ALTER TABLE `trans_aktivitas_pertanian`
  ADD CONSTRAINT `trans_aktivitas_pertanian_ibfk_1` FOREIGN KEY (`ID_Petani`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_aktivitas_pertanian_ibfk_2` FOREIGN KEY (`ID_aktifitas_spesies`) REFERENCES `master_aktifitas_spesies` (`ID_aktifitas_spesies`);

--
-- Constraints for table `trans_anggaraan`
--
ALTER TABLE `trans_anggaraan`
  ADD CONSTRAINT `trans_anggaraan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_anggaraan_ibfk_2` FOREIGN KEY (`ID_Aktifitas`) REFERENCES `master_aktivitas` (`ID_Aktivitas`);

--
-- Constraints for table `trans_ang_petani`
--
ALTER TABLE `trans_ang_petani`
  ADD CONSTRAINT `trans_ang_petani_ibfk_1` FOREIGN KEY (`ID_Kelompok_Tani`) REFERENCES `master_kel_tani` (`ID_Kelompok_Tani`),
  ADD CONSTRAINT `trans_ang_petani_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_petani` (`ID_User`),
  ADD CONSTRAINT `trans_ang_petani_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `trans_detail_anggaran`
--
ALTER TABLE `trans_detail_anggaran`
  ADD CONSTRAINT `trans_detail_anggaran_ibfk_1` FOREIGN KEY (`ID_anggaran`) REFERENCES `trans_anggaraan` (`ID_anggaran`);

--
-- Constraints for table `trans_harga_prod`
--
ALTER TABLE `trans_harga_prod`
  ADD CONSTRAINT `trans_harga_prod_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_harga_prod_ibfk_3` FOREIGN KEY (`ID_Produk`) REFERENCES `master_produk_tani` (`ID_Produk`);

--
-- Constraints for table `trans_hasil_panen`
--
ALTER TABLE `trans_hasil_panen`
  ADD CONSTRAINT `trans_hasil_panen_ibfk_1` FOREIGN KEY (`ID_Petani`) REFERENCES `master_petani` (`ID_User`),
  ADD CONSTRAINT `trans_hasil_panen_ibfk_2` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`),
  ADD CONSTRAINT `trans_hasil_panen_ibfk_3` FOREIGN KEY (`ID_Produk`) REFERENCES `master_produk_tani` (`ID_Produk`);

--
-- Constraints for table `trans_kalender_tanam`
--
ALTER TABLE `trans_kalender_tanam`
  ADD CONSTRAINT `trans_kalender_tanam_ibfk_1` FOREIGN KEY (`ID_Spesies`) REFERENCES `master_spesies _tanaman` (`ID_Spesies`);

--
-- Constraints for table `trans_komentar_diskusi`
--
ALTER TABLE `trans_komentar_diskusi`
  ADD CONSTRAINT `trans_komentar_diskusi_ibfk_1` FOREIGN KEY (`ID_topik`) REFERENCES `trans_topik_diskusi` (`ID_topik`);

--
-- Constraints for table `trans_pelaksanaan_pelatihan`
--
ALTER TABLE `trans_pelaksanaan_pelatihan`
  ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_2` FOREIGN KEY (`ID_Permintaan_Pelatihan`) REFERENCES `trans_permintaan_pelatihan` (`ID_Permintaan_Pelatihan`),
  ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_fasilitator` (`ID_User`),
  ADD CONSTRAINT `trans_pelaksanaan_pelatihan_ibfk_4` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `trans_penawaran_prod_tani`
--
ALTER TABLE `trans_penawaran_prod_tani`
  ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_supplier` (`ID_User`),
  ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_2` FOREIGN KEY (`ID_Barang`) REFERENCES `master_alat_tani` (`ID_Alat`),
  ADD CONSTRAINT `trans_penawaran_prod_tani_ibfk_3` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`);

--
-- Constraints for table `trans_permintaan`
--
ALTER TABLE `trans_permintaan`
  ADD CONSTRAINT `trans_permintaan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_permintaan_ibfk_2` FOREIGN KEY (`ID_Penawaran`) REFERENCES `trans_penawaran_prod_tani` (`ID_Penawaran`);

--
-- Constraints for table `trans_permintaan_pelatihan`
--
ALTER TABLE `trans_permintaan_pelatihan`
  ADD CONSTRAINT `trans_permintaan_pelatihan_ibfk_1` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_permintaan_pelatihan_ibfk_2` FOREIGN KEY (`ID_Materi`) REFERENCES `master_materi_pelatihan` (`ID_Materi`);

--
-- Constraints for table `trans_permintaan_pelayanan`
--
ALTER TABLE `trans_permintaan_pelayanan`
  ADD CONSTRAINT `trans_permintaan_pelayanan_ibfk_2` FOREIGN KEY (`ID_User`) REFERENCES `master_user` (`ID_User`),
  ADD CONSTRAINT `trans_permintaan_pelayanan_ibfk_3` FOREIGN KEY (`ID_Pelayanan`) REFERENCES `master_pelayanan` (`ID_Pelayanan`);

--
-- Constraints for table `trans_realisasi_biaya`
--
ALTER TABLE `trans_realisasi_biaya`
  ADD CONSTRAINT `trans_realisasi_biaya_ibfk_1` FOREIGN KEY (`ID_detail_anggaran`) REFERENCES `trans_detail_anggaran` (`ID_detail_anggaran`);

--
-- Constraints for table `trans_struk_org`
--
ALTER TABLE `trans_struk_org`
  ADD CONSTRAINT `trans_struk_org_ibfk_1` FOREIGN KEY (`ID_Kelompok_Tani`) REFERENCES `master_kel_tani` (`ID_Kelompok_Tani`);

--
-- Constraints for table `trans_topik_diskusi`
--
ALTER TABLE `trans_topik_diskusi`
  ADD CONSTRAINT `trans_topik_diskusi_ibfk_1` FOREIGN KEY (`ID_kategori_topik`) REFERENCES `master_kategori_topik` (`ID_kategori_topik`),
  ADD CONSTRAINT `trans_topik_diskusi_ibfk_2` FOREIGN KEY (`ID_user`) REFERENCES `master_user` (`ID_User`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
