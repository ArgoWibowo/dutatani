<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "PET")
  {
    header("Location:login.php?pesan=hanya untuk user petani");
  } 
?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>

    <body>
       <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="index_user_petani.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="#">Selamat Datang! <span class="fa-stack pull-left" style="margin-top: -1.5%"><i class="fa fa-user-circle-o fa-lg fa-stack-1x"></i></span><strong><?php echo $brsnama['Nama_Petani'] ?></strong></a></li>
        <li><a href="tutup_session.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-out fa-stack-1x"></i></span> Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <div id="wrapper" style="padding-top: 3.6%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-list-alt" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Aktivitas Pertanian</span>
                      </a>
                      <ul class="sub">
                          <li><a href="daftar_trans_aktivitas.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Aktivitas Tani</a></li>
                          <li><a href="Daftar Hasil Panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-check-square fa-stack-1x"></i></span> Hasil Panen</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu active">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li class="active-menu"><a href="Daftar Tanaman-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Daftar Tanaman</a></li>
                        <li><a href="Daftar morfologi-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x "></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Grafik</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Grafikpanenpetani.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz" style="height: 120%">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <?php
                            $id_spesies = $_GET['Id_spesies'];
                            mysql_connect("localhost","root","") or die(mysql_error());
                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                            $query = mysql_query("SELECT * FROM master_spesies_tanaman where ID_Spesies = '$id_spesies'");

                            while($brs = mysql_fetch_assoc($query)){
                            ?>
                            <div class="row">
                                <div class="col-md-8">
                                <h1>Detail Tanaman <?php echo $brs['Nama_Tanaman']?></h1>
                                <a style="font-size:20px" href="Daftar Tanaman-user.php"><span class="glyphicon glyphicon-chevron-left"></span>Kembali ke Daftar Tanaman</a>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-2">

                                    <?php echo '<img src="images/foto1/'.$brs['Foto1'].'"class="img-rounded" height="140" width="140"/>'
                                    ?>
                                </div>
                                 <div class="col-md-2">

                                    <?php echo '<img src="images/foto2/'.$brs['Foto2'].'"class="img-rounded" height="140" width="140"/>'
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Nama Tanaman    
                                            </div>
                                            <div class="col-md-6">
                                            : <?php echo $brs['Nama_Tanaman']
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                             <div class="col-md-6">
                                            Jenis Tanaman
                                             </div>  
                                            <div class="col-md-6"> 
                                            : <?php echo $brs['Jenis_Tanaman']
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Nama Latin
                                            </div>
                                            <div class="col-md-6">
                                            : <?php echo $brs['Nama_Latin']
                                            ?>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Habitat
                                            </div>
                                            <div class="col-md-6">      
                                            : <?php echo $brs['Habitat']
                                            ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Masa Tanam
                                            </div>
                                            <div class="col-md-6">     
                                            : <?php echo $brs['Masa_Tanam']
                                            ?> Hari
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                

                                

                                




                                <?php
                                $id_morfologi = $brs['ID_Morfologi'];
                                 $querymorfologi= mysql_query("SELECT * FROM master_morf_tanaman Where ID_Morfologi = '$id_morfologi'");
                                 $brsmorfologi = mysql_fetch_assoc($querymorfologi);
                                ?> 
                                 


                              


                            </div> 
                              <br>
                             <div class="row">
                                    <div class="col-md-10">

                                    <div class="row">
                                    <div class="col-md-2">
                                       
                                                Akar
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Akar']?>
                                            
                                               
                                    </div>
                                       </div>           
                             <div class="row">
                                    <div class="col-md-2">
                                       
                                                Batang
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Batang']?>
                                            
                                               
                                    </div>
                                       </div>     
                                <div class="row">
                                    <div class="col-md-2">
                                       
                                                Daun
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Daun']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Buah
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Buah']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Biji
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Biji']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Perkembangbiakan
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Perkembangbiakan']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Iklim
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Iklim']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Jenis Tanah
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Jenis_Tanah']?>
                                            
                                               
                                    </div>
                                       </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                       
                                                Kelembababan
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                : <?php echo $brs['Kelembaban']?>
                                            
                                               
                                    </div>
                                       </div>
                                        <div class="row">
                                    <div class="col-md-2">
                                       
                                                Morfologi
                                            
                                               
                                    </div>
                                     <div class="col-md-10">
                                       
                                                  : <?php echo $brsmorfologi['Nama_Morfologi_Tanaman']
                                        ?>
                                            
                                               
                                    </div>
                                       </div>    
                                            </div>

                                <div class="col-md-2">
                                      
                                         
                                     
                                </div>

                                 </div> 
                              

                                    </div>
                                 
                        </div>
                        <br>
                        <a class="btn btn-md btn-danger" href="Daftar Tanaman-user.php" style="margin-left: "> Kembali ke Daftar Tanaman</a>&nbsp;  


                            <?php
                            }
                            ?>

                            <!--end konten-->
    
                     </div>
                </div>
         </div>
         <script src="css/side_menu.js"></script>
        <footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #13780C;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>
    </body>
</html>