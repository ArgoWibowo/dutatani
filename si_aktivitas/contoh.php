<?php
  session_start();
  if(!isset($_SESSION["username"])){
    header("Location:index.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["role"] != "Admin" and $_SESSION["role"] != "Kaprodi")
  {
  	header("Location:index.php?pesan=Hanya Admin atau kaprodi");
  }
?>