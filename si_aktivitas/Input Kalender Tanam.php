<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "ADT")
  {
    header("Location:login.php?pesan=hanya untuk admin");
  } 
?>
<html><head>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>

        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

  </head><body>
  <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="indexadmin.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <?php                   
                    $id_user = $_SESSION['ID_User'];                                       
                    mysql_connect("localhost","root","") or die(mysql_error());
                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                    $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
                    $brsnama = mysql_fetch_array($query)
                    ?>
                    <li><a href="#">Selamat Datang! <span class="fa-stack pull-left" style="margin-top: -1.5%"><i class="fa fa-user-circle-o fa-lg fa-stack-1x"></i></span><strong><?php echo $brsnama['Nama_Petani'] ?></strong></a></li>
                    <li><a href="tutup_session.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-out fa-stack-1x"></i></span> Keluar</a></li>
                </ul>
                    </li>
                 </ul>
            </div>
        </div>
  </nav>
  
    <div id="wrapper" style="padding-top: 3.6%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu active">
                      <a class="active" href="javascript:;">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Tanaman</span>
                      </a>
                      <ul class="sub">
                          <li><a href="daftar_morfologi.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Morfologi Tanaman</a></li>
                          <li><a href="Daftar Tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Tanaman</a></li>
                          <li class="active-menu"><a href="Daftar Kalender Tanam.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x"></i></span> Kalender Tanam</a></li>
                          <li><a href="Daftar Peta Lahan.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x"></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-database" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Data Aktivitas Pertanian</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar Aktivitas.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-list-alt fa-stack-1x"></i></span> Aktivitas</a></li>
                        <li><a href="Daftar Aktivitas Spesies.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-list-alt fa-stack-1x"></i></span> Aktivitas Tanaman</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper --><!-- /#sidebar-wrapper -->

        
        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                        <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Pendataan Kalender Tanam</h1>
                                            <a style="font-size:20px" href="Daftar Kalender tanam.php"><span class="glyphicon glyphicon-list"></span> Daftar Kalender Tanam</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                      <div class="section">
                        <div class="container">
                          <div class="row">
                            <div class="col-md-12">
                              <form class="form-horizontal" role="form" action="input_kalender.php" method="post">
                                 <div class="form-group">
                                                  <div class="col-sm-2">
                                                      <label class="control-label">Spesies</label>
                                                  </div>
                                                  <div class="col-sm-4">
                                                      <select class="form-control" name="id_spesies" required>
                                                          <option value="" disabled selected>Pilih Tanaman</option>
                                                          <?php
                                                          //include ("koneksi.php");
                                                          mysql_connect("localhost","root","") or die(mysql_error());
                                                          mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                          $query= mysql_query('SELECT * FROM master_spesies_tanaman;');
                                                          if (mysql_num_rows($query) != 0){ 
                                                              while($brs = mysql_fetch_assoc($query)){ 
                                                                  echo '<option value="'.$brs['ID_Spesies'].'">'.$brs['Nama_Tanaman'].'</option>';
                                                              }
                                                          }

                                                          ?>
                                                      </select>
                                                  </div>
                                              </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Nama Kalender Tanam</label>
                                  </div>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama_kalender" placeholder="Nama Kalender Tanam" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label for="masatanam" class="control-label">Masa Tanam</label>
                                  </div>
                                  <div class="col-sm-1">
                                    <input type="number" value="1" min="1" max="99" name="masa_tanam" class="form-control" required>
                                  </div>Hari</div>
                                  <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Provinsi</label>
                                  </div>
                                  <div class="col-sm-4">
                                                      <select class="form-control" id="prov" name="provinsi" required>
                                                          <option value="" disabled selected>Pilih Provinsi</option>
                                                          <?php
                                                          //include ("koneksi.php");
                                                          mysql_connect("localhost","root","") or die(mysql_error());
                                                          mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                          $query= mysql_query('SELECT * FROM Provinsi;');
                                                          if (mysql_num_rows($query) != 0){ 
                                                              while($brs = mysql_fetch_assoc($query)){ 
                                                                  echo '<option value="'.$brs['Nama_Provinsi'].'">'.$brs['Nama_Provinsi'].'</option>';
                                                              }
                                                          }

                                                          ?>
                                                      </select>
                                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Kabupaten</label>
                                  </div>
                                  <div class="col-sm-4">
                                                      <select class="form-control" id="txtHint" name="kabupaten" required>
                                                          <option value="" disabled selected>Pilih Kabupaten</option>
                                                         
                                                      </select>
                                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Tanggal awal</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <input type="date" name="tanggal_aw" class="form-control" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Tanggal akhir</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <input type="date" name="tanggal_akhir" class="form-control" required>
                                  </div>
                                </div>
                                 <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Persiapan Lahan Awal</label>
                                  </div>
                                  <div class="col-sm-4"> 
                                    <input type="date" name="persiapan_lahan_awal" class="form-control" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Pengolahan lahan &nbsp;awal</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <input type="date" name="pengolahan_lahan_awal"class="form-control" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Pengolahan Lahan Akhir</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <input type="date" name="pengolahan_lahan_akhir" class="form-control" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Masa Penanaman Awal</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <input type="date" name="masa_tanam_awal"class="form-control" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Masa Pemupukan</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <input type="date" name="masa_pemupukan" class="form-control" required>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-2">
                                    <label class="control-label">Masa Panen</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <input type="date" name="masa_panen"class="form-control" required>
                                  </div>
                                </div>
                              </br>
                                <input class="btn btn-primary btn-md" type="submit" value="Tambah">
                                <a class="btn btn-md btn-danger" href="Daftar Kalender Tanam.php"> Batal</a>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div>
                      </div>

          <!--konten-->
                        </div>
                    </div>
                </div>
            </div>
            <script src="css/side_menu.js"></script>
            <!--end wraper-->

    <footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #13780C;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>
  
</body></html>
 <script>

 $(document).ready(function(){
    $("#prov").change(function(){
        showHint($("#prov").val());
    });
});                                             

function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasil = this.responseText.split(",");
                $("#txtHint").empty();
                //$("#txtHint").append("<option>------ Kabupaten---------</option>");
                for (var i = 0; i < hasil.length - 1; ++i) {
                    $("#txtHint").append(hasil[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintkab.php?q=" + str, true);
        xmlhttp.send();
    }
}
</script>