<?php 
$id_spesies = $_GET['spesies'];
$tahun = $_GET['tahun'];

?>
<html>
<head>
 <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>
         <style type="text/css">
${demo.css}
        </style>
        <script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Berdasarkan Kecamatan'
        },
        subtitle: {
            text: 'Per Tanggal <?php echo date('d/m/Y'); ?>'
        },
        xAxis: {
            categories: [
            <?php
                $koneksi = include("koneksi.php"); 
                $hasil = mysqli_query($koneksi,"SELECT master_petani.Kecamatan as nama, sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$id_spesies' and Tahun_Panen='$tahun'  And Kabupaten = '$_GET[Kabupaten]' GROUP BY Kecamatan ORDER BY Kecamatan");
                while($datakecamatan = mysqli_fetch_array($hasil)){
            ?>
                '<?php echo $datakecamatan['nama']; ?>',
            <?php } ?>
            ],
            crosshair: true
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Jumlah Panen (Kuintal)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">: </td>' +
                '<td style="padding:0;font-size:12px">{point.y:.f} Kuintal</td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {

                borderWidth: 0
                , events: {
                    legendItemClick: function () {
                        return false; 
                    }
                }
            }, 
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            //window.open(this.options.url);
                            document.location = this.options.url;
                        }
                    }
                }
            }
        },
        legend: { symbolHeight: '0px' },
        series: [{
            name: 'Nama Kecamatan',
            data: [
            <?php
                $arrcolor = ['#FF0000','#00FF00','#0000FF','#FFFF00','#FF00FF','#00FFFF','#F00F00','#FFCC00'];
                $koneksi = include("koneksi.php"); 
                $hasil = mysqli_query($koneksi,"SELECT master_petani.Kecamatan as nama, sum(trans_hasil_panen.Jumlah_hasil_Panen) AS jumlah FROM master_petani Inner JOIN trans_hasil_panen ON master_petani.ID_User = trans_hasil_panen.ID_Petani where ID_Spesies='$id_spesies' and Tahun_Panen='$tahun' And Kabupaten = '$_GET[Kabupaten]' GROUP BY provinsi, kabupaten, kecamatan ORDER BY provinsi, kabupaten, kecamatan");
                $i=0;
                while($datakecamatan = mysqli_fetch_array($hasil)){
            ?>
                {y:<?php echo $datakecamatan['jumlah']; ?>,url:'Grafik_detail_kelurahan.php?kecamatan=<?php echo $datakecamatan['nama'];?>&spesies=<?php echo $id_spesies;?>&tahun=<?php echo $tahun;?>'}, 
            <?php 
                $i++;
                } ?>
            ]

        }]
    });
});
        </script>
    </head><body>
       <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="index.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="login.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-in fa-stack-1x"></i></span> Masuk</a></li>
                </ul>
                </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="wrapper" style="padding-top: 3.4%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Daftar Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar Tanaman-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Daftar Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x "></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik jenis tanah</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu active">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Aktivitas</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"><span class="fa-stack pull-left" style="margin-top: 1.8%"><i class="fa fa-clock-o fa-stack-1x "></i></span> Aktivitas petani</a></li>
                        <li class="active-menu"><a href="Grafik_aktivitas_pertanian.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-check-square fa-stack-1x "></i></span> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-balance-scale fa-stack-1x "></i></span> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Detail</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper -->

        <!-- Page Content -->

        <!-- /#page-content-wrapper -->
    <!-- /#wrapper -->
    <!-- jQuery -->
    
<body>
    
            <div class="section">
            <div class="container">
                <div class="row" style="margin-left: 2%;">
                    <div class="col-md-12" style="margin-top: -4%;">
                        <?php                                                         
                            mysql_connect("localhost","root","") or die(mysql_error());
                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                            $query= mysql_query("SELECT * FROM master_spesies_tanaman where ID_Spesies = '$_GET[spesies]';");
                            $brsnama = mysql_fetch_assoc($query);
                            ?>
                        <h1>Informasi Panen Tanaman <?php echo $brsnama['Nama_Tanaman']?> <?php echo $tahun?></h1>
                       <div class="col-md-2">
                            <a style="font-size:20px". onclick="window.history.go(-1);"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</a>
                        </div>

                    </div>
                    <div style="margin-bottom:15px;" align="center">
                     
                    </div>
                </div>
            </div>
        </div>
            <div class="container" style="margin-top: -3%;">
                <div class="row">
                    <div class="col-md-12">
                        
                              
                        <script src="css/highcharts.js"></script>
                        <script src="css/highcharts/modules/exporting.js"></script>
                        

                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                        <br/>
                        <br/>
                        <div class="row" style="margin-top: -3%;">
                            <div class="col-md-8">  
                            </div>
                            <div class="col-md-4 text-left">
                                <a class="btn btn-lg btn-info" href="Summary_grafik_detail_kecamatan.php?Kabupaten=<?php echo $_GET['Kabupaten']?>&spesies=<?php echo $id_spesies ?>&tahun=<?php echo $tahun;?>"> Ringkasan</a>
                            </div>
                        </div>
                            

                            <div class="col-md-6">
                           
                        </div>

                </div>
                <script src="css/side_menu.js"></script>

            </div>
</script>
<footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #13780C;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>
</body>
</html>