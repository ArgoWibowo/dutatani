<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "PET")
  {
    header("Location:login.php?pesan=hanya untuk user petani");
  } 
?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>
    <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="index_user_petani.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
         <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="#">Selamat Datang! <span class="fa-stack pull-left" style="margin-top: -1.5%"><i class="fa fa-user-circle-o fa-lg fa-stack-1x"></i></span><strong><?php echo $brsnama['Nama_Petani'] ?></strong></a></li>
        <li><a href="tutup_session.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-out fa-stack-1x"></i></span> Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="wrapper" style="padding-top: 3.6%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu active">
                      <a class="active" href="javascript:;">
                          <i class="fa fa-lg fa-list-alt" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Aktivitas Pertanian</span>
                      </a>
                      <ul class="sub">
                          <li class="active-menu"><a href="daftar_trans_aktivitas.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Aktivitas Tani</a></li>
                          <li><a href="Daftar Hasil Panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-check-square fa-stack-1x"></i></span> Hasil Panen</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar Tanaman-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Tanaman</a></li>
                        <li><a href="Daftar morfologi-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-user.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x "></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Grafik</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Grafikpanenpetani.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Daftar Aktivitas Pertanian</h1>
                                            <form class="navbar-form col-sm-3 col-md-3" role="search" style="position: absolute;margin-top: -43px;margin-left: 690px">
                                                    <div class="input-group">
                                                    <form action="cari_trans_aktivitas.php" method="post">
                                                        <input type="text" class="form-control" placeholder="Cari" name="q" style="width: 250px;height: 33px;background-color: #F1F1F1">
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-default" type="submit" style="background-color: #5CB85C"><i class="glyphicon glyphicon-search" style="height: 19px;color: white"></i></button>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </form>
                                            <div class="col-md-2">
                                                <a style="font-size:14px;width: 145px" href="input_trans_aktivitas_pertanian.php" class="btn btn-md btn-success"><span class="fa-stack fa-md pull-left" style="margin-top: -4%"><i class="fa fa-plus-square fa-stack-1x "></i></span> Tambah Data </a>
                                            </div>
                                            <div class="col-md-2">
                                                <a style="font-size:14px;width: 100px" href="Daftar laporan aktivitas petani.php?petani=<?php echo $id_user?>" class="btn btn-md btn-success"><span class="fa-stack fa-md pull-left" style="margin-top: -6%"><i class="fa fa-print fa-stack-1x"></i></span> Cetak</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <form role="form" action="Daftar_trans_aktivitas_filter.php" enctype="multipart/form-data" method="post" class="container-fluid xyz" style="width: 100%;background-color: #F5F5F5">
                                <div class="form-group">
                                    <div class = "row">
                                        <div class="col-md-4">
                                            <h3><span class="glyphicon glyphicon-sort" style="color: #13A00C"></span> Sorting</h3>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Urutkan berdasarkan :</label>
                                                    <select class="form-control" name="berdasar" style="cursor: pointer">
                                                        <option value ="Tanggal_Mulai">Tanggal Mulai</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Nama_aktivitas">Nama Aktivitas</option>
                                                        <option value ="Deskripsi">Deskripsi</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <label class="control-label">dari</label>
                                                    <select class="form-control" name="urut" style="cursor: pointer">
                                                        <option value ="ASC">A-Z</option>
                                                        <option value ="DESC">Z-A</option>        
                                                    </select>
                                        </div>
                                        <div class ="col-md-2"> 
                                            </br>  
                                            <input class="btn btn-primary btn-md" type="submit" value="Filter"> 
                                        </div>                               
                                    </div>               
                                </div>
                                
                             </form>
                        <body onload="<?php if (@$_GET['pesan'] != "") { echo "alert('$_GET[pesan]');"; } ?>">
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            
                                            <table style="width:110%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>  
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Tanggal Mulai</th>
                                                        <th>Nama Tanaman</th>
                                                        <th>Nama Aktivitas</th>
                                                        <th>Tanggal Selesai</th>
                                                        <th>Deskripsi</th>
                                                        <th>Pilihan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                    $query = mysql_query("SELECT nama_Petani, date_format(Tanggal_Mulai, '%d-%m-%Y') as Tanggal_Mulai ,Nama_Tanaman, Nama_aktivitas, date_format(Tanggal_Selesai, '%d-%m-%Y') as Tanggal_Selesai,  
                                                        trans_aktivitas_pertanian.Deskripsi, ID_aktifitas_petani
                                                     FROM master_aktivitas inner join master_aktifitas_spesies
                                                      on master_aktivitas.ID_Aktivitas = master_aktifitas_spesies.ID_aktivitas 
                                                      Inner JOIN trans_aktivitas_pertanian 
                                                      ON master_aktifitas_spesies.ID_aktifitas_spesies = trans_aktivitas_pertanian.ID_aktifitas_spesies 
                                                      inner join master_petani on trans_aktivitas_pertanian.ID_Petani = master_petani.ID_User 
                                                      inner join master_spesies_tanaman on master_spesies_tanaman.ID_Spesies = master_aktifitas_spesies.ID_Spesies where ID_Petani= '$id_user'");
                                                    $no = 1;
                                                    while($brs = mysql_fetch_assoc($query)){

                                                    ?>
                                               
                                                   <tr>
                                                        <td><?php echo $no++?></td>
                                                        <td><?php echo $brs['Tanggal_Mulai']?></td>
                                                        <td><?php echo $brs['Nama_Tanaman']?></td>
                                                        <td><?php echo $brs['Nama_aktivitas']?></td>
                                                        <td><?php echo $brs['Tanggal_Selesai']?></td>
                                                        <td><?php echo $brs['Deskripsi']?></td>
                                                        <td><a class="btn btn-md btn-warning" href="edit_trans_aktivitas.php?ID_aktifitas_petani=<?php  echo $brs['ID_aktifitas_petani'] ?>">Ubah</a>&nbsp;
                                                        <a class="btn btn-md btn-danger" href="hapus_trans_aktivitas_pertanian.php?ID_aktifitas_petani=<?php  echo $brs['ID_aktifitas_petani'] ?>" onclick="return confirm('Apakah anda akan menghapus?');">Hapus</a></td>
                                                         
                                                    </tr>
                                                    <?php
                                                    }?>   
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <!--end konten-->
   
                     </div>
                </div>
            </div>
         </div>
    <script src="css/side_menu.js"></script>
    <footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #13780C;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>
</body></html>