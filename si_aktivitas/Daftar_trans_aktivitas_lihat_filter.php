<?php
$berdasar = $_POST['berdasar'];
$urut = $_POST['urut'];
?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index_home.php">Sistem Informasi Tanaman Pertanian</a>
    </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="login.php">Masuk</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                               
                            </ul>
                            <ul>
                                <div style="margin-bottom:15px;" align="right">
                                    <form action="cari_trans_aktivitas_lihat.php" method="post">
                                        <input type="text" name="input_cari_trans_aktivitas" placeholder="Cari" style="width:250px;color:black;" />
                                        <input type="submit" name="cari" value="Cari" class="btn-md btn-primary" style="padding:3px;" margin="6px;" width="50px;"  />
                                    </form>
                                </div>
                            </ul>
                </div><!-- bs-example-navbar-collapse-1 -->
          </div>
    </nav>

 <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Daftar Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-lihat.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafik_jenis_tanaman.php"></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"></span> Grafik jenis tanah</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Informasi Aktivitas</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"></span> Grafik Hasil Panen Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->


<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Daftar Aktivitas Pertanian</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <form role="form" action="Daftar_trans_aktivitas_lihat_filter.php" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <div class = "row">
                                        <div class="col-md-4">
                                            <h3>Sort</h3>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Urutkan berdasarkan :</label>
                                                    <select class="form-control" name="berdasar">
                                                        <option value ="nama_Petani">Nama Petani</option>
                                                        <option value ="Tanggal_Mulai">Tanggal Mulai</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Nama_aktivitas">Nama Aktivitas</option>
                                                        <option value ="Deskripsi">Deskripsi</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <label class="control-label">dari</label>
                                                    <select class="form-control" name="urut">
                                                        <option value ="ASC">A-Z</option>
                                                        <option value ="DESC">Z-A</option>        
                                                    </select>
                                        </div>
                                        <div class ="col-md-4"> 
                                            </br>
                                          <div class ="row">   
                                            <div class ="col-md-2">  
                                            <input class="btn btn-primary btn-md" type="submit" value="Sort">
                                            </div>
                                            <div class ="col-md-2">
                                            <a class="btn btn-md btn-danger" href="Daftar_trans_aktivitas_lihat.php">Hapus</a>
                                            </div>
                                           </div>                       
                                        </div>                                       
                                    </div>               
                                </div> 
                             </form>
                             <form role="form" action="Daftar_trans_aktivitas_lihat_filter_2.php" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <div class = "row">
                                        <div class="col-md-4">
                                            <h3>Filter</h3>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Informasi berdasarkan :</label>
                                                    <select class="form-control" name="filter1">
                                                        <option value ="nama_Petani">Nama Petani</option>
                                                        <option value ="Tanggal_Mulai">Tanggal Mulai</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Nama_aktivitas">Nama Aktivitas</option>
                                                        <option value ="Deskripsi">Deskripsi</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Informasi</label>
                                                    <input class="form-control" name="isi1" placeholder=" " type="text" required>
                                                </div>
                                        </div>
                                                    
                                    </div>               
                                </div>
                                <div class="form-group">  
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Informasi berdasarkan :</label>
                                                    <select class="form-control" name="filter2">
                                                        <option value ="nama_Petani">Nama Petani</option>
                                                        <option value ="Tanggal_Mulai">Tanggal Mulai</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Nama_aktivitas">Nama Aktivitas</option>
                                                        <option value ="Deskripsi">Deskripsi</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Informasi</label>
                                                    <input class="form-control" name="isi2" placeholder=" " type="text">
                                                </div>
                                        </div>
                                                    
                                    </div>               
                                </div>
                                <div class="form-group">    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Informasi berdasarkan :</label>
                                                    <select class="form-control" name="filter3">
                                                        <option value ="nama_Petani">Nama Petani</option>
                                                        <option value ="Tanggal_Mulai">Tanggal Mulai</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Nama_aktivitas">Nama Aktivitas</option>
                                                        <option value ="Deskripsi">Deskripsi</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Informasi</label>
                                                    <input class="form-control" name="isi3" placeholder=" " type="text">
                                                </div>
                                        </div>
                                        <div class ="col-md-4">
                                            <div class = "row">
                                                <br>
                                                <div class ="col-md-2">  
                                                    <input class="btn btn-primary btn-md" type="submit" value="Filter"> 
                                                </div> 
                                            </div>             
                                        </div>            
                                    </div>               
                                </div>
                                       
                             </form>
                        <body>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                             <table style="width:1000px;" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Nama Petani</th>
                                                        <th>Tanggal Mulai</th>
                                                        <th>Nama Tanaman</th>
                                                        <th>Nama Aktivitas</th>
                                                        <th>Tanggal Selesai</th>
                                                        <th>Deskripsi</th>
                                                        
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                    $query = mysql_query("SELECT nama_Petani, Tanggal_Mulai,Nama_Tanaman, Nama_aktivitas, Tanggal_Selesai,  
                                                        trans_aktivitas_pertanian.Deskripsi
                                                     FROM master_aktivitas inner join master_aktifitas_spesies
                                                      on master_aktivitas.ID_Aktivitas = master_aktifitas_spesies.ID_aktivitas 
                                                      Inner JOIN trans_aktivitas_pertanian 
                                                      ON master_aktifitas_spesies.ID_aktifitas_spesies = trans_aktivitas_pertanian.ID_aktifitas_spesies 
                                                      inner join master_petani on trans_aktivitas_pertanian.ID_Petani = master_petani.ID_User 
                                                      inner join master_spesies_tanaman on master_spesies_tanaman.ID_Spesies = master_aktifitas_spesies.ID_Spesies order by $berdasar $urut");
                                                    $no = 1;
                                                    while($brs = mysql_fetch_assoc($query)){

                                                    ?>
                                                
                                                   <tr>
                                                        <td><?php echo $no++?></td>
                                                        <td><?php echo $brs['nama_Petani']?></td>
                                                        <td><?php echo $brs['Tanggal_Mulai']?></td>
                                                        <td><?php echo $brs['Nama_Tanaman']?></td>
                                                        <td><?php echo $brs['Nama_aktivitas']?></td>
                                                        <td><?php echo $brs['Tanggal_Selesai']?></td>
                                                        <td><?php echo $brs['Deskripsi']?></td>
                                                    </tr>
                                                    <?php
                                                    }?>   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <!--end konten-->
   
                     </div>
                </div>
            </div>
         </div>
<script src="css/side_menu.js"></script>
<footer class="navbar navbar-default navbar-fixed-bottom">
  <div class="container-fluid">
    <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
  </div>
</footer>

</body></html>