<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head><body>
    <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="index.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="login.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-in fa-stack-1x"></i></span> Masuk</a></li>
                </ul>
                </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="wrapper" style="padding-top: 3.4%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Daftar Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar Tanaman-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Daftar Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x "></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik jenis tanah</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu active">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Aktivitas</span>
                      </a>
                      <ul class="sub">
                        <li class="active-menu"><a href="Daftar_trans_aktivitas_lihat.php"><span class="fa-stack pull-left" style="margin-top: 1.8%"><i class="fa fa-clock-o fa-stack-1x "></i></span> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-check-square fa-stack-1x "></i></span> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-balance-scale fa-stack-1x "></i></span> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Detail</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Daftar Aktivitas Pertanian</h1>
                                            <div class="col-md-2">
                                                <a style="font-size:14px;width: 100px" href="Daftar laporan aktivitas petani lihat.php" class="btn btn-md btn-success"><span class="fa-stack fa-md pull-left" style="margin-top: -6%"><i class="fa fa-print fa-stack-1x"></i></span> Cetak</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                             <form role="form" action="Daftar_trans_aktivitas_lihat_filter_2.php" enctype="multipart/form-data" method="post" class="container-fluid xyz" style="width: 100%;background-color: #F5F5F5">
                                <div class="form-group">
                                    <div class = "row">
                                        <div class="col-md-4">
                                            <h3><span class="glyphicon glyphicon-filter" style="color: #13A00C"></span>Filter</h3>
                                        </div>
                                    </div>
                                <div class="form-group">    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Informasi berdasarkan :</label>
                                                    <select class="form-control" name="filter3">
                                                        <option value ="nama_Petani">Nama Petani</option>
                                                        <option value ="Tanggal_Mulai">Tanggal Mulai</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Nama_aktivitas">Nama Aktivitas</option>
                                                        <option value ="Deskripsi">Deskripsi</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Informasi</label>
                                                    <input class="form-control" name="isi3" placeholder=" " type="text">
                                                </div>
                                        </div>
                                        <div class ="col-md-4">
                                            <div class = "row">
                                                <br>
                                                <div class ="col-md-2">  
                                                    <input class="btn btn-primary btn-md" type="submit" value="Filter">
                                                </div> 
                                            </div>             
                                        </div>            
                                    </div>               
                                </div>
                                       
                             </form>
                        <body>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                           
                                            <table style="width:1000px;" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Petani</th>
                                                        <th>Tanggal Mulai</th>
                                                        <th>Nama Tanaman</th>
                                                        <th>Nama Aktivitas</th>
                                                        <th>Tanggal Selesai</th>
                                                        <th>Deskripsi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                    $query = mysql_query("SELECT nama_Petani, Tanggal_Mulai,Nama_Tanaman, Nama_aktivitas, Tanggal_Selesai,  
                                                        trans_aktivitas_pertanian.Deskripsi
                                                     FROM master_aktivitas inner join master_aktifitas_spesies
                                                      on master_aktivitas.ID_Aktivitas = master_aktifitas_spesies.ID_aktivitas 
                                                      Inner JOIN trans_aktivitas_pertanian 
                                                      ON master_aktifitas_spesies.ID_aktifitas_spesies = trans_aktivitas_pertanian.ID_aktifitas_spesies 
                                                      inner join master_petani on trans_aktivitas_pertanian.ID_Petani = master_petani.ID_User 
                                                      inner join master_spesies_tanaman on master_spesies_tanaman.ID_Spesies = master_aktifitas_spesies.ID_Spesies");
                                                    $no = 1;
                                                    while($brs = mysql_fetch_assoc($query)){

                                                    ?>
                                                
                                                   <tr>
                                                        <td><?php echo $no++?></td>
                                                        <td><?php echo $brs['nama_Petani']?></td>
                                                        <td><?php echo $brs['Tanggal_Mulai']?></td>
                                                        <td><?php echo $brs['Nama_Tanaman']?></td>
                                                        <td><?php echo $brs['Nama_aktivitas']?></td>
                                                        <td><?php echo $brs['Tanggal_Selesai']?></td>
                                                        <td><?php echo $brs['Deskripsi']?></td>    
                                                    </tr>
                                                    <?php
                                                    }?>   
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <!--end konten-->
   
                     </div>
                </div>
            </div>
         </div>
<script src="css/side_menu.js"></script>
<footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #13780C;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>
</body></html>