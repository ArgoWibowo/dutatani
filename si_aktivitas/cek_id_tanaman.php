<?php
  
  $host="localhost";
  $user="root";
  $pass="";
  $dbname="iais_ukdw";
  
  $dbcon = new PDO("mysql:host={$host};dbname={$dbname}",$user,$pass);
  
  if($_POST) 
  {
      $id_tanaman     = strip_tags($_POST['id_tanaman']);
      
   $stmt=$dbcon->prepare("SELECT ID_Spesies FROM master_spesies_tanaman WHERE ID_Spesies=:id_tanaman");
   $stmt->execute(array(':id_tanaman'=>$id_tanaman));
   $count=$stmt->rowCount();
      
   if($count>0)
   {
    echo "<span style='color:red;'>Maaf ID Spesies Sudah Terpakai !!!</span>";
   }
   else
   {
    echo "<span style='color:green;'>ID Spesies Tersedia</span>";
   }
  }
?>