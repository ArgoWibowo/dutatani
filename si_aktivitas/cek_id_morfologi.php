<?php
  
  $host="localhost";
  $user="root";
  $pass="";
  $dbname="iais_ukdw";
  
  $dbcon = new PDO("mysql:host={$host};dbname={$dbname}",$user,$pass);
  
  if($_POST) 
  {
      $id_morfologi     = strip_tags($_POST['id_morfologi']);
      
   $stmt=$dbcon->prepare("SELECT ID_Morfologi FROM master_morf_tanaman WHERE ID_Morfologi=:id_morfologi");
   $stmt->execute(array(':id_morfologi'=>$id_morfologi));
   $count=$stmt->rowCount();
      
   if($count>0)
   {
    echo "<span style='color:red;'>Maaf ID Morfologi Sudah Terpakai !!!</span>";
   }
   else
   {
    echo "<span style='color:green;'>ID Morfologi Tersedia</span>";
   }
  }
?>