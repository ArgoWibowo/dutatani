<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "ADT")
  {
    header("Location:login.php?pesan=hanya untuk admin");
  } 
?>
<?php
$id_lahan = $_GET['id_lahan'];
mysql_connect("localhost","root","") or die(mysql_error());
mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
$query = mysql_query("SELECT * FROM master_peta_lahan where ID_Lahan = '$id_lahan'");

while($brs = mysql_fetch_assoc($query)){
?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head><body>
<nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="indexadmin.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
         <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="#">Selamat Datang! <span class="fa-stack pull-left" style="margin-top: -1.5%"><i class="fa fa-user-circle-o fa-lg fa-stack-1x"></i></span><strong><?php echo $brsnama['Nama_Petani'] ?></strong></a></li>
        <li><a href="tutup_session.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-out fa-stack-1x"></i></span> Keluar</a></li>
                </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <div id="wrapper" style="padding-top: 3.6%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu active">
                      <a class="active" href="javascript:;">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Tanaman</span>
                      </a>
                      <ul class="sub">
                          <li><a href="daftar_morfologi.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Morfologi Tanaman</a></li>
                          <li><a href="Daftar Tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Tanaman</a></li>
                          <li><a href="Daftar Kalender Tanam.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x"></i></span> Kalender Tanam</a></li>
                          <li class="active-menu"><a href="Daftar Peta Lahan.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x"></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-database" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Data Aktivitas Pertanian</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar Aktivitas.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-list-alt fa-stack-1x"></i></span> Aktivitas</a></li>
                        <li><a href="Daftar Aktivitas Spesies.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-list-alt fa-stack-1x"></i></span> Aktivitas Tanaman</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper --><!-- /#sidebar-wrapper -->

        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                        <div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1>Peta Lahan</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal" role="form" action="update_peta_lahan.php" method="post">
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">ID Lahan</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" placeholder="ID Lahan" value="<?php echo $brs['ID_Lahan']?>" disabled>
                                                     <input type="hidden" class="form-control" name="id_lahan" placeholder="ID Lahan" value="<?php echo $brs['ID_Lahan']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label  class="control-label">Koordinat X</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="koordinat_x" placeholder="Koordinat X" value="<?php echo $brs['Koordinat_X']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Koordinat Y</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="koordinat_y" placeholder="Koordinat Y" value="<?php echo $brs['Koordinat_Y']?>">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Provinsi</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <select  class="form-control" name="provinsi" id="prov" value="<?php echo $brs['Provinsi']?>">
                                                      <?php
                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                        $query= mysql_query('SELECT * FROM provinsi;');
                                                        if(mysql_num_rows($query) != 0){
                                                          while($brsprov = mysql_fetch_assoc($query)){
                                                            if($brs['Provinsi']== $brsprov['Nama_Provinsi'])
                                                             {
                                                              echo '<option value="'.$brsprov['Nama_Provinsi'].'" selected>'.$brsprov['Nama_Provinsi'].'</option>';
                                                             } 
                                                             else
                                                             {
                                                              echo '<option value="'.$brsprov['Nama_Provinsi'].'">'.$brsprov['Nama_Provinsi'].'</option>';
                                                             }
                                                          }
                                                        }

                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Kabupaten</label>
                                                </div>
                                                 <div class="col-sm-8">
                                                    <select  class="form-control" name="kabupaten" id="txtHint" value="<?php echo $brs['Kabupaten']?>">
                                                      <?php
                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                        $query= mysql_query("SELECT * FROM kabupaten where Nama_Provinsi = '$brs[Provinsi]';");
                                                        if(mysql_num_rows($query) != 0){
                                                          while($brskab = mysql_fetch_assoc($query)){
                                                            if($brs['Kabupaten']== $brskab['Nama_Kabupaten'])
                                                             {
                                                              echo '<option value="'.$brskab['Nama_Kabupaten'].'" selected>'.$brskab['Nama_Kabupaten'].'</option>';
                                                             } 
                                                             else
                                                             {
                                                              echo '<option value="'.$brskab['Nama_Kabupaten'].'">'.$brskab['Nama_Kabupaten'].'</option>';
                                                             }
                                                          }
                                                        }

                                                      ?>
                                                    </select>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Kecamatan</label>
                                                </div>
                                                 <div class="col-sm-8">
                                                    <select  class="form-control" name="kecamatan" id="txtHintkec" value="<?php echo $brs['Kecamatan']?>">
                                                      <?php
                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                        $query= mysql_query("SELECT * FROM kecamatan where Nama_Kabupaten = '$brs[Kabupaten]';");
                                                        if(mysql_num_rows($query) != 0){
                                                          while($brskec = mysql_fetch_assoc($query)){
                                                            if($brs['Kecamatan']== $brskec['Nama_Kecamatan'])
                                                             {
                                                              echo '<option value="'.$brskec['Nama_Kecamatan'].'" selected>'.$brskec['Nama_Kecamatan'].'</option>';
                                                             } 
                                                             else
                                                             {
                                                              echo '<option value="'.$brskec['Nama_Kecamatan'].'">'.$brskec['Nama_Kecamatan'].'</option>';
                                                             }
                                                          }
                                                        }

                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Desa</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <select  class="form-control" name="desa" id="txtHintkel"  value="<?php echo $brs['Desa']?>">
                                                      <?php
                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                        $query= mysql_query("SELECT * FROM kelurahan_desa where Nama_Kecamatan = '$brs[Kecamatan]';");
                                                        if(mysql_num_rows($query) != 0){
                                                          while($brskel = mysql_fetch_assoc($query)){
                                                            if($brs['Desa']== $brskel['Nama_Desa'])
                                                             {
                                                              echo '<option value="'.$brskel['Nama_Desa'].'" selected>'.$brskel['Nama_Desa'].'</option>';
                                                             } 
                                                             else
                                                             {
                                                              echo '<option value="'.$brskel['Nama_Desa'].'">'.$brskel['Nama_Desa'].'</option>';
                                                             }
                                                          }
                                                        }

                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                           
                                          
                                           
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Bulan</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name="bulan" value="<?php echo $brs['bulan']?>">
                                                         <?php
                                                             if($brs["bulan"] == "1"){
                                                                echo "<option value="."1"." selected>Januari</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."1".">Januari</option>";
                                                              }

                                                              if($brs["bulan"] == "2"){
                                                                echo "<option value="."2"." selected>Februari</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."2".">Februari</option>";
                                                              }

                                                              if($brs["bulan"] == "3"){
                                                                echo "<option value="."3"." selected>Maret</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."3".">Maret</option>";
                                                              }

                                                              if($brs["bulan"] == "4"){
                                                                echo "<option value="."4"." selected>April</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."4".">April</option>";
                                                              }

                                                              if($brs["bulan"] == "5"){
                                                                echo "<option value="."5"." selected>Mei</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."5".">Mei</option>";
                                                              }

                                                               if($brs["bulan"] == "6"){
                                                                echo "<option value="."6"." selected>Juni</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."6".">Juni</option>";
                                                              }

                                                               if($brs["bulan"] == "7"){
                                                                echo "<option value="."7"." selected>Juli</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."7".">Juli</option>";
                                                              }

                                                              if($brs["bulan"] == "7"){
                                                                echo "<option value="."7"." selected>Juli</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."7".">Juli</option>";
                                                              }

                                                              if($brs["bulan"] == "8"){
                                                                echo "<option value="."8"." selected>Agustus</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."8".">Agustus</option>";
                                                              }

                                                              if($brs["bulan"] == "9"){
                                                                echo "<option value="."9"." selected>September</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."9".">September</option>";
                                                              }

                                                              if($brs["bulan"] == "10"){
                                                                echo "<option value="."10"." selected>Oktober</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."10".">Oktober</option>";
                                                              }

                                                              if($brs["bulan"] == "11"){
                                                                echo "<option value="."11"." selected>November</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."11".">November</option>";
                                                              }

                                                              if($brs["bulan"] == "12"){
                                                                echo "<option value="."12"." selected>Desember</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."12".">Desember</option>";
                                                              }
                                                             ?>
                                                     </select>
                                                </div>
                                            </div><div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Bulan Akhir</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" name="bulan_akhir" value="<?php echo $brs['bulan_akhir']?>">
                                                         <?php
                                                             if($brs["bulan_akhir"] == "1"){
                                                                echo "<option value="."1"." selected>Januari</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."1".">Januari</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "2"){
                                                                echo "<option value="."2"." selected>Februari</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."2".">Februari</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "3"){
                                                                echo "<option value="."3"." selected>Maret</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."3".">Maret</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "4"){
                                                                echo "<option value="."4"." selected>April</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."4".">April</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "5"){
                                                                echo "<option value="."5"." selected>Mei</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."5".">Mei</option>";
                                                              }

                                                               if($brs["bulan_akhir"] == "6"){
                                                                echo "<option value="."6"." selected>Juni</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."6".">Juni</option>";
                                                              }

                                                               if($brs["bulan_akhir"] == "7"){
                                                                echo "<option value="."7"." selected>Juli</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."7".">Juli</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "7"){
                                                                echo "<option value="."7"." selected>Juli</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."7".">Juli</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "8"){
                                                                echo "<option value="."8"." selected>Agustus</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."8".">Agustus</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "9"){
                                                                echo "<option value="."9"." selected>September</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."9".">September</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "10"){
                                                                echo "<option value="."10"." selected>Oktober</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."10".">Oktober</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "11"){
                                                                echo "<option value="."11"." selected>November</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."11".">November</option>";
                                                              }

                                                              if($brs["bulan_akhir"] == "12"){
                                                                echo "<option value="."12"." selected>Desember</option>";
                                                              }else
                                                              {
                                                                echo "<option value="."12".">Desember</option>";
                                                              }
                                                             ?>
                                                     </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Spesies</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="col-sm-8">
                                                    <select  class="form-control" name="id_spesies"  value="<?php echo $brs['ID_Spesies']?>">
                                                      <?php
                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                        $query= mysql_query("SELECT * FROM master_spesies_tanaman;");
                                                        if(mysql_num_rows($query) != 0){
                                                          while($brstnm = mysql_fetch_assoc($query)){
                                                            if($brs['ID_Spesies']== $brstnm['ID_Spesies'])
                                                             {
                                                              echo '<option value="'.$brstnm['ID_Spesies'].'" selected>'.$brstnm['Nama_Tanaman'].'</option>';
                                                             } 
                                                             else
                                                             {
                                                              echo '<option value="'.$brstnm['ID_Spesies'].'">'.$brstnm['Nama_Tanaman'].'</option>';
                                                             }
                                                          }
                                                        }

                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                           </div> 
                                          <br />
                                            <input class="btn btn-primary btn-md" type="submit" value="Ubah" style="margin-left: 18%">
                                            <a href="Daftar Peta Lahan.php?" class="btn btn-danger btn-md">Batal</a>
                                        </form>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="css/side_menu.js"></script>
        <!--end wraper-->
        <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>
    

</body></html>
<script>

 $(document).ready(function(){
    $("#prov").change(function(){
        showHint($("#prov").val());
    });
});                                             

function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasil = this.responseText.split(",");
                $("#txtHint").empty();
                $("#txtHint").append("<option>------ Kabupaten---------</option>");
                for (var i = 0; i < hasil.length - 1; ++i) {
                    $("#txtHint").append(hasil[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintkab.php?q=" + str, true);
        xmlhttp.send();
    }
}
</script>
<script>

 $(document).ready(function(){
    $("#txtHint").change(function(){
        showHintkec($("#txtHint").val());
    });
});                                             

function showHintkec(strkec) {
    if (strkec.length == 0) { 
        document.getElementById("txtHintkec").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasilkec = this.responseText.split(",");
                $("#txtHintkec").empty();
                $("#txtHintkec").append("<option>------ Kecamatan---------</option>");
                for (var i = 0; i < hasilkec.length - 1; ++i) {
                    $("#txtHintkec").append(hasilkec[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintkec.php?qkec=" + strkec, true);
        xmlhttp.send();
    }
}
</script>

<script>

 $(document).ready(function(){
    $("#txtHintkec").change(function(){
        showHintkel($("#txtHintkec").val());
    });
});                                             

function showHintkel(strkel) {
    if (strkel.length == 0) { 
        document.getElementById("txtHintkel").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasilkel = this.responseText.split(",");
                $("#txtHintkel").empty();
                $("#txtHintkel").append("<option>------ Kelurahan/Desa---------</option>");
                for (var i = 0; i < hasilkel.length - 1; ++i) {
                    $("#txtHintkel").append(hasilkel[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintkel.php?qkel=" + strkel, true);
        xmlhttp.send();
    }
}
</script>