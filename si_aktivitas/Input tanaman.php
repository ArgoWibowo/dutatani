<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "ADT")
  {
    header("Location:login.php?pesan=hanya untuk admin");
  } 
?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar-tanaman.css" rel="stylesheet">
        <script type="text/javascript" src="jquery-1.11.3-jquery.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function()
        {    
         $("#id_tanaman").keyup(function()
         {  
          var id_tanaman = $(this).val(); 
          
          if(id_tanaman.length > 2)
          {  
           $("#result").html('checking...');
           $.ajax({
    
            type : 'POST',
            url  : 'cek_id_tanaman.php',
            data : $(this).serialize(),
            success : function(data)
                {
                    //alert(data); 
                    $("#result").html(data);
                   }
            });
            return false;
           
          }
          else
          {
           $("#result").html('');
          }
         });
         
        });
</script>
    </head><body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Sistem Informasi Tanaman Pertanian</a>
                </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="#">HAI! <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li><a href="tutup_session.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>
    
     <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Tanaman</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar morfologi.php">Morfologi Tanaman</a></li>
                        <li><a href="Daftar Tanaman.php">Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam.php">Kalender Tanam</a></li>
                        <li><a href="Daftar Peta Lahan.php">Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span>Data Aktivitas Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Aktivitas.php">Aktivitas</a></li>
                        <li><a href="Daftar Aktivitas Spesies.php">Aktivitas Tanaman</a></li>
                
                    </ul>
                </li>
               
            </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Pendataan Tanaman</h1>
                                            <a style="font-size:20px" href="Daftar Tanaman.php"><span class="glyphicon glyphicon-list"></span> Daftar Tanaman</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <form role="form" action="input_tanaman.php" enctype="multipart/form-data" method="post">
                                                <div class="form-group" >
                                                    <label class="control-label">Id Tanaman*</label>
                                                    <input class="form-control" name="id_tanaman" id="id_tanaman" placeholder="ID Tanaman" type="text" maxlength="10" required> 
                                                    <span id="result"></span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Nama Tanaman</label>
                                                    <input class="form-control" name="nama_tanaman" placeholder="Nama Tanaman" type="text" required>
                                                </div>
                                                <div class="form-group">
                                                        <label class="control-label">Jenis Tanaman</label>
                                                            <select class="form-control" name="jenis_tanaman">
                                                                <option>Persawahan</option>
                                                                <option>Perkebunan</option>
                                                                <option>Kehutanan</option>
                                                            </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Habitat</label>
                                                    <input class="form-control" name="habitat" type="text" placeholder="Habitat" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Nama Latin Tanaman</label>
                                                    <input class="form-control" name="nama_latin"type="text" placeholder="Nama Latin Tanaman" required>
                                                </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Masa Tanam(Hari)</label>
                                                        <input class="form-control" name="masa_tanam" type="number" min="1" max="365" value="1" required>
                                                    </div>
                                                <div class="form-group">
                                                    <label class="control-label">Akar</label>
                                                    <textarea class="form-control" rows="3" name="akar" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Batang</label>
                                                    <textarea class="form-control" rows="3" name="batang" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Daun</label>
                                                    <textarea class="form-control" rows="3" name="daun" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Buah</label>
                                                    <textarea class="form-control" rows="3" name="buah" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Biji</label>
                                                    <textarea class="form-control" rows="3" name="biji" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Perkembangbiakan</label>
                                                    <textarea class="form-control" rows="3" name="perkembangbiakan" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Foto Tanaman 1</label>
                                                    <input type="file"  name="foto1_tanaman" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Foto Tanaman 2</label>
                                                    <input type="file"  name="foto2_tanaman" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Iklim</label>
                                                    <input class="form-control" name="iklim" type="text" placeholder="Iklim" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Jenis Tanah</label>
                                                    <div class="radio">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="jenis_tanah" id="optionsRadios1" value="Tanah Basah" checked="">Tanah Basah</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="jenis_tanah" id="optionsRadios2" value="Tanah kering" checked="">Tanah Kering</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Kelembaban</label>
                                                    <input class="form-control" name="kelembaban" type="text" placeholder="Kelembaban" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Morfologi</label>
                                                        <select class="form-control" name="id_morfologi" required>
                                                                    <option value="" disabled selected>Pilih Morofologi</option>
                                                                    <?php
                                                                    //include ("koneksi.php");
                                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                                    $query= mysql_query('SELECT * FROM master_morf_tanaman ;');
                                                                    if (mysql_num_rows($query) != 0){ 
                                                                        while($brs = mysql_fetch_assoc($query)){ 
                                                                            echo '<option value="'.$brs['ID_Morfologi'].'">'.$brs['Nama_Morfologi_Tanaman'].'</option>';
                                                                        }
                                                                    }

                                                                    ?>
                                                        </select>
                                                </div>

                                                <br />
                                                <input class="btn btn-primary btn-lg" type="submit" value="submit">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--konten-->
                    </div>
                </div>
            </div>
        </div>
        <script src="css/side_menu.js"></script>
        <!--end wraper-->
    <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>
</body></html>