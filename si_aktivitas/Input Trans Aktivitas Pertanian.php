<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "PET")
  {
    header("Location:login.php?pesan=hanya untuk user petani");
  } 
?>
<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
    </head><body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index_user_petani.php">Sistem Informasi Tanaman Pertanian</a>
                </div>  
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="index_user_petani.php">HAI! <?php echo $brsnama['Nama_Petani'] ?></a></li>
        <li><a href="tutup_session.php">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> Menu Tani <?php echo $brsnama['Nama_Petani'] ?></a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                               
                            </ul>
                            <ul>
                </div><!-- bs-example-navbar-collapse-1 -->
    </nav>
   <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Aktivitas Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Trans Aktivitas.php">Aktivitas Tani</a></li>
                        <li><a href="Daftar Hasil Panen.php">Hasil Panen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x  "></i></span>Informasi Tanaman</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman-user.php"> Tanaman</a></li>
                        <li><a href="Daftar morfologi-user.php"> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-user.php"> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-user.php"> Peta Lahan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Grafik</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Grafikpanenpetani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-server fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Aktivitas Pertanian</h1>
                                            <a style="font-size:20px" href="Daftar Trans Aktivitas.php"><span class="glyphicon glyphicon-list"></span> Daftar Aktivitas Pertanian</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form-horizontal" role="form" action="input_Trans_aktivitas.php" method="post">
                                                 <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label">Nama Petani</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" value="<?php echo $brsnama['Nama_Petani']?>"  disabled>
                                                        <input type="hidden" class="form-control" name="id_petani" value="<?php echo $id_user?>">
                                                    </div>
                                                </div>
                                               <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label">Tahun Aktivitas</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="number" min="2000" max="3000" class="form-control" value="2016" name="tahun_aktivitas" placeholder="Tahun Aktivitas" required>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Spesies</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" id="id_spesies" name="id_spesies" required>
                                                        <option value="" disabled selected>Pilih Tanaman</option>
                                                        <?php
                                                        //include ("koneksi.php");
                                                        mysql_connect("localhost","root","") or die(mysql_error());
                                                        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                        $query= mysql_query('SELECT * FROM master_spesies_tanaman;');
                                                        if (mysql_num_rows($query) != 0){ 
                                                            while($brs = mysql_fetch_assoc($query)){ 
                                                                echo '<option value="'.$brs['ID_Spesies'].'">'.$brs['Nama_Tanaman'].'</option>';
                                                            }
                                                        }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Aktivitas</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" id="txtHint" name="" required>
                                                        <option value="" disabled selected>Pilih Aktivitas</option>
        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label">Periode</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" id="id_periode" name="" required>
                                                        <option value="" disabled selected>Pilih Periode</option>
        
                                                    </select>
                                                </div>
                                            </div>


                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label">Aktivitas Spesies</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="id_tani"  name="id_aktivitas_spesies" readonly="readonly">
                                                    </div>
                                                </div>
                                              
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label">Tanggal Mulai</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="date" class="form-control" name="tanggal_mulai" placeholder="Tanggal Mulai" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label">Tanggal Selesai</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="date" class="form-control" name="tanggal_selesai" placeholder="Tanggal Selesai" required>
                                                    </div>
                                                </div>
                                                
                                                   <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="control-label">Deskripsi</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <textarea class="form-control" rows="4" name="deskripsi" required></textarea>
                                                    </div>
                                                </div>
                                                <br />
                                                <input class="btn btn-primary btn-lg" type="submit" value="submit">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                    </div>
                                </div>
                            </div>
                        <!--konten-->
                    </div>
                </div>
            </div>
        </div>
        <script src="css/side_menu.js"></script>
        <!--end wraper-->
    <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>

</body>
</html>

<script>

var spesies = "";
var aktivitas = "";
var periode ="";                                

function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasil = this.responseText.split(",");
                $("#txtHint").empty();
                $("#txtHint").append("<option value=''>------ aktivitas---------</option>");
                for (var i = 0; i < hasil.length - 1; ++i) {
                    $("#txtHint").append(hasil[i]);
                };
                $("#id_periode").empty();
                $("#id_periode").append("<option value=''>------ periode---------</option>");
            }
        };
        xmlhttp.open("GET", "gethintaktivitas.php?q=" + str, true);
        xmlhttp.send();
    }
}

 $(document).ready(function(){
    $("#id_spesies").change(function(){
        showHint($("#id_spesies").val());
        $("#txtHint").val("");
        aktivitas = "";
        spesies = $("#id_spesies").val();
        showPeriode();
    });

    $("#txtHint").change(function(){
        aktivitas = $("#txtHint").val();
        showPeriode();
    });

    $("#id_periode").change(function(){
        periode = $("#id_periode").val();
        showId();
    });
});

function showPeriode()
{
    $("#id_periode").empty();
                $("#id_periode").append("<option value=''>------ Periode---------</option>");
    if(spesies != "" && aktivitas != "")
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasil = this.responseText.split(",");
                $("#id_periode").empty();
                $("#id_periode").append("<option value=''>------ Periode---------</option>");
                for (var i = 0; i < hasil.length - 1; ++i) {
                    $("#id_periode").append(hasil[i]);
                };
            }
        };
        xmlhttp.open("GET", "gethintaktpertanian.php?q1=" + spesies + "&q2=" + aktivitas, true);
        xmlhttp.send();
    }
}

function showId()
{
    if(spesies != "" && aktivitas != "" && periode!="")
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("txtHint").innerHTML = this.responseText;
                var hasil = this.responseText.split(",");
                //$("#id_tani").val("");
                $("#id_tani").val(hasil);
            }
        };
        xmlhttp.open("GET", "gethintidtani.php?q1=" + spesies + "&q2=" + aktivitas + "&q3=" + periode, true);
        xmlhttp.send();
    }
}

</script>






