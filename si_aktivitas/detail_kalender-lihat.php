<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>

    <body>
       <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="index.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="login.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-in fa-stack-1x"></i></span> Masuk</a></li>
                </ul>
                </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="wrapper" style="padding-top: 3.6%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu active">
                      <a class="active" href="javascript:;">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Daftar Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li"><a href="Daftar Tanaman-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li class="active-menu"><a href="Daftar Kalender Tanam-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x "></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik jenis tanah</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Aktivitas</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"><span class="fa-stack pull-left" style="margin-top: 1.8%"><i class="fa fa-clock-o fa-stack-1x "></i></span> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-check-square fa-stack-1x "></i></span> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-balance-scale fa-stack-1x "></i></span> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Detail</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <?php
                            $id_spesies = $_GET['id'];
                            $masa_tanam = $_GET['masa_tanam'];
                            $kabupaten = $_GET['kabupaten'];
                            mysql_connect("localhost","root","") or die(mysql_error());
                            mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                            $query = mysql_query("SELECT  Trans_kalender_tanam.ID_Spesies as ID_Spesies, Nama_Kalender, Masa_Tanam, Kabupaten, Provinsi, date_format(Tanggal_Awal, '%d-%m-%Y') as Tanggal_Awal, date_format(Tanggal_Akhir, '%d-%m-%Y') as Tanggal_Akhir, date_format(Pengolahan_Lahan_Awal, '%d-%m-%Y') as Pengolahan_Lahan_Awal, date_format(Pengolahan_Lahan_Akhir, '%d-%m-%Y') as Pengolahan_Lahan_Akhir, date_format(Persiapan_Lahan_Awal, '%d-%m-%Y') as Persiapan_Lahan_Awal, date_format(Masa_Penanaman_Awal, '%d-%m-%Y') as Masa_Penanaman_Awal, date_format(Masa_Pemupukan, '%d-%m-%Y') as Masa_Pemupukan, date_format(Masa_Panen, '%d-%m-%Y') as Masa_Panen FROM trans_kalender_tanam where ID_Spesies = '$id_spesies' AND Masa_Tanam='$masa_tanam' AND Kabupaten='$kabupaten'");

                            while($brs = mysql_fetch_assoc($query)){
                            $id_spesies_detail = $brs['ID_Spesies'];
                            ?>
                            <?php
                                 $querytanaman= mysql_query("SELECT * FROM master_spesies_tanaman Where ID_Spesies = '$id_spesies_detail'");
                                 $brstanaman = mysql_fetch_assoc($querytanaman);
                                ?> 
                                <h2><?php echo $brs['Nama_Kalender']?></h2>
                                <a style="font-size:20px" href="Daftar Kalender Tanam-lihat.php"><span class="glyphicon glyphicon-chevron-left"></span>Kembali ke Daftar Tanaman</a>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-2">
                                        Tanaman     
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brstanaman['Nama_Tanaman']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Nama Kalender  
                                    </div>   
                                    <div class="col-md-10">
                                        : <?php echo $brs['Nama_Kalender']
                                        ?>
                                    </div>
                                 </div>   
                                 <div class="row">
                                    <div class="col-md-2">
                                        Masa Tanam  
                                    </div>   
                                    <div class="col-md-10">
                                        : <?php echo $brs['Masa_Tanam']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                        Kabupaten
                                    </div>
                                    <div class="col-md-10">     
                                        : <?php echo $brs['Kabupaten']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-2">
                                        Provinsi   
                                    </div>  
                                    <div class="col-md-10">
                                        : <?php echo $brs['Provinsi']
                                        ?>
                                    </div>
                                 </div>  
                                 <div class="row">
                                    <div class="col-md-2"> 
                                        Tanggal Awal
                                    </div>     
                                    <div class="col-md-10"> 
                                        : <?php echo $brs['Tanggal_Awal']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Tanggal Akhir
                                    </div>     
                                    <div class="col-md-10">
                                        : <?php echo $brs['Tanggal_Akhir']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Pengolahan Lahan Awal
                                    </div>    
                                    <div class="col-md-10">
                                        : <?php echo $brs['Pengolahan_Lahan_Awal']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Pengolahan Lahan Akhir
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brs['Pengolahan_Lahan_Akhir']
                                        ?>
                                    </div>
                                 </div> 
                                 <div class="row">
                                    <div class="col-md-2">
                                        Persiapan Lahan Awal
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brs['Persiapan_Lahan_Awal']
                                        ?>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-2">
                                        Masa Penanaman Awal
                                    </div>
                                    <div class="col-md-10">
                                        : <?php echo $brs['Masa_Penanaman_Awal']
                                        ?>
                                    </div>
                                 </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        Masa Pemupukan 
                                    </div> 
                                    <div class="col-md-10">    
                                        : <?php echo $brs['Masa_Pemupukan']
                                        ?>
                                    </div>
                                 </div>
                                       <div class="row">
                                    <div class="col-md-2">
                                        Masa Panen  
                                        </div>  
                                        <div class="col-md-10">
                                        : <?php echo $brs['Masa_Panen']
                                        ?>
                                    </div>
                                 </div>                                

                            </div>   
                        </div>
                        <br>
                        <a class="btn btn-md btn-danger" href="Daftar Kalender Tanam-lihat.php" style="margin-left: "> Kembali ke Daftar Tanaman</a>&nbsp; 


                            <?php
                            }
                            ?>

                            <!--end konten-->
    
                     </div>
                </div>
            </div>
         </div>
         <script src="css/side_menu.js"></script>
         <footer class="navbar navbar-default navbar-fixed-bottom">
             <div class="container-fluid">
                <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
                </div>
            </footer>
    </body>
</html>