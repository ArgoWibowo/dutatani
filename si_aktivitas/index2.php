<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>
    <body>
         <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #0F780A">
            <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="index.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li><a href="login.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-out fa-stack-1x"></i></span> Masuk</a></li> -->
                </ul>
            </div>
        </div>
    </nav>

    <div id="wrapper" style="padding-top: 3.6%">
        <div id="sidebar-wrapper" >
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu active">
                      <a href="javascript:;" class="active">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Daftar Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li class="active-menu"><a href="Daftar Tanaman-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Daftar Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li class="active-menu"><a href="Daftar Peta Lahan-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x "></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-heart" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Tambahan</span>
                      </a>
                      <ul class="sub">
                        <li ><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-heart fa-stack-1x "></i></span> Tambahan Menu1</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-heart fa-stack-1x "></i></span> Tambahan Menu2</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-heart fa-stack-1x "></i></span> Tambahan Menu3</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li ><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik jenis tanah</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu ">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Aktivitas</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"><span class="fa-stack pull-left" style="margin-top: 1.8%"><i class="fa fa-clock-o fa-stack-1x "></i></span> Aktivitas petani</a></li>
                        <li class="active-menu"><a href="Grafik_aktivitas_pertanian.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Tanaman</a></li>
                        <li><a href="Daftar Hasil Panen-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-check-square fa-stack-1x "></i></span> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-balance-scale fa-stack-1x "></i></span> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Detail</a></li>
                      </ul>
                  </li>



                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-music" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Tutup</span>
                      </a>
                      <ul class="sub">
                        <li ><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-music fa-stack-1x "></i></span> Tutup 1</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-music fa-stack-1x "></i></span> Tutup 2</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-music fa-stack-1x "></i></span> Tutup 3</a></li>
                      </ul>
                  </li>



                  <li class="sub-menu ">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-heart" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Buka</span>
                      </a>
                      <ul class="sub">
                        <li ><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-heart fa-stack-1x "></i></span> Buka 1</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-heart fa-stack-1x "></i></span> Buka 2</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-heart fa-stack-1x "></i></span> Buka 3</a></li>
                      </ul>
                  </li>











              </ul>
        </div>

        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
    <!-- <li style="margin-top: 20%; margin-left: 40%"> --><a class="btn-warning" href="login.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-out fa-stack-1x"></i></span> Masuk</a><!-- </li> -->
        <h1 style="margin-top: 3%; margin-left: 7%;"> <i class="fa fa-sign-out">  </i>
           Selamat Datang di Sistem Informasi Tanaman Pertanian 
        </h1>

                                           <div class="col-md-2" style="margin-left: 23%">
                                                <a style="font-size:14px;width: 145px" href="input_trans_aktivitas_pertanian.php" class="btn btn-md btn-success"><span class="fa-stack fa-md pull-left" style="margin-top: -4%"><i class="fa fa-plus-square fa-stack-1x "></i></span> Tambah Data </a>
                                            </div>

                                            <div class="col-md-2">
                                                <a style="font-size:14px;width: 145px" href="input_trans_aktivitas_pertanian.php" class="btn btn-md btn-warning"><span class="fa-stack fa-md pull-left" style="margin-top: -4%"><i class="fa fa-plus-square fa-stack-1x "></i></span> Tambah Data </a>
                                            </div>

                                            <div class="col-md-2">
                                                <a style="font-size:14px;width: 145px" href="input_trans_aktivitas_pertanian.php" class="btn btn-md btn-danger"><span class="fa-stack fa-md pull-left" style="margin-top: -4%"><i class="fa fa-plus-square fa-stack-1x "></i></span> Tambah Data </a>
                                            </div>
        



 <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-11">
                                        
                                            <table style="width:1000px;" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama nama siapa</th>
                                                        <th>Foto foto syantek</th>
                                                        <th>Foto foto syantek syantekk</th>
                                                        <th>Pilihan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                    $query = mysql_query("SELECT * FROM master_spesies_tanaman");
                                                    $no = 1;
                                                    while($brs = mysql_fetch_assoc($query)){
                                                    ?>
                                                
                                                    <tr>
                                                        <td><?php echo $no++?></td>
                                                        <td><?php echo $brs['Nama_Tanaman']?></td>
                                                        <td><img style="width:150px; height:140px;" src="<?php echo 'images/foto1/'.$brs['Foto1']?>"></td>                                                        
                                                        <td><img style="width:150px; height:140px;" src="<?php echo 'images/foto2/'.$brs['Foto2']?>"></td> 
                                                        <td><a class="btn btn-md btn-primary" href="detail_tanaman-lihat.php?Id_spesies=<?php  echo $brs['ID_Spesies'] ?>">Lihat</a><br>
                                                    </tr>
                                                    <?php
                                                    }?>   
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>







                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                                                </div>
                </div>
            </div>
        </div>
        <script src="css/side_menu.js"></script>
        <!--end wraper-->
    <footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #0F780A;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>
</body></html>