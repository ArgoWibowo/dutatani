<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">

        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>
 <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="index.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="login.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-in fa-stack-1x"></i></span> Masuk</a></li>
                </ul>
                </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="wrapper" style="padding-top: 3.4%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu">
                      <a href="javascript:;">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Daftar Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar Tanaman-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Daftar Tanaman</a></li>
                        <li><a href="Daftar morfologi-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x "></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Tanaman</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Grafik_jenis_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Jenis Tanaman</a></li>
                        <li><a href="Grafik_jenis_tanah.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik jenis tanah</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu active">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-lg fa-info-circle" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Informasi Aktivitas</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar_trans_aktivitas_lihat.php"><span class="fa-stack pull-left" style="margin-top: 1.8%"><i class="fa fa-clock-o fa-stack-1x "></i></span> Aktivitas petani</a></li>
                        <li><a href="Grafik_aktivitas_pertanian.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Daerah</a></li>
                        <li><a href="Grafik_aktivitas_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Aktivitas Tanaman</a></li>
                        <li class="active-menu"><a href="Daftar Hasil Panen-lihat.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-check-square fa-stack-1x "></i></span> Hasil Panen</a></li>
                        <li><a href="Grafik_panen_tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Tanaman</a></li>
                        <li><a href="Perkiraan_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-balance-scale fa-stack-1x "></i></span> Perkiraan Panen</a></li>
                        <li><a href="Grafikpanen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                        <li><a href="Grafik_detail_panen.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-area-chart fa-stack-1x "></i></span> Grafik Hasil Panen Detail</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Daftar Hasil Panen</h1>
                                            <div class="col-md-2">
                                                <a style="font-size:14px;width: 100px" href="Daftar laporan hasil panen lihat.php" class="btn btn-md btn-success"><span class="fa-stack fa-md pull-left" style="margin-top: -6%"><i class="fa fa-print fa-stack-1x"></i></span> Cetak</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form role="form" action="Daftar_hasil_panen_lihat_filter_2.php" enctype="multipart/form-data" method="post" class="container-fluid xyz" style="width: 100%;background-color: #F5F5F5">
                                <div class="form-group">
                                    <div class = "row">
                                        <div class="col-md-4">
                                            <h3><span class="glyphicon glyphicon-filter" style="color: #13A00C">
                                            </span>Filter</h3>
                                        </div>
                                    </div>    
                                
                                <div class="form-group">    
                                    <div class="row">
                                        <div class ="col-md-4">
                                                <label class="control-label">Filter berdasarkan :</label>
                                                    <select class="form-control" name="filter3">
                                                        <option value ="Nama_Petani">Nama Petani</option>
                                                        <option value ="Nama_Tanaman">Nama Tanaman</option>
                                                        <option value ="Tahun_Panen">Tahun Panen</option>
                                                        <option value ="Bulan_Panen">Bulan Panen</option>
                                                        <option value ="Periode_Panen">Periode Panen</option>
                                                        <option value ="Nama_Produk">Nama Produk Panen</option>
                                                        <option value ="Luas_Lahan">Luas Lahan</option>         
                                                    </select>
                                        </div>
                                         <div class ="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label"> Filter</label>
                                                    <input class="form-control" name="isi3" placeholder=" " type="text">
                                                </div>
                                        </div>
                                        <div class ="col-md-4">
                                            <div class = "row">
                                                <br>
                                                <div class ="col-md-2">  
                                                    <input class="btn btn-primary btn-md" type="submit" value="Filter"> 
                                                </div> 
                                            </div>             
                                        </div>            
                                    </div>               
                                </div>
                                       
                             </form>
                        <body>
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <table style="width:1000px;" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>Petani</th>
                                                        <th>Tanaman</th>
                                                        <th>Bulan Panen</th>
                                                        <th>Tahun Panen</th>
                                                        <th>Periode Tanam</th>
                                                        <th>Jumlah Hasil Panen (Kuintal)</th>
                                                        <th>Produk</th>
                                                        <th>Luas Lahan(M2)</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                    $query = mysql_query("SELECT Nama_Petani, Nama_Tanaman, Bulan_Panen, Tahun_Panen, Periode_Panen, Jumlah_Hasil_Panen, Nama_Produk, Luas_Lahan FROM trans_hasil_Panen inner join master_petani on trans_hasil_Panen.ID_Petani = master_petani.ID_User inner join master_spesies_tanaman on trans_hasil_panen.ID_Spesies = master_spesies_tanaman.ID_Spesies inner join master_produk_tani on trans_hasil_panen.ID_Produk = master_produk_tani.ID_Produk");
                                                    $no = 1;
                                                    while($brs = mysql_fetch_assoc($query)){   
                                                    ?>
                                               
                                                     <tr>
                                                        <td><?php echo $no++?></td>
                                                        <td><?php echo $brs['Nama_Petani']?></td>
                                                        <td><?php echo $brs['Nama_Tanaman']?></td>
                                                        <td><?php echo $brs['Bulan_Panen']?></td>
                                                        <td><?php echo $brs['Tahun_Panen']?></td>
                                                        <td><?php echo $brs['Periode_Panen']?></td>
                                                        <td><?php echo $brs['Jumlah_Hasil_Panen']?></td>
                                                        <td><?php echo $brs['Nama_Produk']?></td>
                                                        <td><?php echo $brs['Luas_Lahan']?></td>
                                                    </tr>
                                                    <?php
                                                    }?>   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!--end konten-->
                     </div>
                </div>
            </div>
         </div>
          <script src="css/side_menu.js"></script>
        <footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #13780C;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>                        
</body></html>