<?php
  session_start();
 if(!isset($_SESSION["ID_User"])){
    header("Location:login.php?pesan=Mohon login terlebih dahulu");
  }else if($_SESSION["ID_Kategori"] != "ADT")
  {
    header("Location:login.php?pesan=hanya untuk admin");
  } 
?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


        <script src="jquery.dataTables.min.js"></script>
        <script src="dataTables.bootstrap.min.js"></script>
        <script src="dataTables.responsive.js"></script>
        
        <link href="dataTables.bootstrap.css" rel="stylesheet">
        <link href="dataTables.responsive.css" rel="stylesheet">

        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
        <!-- <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jjquery-1.8.3.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script> -->
        <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

        <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                bFilter: false,
                responsive:true
            });
        });
        </script>

    </head>
    <nav class="navbar navbar-default navbar-fixed-top no-margin" id="custom-nav" style="background-color: #13780C">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container-fluid">
            <div class="navbar-header fixed-brand">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                    <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                </button>
                <a class="navbar-brand" href="#" id="menu-toggle-2" style="width: 400px"><i class="fa fa-bars fa-4"></i></a><a class="navbar-brand" href="indexadmin.php" style="position: absolute;margin-left: -26%"><span class="fa-stack pull-left" style="margin-top: -2.5%"><i class="fa fa-leaf fa-stack-1x"></i></span> Sistem Informasi Tanaman Pertanian</a> 
            </div><!-- navbar-header-->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
         <?php                   
        $id_user = $_SESSION['ID_User'];                                       
        mysql_connect("localhost","root","") or die(mysql_error());
        mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
        $query= mysql_query("SELECT * FROM master_petani where ID_User = '$id_user';");
        $brsnama = mysql_fetch_array($query)
        ?>
        <li><a href="#">Selamat Datang! <span class="fa-stack pull-left" style="margin-top: -1.5%"><i class="fa fa-user-circle-o fa-lg fa-stack-1x"></i></span><strong><?php echo $brsnama['Nama_Petani'] ?></strong></a></li>
        <li><a href="tutup_session.php"><span class="fa-stack fa-lg pull-left" style="margin-top: -11%"><i class="fa fa-sign-out fa-stack-1x"></i></span> Keluar</a></li>
                </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

 <div id="wrapper" style="padding-top: 3.6%">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="nav-accordion">
                  <li class="sub-menu active">
                      <a class="active" href="javascript:;">
                          <i class="fa fa-lg fa-folder-open" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Tanaman</span>
                      </a>
                      <ul class="sub">
                          <li class="active-menu"><a href="daftar_morfologi.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Morfologi Tanaman</a></li>
                          <li><a href="Daftar Tanaman.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-book fa-stack-1x"></i></span> Tanaman</a></li>
                          <li><a href="Daftar Kalender Tanam.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-calendar-check-o fa-stack-1x"></i></span> Kalender Tanam</a></li>
                          <li><a href="Daftar Peta Lahan.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-map-marker fa-stack-1x"></i></span> Peta Lahan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-lg fa-database" style="margin-left: -15px"></i>
                          <span style="margin-left: 10px"> Data Aktivitas Pertanian</span>
                      </a>
                      <ul class="sub">
                        <li><a href="Daftar Aktivitas.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-list-alt fa-stack-1x"></i></span> Aktivitas</a></li>
                        <li><a href="Daftar Aktivitas Spesies.php"><span class="fa-stack pull-left" style="margin-top: 2.1%"><i class="fa fa-list-alt fa-stack-1x"></i></span> Aktivitas Tanaman</a></li>
                      </ul>
                  </li>
              </ul>
        </div><!-- /#sidebar-wrapper --><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!--konten-->
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1>Daftar Tanaman</h1>
                                            <div class="col-md-2">
                                            <a style="font-size:20px" href="input_tanaman.php"><span class="fa-stack fa-md pull-left" style="margin-top: -3%"><i class="fa fa-plus-square fa-stack-1x "></i></span> Tambah Data</a>
                                            </div>
                                            <div class="col-md-2">
                                            <a style="font-size:20px" href="daftar laporan tanaman.php"><span class="fa-stack fa-md  pull-left" style="margin-top: -3%"><i class="fa fa-print fa-stack-1x"></i></span> Cetak</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                         <body onload="<?php if (@$_GET['pesan'] != "") { echo "alert('$_GET[pesan]');"; } ?>">
                            <div class="section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            
                                            
                                            <table style="width:1000px;" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Tanaman</th>
                                                        <th>Foto 1</th>
                                                        <th>Foto 2</th>
                                                        <th>Pilihan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    mysql_connect("localhost","root","") or die(mysql_error());
                                                    mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                    $query = mysql_query("SELECT * FROM master_spesies_tanaman");
                                                    $no = 1;
                                                    while($brs = mysql_fetch_assoc($query)){
                                                    ?>
                                                
                                                    <tr>
                                                        <td><?php echo $no++?></td>
                                                        <td><?php echo $brs['Nama_Tanaman']?></td>
                                                        <td><img style="width:150px; height:140px;" src="<?php echo 'images/foto1/'.$brs['Foto1']?>"></td>                                                        
                                                        <td><img style="width:150px; height:140px;" src="<?php echo 'images/foto2/'.$brs['Foto2']?>"></td> 
                                                        <td><a class="btn btn-md btn-primary" href="detail_tanaman.php?Id_spesies=<?php  echo $brs['ID_Spesies'] ?>">Lihat</a>&nbsp;
                                                        <a class="btn btn-md btn-danger" href="hapus_tanaman.php?id=<?php  echo $brs['ID_Spesies'] ?>" onclick="return confirm('Yakin Hapus?');">Hapus</a>&nbsp;
                                                        <a class="btn btn-md btn-warning" href="edit_tanaman.php?id_spesies=<?php  echo $brs['ID_Spesies'] ?>">Ubah</a></td>
                                                    </tr>
                                                    <?php
                                                    }?>   
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end konten-->
                     </div>
                </div>
            </div>
         </div>
        <script src="css/side_menu.js"></script>
         <footer class="navbar navbar-default navbar-fixed-bottom" style="min-height: 1%;background-color: #13780C;border-top: solid black 1px">
        <div class="container-fluid">
            <a class="navbar-brand" style="margin-top: -6px">© Copyright 2018</a>
        </div>
    </footer>
    </body>
</html>