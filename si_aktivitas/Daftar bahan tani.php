<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/Aktivitas Pertanian.css" rel="stylesheet" type="text/css">
        <link href="css/side-bar.css" rel="stylesheet">
    </head><body>
<nav class="navbar navbar-default">
        <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Sistem Informasi Tanaman Pertanian</a>
                </div>  
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">ADMINISTRATOR</a></li>
                    <li><a href="#">Keluar</a></li>
                </ul>
                    </li>
                 </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-default no-margin">
    <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header fixed-brand">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  id="menu-toggle">
                      <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>
                    </button>
                    <a class="navbar-brand" href="#" id="menu-toggle-2"><i class="fa fa-server fa-4"></i> MENU</a> 
                </div><!-- navbar-header-->
 
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                
                            </ul>
                            <ul>
                                <div style="margin-bottom:15px;" align="right">
                                    <form action="cari_bahan_tani.php" method="post">
                                        <input type="text" name="input_cari_bahan" placeholder="Cari Berdasar Nama bahan tani" style="width:250px;color:black;" />
                                        <input type="submit" name="cari" value="Cari" class="btn-md btn-primary" style="padding:3px;" margin="6px;" width="50px;"  />
                                    </form>
                                </div>
                            </ul>
                </div><!-- bs-example-navbar-collapse-1 -->
        </div>
    </nav>
    
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav nav-pills nav-stacked" id="menu" >
                <li>
                    <a><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Data Tanaman</a>
                       <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="input_tanaman.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Tanaman</a></li>
                        <li><a href="Morfologi tanaman.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Morfologi</a></li>
                        <li><a href="Input Kalender tanam.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Kalender Tanam</a></li>
                        <li><a href="Peta Lahan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Peta Lahan</a></li>
                        <li><a href="Input Bahan Tani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Bahan Tani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Data Tanaman Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Tanaman.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Tanaman</a></li>
                        <li><a href="daftar_morfologi.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Morfologi Tanaman</a></li>
                        <li><a href="Daftar Kalender Tanam.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Kalender Tanaman</a></li>
                        <li><a href="Daftar Peta Lahan.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Peta Lahan</a></li>
                        <li><a href="Daftar bahan tani.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Bahan Tani</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Data Aktivitas Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Input Aktivitas.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Aktivitas</a></li>
                        <li><a href="input aktivitas spesies.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Aktivitas Tanaman</a></li>
                        <li><a href="input_trans_aktivitas_pertanian.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Trans Aktivitas</a></li>
                        <li><a href="Input Hasil Panen.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Input Hasil Panen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Data Aktivitas Pertanian</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="Daftar Aktivitas.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Aktivitas</a></li>
                        <li><a href="Daftar Aktivitas Spesies.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Aktivitas Tanaman</a></li>
                        <li><a href="daftar_trans_aktivitas.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Trans Aktivitas</a></li>
                        <li><a href="Daftar Hasil Panen.php"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Hasil Panen</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Grafik</a>
                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                        <li><a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-wrench fa-stack-1x "></i></span> Grafik Hasil Panen</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /#sidebar-wrapper -->

<div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        <!---konten-->

                        <div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1>Daftar Bahan Tani</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Id Bahan</th>
                                                    <th>Nama Bahan</th>
                                                    <th>Deskripsi Bahan</th>
                                                    <th>Spesifikasi Bahan</th>
                                                    <th>Harga Tertinggi</th>
                                                    <th>Harga Terendah</th>
                                                    <th>Fungsi Bahan</th>
                                                    <th>Jenis Bahan</th>
                                                    <th>Kategori</th>
                                                    <th>Pilihan</th>
                                                </tr>
                                                 <?php
                                                mysql_connect("localhost","root","") or die(mysql_error());
                                                mysql_select_db("iais_ukdw") or die("Database tidak ditemukan");
                                                $query = mysql_query("SELECT * FROM master_bahan_tani");
                                                $no = 1;
                                                while($brs = mysql_fetch_assoc($query)){
                                                 $id_kategori = $brs['ID_Kategori']   
                                                ?>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $no++?></td>
                                                    <td><?php echo $brs['ID_Bahan']?></td>
                                                    <td><?php echo $brs['Nama_Bahan']?></td>
                                                    <td><?php echo $brs['Deskripsi_Bahan']?></td>
                                                    <td><?php echo $brs['Spesifikasi_Bahan']?></td>
                                                    <td><?php echo $brs['Harga_Tertinggi']?></td>
                                                    <td><?php echo $brs['Harga_Terendah']?></td>
                                                    <td><?php echo $brs['Fungsi_Bahan']?></td>
                                                    <td><?php echo $brs['Jenis_Bahan']?></td>
                                                     <?php
                                                        $querykategori= mysql_query("SELECT * FROM master_kategori_alatbahan Where ID_Kategori= '$id_kategori' ;");
                                                        $brskategori = mysql_fetch_assoc($querykategori);
                                                        ?>
                                                    <td><?php echo $brskategori['Nama_kategori']?></td>
                                                    <td><a class="btn btn-md btn-danger" href="hapus_bahan_tani.php?id_bahan=<?php  echo $brs['ID_Bahan'] ?>">Hapus</a></td>
                                                     <td><a class="btn btn-md btn-warning" href="edit_bahan_tani.php?id_bahan=<?php  echo $brs['ID_Bahan'] ?>">Edit</a></td>
                                                </tr>
                                                <?php
                                                }?>   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    <!--konten-->
                        </div>
                    </div>
                </div>
            </div>
            <script src="css/side_menu.js"></script>
            <!--end wraper-->

    <footer class="navbar navbar-default navbar-fixed-bottom">
         <div class="container-fluid">
             <a class="navbar-brand">Sistem Informasi Tanaman Pertanian</a>
        </div>
    </footer>

</body></html>